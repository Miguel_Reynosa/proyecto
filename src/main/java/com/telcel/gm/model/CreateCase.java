package com.telcel.gm.model;

import java.util.Map;

public class CreateCase {

private String  incidentNumber;

private String  plantilla;
private String  groupUser;
private String  groupAssigned;
private String  externalId;
private String  ci;
private String  desireddDte;
private String  font;
private String  groupprovider;
private String  typeIncidents;
private String  NumTicket;
private String  typeStatus;
private String  resolution;

private String  catPernalLivel1;
private String  catPernalLivel2;
private String  catPernalLivel3;

private String  catProductLIvel1;
private String  catProductLIvel2;
private String  catProductLIvel3;

private String  note;// notas
private String  nameClient;
private String  typeClient;
private String  homeTel;
private String  cpdMarca;
private String  cpdModel;
private String  cpdTechnology;
private String  cpdTelOrigin;
private String  regionOrigin;
private String  NumOrigin;
private String  cpdContratacion;
private String  cpdTelDestino;
private String  regionDestino;
private String  numDestino;

private String charSuburb;
private String chrAdress;
private String chrCP;
private String chrStreets;
private String lstProblem;
private String summary;
private String modService;
private String chrRegion;
private String coordinates;
private String firstName;
private String lastName;
private String impact;
private String urgency;
private String serviceType;
private String reportedSource;
private String caseType;
private String process;
private String xxxx;
private String xxxxx;
private String status;
private String attatchmentcomment;
private String campOrDest;


private String enterpriseId;
private String correlationId;
private String providerId;
private String country;
private String countryM2M;
private Long eventIdint;
private String  newIncident;






public CreateCase() {
	
}
public CreateCase(String enterpriseId, String correlationId, String providerId, String country, String countryM2M
		) {
	
	this.enterpriseId = enterpriseId;
	this.correlationId = correlationId;
	this.providerId = providerId;
	this.country = country;
	this.countryM2M = countryM2M;
}

public CreateCase(Map<String,String> headerRequest) {
	
	this.enterpriseId = headerRequest.get("enterpriseid");
	this.correlationId = headerRequest.get("correlationid");
	this.providerId = headerRequest.get("providerid");
	this.country = headerRequest.get("country");
	this.countryM2M = headerRequest.get("countrym2m");
}


public String getEnterpriseId() {
	return enterpriseId;
}
public void setEnterpriseId(String enterpriseId) {
	this.enterpriseId = enterpriseId;
}
public String getCorrelationId() {
	return correlationId;
}
public void setCorrelationId(String correlationId) {
	this.correlationId = correlationId;
}
public String getProviderId() {
	return providerId;
}
public void setProviderId(String providerId) {
	this.providerId = providerId;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getCountryM2M() {
	return countryM2M;
}
public void setCountryM2M(String countryM2M) {
	this.countryM2M = countryM2M;
}
public Long getEventIdint() {
	return eventIdint;
}
public void setEventIdint(Long eventIdint) {
	this.eventIdint = eventIdint;
}
public String getNewIncident() {
	return newIncident;
}
public void setNewIncident(String newIncident) {
	this.newIncident = newIncident;
}
public String getIncidentNumber() {
	return incidentNumber;
}
public void setIncidentNumber(String incidentNumber) {
	this.incidentNumber = incidentNumber;
}
public String getCampOrDest() {
	return campOrDest;
}
public void setCampOrDest(String campOrDest) {
	this.campOrDest = campOrDest;
}
public String getPlantilla() {
	return plantilla;
}
public void setPlantilla(String plantilla) {
	this.plantilla = plantilla;
}
public String getGroupUser() {
	return groupUser;
}
public void setGroupUser(String groupUser) {
	this.groupUser = groupUser;
}
public String getGroupAssigned() {
	return groupAssigned;
}
public void setGroupAssigned(String groupAssigned) {
	this.groupAssigned = groupAssigned;
}
public String getExternalId() {
	return externalId;
}
public void setExternalId(String externalId) {
	this.externalId = externalId;
}
public String getCi() {
	return ci;
}
public void setCi(String ci) {
	this.ci = ci;
}
public String getDesireddDte() {
	return desireddDte;
}
public void setDesireddDte(String desireddDte) {
	this.desireddDte = desireddDte;
}
public String getFont() {
	return font;
}
public void setFont(String font) {
	this.font = font;
}
public String getGroupprovider() {
	return groupprovider;
}
public void setGroupprovider(String groupprovider) {
	this.groupprovider = groupprovider;
}
public String getTypeIncidents() {
	return typeIncidents;
}
public void setTypeIncidents(String typeIncidents) {
	this.typeIncidents = typeIncidents;
}
public String getNumTicket() {
	return NumTicket;
}
public void setNumTicket(String numTicket) {
	NumTicket = numTicket;
}
public String getTypeStatus() {
	return typeStatus;
}
public void setTypeStatus(String typeStatus) {
	this.typeStatus = typeStatus;
}
public String getResolution() {
	return resolution;
}
public void setResolution(String resolution) {
	this.resolution = resolution;
}
public String getCatPernalLivel1() {
	return catPernalLivel1;
}
public void setCatPernalLivel1(String catPernalLivel1) {
	this.catPernalLivel1 = catPernalLivel1;
}
public String getCatPernalLivel2() {
	return catPernalLivel2;
}
public void setCatPernalLivel2(String catPernalLivel2) {
	this.catPernalLivel2 = catPernalLivel2;
}
public String getCatPernalLivel3() {
	return catPernalLivel3;
}
public void setCatPernalLivel3(String catPernalLivel3) {
	this.catPernalLivel3 = catPernalLivel3;
}
public String getCatProductLIvel1() {
	return catProductLIvel1;
}
public void setCatProductLIvel1(String catProductLIvel1) {
	this.catProductLIvel1 = catProductLIvel1;
}
public String getCatProductLIvel2() {
	return catProductLIvel2;
}
public void setCatProductLIvel2(String catProductLIvel2) {
	this.catProductLIvel2 = catProductLIvel2;
}
public String getCatProductLIvel3() {
	return catProductLIvel3;
}
public void setCatProductLIvel3(String catProductLIvel3) {
	this.catProductLIvel3 = catProductLIvel3;
}
public String getNote() {
	return note;
}
public void setNote(String note) {
	this.note = note;
}
public String getNameClient() {
	return nameClient;
}
public void setNameClient(String nameClient) {
	this.nameClient = nameClient;
}
public String getTypeClient() {
	return typeClient;
}
public void setTypeClient(String typeClient) {
	this.typeClient = typeClient;
}
public String getHomeTel() {
	return homeTel;
}
public void setHomeTel(String homeTel) {
	this.homeTel = homeTel;
}
public String getCpdMarca() {
	return cpdMarca;
}
public void setCpdMarca(String cpdMarca) {
	this.cpdMarca = cpdMarca;
}
public String getCpdModel() {
	return cpdModel;
}
public void setCpdModel(String cpdModel) {
	this.cpdModel = cpdModel;
}
public String getCpdTechnology() {
	return cpdTechnology;
}
public void setCpdTechnology(String cpdTechnology) {
	this.cpdTechnology = cpdTechnology;
}
public String getCpdTelOrigin() {
	return cpdTelOrigin;
}
public void setCpdTelOrigin(String cpdTelOrigin) {
	this.cpdTelOrigin = cpdTelOrigin;
}
public String getRegionOrigin() {
	return regionOrigin;
}
public void setRegionOrigin(String regionOrigin) {
	this.regionOrigin = regionOrigin;
}
public String getNumOrigin() {
	return NumOrigin;
}
public void setNumOrigin(String numOrigin) {
	NumOrigin = numOrigin;
}
public String getCpdContratacion() {
	return cpdContratacion;
}
public void setCpdContratacion(String cpdContratacion) {
	this.cpdContratacion = cpdContratacion;
}
public String getCpdTelDestino() {
	return cpdTelDestino;
}
public void setCpdTelDestino(String cpdTelDestino) {
	this.cpdTelDestino = cpdTelDestino;
}
public String getRegionDestino() {
	return regionDestino;
}
public void setRegionDestino(String regionDestino) {
	this.regionDestino = regionDestino;
}
public String getNumDestino() {
	return numDestino;
}
public void setNumDestino(String numDestino) {
	this.numDestino = numDestino;
}
public String getCharSuburb() {
	return charSuburb;
}
public void setCharSuburb(String charSuburb) {
	this.charSuburb = charSuburb;
}
public String getChrAdress() {
	return chrAdress;
}
public void setChrAdress(String chrAdress) {
	this.chrAdress = chrAdress;
}
public String getChrCP() {
	return chrCP;
}
public void setChrCP(String chrCP) {
	this.chrCP = chrCP;
}
public String getChrStreets() {
	return chrStreets;
}
public void setChrStreets(String chrStreets) {
	this.chrStreets = chrStreets;
}
public String getLstProblem() {
	return lstProblem;
}
public void setLstProblem(String lstProblem) {
	this.lstProblem = lstProblem;
}
public String getSummary() {
	return summary;
}
public void setSummary(String summary) {
	this.summary = summary;
}
public String getModService() {
	return modService;
}
public void setModService(String modService) {
	this.modService = modService;
}
public String getChrRegion() {
	return chrRegion;
}
public void setChrRegion(String chrRegion) {
	this.chrRegion = chrRegion;
}
public String getCoordinates() {
	return coordinates;
}
public void setCoordinates(String coordinates) {
	this.coordinates = coordinates;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public String getImpact() {
	return impact;
}
public void setImpact(String impact) {
	this.impact = impact;
}
public String getUrgency() {
	return urgency;
}
public void setUrgency(String urgency) {
	this.urgency = urgency;
}
public String getServiceType() {
	return serviceType;
}
public void setServiceType(String serviceType) {
	this.serviceType = serviceType;
}
public String getReportedSource() {
	return reportedSource;
}
public void setReportedSource(String reportedSource) {
	this.reportedSource = reportedSource;
}
public String getCaseType() {
	return caseType;
}
public void setCaseType(String caseType) {
	this.caseType = caseType;
}
public String getProcess() {
	return process;
}
public void setProcess(String process) {
	this.process = process;
}
public String getXxxx() {
	return xxxx;
}
public void setXxxx(String xxxx) {
	this.xxxx = xxxx;
}
public String getXxxxx() {
	return xxxxx;
}
public void setXxxxx(String xxxxx) {
	this.xxxxx = xxxxx;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getAttatchmentcomment() {
	return attatchmentcomment;
}
public void setAttatchmentcomment(String attatchmentcomment) {
	this.attatchmentcomment = attatchmentcomment;
}
@Override
public String toString() {
	return "CreateCase [incidentNumber=" + incidentNumber + ", plantilla=" + plantilla + ", groupUser=" + groupUser
			+ ", groupAssigned=" + groupAssigned + ", externalId=" + externalId + ", ci=" + ci + ", desireddDte="
			+ desireddDte + ", font=" + font + ", groupprovider=" + groupprovider + ", typeIncidents=" + typeIncidents
			+ ", NumTicket=" + NumTicket + ", typeStatus=" + typeStatus + ", resolution=" + resolution
			+ ", catPernalLivel1=" + catPernalLivel1 + ", catPernalLivel2=" + catPernalLivel2 + ", catPernalLivel3="
			+ catPernalLivel3 + ", catProductLIvel1=" + catProductLIvel1 + ", catProductLIvel2=" + catProductLIvel2
			+ ", catProductLIvel3=" + catProductLIvel3 + ", note=" + note + ", nameClient=" + nameClient
			+ ", typeClient=" + typeClient + ", homeTel=" + homeTel + ", cpdMarca=" + cpdMarca + ", cpdModel="
			+ cpdModel + ", cpdTechnology=" + cpdTechnology + ", cpdTelOrigin=" + cpdTelOrigin + ", regionOrigin="
			+ regionOrigin + ", NumOrigin=" + NumOrigin + ", cpdContratacion=" + cpdContratacion + ", cpdTelDestino="
			+ cpdTelDestino + ", regionDestino=" + regionDestino + ", numDestino=" + numDestino + ", charSuburb="
			+ charSuburb + ", chrAdress=" + chrAdress + ", chrCP=" + chrCP + ", chrStreets=" + chrStreets
			+ ", lstProblem=" + lstProblem + ", summary=" + summary + ", modService=" + modService + ", chrRegion="
			+ chrRegion + ", coordinates=" + coordinates + ", firstName=" + firstName + ", lastName=" + lastName
			+ ", impact=" + impact + ", urgency=" + urgency + ", serviceType=" + serviceType + ", reportedSource="
			+ reportedSource + ", caseType=" + caseType + ", process=" + process + ", xxxx=" + xxxx + ", xxxxx=" + xxxxx
			+ ", status=" + status + ", attatchmentcomment=" + attatchmentcomment + ", campOrDest=" + campOrDest
			+ ", enterpriseId=" + enterpriseId + ", correlationId=" + correlationId + ", providerId=" + providerId
			+ ", country=" + country + ", countryM2M=" + countryM2M + ", eventIdint=" + eventIdint + ", newIncident="
			+ newIncident + "]";
}














}
