package com.telcel.gm.model;

import java.util.List;

import org.assertj.core.util.Arrays;

import com.telcel.gm.component.console;
import com.telcel.remedy.model.entity.ArsFormulario;
import com.telcel.remedy.model.services.IArsFormularioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class Formulario {
	

@Autowired
IArsFormularioService iFormulario;

private	String crear;
private String query;
private String update;
private String  attachment;
List<ArsFormulario> formularios;

//public Formulario(List<ArsFormulario> formularios) {
//	console.println("objeto formulario",formularios);
//		formularios.forEach(item->{
//			console.println("toString formulario1111",toString());
//
//			if((item.getCrudAction().contains("C")&crear==null))
//				 crear=item.getFormulario();
//			   if((item.getCrudAction().contains("R")&query==null))
//				   query=item.getFormulario();
//			   if((item.getCrudAction().contains("U")&update==null))
//				   update=item.getFormulario();
//			   if((item.getCrudAction().contains("A")&attachment==null))
//				   attachment=item.getFormulario();
//			console.println("toString formulario22222",toString());
//							
//		});
//	
//		
//}

public IArsFormularioService getIFormularioService() {
	return iFormulario;
}

public void filtrarPorPais(String pais ) {
	List<ArsFormulario> formularios=iFormulario.findByPais(pais);
	formularios.forEach(item->{
		console.println("toString formulario1111",toString());

		if((item.getCrudAction().contains("C")&crear==null))
			 crear=item.getFormulario();
		   if((item.getCrudAction().contains("R")&query==null))
			   query=item.getFormulario();
		   if((item.getCrudAction().contains("U")&update==null))
			   update=item.getFormulario();
		   if((item.getCrudAction().contains("A")&attachment==null))
			   attachment=item.getFormulario();
		console.println("toString formulario22222",toString());
	});

}

public String identificando(String crudAction,String formulario,CharSequence  tipo) {
	   String tipoEncontrado=  	((crudAction.contains(tipo))?formulario:"");

//	   console.println("el crud es",crudAction);
//	   console.println("el tipo  es",tipo);
//	   console.println("create",((crudAction.contains(tipo)&crear==null)));
//	   console.println("query",((crudAction.contains(tipo)&query==null)));
//	   console.println("update",((crudAction.contains(tipo)&update==null)));
//	   console.println("attacment",((crudAction.contains(tipo)&attachment==null)));
		
	   if((crudAction.contains(tipo)&crear==null))
		    crear=formulario;
	   if((crudAction.contains(tipo)&query==null))
				query=formulario;
	   if((crudAction.contains(tipo)&update==null))
				update=formulario;
	   if((crudAction.contains(tipo)&attachment==null))
				attachment=formulario;
	   
	 console.println("tippo de formulario",tipoEncontrado);
   return tipoEncontrado;
}
public String verificarValor(String valor) {
//	 console.println("verificar Crear",crear!=null);
//	 console.println("verificar query",query!=null);
//	 console.println("verificar updagte",update!=null);
//	 console.println("verificar attcahment",attachment!=null);
//	  
//	 console.println("valor",valor);
//	 console.println("valor is empty",valor.isEmpty());

	   
	   if(crear!=null&&crear.equals(valor)&&!valor.isEmpty())
		   return crear;
	   if(query!=null&&query.equals(valor)&&!valor.isEmpty())
		   return query;
	   if(update!=null&&update.equals(valor)&&!valor.isEmpty())
		   return update;
	   if(attachment!=null&&attachment.equals(valor)&&!valor.isEmpty())
		   return attachment;
	   
	return valor;
}





public String getCrear() {
	return crear;
}
public void setCrear(String crear) {
	this.crear = crear;
}
public String getQuery() {
	return query;
}
public void setQuery(String query) {
	this.query = query;
}
public String getUpdate() {
	return update;
}
public void setUpdate(String update) {
	this.update = update;
}


public String getAttachment() {
	return attachment;
}

public void setAttachment(String arrachment) {
	this.attachment = arrachment;
}

@Override
public String toString() {
	return "Formulario [crear=" + crear + ", query=" + query + ", update=" + update + ", attachment=" + attachment
			+ "]";
}








}
