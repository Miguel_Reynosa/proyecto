package com.telcel.gm.model;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@NotNull
@Valid
public class TroubleTicketAttachment {
	
	private String id;
	
	@NotNull
	private String troubleTicketId;
	@NotNull
	@NotEmpty
	private String name;
	private String description;
	@NotNull
	private String externalId;
	@NotNull
	@Max(5242880)
	private Integer size;
	//private Integer sizeUnit;
	@NotNull
	private String mimeType;
	@NotNull
	private String  file;
	private String sizeType;

	private String idWorklog;

	public String getIdWorklog() {
		return idWorklog;
	}

	public void setIdWorklog(String idWorklog) {
		this.idWorklog = idWorklog;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getSizeType() {
		return sizeType;
	}

	public void setSizeType(String sizeType) {
		this.sizeType = sizeType;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	private String URL;
	public String getTroubleTicketId() {
		return troubleTicketId;
	}

	public void setTroubleTicketId(String troubleTicketId) {
		this.troubleTicketId = troubleTicketId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String attchment) {
		file = attchment;
	}

	public String toCadena() {
		return "TroubleTicketAttachment [id=" + id + ", troubleTicketId=" + troubleTicketId + ", name=" + name
				+ ", description=" + description + ", externalId=" + externalId + ", size=" + size + ", mimeType="
				+ mimeType + ", file=" + file.length() + ", sizeType=" + sizeType + ", URL=" + URL + "]";
	}
	
	

}
