package com.telcel.gm.model;

import java.util.List;
import java.util.Map;

import com.telcel.gm.match.GestionCampoPais;
import com.telcel.gm.match.Validador;
import com.telcel.remedy.model.entity.ArsPaisCompania;

public class singletonGm {

	
	private static singletonGm singletonGm;
	public static Map<String,String> headerRequest;
	public String country;
	public String countryM2M;
	public String client;
	public String enterpriseId;
	public String correlationId;
	public String providerId;
	Validador validar;
	private singletonGm(){}
	
	
	
	public static singletonGm getInstance(){
	        if(singletonGm == null){
	        	singletonGm = new singletonGm();
	        }
	        return singletonGm;
	    }
	public static singletonGm getInstance(Map<String,String> Request,Validador validar){
        if(singletonGm == null){
        	singletonGm = new singletonGm();
        }
        headerRequest=Request;
        singletonGm.validar=validar;
        	singletonGm.setearHeader(headerRequest,validar);
        return singletonGm;
    }
	public void setearHeader(Map<String,String> Request,Validador validar) {
		   enterpriseId=headerRequest.get("enterpriseid");
		   correlationId=headerRequest.get("correlationid");
		   providerId=headerRequest.get("providerid");
		   country=headerRequest.get("country");
		   countryM2M=headerRequest.get("countrym2m");
		   GestionCampoPais gestioncampo=new GestionCampoPais();
			List<ArsPaisCompania> clients=	validar.getDataSetController().
										fncBusquedaFiltroCompaniaAndCountry(enterpriseId,
												gestioncampo.getToCountryM2MCountry(headerRequest.get("countrym2m")));
			client=clients.get(0).getArsClient().getArcId();
			
	};
	
}
