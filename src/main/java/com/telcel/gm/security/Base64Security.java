package com.telcel.gm.security;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.telcel.gm.services.AttachmentService;
import com.telcel.remedy.ars.itsm.RmdInsert;
import com.telcel.remedy.ars.itsm.RmdSelect;
import com.telcel.remedy.ars.itsm.RmdUpdate;

public class Base64Security {
	@Autowired
	public Environment env;
	
	
	public Base64Security(Environment env) {
		
		this.env=env;

	}
	
	
	
	public String codificar(String valor) {
		byte [] encodedusrBytes = Base64.getEncoder().encode(valor.getBytes());



		return new String(encodedusrBytes);
	}

	public String decodificador(HttpServletRequest headerRequest) {	
		String aut="";



	if(headerRequest.getHeader("Authorization")==null) {

	 aut = " ";

	}
	else 
	   aut=headerRequest.getHeader("Authorization").trim().replaceAll("Basic", "");


	 	byte [] decoded = Base64.getMimeDecoder().decode(((aut==":")?"":aut));
		return new String(decoded);

	}	
	public boolean isAut(String aut) {
	

		if(aut.contentEquals("")||aut.contentEquals(":")) {

			 aut = ":";

			}

		String user=aut==":"?"":aut.split(":")[0];
	   String passd=aut==":"?"":aut.split(":")[1];

		 return (codificar(user).equals(env.getProperty("usr"))&&(codificar(passd).equals(env.getProperty("pswrd"))));
	}
}
