package com.telcel.gm.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.telcel.gm.component.console;
import com.telcel.remedy.model.services.UserDetailsServiceImpl;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;

@Configuration
@EnableSwagger2
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter 
{
	@Autowired
    private UserDetailsService userDetailsService;

	@Autowired
    private UserDetailsServiceImpl detailsService;

	@Autowired
	private BasicAuthenticationPoint basicAuthenticationPoint;

	  
	  
//	    @Bean
//	    public Docket api() {
//	        return new Docket(DocumentationType.SWAGGER_2)
//	                .apiInfo(getApiInfo())
//	                .select()
//	                .apis(RequestHandlerSelectors.basePackage("com.telcel.remedy.controllers"))
//	                .paths(PathSelectors.any())
//	                .build();
//	    }

	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http.csrf().disable();
		
		//CON CONTRASEŅA
//		http.authorizeRequests().antMatchers().permitAll()
//			.anyRequest().authenticated();
		
		//SIN CONTRASEŅA
		http.authorizeRequests().antMatchers("/", "/api/**").permitAll()
        	.anyRequest().authenticated();
		
		http.httpBasic().authenticationEntryPoint(basicAuthenticationPoint);
	}
	

//	    private ApiInfo getApiInfo() {
//	        Contact contact = new Contact("Chandana Napagoda", "http://blog.napagoda.com", "cnapagoda@gmail.com");
//	        return new ApiInfoBuilder()
//	                .title("Example Api Title")
//	                .description("Example Api Definition")
//	                .version("1.0.0")
//	                .license("Apache 2.0")
//	                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0")
//	                .contact(contact)
//	                .build();
//	    }

//	    @Autowired
//	    protected void configure(AuthenticationManagerBuilder auth)	     throws Exception {
//	    // public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//	    	auth
//            .inMemoryAuthentication()
//                .withUser("hola").password(passwordEncoder().encode("hola")).roles("USER");
//	    
//                //auth.inMemoryAuthentication().withUser("hola").password("hola").roles("USER");
//	    }
	

//	    @Override
//	    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
//	        endpoints.tokenStore(tokenStore()).authenticationManager(authenticationManager).userDetailsService(userDetailsService);
//	    }	
	    
	    @Bean
	    public DaoAuthenticationProvider authenticationProvider() {
	    	
	        
	        DaoAuthenticationProvider authProvider   = new DaoAuthenticationProvider();
	        authProvider.setUserDetailsService(userDetailsService);
	        authProvider.setPasswordEncoder(passwordEncoder());
             
	        return authProvider;
	    }
	    
	    
	    
	    @SuppressWarnings("deprecation")
	    public static NoOpPasswordEncoder passwordEncoder() {
	        return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
	    }
	    
	    
//	    @Bean
//	    public PasswordEncoder passwordEncoder() {
//	        return new BCryptPasswordEncoder();
//	    }
	    
//	    @Bean
//	    public PasswordEncoder encoder() {
//	        return new BCryptPasswordEncoder(11);
//	    }
	
	
	
 // Authentication : User --> Roles
// protected void configure(AuthenticationManagerBuilder auth)
//     throws Exception {
//   auth.inMemoryAuthentication().passwordEncoder(
//		   org.springframework.security.crypto.password.NoOpPasswordEncoder.getInstance()).withUser("gm_gobal").password("Gm_514l")
//       .roles("USER").and().withUser("gm_gobal").password("Gm_514l")
//       .roles("USER", "ADMIN");
// }

 // Authorization : Role -> Access
// protected void configure(HttpSecurity http) throws Exception {
//   http.httpBasic().and().authorizeRequests().antMatchers("/students/")
//       .hasRole("USER").antMatchers("/").hasRole("ADMIN").and()
//       .csrf().disable().headers().frameOptions().disable();
// }

}