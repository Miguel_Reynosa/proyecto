package com.telcel.gm.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bmc.arsys.api.ARException;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.telcel.gm.component.Herramienta;
import com.telcel.gm.component.ValidarProvider;
import com.telcel.gm.component.console;
import com.telcel.gm.match.GestionCampoPais;
import com.telcel.gm.match.Validador;
import com.telcel.gm.match.creaObjeto;
import com.telcel.gm.match.excepcionElemento;
import com.telcel.gm.model.Formulario;
import com.telcel.gm.respuesta.Respuesta;
import com.telcel.gm.security.Base64Security;
import com.telcel.remedy.ars.itsm.RmdInsert;
import com.telcel.remedy.ars.itsm.RmdSelect;
import com.telcel.remedy.ars.itsm.RmdUpdate;
import com.telcel.remedy.ars.model.FieldRC;
import com.telcel.remedy.ars.model.InsertVO;
import com.telcel.remedy.ars.model.RemedyControlEntry;
import com.telcel.remedy.ars.model.RemedyControlResultado;
import com.telcel.remedy.ars.model.UpdateVO;
import com.telcel.remedy.controllers.DataSetController;
import com.telcel.remedy.model.constantes.ConstantesRemedy;
import com.telcel.remedy.model.entity.ArsPaisCompania;
import com.telcel.remedy.model.services.IArsFormularioService;

//@RestController
public class UpdateCaseService {
	
	static Logger logger = Logger.getLogger(CreateCaseService.class);

	//@Autowired
	public Environment env;
	RmdUpdate voUpdate;
	AttachmentService voAttachmentService;
	RmdSelect voSelect;
	RmdInsert voInsert;
//	
	@Autowired
	@Qualifier("validador")
	Validador validador;
	private String repetidos="";
	private int sizerepetidoslvl1=0;
	private int sizerepetidoslvl2=0;
	private String clientPais="";
	
	//@Autowired
	//@Qualifier("ArsFormularioServiceImpl")
	IArsFormularioService iFormulario;
	
	
	Formulario formulario ;
	ValidarProvider valP = new ValidarProvider();
	
	public  UpdateCaseService() {
		
	}
public UpdateCaseService(RmdUpdate update,RmdSelect selec,RmdInsert inser,AttachmentService attachmentService,Environment env,Validador validador,IArsFormularioService iFormulario) {
	this.voUpdate=update;
	this.env=env;
	this.voSelect=selec;
	this.voInsert=inser;
	this.voAttachmentService=attachmentService;
	this.validador=validador;
	this.iFormulario=iFormulario;
}	
public UpdateCaseService(RmdUpdate update,RmdSelect selec,RmdInsert inser,AttachmentService attachmentService,Environment env, Validador validador,Formulario formulario) {
	this.voUpdate=update;
	this.env=env;
	this.voSelect=selec;
	this.voInsert=inser;
	this.voAttachmentService=attachmentService;
	this.validador=validador;
	this.formulario=formulario;
}

	public ResponseEntity<String> updateCase(
			String voAttachment
			, @PathVariable String id
			, @RequestHeader Map<String,String> headerRequest
		) throws ARException
	{
	    logger.info("Log4j : empieza updateCase !!");
	    
		String enterpriseId=headerRequest.get("enterpriseid");
		String correlationId=headerRequest.get("correlationid");
		String providerId=headerRequest.get("providerid");
		String country=headerRequest.get("country");
		String countryM2M=headerRequest.get("countrym2m");
		String idLocal=headerRequest.get("idlocal");
		String tipo=ConstantesRemedy.tipoLocalToCloud; //Se agreg� para diferenciar el tipo de notas ya sea de Local a Cloud o viceversa
		
	 	console.println("header: "+headerRequest);
	 	console.println("idLocal (en UpdateCaseService): "+idLocal);
	 	
	    GestionCampoPais campoPais=new GestionCampoPais();
	    
	    Respuesta respuesta = new Respuesta();
		HttpHeaders responseHeaders = new HttpHeaders();
		
		responseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
	    List<String> validarHeader = validador.validarCabezera(headerRequest);
	    
		if(validarHeader.size()>0)
		{
			respuesta.setCode("RTT002");
			respuesta.setReason(validarHeader+" is required ");
			console.println("esta cosa entro aqui????");
			// respuesta.setMessage(viInsert.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(respuesta.getRepuestaError().toString());
		}
		
		validador.setTipoServicio("U");
			
		String[] voProvId = env.getProperty("ProviderID").split(",");
		if(valP.valProv(providerId, voProvId))
		{
			return valP.closeRequest(respuesta, providerId);
		}
		
	   	UpdateVO viUpdate = new UpdateVO();
	   List<ArsPaisCompania> clients=	validador.getDataSetController().fncBusquedaFiltroCompaniaAndCountry(enterpriseId, campoPais.getToCountryM2MCountry(countryM2M));
	   console.println("enterpriseId........."+enterpriseId);
	   console.println("country........."+country);
	   console.println("clients........."+clients);
	   console.println("countrym2m........."+countryM2M);
	   clientPais=clients.get(0).getArsClient().getArcId();
	   validador.getMappingError().bucadorTags(validador,clientPais,campoPais.getToCountryM2MCountry(countryM2M),enterpriseId);
	
		//    validador.getMappingError().bucadorTags(validador,"TEMIP","MEXICO","GM_MX");
	   formulario.filtrarPorPais(campoPais.getToCountryM2MCountry(countryM2M));
		
	   //formulario=new Formulario(iFormulario.findByPais(campoPais.getToCountryM2MCountry(countryM2M)));
			List<String> validarFound = validador.notfound(voAttachment);
		GestionCampoPais gestionador=new GestionCampoPais();
	
		console.println("id: "+id);
		 String  valorNum=id.substring(3,id.length());
		 String valorCad=id.substring(0, 3);
		 String nuevotroubleticket=valorCad.toUpperCase()+valorNum;
			// if (!nuevotroubleticket.matches("WLG"+"\\d{12}")) {	 
				
		if (!nuevotroubleticket.matches("INC"+"\\d{12}")| !Herramienta.StringToDouble(valorNum)|id.length()!=15) {	 
		respuesta.setCode("RTT001");
		respuesta.setReason("idtroubletickets is no supported ");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
				.body(respuesta.getRepuestaError().toString());
		}
	

		if (validarFound.size() >=1) {
			respuesta.setCode("PTT004");
			respuesta.setReason(validarFound + " is/are not found ");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(respuesta.getRepuestaError().toString());
		}
	
		List<String> requiredmap = validador.requered(voAttachment);
		
		if (requiredmap.size() >=1) {
			respuesta.setCode("PTT002");
			respuesta.setReason(requiredmap + " is/are  required ");
			// respuesta.setMessage(viInsert.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(respuesta.getRepuestaError().toString());
		}
		
		List<String> noSupport = validador.noSupport(voAttachment);
		if (noSupport.size() >= 1)
		{
			respuesta.setCode("PTT001");
			respuesta.setReason(noSupport + " is/are not supported ");
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(respuesta.getRepuestaError().toString());
		}

		List<FieldRC> voFields = new ArrayList<>();
		List<RemedyControlEntry> voInsertValues = new ArrayList<>();
		RemedyControlEntry voEntry = new RemedyControlEntry();

		voEntry.setField(voFields);
		voInsertValues.add(voEntry);

		viUpdate.setSistema(clientPais);
		
		console.println("formularios", formulario);
		
		viUpdate.setFormulario(formulario.getUpdate());
		
		try
		{
			seteandoValorCategorias(voAttachment, voFields);
			console.println("Se ha agregado la notaseteandoValorCategorias ");

			// iterando(voAttachment,voFields);
		}
		catch (excepcionElemento e1)
		{
			e1.printStackTrace();
		}
		
		console.println("voFields despues de interando: " + voFields);

		String idSent = id; // Agregado para separar UpdateStatus de SendNotes

		try
		{
			String sendNotes = "";
			String changeInc = "";

			sendNotes = headerRequest.get("sendnotes");
			changeInc = headerRequest.get("getinc");

			boolean notes = false;
			boolean getInc= false;

			if (sendNotes != null)
				if (sendNotes.compareTo("true") == 0)
				{
					notes = true;
				}

			if (changeInc != null)
				if (changeInc.compareTo("true") == 0)
				{
					getInc = true;
				}

			console.println("idLocal: "		+ idLocal);
			console.println("changeInc: "	+ changeInc);
			console.println("getInc: "		+ getInc);
			console.println("sendNotes: "	+ sendNotes);
			console.println("notes: "		+ notes);

			String incL2C = "";
			String incC2L = "";

			// Aqui empiezan los flujos
			if (idLocal == null)
				idLocal = "";
			if (idLocal.compareTo("true") == 0)
			{
				if (getInc == true)
				{
					console.println("\nEntra a 'Change incLocal to incCloud'");
					
					clientPais = "LOCAL";
					
					incL2C = getCloudIncidentNumber(id); // Consigue el incCloud con el incLocal
					
					console.println("\nCon el incidente Local[" + id + "] se consigui� el incidente Cloud[" + incL2C + "]");
					
					idSent = incL2C;
					
					console.println("idSent: " + idSent);
				}
			}
			else
			{
				tipo = ConstantesRemedy.tipoCloudToLocal;
				
				console.println("tipo: " + tipo);
				
				// Comes from Remedy Cloud
				clientPais = "LOCAL";
				
				if (getInc == true)
				{
					// Change incCloud to incLocal
					console.println("Entra a 'Change incCloud to incLocal'");

					incC2L = getLocalIncidentNumber(id);
					
					console.println("Con el incidente Cloud[" + id + "] se consigui� el incidente Local[" + incC2L + "]");
					
					idSent = incC2L;
					
					console.println("idSent: " + idSent);
				}
			}

			console.println("idSent???: " + idSent);
			console.println("sendNotes??? (boolean): " + notes);

			if (notes == true)
			{
				// SendNotes
				console.println("\nSendNotes.");
				console.println("voAttachment para notas: " + voAttachment);
				
				JsonObject jsonObject = new JsonParser().parse(voAttachment).getAsJsonObject();
				
				if (jsonObject.get("note") != null && jsonObject.get("note").getAsString().compareTo("") != 0)
				{
					String notas = jsonObject.get("note").getAsString();
					return ResponseEntity.status(HttpStatus.ACCEPTED).headers(responseHeaders)
							.body(addAttachmentWL(notas, idSent, tipo).toString());
				}
			}
			else
			{
				console.println("\nHace update");

				if (idLocal.compareTo("true") == 0)
				{
					console.println("Update de Remedy Local a Remedy Cloud.");

					if (getInc == true)
					{
						console.println("Consigue el id=1 para Update Local to Cloud");
						console.println("id para buscar el id=1: " + idSent);
						
						String idUpdateL2C = getUpdateIncident(idSent);
						idSent = idUpdateL2C;
					}
				}
				else
				{
					idSent = getUpdateIncident(id);
					console.println("\nIncidente de Cloud");
				}
				
				console.println("voAttachment para notas: " + voAttachment);
				
				JsonObject jsonObject = new JsonParser().parse(voAttachment).getAsJsonObject();
				
				String gotStatus = null;
				if (jsonObject.get("status") != null)
					gotStatus = jsonObject.get("status").getAsString();
				
				String notas = null;
				if (jsonObject.get("resolution") != null)
					gotStatus = jsonObject.get("resolution").getAsString();
				
				console.println("gotStatus: " + gotStatus);
				console.println("notas: " + notas);

				if (gotStatus != null)
					if (gotStatus.compareTo("4") == 0)
					{
						console.println("Status: Closed -> Resolved");
						
						addAttachmentWL(notas, incL2C, tipo);
						
						seteandoValorCategorias(voAttachment, voFields);
						
						console.println("Se ha agregado la nota [" + notas + "] al incidente de Cloud [" + incL2C + "].");
						console.println("valor..voFields despues de seteandoValorCategorias " + voFields);
					}
			}

			console.println("Termina la parte agregada.");
			console.println("idSent (LAST): " + idSent);
			
			viUpdate.setId(idSent); // Separando UpdateStatus de SendNotes
		}
		catch (excepcionElemento | IOException e)
		{
			e.printStackTrace();
			respuesta.setCode("RTT003");
			respuesta.setReason(e.getMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(respuesta.getRepuestaError().toString());
		}
		
		viUpdate.setColumnas(voEntry);// viInsert.setColumnas(voInsertValues); CHECAR ESTO PARA VER SI ENVIAR EL voEntry

		creaObjeto creaObjetoCreate = new creaObjeto();
		
		console.println("Contenido de size repetidos lvl1: " + sizerepetidoslvl1 + " lvl2: " + sizerepetidoslvl2);
		
		if (sizerepetidoslvl1 != 0 || sizerepetidoslvl2 != 0)
			try {
				if (repetidos != null)
					if (!repetidos.isEmpty())
						addAttachmentWL(repetidos.toString().replace("[", "").replace("]", ""), idSent, tipo);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		UpdateVO update = voUpdate.fncRmdUpdate(viUpdate);
		
		responseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

		if (viUpdate.getResultado().equals("OK"))
		{
			responseHeaders.set("correlationId", id);
			
			console.println("");
			
			return ResponseEntity.status(HttpStatus.ACCEPTED).headers(responseHeaders).body("");
		}
		else
		{
			respuesta.setCode("PTT001");
			respuesta.setReason("Downstream System Error: " + viUpdate.getResultado());
			respuesta.setMessage(viUpdate.getResultado());

			responseHeaders.set("correlationId", id);
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(respuesta.getRepuestaError().toString());
		}
	}


	public void seteandoValorCategorias(String voAttachment, List<FieldRC> voFields) throws excepcionElemento {
		List<String> repetidos = new ArrayList<>();
		
		JsonObject jsonObject = (new JsonParser()).parse(voAttachment).getAsJsonObject();
		jsonObject.getAsJsonArray(voAttachment);
		JsonParser parser = new JsonParser();
		JsonElement tradeElement = parser.parse(voAttachment);
		JsonObject trade = tradeElement.getAsJsonObject();

		Object[] tradeArray = trade.keySet().toArray();
		List<String> listaTag=validador.getMappingError().mapEveryIdM(validador.getMappingError().getBuscador());
	    Map<String, String> mapaListaTag=validador.getMappingError().getMapTagIdremedyOpcional();
		for (int i = 0; i < tradeArray.length; i++) {
			JsonElement value = trade.get((String) tradeArray[i]);
			
			tradeArray[i]=validador.getMappingError().extraerValorCunaAdmiracion((String) tradeArray[i]);;
			String userBucketPath = mapaListaTag.get((String) tradeArray[i]);
			console.println("userBucketPath.....antes de enviar a agregarVoField.",userBucketPath);;
			
			agregarVoField(voFields, userBucketPath, value);
			if (userBucketPath == null) {
				setRepeditos(repetidos, userBucketPath, value,
						tradeArray[i] + ":" + value.toString().replaceAll("\"", ""));
				sizerepetidoslvl1 = repetidos.size();
				if (value.isJsonArray()) {
					JsonObject newJsonObject = value.getAsJsonArray().get(0).getAsJsonObject();
					Object[] newtradeArray = newJsonObject.keySet().toArray();
					niveles(newtradeArray, newJsonObject, repetidos, voFields);
					sizerepetidoslvl2 = repetidos.size();

				}
			}

		}
		this.repetidos = repetidos.toString();

	

	}
	
	public InsertVO addAttachmentWL(
			String repetidos
			, String id
			, String tipo //SE AGREG� PARA EL TIPO DE NOTAS
				) throws ARException, IOException {	
		//	voInsert.fncRmdInsert(voAttachmentService.fncAttachmentInsertWL(repetidos,id)).getResultado().getNuevo();
		return voInsert.fncRmdInsert(voAttachmentService.fncAttachmentInsertWL(repetidos,id,clientPais, tipo));		
	}
	
public String queryRealInc( String id) throws ARException {
		RemedyControlResultado cosa=new RemedyControlResultado();
		 cosa.setSistema(clientPais);
		 cosa.setFormulario(formulario.getQuery());
//		 cosa.setFormulario("HPD:Help Desk");
		 cosa.setColumnas("1000000161 1");
		 cosa.setCondiciones("'1' LIKE \""+id+"\"");
		 
		 console.println("el id es:"+id);
		 RemedyControlResultado rmdSelect=voSelect.fncRmdSelect(cosa);
		 String queyrealInc = rmdSelect.getResultado().get(0).getField().get(1).getValue();
		 String queyrealInc2 = rmdSelect.getResultado().get(0).getField().get(0).getValue();
			
			console.println("161***********************"+queyrealInc);
			console.println("1�***************"+queyrealInc2);
		 console.println("aqui el incidente generado"+queyrealInc);
		 return queyrealInc;			
	}


	public String getUpdateIncident(String id) throws ARException, excepcionElemento {
		
		console.println("\nEntra a 'getUpdateIncident'");
		
		RemedyControlResultado cosa=new RemedyControlResultado();
		cosa.setSistema(clientPais);
		cosa.setFormulario("HPD:Help Desk");
		
		console.println("formulario insertado por aqui para local",formulario.getQuery());
		cosa.setColumnas("1000000161 1 536870928 536890101"); //cambio de nameclient a idremedy
		cosa.setCondiciones("'1000000161' LIKE \""+id+"\"");
		//536890007  536870928
		console.println("el id es:"+id);
		RemedyControlResultado rmdSelect=voSelect.fncRmdSelect(cosa);
		console.println("sdfdsfsdf: "+rmdSelect.getResultado());
		if(voSelect.fncRmdSelect(cosa).getResultado().size()==0)
			throw  new excepcionElemento("id is not found");
		
		return rmdSelect.getResultado().get(0).getField().get(0).getValue();
	}


	
	//	Inversa (Remedy Cloud a Local)
		public String getCloudIncidentNumber( String id) throws ARException, excepcionElemento {
			console.println("Entra a 'getCloudIncidentNumber'.");
		
			RemedyControlResultado cosa=new RemedyControlResultado();
			cosa.setSistema(clientPais);
			//------------ cosa.setFormulario("HPD:Help Desk");
			cosa.setFormulario(formulario.getQuery());
			console.println("formulario insertado por aqui para local",formulario.getQuery());
			cosa.setColumnas("1000000161 1 536870928 536890101"); //cambios de nameclient a idremedy
			cosa.setCondiciones("'536890101'=\""+id+"\""); // cambios de nameclient a idremedy
			//536890007  536870928
			console.println("el id es:"+id);
		
			
			console.println("cosa: "+cosa);
			RemedyControlResultado rmdSelect=voSelect.fncRmdSelect(cosa);
			
			console.println("sdfdsfsdf....."+rmdSelect.getResultado());
			
			if(rmdSelect.getResultado().size()==0)
				throw  new excepcionElemento("id is not found");
	
			String idRegresadoField="", idRegresado="";
			if("".equals(rmdSelect.getResultado().get(0).getField().get(1).toString())) {
				console.println("el campo de voSelect seleccionado est� vac�o.");
			} else {
				idRegresadoField = rmdSelect.getResultado().get(0).getField().get(1).toString();
				idRegresado = rmdSelect.getResultado().get(0).getField().get(1).getValue();
			}
			
			console.println("id regresado........Field: "+idRegresadoField);
			console.println("id regresado........Value: "+idRegresado);
			
			return idRegresado;
		}
		
		
		public String getLocalIncidentNumber( String id) throws ARException, excepcionElemento {
			console.println("\nEntra a 'getLocalIncidentNumber'.");
			
			RemedyControlResultado cosa=new RemedyControlResultado();
			cosa.setSistema(clientPais);
			cosa.setFormulario(formulario.getQuery());
			console.println("formulario insertado por aqui para local",formulario.getQuery());
			cosa.setColumnas("1000000161 1 536870928 536890101");//cambios de nameclient a idremedy
			cosa.setCondiciones("'1000000161' LIKE \""+id+"\"");
			
			//Buscar 1000000161=INC000008954995 para sacar el valor de 536890007=???
			RemedyControlResultado rmdSelect=voSelect.fncRmdSelect(cosa);
			if(rmdSelect.getResultado().size()==0)
				throw  new excepcionElemento("id is not found");
	
			if("".equals(rmdSelect.getResultado().get(0).getField().get(3))){
				console.println("NO SE ENCONTR� EL N�MERO DE INCIDENTE LOCAL");
				return null;
			}else {
				console.println("536890101..........field: "+rmdSelect.getResultado().get(0).getField().get(3)); //cambio de nameclient a idremedy
				console.println("536890101..........value: "+rmdSelect.getResultado().get(0).getField().get(3).getValue()); //cambio de nameclient a idremedy
			}
			
			return rmdSelect.getResultado().get(0).getField().get(3).getValue();
		}

public String getIncInterface( String id) throws ARException, excepcionElemento {
		console.println("\nEntra a 'getIncInterface'");

	RemedyControlResultado cosa=new RemedyControlResultado();
	 cosa.setSistema(clientPais);
	 console.println("clientPais..........."+clientPais);
	// cosa.setFormulario("HPD:Help Desk");
	 console.println("formulario insertado por aqui",formulario.getQuery());
	 cosa.setFormulario(formulario.getQuery());
	 cosa.setColumnas("1000000161 1");
	 cosa.setCondiciones("'1000000161' LIKE \""+id+"\"");
	 
	 console.println("el id es:"+id);
	RemedyControlResultado rmdSelect=voSelect.fncRmdSelect(cosa);
		
	console.println("sdfdsfsdf"+rmdSelect.getResultado());
	if(rmdSelect.getResultado().size()==0)
		throw  new excepcionElemento("id is not found");
			 console.println("primero",rmdSelect.getResultado().get(0).getField());
	 String queyrealInc = rmdSelect.getResultado().get(0).getField().get(1).getValue();
		String queyrealInc2 = rmdSelect.getResultado().get(0).getField().get(0).getValue();
		
		console.println("161***********************"+queyrealInc);
		console.println("1�***********.****"+queyrealInc2);
	 console.println("aqui el incidente generado"+queyrealInc);
	 console.println("est� enviando este incidente..........queyrealInc2......... "+queyrealInc2);
	 return queyrealInc; //Debe traer el incidente de Cloud cuando se le da el incidente Local
				
}

public String getIncWeb( String id) throws ARException {
	RemedyControlResultado cosa=new RemedyControlResultado();
	 cosa.setSistema(clientPais);
	// cosa.setFormulario("HPD:Help Desk");
	 cosa.setFormulario(formulario.getQuery());
	 cosa.setColumnas("1000000161 1");
	 cosa.setCondiciones("'1' LIKE \""+id+"\"");
	 
	 console.println("el id es:"+id);
	 RemedyControlResultado rmdSelect=voSelect.fncRmdSelect(cosa);
		
	 String queyrealInc =rmdSelect.getResultado().get(0).getField().get(1).getValue();
		String queyrealInc2 = rmdSelect.getResultado().get(0).getField().get(0).getValue();
		
		console.println("161******asuiq*****************"+queyrealInc);
		console.println("1�*******asd********"+queyrealInc2);
	 console.println("aqui el incidente generado"+queyrealInc);
	 return queyrealInc;			
}

public void niveles(Object[] newtradeArray,JsonObject newJsonObject, List<String> repetidos,List<FieldRC> voFields ) {
	for(int j=0;j<newtradeArray.length;j++) {
	JsonElement newvalue = newJsonObject.get((String) newtradeArray[j]);
	 String newuserBucketPath = env.getProperty((String) newtradeArray[j]);
 
	 setRepeditos(repetidos,newuserBucketPath,newvalue, newtradeArray[j]+":"+newvalue.toString().replaceAll("\"", ""));
	 agregarVoField(voFields,newuserBucketPath,newvalue);
 }

		
}

public  void setRepeditos(List<String> lista ,String userBucketPath, JsonElement value, String valor){
	  if(userBucketPath==null&!value.isJsonArray())
	     lista.add(valor);
}



public void agregarVoField(List<FieldRC> voFields, String userBucketPath,JsonElement value) {
	
	if(userBucketPath!=null)
		 voFields.add(new FieldRC(Integer.valueOf(userBucketPath) ,value.toString().replace("\"", "")));
}

//public void iterando(String voAttachment,List<FieldRC> voFields) {
//	List<String> repetidos =new ArrayList<>();;
//	JsonObject jsonObject = (new JsonParser()).parse(voAttachment).getAsJsonObject();
//	jsonObject.getAsJsonArray(voAttachment);
//	JsonParser parser = new JsonParser();
//	JsonElement tradeElement = parser.parse(voAttachment);
//	JsonObject trade = tradeElement.getAsJsonObject();
//	
//	Object[] tradeArray = trade.keySet().toArray();
//	
//	for(int i = 0;i<tradeArray.length;i++) {
//		JsonElement value = trade.get((String) tradeArray[i]);
//		 String userBucketPath = env.getProperty((String) tradeArray[i]);
//			agregarVoField(voFields,userBucketPath,value);
//		if(userBucketPath==null) {
//			 setRepeditos(repetidos,userBucketPath,  value,tradeArray[i]+":"+value.toString().replaceAll("\"", ""));
//			 sizerepetidoslvl1= repetidos.size();
//			if(value.isJsonArray()) {
//				JsonObject newJsonObject = value.getAsJsonArray().get(0).getAsJsonObject();
//				Object[] newtradeArray = newJsonObject.keySet().toArray();
//				niveles(newtradeArray, newJsonObject, repetidos,voFields);
//				 sizerepetidoslvl2= repetidos.size();
//
//			}
//		}
//		
//	}
//	this.repetidos=repetidos.toString();
//	
//	 
//}
//	


public void iterando(String voAttachment, List<FieldRC> voFields) throws excepcionElemento {
	List<String> repetidos = new ArrayList<>();
	
	JsonObject jsonObject = (new JsonParser()).parse(voAttachment).getAsJsonObject();
	jsonObject.getAsJsonArray(voAttachment);
	JsonParser parser = new JsonParser();
	JsonElement tradeElement = parser.parse(voAttachment);
	JsonObject trade = tradeElement.getAsJsonObject();

	Object[] tradeArray = trade.keySet().toArray();
	List<String> listaTag=validador.getMappingError().mapEveryIdM(validador.getMappingError().getBuscador());
    Map<String, String> mapaListaTag=validador.getMappingError().getMapTagIdremedy();
    console.println("mapaListaTag antes de insertar en interando",mapaListaTag);
    console.println("tradeArray.... en iterando",tradeArray);
    
	for (int i = 0; i < tradeArray.length; i++) {
		JsonElement value = trade.get((String) tradeArray[i]);
		
		tradeArray[i]=validador.getMappingError().extraerValorCunaAdmiracion((String) tradeArray[i]);;
		
		//----String userBucketPath = env.getProperty((String) tradeArray[i]);
		String userBucketPath = mapaListaTag.get((String) tradeArray[i]);
		console.println("userBucketPath.....antes de enviar a agregarVoField.",userBucketPath);;
		
		console.println(" parseando userBucketPath.....antes de enviar a agregarVoField.",validador.getMappingError().extraerValorCunaAdmiracion((String) tradeArray[i]));;
		agregarVoField(voFields, userBucketPath, value);
		if (userBucketPath == null) {
			setRepeditos(repetidos, userBucketPath, value,
					tradeArray[i] + ":" + value.toString().replaceAll("\"", ""));
			sizerepetidoslvl1 = repetidos.size();
			if (value.isJsonArray()) {
				JsonObject newJsonObject = value.getAsJsonArray().get(0).getAsJsonObject();
				Object[] newtradeArray = newJsonObject.keySet().toArray();
				console.println("newtradeArray.....antes de enviar a niveles.",newtradeArray);;
				
				niveles(newtradeArray, newJsonObject, repetidos, voFields);
				sizerepetidoslvl2 = repetidos.size();

			}
		}

	}
	this.repetidos = repetidos.toString();

}

}
