package com.telcel.gm.services;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.bmc.arsys.api.ARException;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.telcel.gm.component.console;
import com.telcel.gm.match.creaObjeto;
import com.telcel.gm.respuesta.Respuesta;
import com.telcel.gm.security.Base64Security;
import com.telcel.remedy.ars.itsm.RmdInsert;
import com.telcel.remedy.ars.itsm.RmdSelect;
import com.telcel.remedy.ars.itsm.RmdUpdate;
import com.telcel.remedy.ars.model.UpdateVO;
import com.telcel.remedy.ars.model.FieldRC;
import com.telcel.remedy.controllers.DataSetController;

import com.telcel.remedy.ars.model.RemedyControlEntry;
import com.telcel.remedy.ars.model.RemedyControlResultado;

public class DeleteCase {
	static Logger logger = Logger.getLogger(CreateCaseService.class);

	// @PatchMapping("/deleteCase/{id}")
	// public UpdateVO updateCase(@RequestBody String voAttachment,@PathVariable
	// String id) throws ARException {
	// public ResponseEntity<String> deleteCase(@RequestBody String
	// voAttachment,@PathVariable String id , HttpServletRequest headerRequest)
	// throws ARException {
	@Autowired
	public Environment env;

	RmdUpdate voUpdate;

	AttachmentService voAttachmentService;

	RmdSelect voSelect;

	RmdInsert voInsert;

	public DeleteCase(RmdUpdate voUpdate, AttachmentService voAttachmentService, RmdSelect voSelect, RmdInsert voInsert,
			Environment env) {

		this.env = env;
		this.voUpdate = voUpdate;
		this.voAttachmentService = voAttachmentService;
		this.voSelect = voSelect;
		this.voInsert = voInsert;
	}
	public DeleteCase() {
		
	}

	public String codificar(String valor) {
		byte[] encodedusrBytes = Base64.getEncoder().encode(valor.getBytes());
		return new String(encodedusrBytes);
	}

	public String decodificador(HttpServletRequest headerRequest) {
		String aut = "";
		if (headerRequest.getHeader("Authorization") == null) {

			aut = " ";
		} else
			aut = headerRequest.getHeader("Authorization").trim().replaceAll("Basic", "");

		byte[] decoded = Base64.getMimeDecoder().decode(((aut == ":") ? "" : aut));
		return new String(decoded);

	}

	public boolean isAut(String aut) {
		console.println("el aut es:" + aut);
		String user = aut == ":" ? "" : aut.split(":")[0];
		String passd = aut == ":" ? "" : aut.split(":")[1];
		return (codificar(user).equals(env.getProperty("usr")) && (codificar(passd).equals(env.getProperty("pswrd"))));
	}

	public String getIncInterface(String id) throws ARException {
		RemedyControlResultado cosa = new RemedyControlResultado();
		cosa.setSistema("TEMIP");
		cosa.setFormulario("HPD:Help Desk");
		cosa.setColumnas("1000000161 1");
		cosa.setCondiciones("'1000000161' LIKE \"" + id + "\"");

		String queyrealInc = voSelect.fncRmdSelect(cosa).getResultado().get(0).getField().get(1).getValue();
		String queyrealInc2 = voSelect.fncRmdSelect(cosa).getResultado().get(0).getField().get(0).getValue();

		return queyrealInc2;
	}

	public String getIncWeb(String id) throws ARException {
		RemedyControlResultado cosa = new RemedyControlResultado();
		cosa.setSistema("TEMIP");
		cosa.setFormulario("HPD:Help Desk");
		cosa.setColumnas("1000000161 1");
		cosa.setCondiciones("'1' LIKE \"" + id + "\"");

		String queyrealInc = voSelect.fncRmdSelect(cosa).getResultado().get(0).getField().get(1).getValue();
		String queyrealInc2 = voSelect.fncRmdSelect(cosa).getResultado().get(0).getField().get(0).getValue();

		return queyrealInc;
	}

	public ResponseEntity<String> delete(String voAttachment,String id,HttpServletRequest headerRequest, HttpServletResponse httpServletResponse)throws ARException {
	    httpServletResponse.setHeader("X-API-Version", "value1");
	    httpServletResponse.setHeader("enterpriseId", "value2");
	    httpServletResponse.setHeader("correlationId", "value3");
	    httpServletResponse.setHeader("providerId", "value4");
	    httpServletResponse.setHeader("country", "value5");
	    httpServletResponse.setHeader("countryM2M", "value6");

		HttpHeaders responseHeaders = new HttpHeaders();
    	UpdateVO viUpdate = new UpdateVO();
    	
    	Base64Security base64Security = new Base64Security(env);
        console.println("isAout ",base64Security.isAut(base64Security.decodificador(headerRequest)));
//        if(!base64Security.isAut(base64Security.decodificador(headerRequest))) {
//        	return ResponseEntity.status(401).body("unauthorized") ;}
    	

    	
    	
    
	    List<FieldRC> voFields = new ArrayList<>();
		List<RemedyControlEntry> voInsertValues = new ArrayList<>();
		RemedyControlEntry voEntry = new RemedyControlEntry();

		JsonObject jsonObject = (new JsonParser()).parse(voAttachment).getAsJsonObject();
		jsonObject.getAsJsonArray(voAttachment);
		JsonParser parser = new JsonParser();
		JsonElement tradeElement = parser.parse(voAttachment);
		console.println("Trade element " + tradeElement);

		JsonObject jobj = new Gson().fromJson(tradeElement, JsonObject.class);

		String result = jobj.get("resolution").getAsString();
		viUpdate.setSistema("TEMIP");
		voFields.add(new FieldRC(7, "6"));
		voFields.add(new FieldRC(1000000156, result));

		voEntry.setField(voFields);
		voInsertValues.add(voEntry);
		viUpdate.setFormulario("HPD:Help Desk");
		logger.info("info3");
		console.println("debug1" + id);
		console.println("dldldl" + getIncInterface(id));
		viUpdate.setId(getIncInterface(id));

		viUpdate.setColumnas(voEntry);// viInsert.setColumnas(voInsertValues); CHECAR ESTO PARA VER SI ENVIAR EL
										// voEntry

		creaObjeto creaObjetoCreate = new creaObjeto();
		console.println("CAso eliminado");
		UpdateVO update = voUpdate.fncRmdUpdate(viUpdate);
		ResponseEntity<String> voResponse = null;
		Respuesta respuesta = new Respuesta();
	
		responseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
	
		if (viUpdate.getResultado().equals("OK")) {
	
			responseHeaders.set("correlationId", id);

			return ResponseEntity.status(HttpStatus.ACCEPTED).headers(responseHeaders).body("");
		} else {
			responseHeaders.set("correlationId", id);

			respuesta.setCode(viUpdate.getResultado());
			respuesta.setMessage(viUpdate.getResultado());

			console.println(respuesta.getRepuestaError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(respuesta.getRepuestaError().toString());
//	       return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(viInsert.getResultado().toString());

		}

	}

	/// PONER EL ID 151 Y 1 CORRECTAMENTE

}
