package com.telcel.gm.services;





import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.xa.Xid;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Timestamp;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.telcel.gm.component.console;
import com.telcel.gm.match.GestionCampoPais;
import com.telcel.gm.match.Validador;
import com.telcel.gm.match.clasificadorIds;
import com.telcel.gm.match.creaObjeto;
import com.telcel.gm.match.excepcionElemento;
import com.telcel.gm.match.ids;
import com.telcel.gm.match.tags;
import com.telcel.gm.model.CreateCase;
import com.telcel.gm.model.Formulario;
import com.telcel.gm.model.JsonNote;
import com.telcel.gm.model.TroubleTicketAttachment;
import com.telcel.gm.respuesta.Respuesta;
import com.telcel.gm.security.Base64Security;
import com.telcel.remedy.ars.itsm.RmdInsert;
import com.telcel.remedy.ars.itsm.RmdSelect;
import com.telcel.remedy.ars.model.FieldRC;
import com.telcel.remedy.ars.model.InsertVO;
import com.telcel.remedy.ars.model.RemedyControlEntry;
import com.telcel.remedy.ars.model.RemedyControlResultado;
import com.telcel.remedy.controllers.DataSetController;
import com.telcel.remedy.model.entity.ArsPaisCompania;
import com.telcel.remedy.model.services.IArsFormularioService;
import com.telcel.remedy.model.dao.IArsClientTag;
import com.telcel.remedy.model.entity.ArsClientTag;
import com.telcel.remedy.model.entity.ArsTag;
import com.telcel.remedy.model.services.ArsTagServiceImpl;

import net.bytebuddy.agent.Attacher;
import com.telcel.remedy.ars.model.UpdateVO;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import util.Sistema;
public class QueryCase {
	
	@Autowired
	private Environment env;
	
	 Validador validador;
	
	@Autowired
	@Qualifier("rmdSelect")
	RmdSelect voSelect;
	AttachmentService voAttachmentService;
	@Autowired
	DataSetController dataSetController;
	private String clientPais="";
	IArsFormularioService iFormulario;
	
	Formulario formulario ;
	public QueryCase(Environment env,RmdSelect voSelect,AttachmentService voAttachmentService,Validador validador,IArsFormularioService iFormulario) {
		this.env=env;
		this.voSelect=voSelect;
		this.voAttachmentService=voAttachmentService;
		this.validador= validador;
		this.iFormulario=iFormulario;
		
	}
	public QueryCase(Environment env,RmdSelect voSelect,AttachmentService voAttachmentService,Validador validador,Formulario formulario) {
		this.env=env;
		this.voSelect=voSelect;
		this.voAttachmentService=voAttachmentService;
		this.validador= validador;
		this.formulario=formulario;
		
	}
	
	
	public QueryCase(Validador validador) {
		this.validador=validador;		
		this.env=validador.getEnv();
	}
	public ResponseEntity<ObjectNode>   queryCpd( String id, CreateCase model, @RequestHeader Map<String,String> headerRequest) throws ARException {
		   GestionCampoPais campoPais=new GestionCampoPais();
		   headerRequest=campoPais.changeHeaderToCloud(headerRequest);
		   String enterpriseId=headerRequest.get("enterpriseid");
		   String correlationId=headerRequest.get("correlationid");
		   String providerId=headerRequest.get("providerid");
		   String country=headerRequest.get("country");
		   String countryM2M=headerRequest.get("countrym2m");
		   
		   console.println("enterpriseId.....",enterpriseId);
		   console.println("correlationId.....",correlationId);
		   console.println("providerId.....",providerId);
		   console.println("country.....",country);
		   console.println("countryM2M.....",countryM2M);
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
				
		Respuesta respuesta=new Respuesta();
		  validador.setTipoServicio("Q");
		     
		model.setIncidentNumber(id);
		 RemedyControlResultado cosa=new RemedyControlResultado();
		 
		 List<ArsPaisCompania> clients=	validador.getDataSetController().fncBusquedaFiltroCompaniaAndCountry(enterpriseId, campoPais.getToCountryM2MCountry(countryM2M) );
		//  console.println("clients.....",clients);
		  clientPais=clients.get(0).getArsClient().getArcId();
		  formulario.filtrarPorPais(campoPais.getToCountryM2MCountry(countryM2M));
			
			//formulario=new Formulario(iFormulario.findByPais(campoPais.getToCountryM2MCountry(countryM2M)));
	     console.println("clientes sistema", clientPais);
		 cosa.setSistema(clientPais);
	     console.println("el formulario ", formulario);
		 cosa.setFormulario(formulario.getQuery());
	     console.println("el formulario en query", formulario.getQuery());
		 cosa.setColumnas(EliminarCaracter(env.getProperty("columnasQuery") ));
		
		 cosa.setCondiciones("'1000000161' = \""+model.getIncidentNumber()+"\"");
					
		//console.println("incidentes recibido : "+model.getIncidentNumber());
	    	List<String> validarHeader = validador.validarCabezera(headerRequest);

			if(validarHeader.size()>0) {
				respuesta.setCode("RTT002");
				respuesta.setReason(validarHeader+" is required ");
				console.println("esta cosa entro aqui????");
				// respuesta.setMessage(viInsert.getResultado().getError());
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
						.body(respuesta.getRepuestaError());
			}
			
			  
			validador.getMappingError().bucadorTags(validador,clientPais,campoPais.getToCountryM2MCountry(countryM2M),enterpriseId);
		
			//  validador.getMappingError().bucadorTags(validador,"TEMIP","MEXICO","GM_MX");
	       
				 if (!id.contains("INC")) {	 
					respuesta.setCode("RTT001");
					respuesta.setReason("id is no supported ");
					// respuesta.setMessage(viInsert.getResultado().getError());
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
							.body(respuesta.getRepuestaError());
				}
	       if (id.length()!=15) {	 
		   		respuesta.setCode("RTT002");
		   		respuesta.setReason("id is  required ");
		   		// respuesta.setMessage(viInsert.getResultado().getError());
		   		return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
		   				.body(respuesta.getRepuestaError());
		   	}
	        String  valorNumerico=id.substring(3,id.length());
			String valorCadena=id.substring(0, 3);
			String nuevoId=valorCadena.toUpperCase()+valorNumerico;
			//console.println("valor de los 3 primeros ",nuevoId.matches("INC"+"\\d{12}"));
			//console.println(" primeros --------");
			

	   	if (!nuevoId.matches("INC"+"\\d{12}") ) {	 
	   		respuesta.setCode("RTT001");
	   		respuesta.setReason("id is no supported ");
	   		// respuesta.setMessage(viInsert.getResultado().getError());
	   		return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
	   				.body(respuesta.getRepuestaError());
	   	}
	  //<String> validarFound = validador.notfound(id);
	//	JsonObject g = new JsonParser().parse(f).getAsJsonObject();
	     
	   	creaObjeto creaobjeto=new creaObjeto();
		JsonArray responseArray = new JsonArray();
	    JsonObject mapping = new JsonObject();
	    creaobjeto.getMap();
      

      ObjectNode queryHelpDesk=null;
      Map<String, String> attachment=null;
	try {
		//cosita=validador.nuevoMapita();
		JsonNode objeto=creaobjeto.StringtoJsonNode((voSelect.fncRmdSelect(cosa)));
		console.println("consulta del incidente",objeto);
		if(objeto==null) {//NO SIRBE ESTO, PONERFOWNSTREAM ERROR DIFERENTE
		    respuesta.setCode("RTT004");
		    respuesta.setReason(model.getIncidentNumber()+" is not found");
		    return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders).body(respuesta.getRepuestaError());
		  
			}
		queryHelpDesk = validador.nuevoMapita( validador.cambiadorDeValor(objeto ));
		//console.println("cosita antes de cpdWl "+queryHelpDesk);
			
		 try {
			attachment=queryCpdWL(id,model,queryHelpDesk);
			//console.println("sddsdssdlknsdavlkjdflk"+attachment);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		
		//--------------------------------------------------------------------
			JsonObject jsonInc = new JsonObject();
			JsonArray jsonArrayInc = new JsonArray();
			JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
			ArrayNode jsonArray = nodeFactory.arrayNode();
			ArrayNode jsonArray2 = nodeFactory.arrayNode();
			ArrayNode jsonArray3 = nodeFactory.arrayNode();
			ObjectNode jsonNode = nodeFactory.objectNode();
//			console.println("attachmeeeeeeeeent" + attachment);
//			console.println("remedyv1.1.0");
//			console.println("remedyv1.1.3");
//			
			
			
			for (Entry<String, String> entrynewmap : attachment.entrySet()) {//aqui
				ObjectNode jsonAtt = nodeFactory.objectNode();
				jsonAtt.put("id", entrynewmap.getKey());
			//	console.println("entrynewmap.getValue()" + entrynewmap.getValue());
				String newMapString = entrynewmap.getValue().replace("[", "").replace("]", "");

				Map<String, String> reconstructedUtilMap = Arrays.stream(newMapString.split(",")).map(s -> s.split("="))
						.collect(Collectors.toMap(s -> s[0], s -> s[1]));

				//console.println("reconstructedUtilMap  " + reconstructedUtilMap);
				for (Entry<String, String> newreconstructedUtilMap : reconstructedUtilMap.entrySet()) {
					if (!newreconstructedUtilMap.getValue().contains("null")) {
						jsonAtt.put(newreconstructedUtilMap.getKey().trim(), newreconstructedUtilMap.getValue());
					} else
						jsonAtt.set(newreconstructedUtilMap.getKey().trim(), null);
					// jsonAtt.addProperty(newreconstructedUtilMap.getKey(),
					// newreconstructedUtilMap.getValue());
				}

				jsonNode.set("startDateTime", null);
				jsonNode.set("endDateTime", null);
				jsonAtt.set("href", null);
				jsonAtt.putPOJO("validFor", jsonNode);
	///			console.println("VALIDACION XADANI " + jsonAtt);
				if (!jsonAtt.get("name").equals(null)) {
		//			console.println("INSIDE HERE");
					jsonArray.add(jsonAtt);

					//		console.println("VALID 1" + jsonArray);
				}
				//System.out.println("VALID 2" + jsonArray);
				//console.println("VALID 2" + jsonArray);
			}
			for (int x = 0; x < jsonArray.size(); x++) {
				//console.println("XADANI 1 " + jsonArray.get(x).get("name"));
				if (jsonArray.get(x).get("name") == null) {
				}
				if (jsonArray.get(x).get("name").toString() == "null") {
					JsonNote jsonNote2 = new JsonNote();

					jsonNote2.setDetail(jsonArray.get(x).get("description note").toString().replaceAll("\"", ""));
					jsonNote2.setId(jsonArray.get(x).get("id").toString().replaceAll("\"", ""));
					jsonNote2.setDate(jsonArray.get(x).get("date").toString().replaceAll("\"", ""));
					Gson gson = new Gson();

					String json = gson.toJson(jsonNote2);
					creaObjeto creaObjetoCreate = new creaObjeto();
					jsonArray2.add((ObjectNode) creaObjetoCreate.StringtoJsonNode(json));
				}
				if (jsonArray.get(x).get("name").toString() != "null") {
					jsonArray3.add(jsonArray.get(x));
					JsonNote jsonNote2 = new JsonNote();

					jsonNote2.setDetail(jsonArray.get(x).get("description").toString().replaceAll("\"", ""));
					jsonNote2.setId(jsonArray.get(x).get("id").toString().replaceAll("\"", ""));
					jsonNote2.setDate(jsonArray.get(x).get("date").toString().replaceAll("\"", ""));
					
					Gson gson = new Gson();
					String json = gson.toJson(jsonNote2);

					creaObjeto creaObjetoCreate = new creaObjeto();
					jsonArray2.add((ObjectNode) creaObjetoCreate.StringtoJsonNode(json));
				}

			}
			if (jsonArray3 != null && jsonArray3.size() <= 0) {
				queryHelpDesk.putPOJO("attachment", null);

			} else {
				queryHelpDesk.putPOJO("attachment", jsonArray3);
			}

			queryHelpDesk.putPOJO("serviceUpdates", jsonArray2);

			console.println("fortugivetome     llll" + queryHelpDesk.toString());
			// -------------------------------------------------------

		} catch (excepcionElemento e) {// | net.minidev.json.parser.ParseException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// console.println(cosita);

			//	if(attachment!=null)
			//     cosita.putAll(attachment);//COMENTADO POR UNOS MOMENTOS
			//		console.println("uno a" + queryHelpDesk);
			//		console.println("dos b" + queryHelpDesk.toString());
			//		console.println("rtres a" + queryHelpDesk.asText().toString());

		return ResponseEntity.status(HttpStatus.ACCEPTED).headers(responseHeaders).body(queryHelpDesk);
	}
	
	
	
	static Logger logger = Logger.getLogger(CreateCaseService.class);
	  
	  public String[] GetIncNumbers (
	      @PathVariable String id
	      , @RequestHeader Map<String,String> headerRequest
	    )throws ARException  {
	    
	    String enterpriseId=headerRequest.get("enterpriseid");
	    String correlationId=headerRequest.get("correlationid");
	    String providerId=headerRequest.get("providerid");
	    String country=headerRequest.get("country");
	    String countryM2M=headerRequest.get("countrym2m");
	     String idLocal=headerRequest.get("idlocal");
	     

	    // console.println("header: "+headerRequest);
	    // console.println("idLocal (en QueryCase): "+idLocal);
	     
	     GestionCampoPais campoPais=new GestionCampoPais();
	     
	     Respuesta respuesta = new Respuesta();
	    HttpHeaders responseHeaders = new HttpHeaders();

	    responseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
	       UpdateVO viUpdate = new UpdateVO();
	       
	       List<ArsPaisCompania> clients =  validador.getDataSetController()
	           .fncBusquedaFiltroCompaniaAndCountry(enterpriseId, campoPais.getToCountryM2MCountry(countryM2M));
	       console.println("\nEntra aqui"
	           + "\nenterpriseId........."+enterpriseId
	           + "\ncountry........."+country
	           + "\ncountry M2M........."+countryM2M
	    );
	       console.println("clients: "+clients);
	       clientPais=clients.get(0).getArsClient().getArcId();
	       console.println("clientPais: "+clientPais);
	       validador.getMappingError().bucadorTags(validador,clientPais,campoPais.getToCountryM2MCountry(countryM2M),enterpriseId);
	  
	    //    validador.getMappingError().bucadorTags(validador,"TEMIP","MEXICO","GM_MX");
	    validador.setTipoServicio("Q");
	    formulario.filtrarPorPais(campoPais.getToCountryM2MCountry(countryM2M));
		
	   // formulario=new Formulario(iFormulario.findByPais(campoPais.getToCountryM2MCountry(countryM2M)));
	   // console.println("\niFormulario.findByPais(country)....."+formulario.findByPais(campoPais.getToCountryM2MCountry(countryM2M)));
	    GestionCampoPais gestionador=new GestionCampoPais();
	      List<FieldRC> voFields = new ArrayList<>();
	    List<RemedyControlEntry> voInsertValues = new ArrayList<>();
	    RemedyControlEntry voEntry = new RemedyControlEntry();
	    
	    voEntry.setField(voFields);
	    voInsertValues.add(voEntry);
	    
	    viUpdate.setSistema(clientPais);
	    viUpdate.setFormulario(formulario.getQuery());

	    // PARTE IMPORTANTE PARA REGRESAR EL N�MERO DE INCIDENTE
	    try {
	      if("true".equals(idLocal)) {
	        String CloudRequest[] = getIncNumRCloud(id);
	        
	        return CloudRequest;
	        
	      } else {
	       // console.println("de Remedy Cloud a Remedy Local.");
	        String LocalRequest[] = new String[2];
	        
	        String incLocal = getIncNumRLocal(id);
	        
//	        console.println(
//	          "id: "+id
//	          + "\nincLocal: "+incLocal
//	        );
	        
	        return LocalRequest;
	      }}
	    catch(Exception e) {
	      
	    }
	    return null;
	  }


	  public String getIncNumRLocal(String id) throws ARException, excepcionElemento {
		    console.println("\nEntra a 'getIncNumRLocal'.");
		    
		    RemedyControlResultado cosa = new RemedyControlResultado();
		    cosa.setSistema(clientPais);
		    cosa.setFormulario(formulario.getQuery());
//		    console.println("formulario insertado por aqui para local",formulario.getQuery());
		    cosa.setColumnas("1000000161 1 536870928 536890101"); //Cambio de nameClient a idremedy
		    cosa.setCondiciones("'1000000161' LIKE \""+id+"\"");
		    
		    //Buscar 1000000161=INC000008954995 para sacar el valor de 536890007=???
		    RemedyControlResultado consulta= voSelect.fncRmdSelect(cosa);
			 
		 //   console.println("incidentNumber LOCAL........CLOUD a Local"+voSelect.fncRmdSelect(cosa).getResultado());
		    if(consulta.getResultado().size()==0)
		      throw  new excepcionElemento("id is not found");

		    if("".equals(consulta.getResultado().get(0).getField().get(3))){
		      console.println("NO SE ENCONTR� EL N�MERO DE INCIDENTE LOCAL");
		      return null;
		    }else {
		      String campo536890101 = consulta.getResultado().get(0).getField().get(3).toString(); //Cambio de la variable que posee id_remedy de nameclient a id_remedy de idremedy
		      String valor536890101= consulta.getResultado().get(0).getField().get(3).getValue(); //Cambio de la variable que posee id_remedy de nameclient a id_remedy de idremedy
		      
//		      console.println("536890101..........field: "+campo536890101
//		              + "\536890101..........value: "+valor536890101);//cambio de nameClient a idremedy
//		      
//		      console.println("\n536890101..........field: "+voSelect.fncRmdSelect(cosa).getResultado().get(0).getField().get(3));//cambio de nameclient a idremedy
//		      console.println("536890101..........value: "+voSelect.fncRmdSelect(cosa).getResultado().get(0).getField().get(3).getValue());//cambio de nameclient a idremedy
		      
		      return valor536890101; //Cambio de la variable que posee id_remedy de nameclient a id_remedy de idremedy
		    }
		    
//		    return voSelect.fncRmdSelect(cosa).getResultado().get(0).getField().get(3).getValue();
//		    return null;
		  }
		  
		  // Para conseguir el id de R Cloud
		  public String[] getIncNumRCloud(String id) throws ARException, excepcionElemento {
		  //  console.println("Entra a 'getIncNumRCloud'.");
		    
		    String CloudResponse[] = new String[2];
		    
		    RemedyControlResultado cosa = new RemedyControlResultado();
		    cosa.setSistema(clientPais);
		    cosa.setFormulario(formulario.getQuery());
		    
//		    console.println("formulario insertado por aqui para local interface create",formulario.getCrear());
//		    console.println("formulario insertado por aqui para local",formulario.getQuery());
		    
		    cosa.setColumnas("1000000161 1 536870928 536890101 1000000217");//cambio nameclient a id remedy
		    cosa.setCondiciones("'536890101' = \""+id+"\""); //cambio nameclient a id remedy
		    //536890007  536870928

		    
		    //console.println("\nvoSelect....."+voSelect.fncRmdSelect(cosa).getResultado());

		    //console.println("\nGrupo de soporte....."+voSelect.fncRmdSelect(cosa).getResultado().get(0).getField().get(4));
		    //console.println("\nGrupo de soporte....."+voSelect.fncRmdSelect(cosa).getResultado().get(0).getField().get(4).getValue());
		    
		   RemedyControlResultado consulta= voSelect.fncRmdSelect(cosa);
		    if(consulta.getResultado().size()==0)
		      throw  new excepcionElemento("id is not found");

		    String idRemLocalField="", idRemCloud="";
		    if("".equals(consulta.getResultado().get(0).getField().get(1).toString())) {
		      console.println("el campo de voSelect seleccionado est� vac�o.");
		    } else {
		      idRemLocalField = consulta.getResultado().get(0).getField().get(1).toString();
		      idRemCloud =consulta.getResultado().get(0).getField().get(1).getValue();
		      
		      String GrupoSoporteField = consulta.getResultado().get(0).getField().get(4).toString();
		      String GrupoSoporteValue = consulta.getResultado().get(0).getField().get(4).getValue();
		      
		      

		      CloudResponse[0] = idRemCloud;
		      CloudResponse[1] = GrupoSoporteValue;
		    }
		    
		    console.println("id regresado........Field: "+idRemLocalField);
		    console.println("id regresado........Value: "+idRemCloud);
		    
		    return CloudResponse;
//		    return voSelect.fncRmdSelect(cosa).getResultado().get(0).getField().get(0).getValue();
//		    return queyrealInc2;
		  }


    public Map<String, String> queryCpdWL(@PathVariable String id, CreateCase model, ObjectNode queryHelpDesk)
			throws ARException, IOException, Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		Map<String, String> map = new HashMap<String, String>();
		Map<String, String> map2 = new HashMap<String, String>();

		responseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		Respuesta respuesta = new Respuesta();
		model.setIncidentNumber(id);
		RemedyControlResultado cosa = new RemedyControlResultado();
		// cosa.setSistema("RM");
		cosa.setSistema(clientPais);
		console.println("hola por aqui el formulario de attachment", formulario.getAttachment());
		cosa.setFormulario(formulario.getAttachment());
		cosa.setColumnas("1 1000000351 1000000151 1000000761 1000000157");

		cosa.setCondiciones("'1000000161' = \"" + model.getIncidentNumber() + "\"");
		console.println("incidentes recibido : " + model.getIncidentNumber());
		creaObjeto creaobjeto = new creaObjeto();
		console.println("Entra aqui1. 3 ");
		JsonArray responseArray = new JsonArray();
		JsonObject mapping = new JsonObject();
		creaobjeto.getMap();
		ObjectNode queryWorklog = null;

		queryWorklog = queryHelpDesk;
		console.println("COSITA POR FUERA", queryWorklog);
		JsonNode objeto = creaobjeto.StringtoJsonNode((voSelect.fncRmdSelect(cosa)));
		ObjectNode attachment = (ObjectNode) objeto;
		JsonNode resultado = attachment.get("resultado");
		JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
		Map<String, Object> mappingJson = new HashMap<String, Object>();

		List<String> lista = new ArrayList<String>();

		console.println("attachment" + queryWorklog.get("attachment"));
		for (int y = 0; y < resultado.size(); y++) {
			JsonNode ultimoObjeto = resultado.get(y);
			if (ultimoObjeto == null)
				return null;
			if (ultimoObjeto.get("field") == null)
				return null;
			if (ultimoObjeto.get("field").get(0) == null)
				return null;
			console.println("asdasdads" + ultimoObjeto.get("field").get(0).get("value").toString());
			String valor1 = ultimoObjeto.get("field").get(0).get("value").toString();
			JsonNode valor2 = ultimoObjeto.get("field").get(1).get("value");
			JsonNode valor3 = ultimoObjeto.get("field").get(2).get("value");
			JsonNode valor4 = ultimoObjeto.get("field").get(3).get("value");
			JsonNode date = ultimoObjeto.get("field").get(4).get("value");
			String dateTimeStamp = validador
					.timeTimestampToString(date.toString().replaceAll("\"", "").replaceAll("[^0-9]", ""));
			String sdf = ultimoObjeto.get("field").get(2).get("value").toString().replaceAll("\"", "").replaceAll(",",
					";");

			console.println("time vvvv " + date);
			ObjectNode onlyattachment = null;
			if (!valor2.isNull()) {

				Map<String, String> reconstructedUtilMap = Arrays
						.stream(valor2.toString().replaceAll("\"", "").replace("[", "").replace("]", "").split(","))
						.map(s -> s.split("=")).collect(Collectors.toMap(s -> s[0], s -> s[1]));
				console.println("reconstructedUtilMap " + reconstructedUtilMap);
				console.println("attachment   " + queryWorklog.get("attachment"));
				ObjectNode jsonAtt = nodeFactory.objectNode();
				ObjectMapper mapper = new ObjectMapper();
				onlyattachment = creaobjeto.StringtoObjectNode(
						queryWorklog.get("attachment").toString().replace("[", "").replace("]", ""));

				String b = queryWorklog.get("attachment").toString().replace("[", "").replace("]", "");
				JsonObject jsonObject = new JsonParser().parse(b).getAsJsonObject();

				console.println("all the rage back home" + jsonObject);
				Gson gson = new Gson();
				mappingJson = (Map<String, Object>) gson.fromJson(jsonObject, map.getClass());
				String value = map.get("Size");

				console.println("mappingJson  " + mappingJson);

				mappingJson.replace("size", null, value);

				onlyattachment.put("size", reconstructedUtilMap.get("Orig Size"));
				onlyattachment.put("sizeUnit", "byte");
				onlyattachment.put("name", reconstructedUtilMap.get("Name"));
				onlyattachment.put("description", sdf);
				onlyattachment.put("date", dateTimeStamp);
				onlyattachment.remove("validFor");
				console.println("onlyattachment" + onlyattachment);

				mappingJson.replace("size", null, reconstructedUtilMap.get("Orig Size"));
				mappingJson.replace("sizeUnit", null, "byte");
				mappingJson.remove("validFor");
				mappingJson.replace("name", null, reconstructedUtilMap.get("Name"));
				mappingJson.put("description", sdf);
				mappingJson.put("date", dateTimeStamp);

				String newMappingJson = mappingJson.toString().replace("{", "[").replace("}", "]");
				console.println("newMappingJson" + newMappingJson);
				console.println("mappingJson  " + mappingJson);
				console.println("valuevaluevaluevalue" + value);
			}
			if (!valor2.isNull())
			{
				console.println(valor1.replaceAll("\"", "") + "pppppppp   |  "
						+ mappingJson.toString().replace("{", "[").replace("}", "]"));
				map.put(valor1.replaceAll("\"", ""), mappingJson.toString().replace("{", "[").replace("}", "]"));
				// map.put(valor1.replaceAll("\"", ""), onlyattachment.toString().replace("{",
				// "[").replace("}", "]"));
				// map.put(valor1.replaceAll("\"", ""), mappingJson.toString());
			} else if (valor4.toString().replaceAll("\"", "").equals("1")) {
				console.println("[description note=" + sdf
						+ ",size=null, name=null, description=null, sizeUnit=null, href=null, mimeType=null, type=null, url=null]");
				// map.put(valor1.replaceAll("\"", ""), "[description note="+sdf+",
				// date="+dateTimeStamp+",size=null, name=null, description=null, sizeUnit=null,
				// href=null, mimeType=null, type=null, url=null]");
				map.put(valor1.replaceAll("\"", ""), "[description note=" + sdf + ", date=" + dateTimeStamp
						+ ",size=null, name=null, description=null, sizeUnit=null, href=null, mimeType=null, type=null, url=null]");
			}

		}
		console.println("map.toString() " + map.toString());

		String csv = String.join(",", lista);

		return map;

	}

	public String getIdWl(String idweb) {
		return "";
	}

	public String EliminarCaracter(String cadena) {

		return cadena.replaceAll(",", " ");
	}
}
