package com.telcel.gm.services;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bmc.arsys.api.ARException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.telcel.gm.component.console;
import com.telcel.gm.match.GestionCampoPais;
import com.telcel.gm.match.GestionCategoria;
import com.telcel.gm.match.Validador;
import com.telcel.gm.match.creaObjeto;
import com.telcel.gm.match.excepcionElemento;
import com.telcel.gm.model.CreateCase;
import com.telcel.gm.model.Formulario;
import com.telcel.gm.model.ResposeGM;
import com.telcel.gm.model.TroubleTicketAttachment;
import com.telcel.gm.respuesta.MappingError;
import com.telcel.gm.respuesta.Respuesta;
import com.telcel.gm.security.Base64Security;
import com.telcel.gm.services.AttachmentService;
import com.telcel.remedy.ars.itsm.RmdInsert;
import com.telcel.remedy.ars.itsm.RmdSelect;
import com.telcel.remedy.ars.itsm.RmdUpdate;
import com.telcel.remedy.ars.model.FieldRC;
import com.telcel.remedy.ars.model.InsertVO;
import com.telcel.remedy.ars.model.RemedyControlEntry;
import com.telcel.remedy.ars.model.RemedyControlResultado;
import com.telcel.remedy.controllers.DataSetController;
import com.telcel.remedy.model.constantes.ConstantesRemedy;
import com.telcel.remedy.model.entity.ArsClientTag;
import com.telcel.remedy.model.entity.ArsPaisCompania;
import com.telcel.remedy.model.services.IArsFormularioService;
import com.telcel.remedy.model.services.IArsUsersService;

//@RestController()
public class CreateCaseService {
	static Logger logger = Logger.getLogger(CreateCaseService.class);
	RmdInsert voInsert;
	RmdSelect voSelect;
	AttachmentService voAttachmentService;
	DataSetController dataSetController;
	
	@Autowired
	private Environment env;
	private String repetidos = "";
	private int sizerepetidoslvl1 = 0;
	private int sizerepetidoslvl2 = 0;
	private String usuarioRemedy="";
	
	
//	@Autowired
	//@Qualifier("ArsFormularioServiceImpl")
	IArsFormularioService iFormulario;
	
	
	Formulario formulario ;
	
	 Validador validado;
	 GestionCategoria categoria;
		GestionCampoPais gestioncampo;
	 Map<String, String> resultadoGrupoToMexico;
	 public Boolean isException=false;
	 String countryToCreateInciden;
	public CreateCaseService(RmdInsert voInsert, RmdSelect voSelect, AttachmentService voAttachmentService,
			DataSetController dataSetController,IArsFormularioService iFormulario,Environment env) {
		super();
		this.voInsert = voInsert;
		this.voSelect = voSelect;
		this.voAttachmentService = voAttachmentService;
		this.dataSetController = dataSetController;
	    this.iFormulario=iFormulario;
	    this.env=env;
	     categoria=new GestionCategoria(dataSetController);
	     validado = new Validador(env,dataSetController);
	     gestioncampo=new GestionCampoPais();
	}
	
	
	public CreateCaseService(RmdInsert voInsert, RmdSelect voSelect, String cuerpo,
			DataSetController dataSetController,Formulario iFormulario,	AttachmentService voAttachmentService) {
		super();
		this.voInsert = voInsert;
		this.voSelect = voSelect;
		this.voAttachmentService = voAttachmentService;
		this.dataSetController = dataSetController;
	    this.formulario=iFormulario;
	     categoria=new GestionCategoria(dataSetController);
	     validado = new Validador(env,dataSetController);
	     gestioncampo=new GestionCampoPais();
	}
	
	

	
	
	
	
	
	public ResponseEntity<String> createCase(
			@RequestBody String voAttachment
			, @RequestHeader Map<String,String> headerRequest
		//	, HttpServletResponse httpServletResponse
				) throws ARException, excepcionElemento {
		
		logger.info("Log4j: empieza Create Case !!");
		String enterpriseId=headerRequest.get("enterpriseid");
		String correlationId=headerRequest.get("correlationid");
		String providerId=headerRequest.get("providerid");
		String country=headerRequest.get("country");
		String countryM2M=headerRequest.get("countrym2m");
		String tipo="";
		String idLocal = headerRequest.get("idlocal"); 
		countryToCreateInciden=gestioncampo.getToCountryM2MCountry(headerRequest.get("countrym2m"));
		//idLocal=null;
		if(idLocal==null) {
			tipo = ConstantesRemedy.tipoCloudToLocal;
		} else if(idLocal.compareTo("true")==0) {
			tipo = ConstantesRemedy.tipoLocalToCloud;
		}
		
		 List<String> validarHeader = validado.validarCabezera(headerRequest);
		 Respuesta respuesta = new Respuesta();

				
				
			//	gestioncampo.mapeadorCountry("ARG");;
			HttpHeaders responseHeaders = new HttpHeaders();
			if(validarHeader.size()>0) {
				respuesta.setCode("RTT002");
				respuesta.setReason(validarHeader+" is required ");
				console.println("esta cosa entro aqui????");
				// respuesta.setMessage(viInsert.getResultado().getError());
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
						.body(respuesta.getRepuestaError().toString());
			}

			List<FieldRC> voFields = new ArrayList<>();
			responseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
			InsertVO viInsert = new InsertVO();
		
			validado.getMappingError().error();
			List<ArsPaisCompania> clients=	validado.getDataSetController().fncBusquedaFiltroCompaniaAndCountry(enterpriseId, countryToCreateInciden);
	    	usuarioRemedy=clients.get(0).getArsClient().getArcId();
			console.println("validado.....",clients);
			validado.getMappingError().bucadorTags(validado,usuarioRemedy,countryToCreateInciden,enterpriseId);
				  if(clients.size()==0) {
				respuesta.setCode("RTT002");
				respuesta.setReason(enterpriseId+" o "+ countryM2M+ " is/are not found ");
				console.println("esta cosa entro aqui????");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
						.body(respuesta.getRepuestaError().toString());
			}
			validado.setTipoServicio("C");
			
			
			creaObjeto creaObjetoCreate = new creaObjeto();
			
			
			JsonObject cuerpoPrincipal=validado.getCuerpo(voAttachment);
			//cuerpoPrincipal.get("f").
			//ObjectNode cuerpoPrincipal=creaObjetoCreate.StringtoObjectNode(voAttachment);
		    
			//-------18/06/2019--  categoria.consultadorCampo(cuerpoPrincipal.get("ticketType").toString(),  mapeadorPais.get(countryM2M));// SE TIENE QUE METERLO EN EL ATTACHMENT
		    creaObjetoCreate.StringtoObjectNode(voAttachment);
			List<String> validarFound = validado.notfound(voAttachment);
			formulario.filtrarPorPais(countryToCreateInciden);
	
	//		formulario=new Formulario(iFormulario.findByPais(countryToCreateInciden));
			if (validarFound.size() >= 1) {
				respuesta.setCode("CTT004");
				respuesta.setReason(validarFound + " is/are not found ");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
						.body(respuesta.getRepuestaError().toString());
			}

			List<String> requiredmap = validado.requered(voAttachment);
			if (requiredmap.size() >= 1) {
				respuesta.setCode("CTT002");
				respuesta.setReason(requiredmap + " is/are  required ");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
						.body(respuesta.getRepuestaError().toString());
			}
	
			List<String> noSupport = validado.noSupport(voAttachment);
			if (noSupport.size() >=1) {
				respuesta.setCode("CTT001");
				respuesta.setReason(noSupport + " is/are not supported ");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
						.body(respuesta.getRepuestaError().toString());
			}


			List<RemedyControlEntry> voInsertValues = new ArrayList<>();
			RemedyControlEntry voEntry = new RemedyControlEntry();
		
		
		
			ObjectNode voAttachmenNode= categoria.gestionSeverityPriority(creaObjetoCreate.StringtoJsonNode(voAttachment),enterpriseId, countryToCreateInciden);// PARA SEVERITY Y PRIORITY
			voAttachmenNode.putAll(validado.getMappingError().getCamposDefault());
			console.println("que sale en campodefault", validado.getMappingError().getCamposDefault());
			
			console.println("es local......",categoria.isLocal());
			if(!categoria.isLocal()&!isPAU(countryM2M)&!createCloud(countryM2M)) {
				respuesta.setCode("notLocal");
				respuesta.setReason("not create to local");
				console.println("esta cosa entro aqui????");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(respuesta.getRepuestaError().toString());
			}
			voAttachment=voAttachmenNode.toString();
			console.println("voAttachment con los campos default agreagados------------------------------",voAttachment);
			
			//console.println("final de voAttacment con los campos default agreagados");
			iterando(voAttachment, voFields);
			//StringtoObjectNode
			//iterando(cuerpoPrincipal, voFields); //MEJORADO

			Map<String, String> resultadoCategoria=categoria.getIdColumnas("COLUMNAS");
			//if(!categoria.isLocal()|) {
		    resultadoCategoria.forEach((llave,valor)->{
		    	console.println(" agregando categoria.....id.",llave.trim());
		    	console.println(" agregando categoria.....valor.",valor);
				voFields.add(new FieldRC(Integer.valueOf(llave.trim().replaceAll("\"", "")), valor ));
				});
			//}
		    // esta linea de abajo debe de desaparecer para la mejora
			if(countryToCreateInciden.equals("ARG")|countryToCreateInciden.equals("URY")|countryToCreateInciden.equals("PRY")|countryToCreateInciden.equals("BRA")) {
			    resultadoCategoria.forEach((llave,valor)->{
			    	console.println(" agregando categoria.....id.",llave.trim());
			    	console.println(" agregando categoria.....valor.",valor);
					voFields.add(new FieldRC(Integer.valueOf(llave.trim().replaceAll("\"", "")), valor ));
					});
				}
		    console.println("resultadoGrupoToMexico.....",resultadoGrupoToMexico);
	    	if(resultadoGrupoToMexico!=null)
	    	if(!resultadoGrupoToMexico.isEmpty()&countryToCreateInciden.equals("BRA")) 
	    		resultadoGrupoToMexico.forEach((llave,valor)->{
	    			console.println("resultadoGrupoToMexico.....llave....",llave);
					voFields.add(new FieldRC(Integer.valueOf(llave.trim().replaceAll("\"", "")), valor ));
					});

			
		    if(!categoria.isLocal()|countryToCreateInciden.equals("ARG")|countryToCreateInciden.equals("URY")|countryToCreateInciden.equals("PRY")) //CHECARLO PARA QUE NO APLIQUE A ARGENTINA O VERIFICAR ARGENTINA
			{
				//console.println("validando si entra en is local");;
			  Map<String, String> resultadoGrupo=categoria.getIdColumnas("GRUPO");
	      
	       	resultadoGrupo.forEach((llave,valor)->{
				voFields.add(new FieldRC(Integer.valueOf(llave.trim().replaceAll("\"", "")), valor ));
				});
	      //  console.println("Resultado columnas",resultadoCategoria);;
	      	//console.println("Resultado grupos",resultadoGrupo);;
			}
	      	        
       
		console.println("****************************************************************\n"
					   + "****************************************************************\n"
					   + "*****************************************************************\n"
					   + "campos enviado a remedy*******************************************\n"
					   + "",voFields);
		console.println("\n****************************************************************\n"
				+ "*************************************************************************\n"
				+ "***************************************************************************\n"
				+ "");
		
		System.out.println("campos enviado a remedy*******************************************"+voFields);
		
		voEntry.setField(voFields);
		voInsertValues.add(voEntry);
		viInsert.setSistema(usuarioRemedy);
		//viInsert.setFormulario("HPD:IncidentInterface_Create");
		console.println("formulario es",formulario.getCrear());
		viInsert.setFormulario(formulario.getCrear());
		viInsert.setColumnas(voEntry);
		InsertVO insert = voInsert.fncRmdInsert(viInsert);

		if (viInsert.getResultado().getError() == null) {
			String respuestaBody = respuesta.getRespuestaJsonResultado(creaObjetoCreate.convertir(insert))
					.getAsJsonObject().get("Nuevo").getAsString();
			//responseHeaders.set("correlationId", respuestaBody);
			String INC1= queryRealInc(respuestaBody);
		responseHeaders.set("correlationId", INC1);
       
		 		
		
		
		if (sizerepetidoslvl1 != 0 || sizerepetidoslvl2 != 0)
			try {
				
				//System.out.println("repetidos.....  "+repetidos);
				repetidos=repetidos.toString().replace("[", "").replace("]", "");
				if(!repetidos.contains("description"))
				repetidos=repetidos+",description:"+cuerpoPrincipal.get("description").toString()+"\n";
				//System.out.println("repetidos.....  "+repetidos);
				addAttachmentWL(repetidos, INC1,usuarioRemedy, tipo);
			//	addAttachmentWL(voAttachmenNode.toString(), INC1,usuarioRemedy, tipo); IMPLEMENTADO PARA LA MEJORA
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}//aqui attachment

		
			return ResponseEntity.status(HttpStatus.ACCEPTED).headers(responseHeaders).body("");
		} else {
			respuesta.setCode("CTT001");
			respuesta.setReason("Downstream System Error: " + viInsert.getResultado().getError());
			respuesta.setMessage(viInsert.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(respuesta.getRepuestaError().toString());
		}
	}
	
	
	public boolean isPAU(String pau) {
		 return (pau.toUpperCase().equals("AR")|pau.toUpperCase().equals("UY")|pau.toUpperCase().equals("PY"));
	}
	
	public boolean createCloud(String pais) {
		
		return (pais.toUpperCase().equals("BR"));
	}
	
	public Map<String, String> getGroupLocalToCloud(String voAttachment,String country) throws excepcionElemento {
		JsonObject cuerpoPrincipal=validado.getCuerpo(voAttachment);
	    
		categoria.consultadorCampo(cuerpoPrincipal.get("ticketType").toString(), country);// SE TIENE QUE METERLO EN EL ATTACHMENT
		resultadoGrupoToMexico=null;
		//try {
			resultadoGrupoToMexico= categoria.getIdColumnas("GRUPO");
		//} catch (excepcionElemento e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		//}
		return resultadoGrupoToMexico;    
	}

	public GestionCategoria getCategoria() {
		return categoria;
	}
	public void throwsNotify(CreateCase create) {
		Long a = System.currentTimeMillis();
	       Long b = 0L;
	       
	       
	       try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
//	             new FileOutputStream("C:/Users/OHernandez/lol.txt", true), "UTF-8"))) {
	      
	new FileOutputStream("/home/remedy/instancesToSend.txt", true), "UTF-8"))) {
		  Gson gson = new Gson();
	          long eventIdint = System.currentTimeMillis();
	        create.setEventIdint(eventIdint);
	          String input = "{\"enterpriseId\":"+"\""+create.getEnterpriseId()+"\",\"providerId\":\""+create.getProviderId()+"\",\"id\":\""+create.getEventIdint()+"\",\"country\":\""+create.getCountry()+"\",\"countryM2M\":\""+create.getCountryM2M()+"\",\"helpId\":\""+create.getEventIdint()+"\",\"eventId\":\""+create.getNewIncident()+"\",\"eventType\":"+"\"TroubleTicketCreateNotification\""+",\"externalId\":"+"null"+",\"status\":\""+"OPEN"+"\""+",\"CorrelationId\":\""+create.getCorrelationId()+"\""+"}";
						console.println(input);
	              writer.write(input);
	              writer.newLine();
		
	       } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	public void niveles(Object[] newtradeArray, JsonObject newJsonObject, List<String> repetidos,
			List<FieldRC> voFields) throws excepcionElemento {
		console.println("newtradeArray... en niveles",newtradeArray);
		
		Map<String, String> mapaListaTag=validado.getMappingError().getMapTagIdremedy();
		for (int j = 0; j < newtradeArray.length; j++) {
			JsonElement newvalue = newJsonObject.get((String) newtradeArray[j]);
			//console.println("newvalue... en niveles",newvalue);
			
			newtradeArray[j]=validado.getMappingError().extraerValorCunaAdmiracion((String) newtradeArray[j]);
			//console.println("newtradeArray[j].............",newtradeArray[j]);
			
		//	String newuserBucketPath = env.getProperty((String) newtradeArray[j]);
			String newuserBucketPath = mapaListaTag.get((String) newtradeArray[j]);

			setRepeditos(repetidos, newuserBucketPath, newvalue,
					newtradeArray[j] + ":" + newvalue.toString().replaceAll("\"", ""));
			
			agregarVoField(voFields, newuserBucketPath, newvalue);
			
		}

	}

	public void setRepeditos(List<String> lista, String userBucketPath, JsonElement value, String valor) {
		if (userBucketPath == null & !value.isJsonArray())
			lista.add(valor+"\n");
	}

//	public void agregarVoField(List<FieldRC> voFields, String userBucketPath, JsonElement value) {

public void agregarVoField(List<FieldRC> voFields, String userBucketPath, JsonElement value) throws excepcionElemento {
	String valor=value.toString().replaceAll("\"", "");
	
		if (userBucketPath != null) {
			// console.println("rrrrrrrrrrrrrrrrrrrrrrrrrrr",value.toString().toUpperCase().replaceAll("\"", ""));
			// console.println("ttttttttttttttttt",userBucketPath);
				
			 if(userBucketPath.equals("7"))
				 valor = mapStatus(userBucketPath,value.toString());
				
				voFields.add(new FieldRC(Integer.valueOf(userBucketPath), valor));
			}
		
		//if (userBucketPath != null)
			//voFields.add(new FieldRC(Integer.valueOf(userBucketPath), value.toString().replace("\"", "")));
	}

public String mapStatus(String userBucketPath,String value) throws excepcionElemento {
	String valor="1";
	
	Map<String, String> status = new HashMap<>();
    status.put("NEW", "0");
    status.put("ASSIGNED", "1");
    status.put("IN PROGRESS", "2");
    status.put("PENDING", "3");
    status.put("RESOLVED", "4");
    status.put("CLOSED", "5");
    status.put("CANCELLED", "6");
    status.put("OPEN", "1");	

    status.put("0", "1");
    status.put("1", "1");
    status.put("2", "1");
   
	//console.println("estatus para esto....",status.get(value.toUpperCase().replaceAll("\"", "")));
  
	String newvalor=status.get(value.toUpperCase().toUpperCase().replaceAll("\"", ""));
    //console.println("newvalor",newvalor);
    //console.println("value",value);
    //console.println("userBucketPath",userBucketPath);

    if(newvalor==null)
    	throw new excepcionElemento("paramentro incorrecto");
    return newvalor;


	
}

public void iterando(String voAttachment, List<FieldRC> voFields) throws excepcionElemento {
	List<String> repetidos = new ArrayList<>();
	
	JsonObject jsonObject = (new JsonParser()).parse(voAttachment).getAsJsonObject();
	jsonObject.getAsJsonArray(voAttachment);
	JsonParser parser = new JsonParser();
	JsonElement tradeElement = parser.parse(voAttachment);
	JsonObject trade = tradeElement.getAsJsonObject();

	Object[] tradeArray = trade.keySet().toArray();
	List<String> listaTag=validado.getMappingError().mapEveryIdM(validado.getMappingError().getBuscador());
    Map<String, String> mapaListaTag=validado.getMappingError().getMapTagIdremedy();
   // console.println("mapaListaTag antes de insertar en interando",mapaListaTag);
    //console.println("tradeArray.... en iterando",tradeArray);
    
	for (int i = 0; i < tradeArray.length; i++) {
		JsonElement value = trade.get((String) tradeArray[i]);
		
		tradeArray[i]=validado.getMappingError().extraerValorCunaAdmiracion((String) tradeArray[i]);;
		
		//----String userBucketPath = env.getProperty((String) tradeArray[i]);
		String userBucketPath = mapaListaTag.get((String) tradeArray[i]);
		//console.println("userBucketPath.....antes de enviar a agregarVoField.",userBucketPath);;
		
		//console.println(" parseando userBucketPath.....antes de enviar a agregarVoField.",validado.getMappingError().extraerValorCunaAdmiracion((String) tradeArray[i]));;
		agregarVoField(voFields, userBucketPath, value);
		if (userBucketPath == null) {
			setRepeditos(repetidos, userBucketPath, value,
					tradeArray[i] + ":" + value.toString().replaceAll("\"", ""));
			sizerepetidoslvl1 = repetidos.size();
			if (value.isJsonArray()) {
				
				JsonArray arrayJson=value.getAsJsonArray();
				arrayJson.forEach(valor->{
							   System.out.println("arrayJson+"+valor);
							   JsonObject newJsonObject = valor.getAsJsonObject();
							   Object[] newtradeArray = newJsonObject.keySet().toArray();
							   try {
								niveles(newtradeArray, newJsonObject, repetidos, voFields);
							} catch (excepcionElemento e) {
								isException=true;
							}
								sizerepetidoslvl2 = repetidos.size();
				
				  });
			if(isException)
				 throw new excepcionElemento("paramentro incorrecto");
				
//				JsonObject newJsonObject = value.getAsJsonArray().get(0).getAsJsonObject();
//				Object[] newtradeArray = newJsonObject.keySet().toArray();
//			//	console.println("newtradeArray.....antes de enviar a niveles.",newtradeArray);;
//				
//				niveles(newtradeArray, newJsonObject, repetidos, voFields);
//				sizerepetidoslvl2 = repetidos.size();

			}
		}

	}
	this.repetidos = repetidos.toString();

}
public void iterando(JsonObject voAttachment, List<FieldRC> voFields) throws excepcionElemento {
	List<String> repetidos = new ArrayList<>();
		List<String> listaTag=validado.getMappingError().mapEveryIdM(validado.getMappingError().getBuscador());
    Map<String, String> mapaListaTag=validado.getMappingError().getMapTagIdremedy();

    voAttachment.entrySet().stream().forEach(item->{ try {
		funcion( voAttachment,item.getKey(),item.getValue(), voFields , repetidos,  mapaListaTag);
	} catch (excepcionElemento e) {
		isException=true;
	}});
    
    if(isException)
       throw new excepcionElemento("paramentro incorrecto");
    
    System.out.println("repetidos..."+repetidos);

	this.repetidos = repetidos.toString();

}


public JsonObject funcion(JsonObject trade,String key,JsonElement valor, List<FieldRC> voFields ,List<String> repetidos, Map<String, String> mapaListaTag) throws excepcionElemento{
    //trade.add(validado.getMappingError().extraerValorCunaAdmiracion(key),valor);
	String userBucketPath =mapaListaTag.get(key);
	agregarVoField(voFields, userBucketPath,valor);
	if (userBucketPath == null) {
		setRepeditos(repetidos, userBucketPath, valor,
				key + ":" + valor.toString().replaceAll("\"", ""));
		sizerepetidoslvl1 = repetidos.size();
		if (valor.isJsonArray()) {
			JsonArray arrayJson=valor.getAsJsonArray();
				arrayJson.forEach(item->{
							   System.out.println("arrayJson+"+item);
							   JsonObject newJsonObject = item.getAsJsonObject();
							   Object[] newtradeArray = newJsonObject.keySet().toArray();
							   try {
								niveles(newtradeArray, newJsonObject, repetidos, voFields);
							} catch (excepcionElemento e) {
								isException=true;
							}
								sizerepetidoslvl2 = repetidos.size();
				
				  });
			if(isException)
				 throw new excepcionElemento("paramentro incorrecto");
				

		}
	}
	return trade;

}

	public InsertVO addAttachmentWL(String repetidos, String id, String usuarioRemedy, String tipo) throws ARException, IOException {
		//		voInsert.fncRmdInsert(voAttachmentService.fncAttachmentInsertWL(repetidos, id)).getResultado().getNuevo();
		
		return voInsert.fncRmdInsert(voAttachmentService.fncAttachmentInsertWL(repetidos, id,usuarioRemedy, tipo));

	}

	public String queryRealInc(String id) throws ARException {
		RemedyControlResultado cosa = new RemedyControlResultado();
		cosa.setSistema(usuarioRemedy);

		cosa.setFormulario(formulario.getCrear());
		//cosa.setFormulario("HPD:Help Desk");
		cosa.setColumnas("1000000161 1");
		cosa.setCondiciones("'1' LIKE \"" + id + "\"");

		console.println("el id es:" + id);

		String queyrealInc = voSelect.fncRmdSelect(cosa).getResultado().get(0).getField().get(1).getValue();
		String queyrealInc2 = voSelect.fncRmdSelect(cosa).getResultado().get(0).getField().get(0).getValue();
		
		console.println("1***********************"+queyrealInc);
		console.println("161�***************"+queyrealInc2);
		
		console.println("el id es algo mas aqui:" + queyrealInc);
		console.println("aqui el incidente generado" + queyrealInc);
		return queyrealInc;
	}

	
	
}
