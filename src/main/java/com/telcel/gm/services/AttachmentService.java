	package com.telcel.gm.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.activation.MimetypesFileTypeMap;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.bmc.arsys.api.ARException;
import com.telcel.gm.component.console;
import com.telcel.gm.match.GestionCampoPais;
import com.telcel.gm.match.Validador;
import com.telcel.gm.match.creaObjeto;

import org.apache.tika.Tika;
import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;
import org.json.JSONException;
import org.springframework.stereotype.Service;

import com.telcel.gm.model.ResposeGM;
import com.telcel.gm.model.TroubleTicketAttachment;
import com.telcel.gm.respuesta.Respuesta;
import com.telcel.remedy.ars.itsm.RmdInsert;
import com.telcel.remedy.ars.model.FieldRC;
import com.telcel.remedy.ars.model.InsertVO;
import com.telcel.remedy.ars.model.RemedyControlEntry;
import com.telcel.remedy.controllers.DataSetController;
import com.telcel.remedy.model.constantes.ConstantesRemedy;
import com.telcel.remedy.model.entity.ArsPaisCompania;

@Service
public class AttachmentService {

	
	
	public ResponseEntity<ResposeGM> startAttachment(@RequestBody @Valid TroubleTicketAttachment voAttachment
			, @RequestHeader Map<String,String> headers
			, BindingResult result,Environment env,DataSetController dataController,
			RmdInsert voInsert) {
		
		//String vsUser = headers.get("Authorization").stream().findFirst().orElse("");
	GestionCampoPais campoPais=new GestionCampoPais();
	headers=campoPais.changeHeaderToCloud(headers);
	
		Respuesta respuesta=new Respuesta();
		String enterpriseId=headers.get("enterpriseid");
		String correlationId=headers.get("correlationid");
		String providerId=headers.get("providerid");
		String country=headers.get("country");
		String countryM2M=headers.get("countrym2m");
		
		//String vsUser = "TEMIP";
		
		console.println("voAttachment "+voAttachment.toCadena());
		console.println("getDescription"+voAttachment.getDescription());
		  if(voAttachment.getDescription()==null) {
		   voAttachment.setDescription(voAttachment.getName());
		   
		  }
		  
		ResposeGM voattach = new ResposeGM();
		creaObjeto creaObjeto=new creaObjeto();


		HttpHeaders responseHeaders = new HttpHeaders();
		Validador validado = new Validador(env,dataController);
		validado.getMappingError().error();
		GestionCampoPais gestioncampo=new GestionCampoPais();
		List<ArsPaisCompania> clients=	validado.getDataSetController().fncBusquedaFiltroCompaniaAndCountry(enterpriseId, gestioncampo.getToCountryM2MCountry(countryM2M));
		String vsUser = clients.get(0).getArsClient().getArcId();
	
		validado.getMappingError().bucadorTags(validado,"addAtachment","MEXICO","GM_MX");
		validado.setTipoServicio("A");
		Integer tama�o= voAttachment.getSize();
		String objetito=creaObjeto.toString(voAttachment);
		List<String> validarFound = validado.notfound(objetito);
		console.println("validarFound",validarFound);
		List<String> validarHeader = validado.validarCabezera(headers);
    if(validarHeader.size()>0) {
      voattach.setCode("ATTA002");
      voattach.setReason(validarHeader +" is required ");
          return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
          .body(voattach);
      
    }
    
    if (voAttachment.getTroubleTicketId().length() < 15) {
      voattach.setCode("ATTA001");
      voattach.setReason(voAttachment.getTroubleTicketId() + " is/are not supported ");
          return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
          .body(voattach);
		}
		if (tama�o > 5242880 || tama�o==0) {
      voattach.setCode("ATTA003");
      voattach.setReason("Downstream System Error: "+ tama�o + " is/are not supported ");
    
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
          .body(voattach);
    }
    
    if ( voAttachment.getTroubleTicketId().length()> 15) {
      voattach.setCode("ATTA004");
      voattach.setReason("Incident number "+ voAttachment.getTroubleTicketId() +" is/are not found ");
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
          .body(voattach);
    }    
    
    if(correlationId.length()<3) {
      voattach.setCode("ATTA001");
      voattach.setReason(correlationId + " is  no supported ");
      // respuesta.setMessage(viInsert.getResultado().getError());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
          .body(voattach);
    }
		if(countryM2M.length()!=2) {
      voattach.setCode("ATTA001");
      voattach.setReason(countryM2M + " is  no supported ");
      // respuesta.setMessage(viInsert.getResultado().getError());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
          .body(voattach);
		}
		if(enterpriseId.length()!=12) {
			voattach.setCode("ATTA001");
			voattach.setReason(enterpriseId + " is  no supported ");
			// respuesta.setMessage(viInsert.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(voattach);
		}
	

		if(country.length()!=3) {
			voattach.setCode("ATTA001");
			voattach.setReason(country + " is  no supported ");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(voattach);
		}
		
			if (!voAttachment.getTroubleTicketId().matches("INC"+"\\d{12}") ) {	 
			voattach.setCode("ATT001");
			voattach.setReason("id is no supported ");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(voattach);
		}
		if (validarFound.size() >= 1) {
			respuesta.setCode("CTT004");
			respuesta.setReason(validarFound + " is/are not found ");
			voattach.setReason(" is/are not found ");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(voattach);
		}
		List<String> requiredmap = validado.requered(objetito);
		if (requiredmap.size() >= 1) {
			respuesta.setCode("CTT002");
			respuesta.setReason(requiredmap + " is/are  required ");
			voattach.setReason(" is/are not found ");
			
			// respuesta.setMessage(viInsert.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(voattach);
		}
		
		String idLocal = headers.get("idLocal"); //Sin esto no puedo saber si viene de local o no.
		String tipo="";
		
		console.println("idLocal: "+idLocal); // Para saber si entra a idLocal = true o a else
		if(idLocal==null){
			tipo = ConstantesRemedy.tipoCloudToLocal;
		} else if(idLocal.compareTo("true")==0) {
			tipo = ConstantesRemedy.tipoLocalToCloud;
		} else {
			console.println("Error en el idLocal: "+idLocal);
		}
		
		console.println("tipo: "+tipo);
		
		ResponseEntity<ResposeGM> voResponse = null;
		
		InsertVO voResult=null;
		try {
			voResult = voInsert.fncRmdInsert(
					fncAttachmentInsert(
							voAttachment, vsUser, tipo));
		} catch (ARException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		}
		
		if (result.hasErrors()) {
			  return null;
		 }

		if (voResult.getResultado().getError() == null) {
//			voResponse = ResponseEntity.status(HttpStatus.ACCEPTED)
//					.header("correlationId", voResult.getResultado().getNuevo()).body(null);	
//							
//					String bodyJson = "{\"eventType\":"+"\"AddTroubletTicketAttachment\""+",\"statusNotify\":\""+"ATTACHED"+"\""+",\"eventId\":\""+voAttachment.getTroubleTicketId()+"\""+"}";
//																					
//										
//					console.println("bodyjson  sds"+bodyJson); 
//			try {
//						notifyService(bodyJson, headers);
//					} catch (IOException | JSONException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//						
			voResponse = ResponseEntity.status(HttpStatus.ACCEPTED)
			.header("correlationId", voResult.getResultado().getNuevo()).body(null);	
		
			voAttachment.setIdWorklog( voResult.getResultado().getNuevo());
			return voResponse;

			
		} else {
			voattach.setCode("400");
			console.println("ff", voResult);
			voattach.setReason(voResult.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(voattach);
		}

	}
	
	
	public InsertVO fncAttachmentInsert(
			TroubleTicketAttachment voAttachment
			, String psUser
			, String tipo
				) {
		
		InsertVO viInsert = new InsertVO();
		RemedyControlEntry voEntry = new RemedyControlEntry();
		console.println("~~~~~~~~~~~~~~~~~~~~~~~~~" );
		MimeTypes allTypes = MimeTypes.getDefaultMimeTypes();
		
		MimeType jpeg=null;
		String voExtencion="";
		try {
			jpeg = allTypes.forName(MediaType.parse(voAttachment.getMimeType()).toString());
			 voExtencion = jpeg.getExtension()	;
				String jpegExt2 = jpeg.getExtension(); // .jpg
			
		} catch (MimeTypeException |NullPointerException r) {
			voExtencion=voAttachment.getMimeType();
			if(!voExtencion.contains("."))
				voExtencion="."+voExtencion;
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		
//		String voExtencion = MediaType.parse(voAttachment.getMimeType()).getSubtype();
		
		List<FieldRC> voFields = new ArrayList<>();
		voFields.add(new FieldRC(1000000151, voAttachment.getDescription()));
		voFields.add(new FieldRC(1000001476, "0"));
		voFields.add(new FieldRC(1000000761, "1"));
		voFields.add(new FieldRC(1000000655, "8000"));//System Assignment
		voFields.add(new FieldRC(1000000492, voAttachment.getTroubleTicketId()));
			voFields.add(new FieldRC(1000000170, tipo));
		voFields.add(new FieldRC(1000000161, voAttachment.getTroubleTicketId()));
		voFields.add(new FieldRC(1000000000, "ACTUALIZACION SERVICIOS AMX"));//CAMPO ADICIONAL PARA CHILE
//		voFields.add(new FieldRC(1000000000, voAttachment.getDescription()));//CAMPO ADICIONAL PARA CHILE
		voFields.add(new FieldRC(1000000351,
				"<FILE>" + voAttachment.getName()  + voExtencion + "~" + voAttachment.getFile() + "~" + voAttachment.getSize() + "</FILE>"));
		voEntry.setField(voFields);
		viInsert.setSistema(psUser);
		viInsert.setFormulario("HPD:WorkLog");
		viInsert.setColumnas(voEntry);
		return viInsert;
	}

	public InsertVO fncAttachmentInsertWL(
			String repetidos
			, String id
			, String usuarioRemedy
			, String tipo
	){
		InsertVO viInsert = new InsertVO();
		RemedyControlEntry voEntry = new RemedyControlEntry();
		List<FieldRC> voFields = new ArrayList<>();
		voFields.add(new FieldRC(1000000151, repetidos));
		console.println("Entra solo a notas en fncattatchment" );
		voFields.add(new FieldRC(1000001476, "0"));
		voFields.add(new FieldRC(1000000761, "1"));
		voFields.add(new FieldRC(1000000655, "7000"));
		voFields.add(new FieldRC(1000000492, id));
//		voFields.add(new FieldRC(1000000170, "2000")); // el valor 2000 debe ser una variable
		voFields.add(new FieldRC(1000000170, tipo)); // el valor 2000 debe ser una variable
		voFields.add(new FieldRC(1000000000,"ACTUALIZACION SERVICIOS AMX"));//CAMPO ADICIONAL PARA CHILE
		
		voFields.add(new FieldRC(1000000161, id));
		voEntry.setField(voFields);
		viInsert.setSistema(usuarioRemedy);
		viInsert.setFormulario("HPD:WorkLog");
		viInsert.setColumnas(voEntry);
		//console.println("viInsert..........",viInsert);
		//console.println("voEntry..........",voEntry);
		return viInsert;
	}
}
