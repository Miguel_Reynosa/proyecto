package com.telcel.gm.match;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonObject;
import com.telcel.gm.component.console;
import com.telcel.remedy.pruebasCodigo;
import com.telcel.remedy.controllers.DataSetController;
import com.telcel.remedy.model.entity.ArsCategoria;
import com.telcel.remedy.model.entity.ArsUrgency;

public class GestionCategoria {

	DataSetController dataController;
	
	    List<Object> numeros;
		List<Object> motivos;
		List<Object> significados;
		List<Object> gruposIdRemedy;
		List<Object> IdRemedy;
		List<Object> valoresConcatenadosSinSgp;
		List<Object> valoresConcatenadosConSgp;
		List<ArsCategoria> filtrado;
		
		private Boolean creacionLocal;

	public GestionCategoria(DataSetController dataController) {
		this.dataController=dataController; 
		this.numeros=new ArrayList<>();
		this.motivos=new ArrayList<>();
		this.significados=new ArrayList<>();
		this.gruposIdRemedy=new ArrayList<>();
		this.IdRemedy=new ArrayList<>();

		//algo();
	}

public void setEmpty() {
	this.numeros.clear();
	this.motivos.clear();
	this.significados.clear();
	this.gruposIdRemedy.clear();
	this.IdRemedy.clear();
}
	public void consultadorCampo(String datosAFiltrar,String country) {
		List<ArsCategoria> listita;//=new ArrayList<>();
				
		String valores="123;124;444;124";
		
		//console.println("controladores",dataController.getCategoria());
		listita=dataController.getCategoria();
		//listaUrgency=dataController.getUrgencys();
		
		//filtrado= filtrarByNumero(listita,"285;8194;75768;75802");
		console.printlnInfo("todas las lista de categoria",listita);
		console.println("categoria a filtrar",datosAFiltrar);
		console.println("pais a filtrar",country);
		//filtrado=null;
		 filtrado= filtrarByNumero(listita,datosAFiltrar,country);
		 
		 
		 if(filtrado.size()>1)
				filtrado.remove(1);
			
		 if(filtrado.size()==0)
 	       filtrado=filtrarByNumeroIgnoreFiveReasonByCountry(filtrarByNumeroIgnoreFiveReason(listita,datosAFiltrar),country);
		    
 	       console.println("lista de categoria ya filtrado",filtrado);
			//console.println("id de remedy fffffffffff en antes del forEach campo....................",IdRemedy);	
			
			filtrado.forEach(item->{
	   		numeros.addAll(Arrays.asList(item.getId().split(";")));
			motivos.addAll(Arrays.asList(item.getMotivo().getMotivos().split(";")));
	   		significados.addAll(Arrays.asList(item.getNotas().split(";")));
	   		gruposIdRemedy.addAll(Arrays.asList(item.getGrupo().getId_remedy().split(";")	));
	   		
	   		IdRemedy.addAll(Arrays.asList(item.getId_remedy().split(";")	));
	   		//console.println("id remedy en consultadorCampo ",Arrays.asList(item.getIdRemedy().split(";")	));
		   	
	   		valoresConcatenadosSinSgp=item.getGrupo().getConcatenarValoresSinSgp();
	   		creacionLocal=(item.getEscala().toUpperCase().equals("SI"));
			//console.println("valoresConcatenadosConSgp....................",valoresConcatenadosConSgp);	
			//console.println("id de remedy en metodo campo....................",valoresConcatenadosSinSgp);	
			
			});
			
			//  Stream<ArsCategoria> f= listita.stream().filter(item->(item.getId().equals("285;8194;75768;75802")));
	   

		
	}
	public Boolean isLocal() {
		return creacionLocal;
	}
	
	public Map<String, String> getIdColumnas(String tipoObjeto) throws excepcionElemento {
		console.println("***********CONSULTA DE LOS CAMPOS DE CATEGORIA*******************");
		Map<String, String> resultado=null;
		if(filtrado.size()==0)
			 throw new excepcionElemento("ticketType is  not found");
		
		switch (tipoObjeto) {
		
		case "COLUMNAS":			
			resultado=convertidorDeValores(IdRemedy,motivos,tipoObjeto);
			//console.println("******************************BBBBBBBBBBBBBBBBB************************",resultado);
			break;
		case "GRUPO":
			List<Object> valores=valoresConcatenadosSinSgp;
			//console.println("******rrrr**tttt**********",valoresConcatenadosSinSgp);
			//console.println("******rrrr***************",filtrado.get(0).getGrupo().getId_sgp());
			if(filtrado.get(0).getGrupo().getId_sgp()!=null)
				valores=valoresConcatenadosConSgp;
			// console.println("valor de la variable valores...",valores);
			resultado=convertidorDeValores(gruposIdRemedy,valores,tipoObjeto);
				  break;
		
		default:
			break;
		}
		return resultado;
		}
	
	
	public Map<String,String> convertidorDeValores(List<Object> objetoListId,List<Object> objetoListaValor,String tipoObjeto) throws excepcionElemento {
		Map<String,String> mapColumnas= new HashMap<>();
		 if(objetoListaValor.size()==0)
			 throw new excepcionElemento("ticketType is  not found");
		 for (int i = 0; i < objetoListaValor.size(); i++) {
			  //mapColumnas.put(objetoListId.get(objetoListaValor.indexOf(item)).toString(),item.toString());
			  mapColumnas.put(objetoListId.get(i).toString(),objetoListaValor.get(i).toString());
		//	  console.println("mapColumnas para esto...",mapColumnas);

		}
		 

		if(tipoObjeto.equals("COLUMNAS")) {
			mapColumnas.put(objetoListId.get(objetoListId.size()-1).toString(),filtrado.get(0).getResumen() );
			mapColumnas.put(objetoListId.get(objetoListId.size()-2).toString(), filtrado.get(0).getNotas() );
						
		}
		//console.println("algoe",mapColumnas);
		return mapColumnas;
		
	}
	
	
	public List<ArsUrgency> recorrddddde2() {
		   //public List<ArsUrgency> recorre2(JsonObject cuerpoPrincipal) {
	       //public List<ArsUrgency> recorre2(DataSetController dataSetc,JsonObject cuerpoPrincipal) {
		   List<ArsUrgency> list =dataController.fncBusquedaUrgency();
		   String country="BRA";
		   final String valor="MINOR";
			// final String valor2="SEVERITY;CRITICAL";
		   String valor4="CRITICAL";
		   String[] valo=valor.split(";");
		   //String[] valo2=valor2.split(";");
		   String compania="GM_CLARO_BRA";
		 	   //String val="MINOR";
		  
		    Map<String,String> mapa=new LinkedHashMap<>();
			mapa.put("name","nome");
			mapa.put("externalId","5001b000005C1CFAA0");
			mapa.put("ticketType","150;8199;8653;75776");
			mapa.put("creationDate","2018-10-04T18:20:19.000Z");
			mapa.put("severity","Critical");
			mapa.put("priority","Critical");
			mapa.put("sla","2009-01-01T12:03:04-03:00");
			mapa.put("status","Opened");
			mapa.put("type","networkTicket");
			mapa.put("baseType","TroubleTicket");
			
			//List<ArsUrgency> listaFiltrado=getUrgency(list,valor,country);
		    //List<ArsUrgency> listFiltra=filtrado(listaFiltrado, compania, valor, valor);
		    //List<ArsUrgency> listFiltra=filtrado(list,country,compania,mapa.get("severity"));
			//List<ArsUrgency> listFiltra2=filtrado(list,country,compania,mapa.get("priority"));
		
			List<ArsUrgency> listFiltra=filtroByUrgency(list,country,compania,"",valor4);
			List<ArsUrgency> listFiltra2=filtroByUrgency(list,country,compania,"",valor4);
			
			//console.println("fffww",listFiltra);
			String cambio=String.valueOf(listFiltra.get(0).getNumero()); 
			String cambio2=String.valueOf(listFiltra2.get(0).getNumero()); 
			
		//	mapa.replace("severity",cambio);
		//	 mapa.replace("priority",cambio2);
			// System.out.println("---------------------------------------------");
			// System.out.println(mapa);
		
		//	mapa.values().stream().forEach(miArray->Arrays.asList(miArray).stream().forEach(System.out::println));				
	   
			//	 String cambio=String.valueOf(listaFiltrado.get(0).getNumero());
	 		console.println("imprimiento lista filtrado",listFiltra); 
	 		//System.out.println("------------------------------------------------------------------");
	 		// console.println("El mensaje  fue  filtrado",listaFiltrado);
	 		System.out.println("-----------------------------------------------------------");
	 		//List<ArsUrgency> listFiltrado=filtrado(list, country, compania,valor4);
	 		List<ArsUrgency> listFiltrado;
		    listFiltrado=filtroByUrgency(list, country, compania,"",valor4);
			console.println("filtracion y mapeador",listFiltrado);
				return listFiltrado; 			
		}
	
	public ObjectNode gestionSeverityPriority(JsonNode cuerpoPrincipal,String compania,String country) {
			List<ArsUrgency> list =dataController.fncBusquedaUrgency();

			final String valor="MINOR";
			String valor4="CRITICAL";
			String[] valo=valor.split(";");
			
			int numero=2000;
		//	console.println("cuerpo principal convertido en jsonNode...",cuerpoPrincipal);
			ObjectNode mapa=(ObjectNode) cuerpoPrincipal;
			
			List<ArsUrgency> toNumber=filtroByUrgency(list,country,compania,"severity",mapa.get("severity").toString());
			List<ArsUrgency> toNumber2=filtroByUrgency(list,country,compania,"priority",mapa.get("priority").toString());// filtrado del cuerpo al codigo numerico
//			List<ArsUrgency> toNumber=filtroByUrgency(list,"COL","GM_CLARO_COL","severity","CRITICAL");
//			List<ArsUrgency> toNumber2=filtroByUrgency(list,"COL","GM_CLARO_COL","priority","CRITICAL");// filtrado del cuerpo al codigo numerico
			console.println("toNumber...",toNumber);
			console.println("toNumber2.......",toNumber2);
			List<ArsUrgency> listFiltra=filtroByNumberUrgency(list,country,compania,toNumber.get(0).getNumero());
			List<ArsUrgency> listFiltra2=filtroByNumberUrgency(list,country,compania,toNumber2.get(0).getNumero());// logica de numerico a cuerpo
			
			String NumberToString=String.valueOf(toNumber.get(0).getNumero()); 
			String NumberToString2=String.valueOf(toNumber2.get(0).getNumero()); 
			
			String cambio=String.valueOf(listFiltra.get(0).getNumero()); 
			String cambio2=String.valueOf(listFiltra2.get(0).getNumero()); 
			
			mapa.put("severity", NumberToString);
			mapa.put("priority", NumberToString2);

			//mapa.replace("severity",cambio);
			//mapa.replace("priority",cambio2);
			console.println("********** Severity y Priority mapeado ****************");
			console.println("severity",cambio);
			console.println("priority",cambio2);
			//Aqui lo estoy definiendo el mapa
		//	console.println("El valor de mapa,por numero: "+mapa);
			//System.out.println("+++++++++++++++++++++++++++-");
			List<ArsUrgency> listFiltrado=filtroByNumberUrgency(list, country, compania,numero);
			//console.println("xxxxxxxxxxxxxxxxxxxx listFiltrado "+listFiltrado);		

			//console.println("xxxxxxxxxxxxxxxxxxxx mapa final "+mapa);	
				return mapa; 			
		}
	
	public List<ArsUrgency> filtroByUrgency(List<ArsUrgency> listaFiltrado,String country,String compania,String urgency,String valorUrgency) {	
	
		 
//		console.println("ijewiofjweoifjweoifjewo", listaFiltrado.stream().filter(item->(item.getPaisCompania().getArsPais().getPais().equals(country))&
//				//.collect(Collectors.toList());
//			   (item.getPaisCompania().getArsCompania().getCompania().equals(compania))&		
//			  	 (item.getValor().split(";")[1].toUpperCase().equals(valorUrgency.toUpperCase().replaceAll("\"", "")))&
//			  	 (item.getValor().split(";")[0].toUpperCase().equals(urgency.toUpperCase().replaceAll("\"", "")))
//			   ).collect(Collectors.toList()));
//		
		return listaFiltrado.stream().filter(item->(item.getPaisCompania().getArsPais().getPais().equals(country))&
				//.collect(Collectors.toList());
			   (item.getPaisCompania().getArsCompania().getCompania().equals(compania))&		
			   (item.getValor().split(";")[1].toUpperCase().equals(valorUrgency.toUpperCase().replaceAll("\"", "")))&
			   (item.getValor().split(";")[0].toUpperCase().equals(urgency.toUpperCase().replaceAll("\"", "")))
			   )
				
			 .collect(Collectors.toList());
	}


	//Metodo de filtrado2, por numero 
	public List<ArsUrgency> filtroByNumberUrgency(List<ArsUrgency> listaFiltrado,String country,String compania,int numero) {		
		return listaFiltrado.stream().filter(item->(item.getPaisCompania().getArsPais().getPais().equals(country))&
				//.collect(Collectors.toList());
			   (item.getPaisCompania().getArsCompania().getCompania().equals(compania))&		
		//  (item.getValor().split(";")[1].toUpperCase().equals(x)&&item.getNumero()==numero))	 		
		   (item.getNumero()==numero))
				.collect(Collectors.toList());
		}
	
	
	public List<ArsCategoria> filtrarByNumero(List<ArsCategoria> listita,String cadenaNumerico,String country) {
		  		
		return null;
//		return listita.stream().filter(item->(item.getId_cat().trim().equals(cadenaNumerico.replaceAll("\"", ""))&
//									 (item.getIdPaisCompania().getArsPais().getPais().equals(country))))
//							 .collect(Collectors.toList());
    }
	
	public List<ArsCategoria> filtrarByNumeroIgnoreFiveReasonByCountry(List<ArsCategoria> listita,String country) {
		  		
		
		return listita.stream().filter(item->item.getPaisCompania().getArsPais().getPais().equalsIgnoreCase(country))
							 .collect(Collectors.toList());
    }


	public List<ArsCategoria> filtrarByNumeroIgnoreFiveReason(List<ArsCategoria> listaCategoria,String cadenaNumerico) {
 return null;			
//		return listaCategoria.stream().filter(item-> cadenaNumerico.contains(item.getId_cat())).collect(Collectors.toList());
	}

}
