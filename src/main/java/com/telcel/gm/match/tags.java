package com.telcel.gm.match;

import java.util.ArrayList;

public class tags {
	private String nombreTags;
	private String id_remedy;
	private String requerido;
	private String tipo;

	private String formulario;
	private String valorDefault;
	private ArrayList<String> tagsValidos;
	
	
	public  tags(String tag) {
		this.nombreTags=tag;
		
	}
	public tags() {
		
	}
	public  tags(String tag,String id_remedy,String formulario,String requerido,String tipo,String valorDefault) {
		this.nombreTags=tag;
		this.id_remedy=id_remedy;
		this.requerido=requerido;
		this.formulario=formulario;
		this.tipo=tipo;
		this.valorDefault=valorDefault;
	}
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getNombreTags() {
		return nombreTags;
	}
	public void setNombreTags(String nombreTags) {
		this.nombreTags = nombreTags;
	}
	public ArrayList<String> getTagsValidos() {
		return tagsValidos;
	}
	public void setTagsValidos(ArrayList<String> tagsValidos) {
		this.tagsValidos = tagsValidos;
	}
	
	public boolean esValido(tags objeto) {
		return true;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return nombreTags.replaceAll("\"", "");
	}
	public String getId_remedy() {
		return id_remedy;
	}
	public void setId_remedy(String id_remedy) {
		this.id_remedy = id_remedy;
	}
	public String getRequerido() {
		return requerido;
	}
	public void setRequerido(String requerido) {
		this.requerido = requerido;
	}
	public String getFormulario() {
		return formulario;
	}
	public void setFormulario(String formulario) {
		this.formulario = formulario;
	}
	public String getValorDefault() {
		return valorDefault;
	}
	public void setValorDefault(String valorDefault) {
		this.valorDefault = valorDefault;
	}
	
	

	

}
