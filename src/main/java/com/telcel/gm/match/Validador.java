package com.telcel.gm.match;

//import static org.hamcrest.CoreMatchers.both;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//import org.assertj.core.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.bmc.thirdparty.org.apache.commons.lang.ArrayUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.telcel.gm.component.ConsumirDao;
import com.telcel.gm.component.console;
import com.telcel.gm.respuesta.MappingError;
import com.telcel.remedy.ars.model.FieldRC;
import com.telcel.remedy.controllers.DataSetController;
import com.telcel.remedy.model.entity.ArsClientTag;
import com.telcel.remedy.model.entity.ArsTag;
import com.telcel.remedy.model.services.IArsClientTagService;

//@Autowired
//@Component
//@Bean
public class Validador {

	public Environment env;

	Map<ids, String> objeto;

	// @Autowired
	private ConsumirDao dao;
	DataSetController dataSetController;
	private MappingError mappingError;
	private String tipoServicio;

	// @Autowired
	public Validador(Environment env, DataSetController dataSetController) {
		this.env = env;
		this.dataSetController = dataSetController;
		mappingError = new MappingError();
		tipoServicio = "";
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public String getTipoServicio() {
		return this.tipoServicio;
	}

	public DataSetController getDataSetController() {
		return dataSetController;
	}

	public Environment getEnv() {
		return env;
	}

	public MappingError getMappingError() {
		return mappingError;
	}

	public void setMappingError(MappingError mappingError) {
		this.mappingError = mappingError;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

	JsonObject cuerpo;

	public JsonObject getCuerpo(String voAttachment) {
		Object[] newtradeArray = null;
		List<String> repetidos = new ArrayList<>();
		;
		JsonObject jsonObject = (new JsonParser()).parse(voAttachment).getAsJsonObject();
		return jsonObject;
		// return cuerpo;
	}

	public List<Object> getBodyrequestToList(String voAttachment) {
		Object[] newtradeArray = null;
		System.out.println("voAttachment update "+voAttachment);
		JsonObject jsonObject = (new JsonParser()).parse(voAttachment).getAsJsonObject();
		jsonObject.getAsJsonArray(voAttachment);
		JsonParser parser = new JsonParser();
		JsonElement tradeElement = parser.parse(voAttachment);
	    JsonObject trade = tradeElement.getAsJsonObject();
			
		Object[] tradeArray = trade.keySet().toArray();
		List<Object> toList=null;
		for(int i = 0;i<tradeArray.length;i++) {		
			JsonElement value = trade.get((String) tradeArray[i]);
				if(value.isJsonArray()) {
					JsonObject newJsonObject = value.getAsJsonArray().get(0).getAsJsonObject();
				newtradeArray = newJsonObject.keySet().toArray();
				}
			   Object[] both =  ArrayUtils.addAll(tradeArray, newtradeArray);
			     toList=Arrays.asList(both);
		}
		return toList;
}
//	public List<Object> recorridottttt(String voAttachment) {
//		Map mapError = null;
//		Object[] newtradeArray = null;
//		List<String> repetidos = new ArrayList<>();
//		;
//
//		JsonObject jsonObject = (new JsonParser()).parse(voAttachment).getAsJsonObject();
//		jsonObject.getAsJsonArray(voAttachment);
//
//		JsonParser parser = new JsonParser();
//		JsonElement tradeElement = parser.parse(voAttachment);
//		JsonObject trade = tradeElement.getAsJsonObject();
//
//		Object[] tradeArray = trade.keySet().toArray();
//		List<Object> toList = null;
//		for (int i = 0; i < tradeArray.length; i++) {
//			JsonElement value = trade.get((String) tradeArray[i]);
//			String userBucketPath = env.getProperty((String) tradeArray[i]);
//			if (userBucketPath == null) {
//				if (value.isJsonArray()) {
//					JsonObject newJsonObject = value.getAsJsonArray().get(0).getAsJsonObject();
//					newtradeArray = newJsonObject.keySet().toArray();
//				}
//			}
//			List<String> someList = new ArrayList<String>();
//			Object[] both = ArrayUtils.addAll(tradeArray, newtradeArray);
//
//			toList = Arrays.asList(both);
//		}
//		return toList;
//	}
//	
	
	public List<String> notfound(String voAttachment) {
		 List<Object> toList= getBodyrequestToList(voAttachment);
		 
	     List<String> lista=mappingError.mapEveryIdM(mappingError.getBuscador());
		 List<String> nofound=new ArrayList<String>();
		 lista.add("CHARACTERISTIC");//ESTO DOS SE AGREGO POR EL MOMENTO TAMBIEN PARA VALIDAR BIEN TODOS ESTOS CAMBPOS
	     lista.add("VALIDFOR");
	     console.println("toList en notfoud,",toList);
	     console.println("lista en notfoud",lista);
        toList.forEach(llave->{
		   	  if(!lista.contains(llave.toString().toUpperCase()))
		 		  nofound.add(llave.toString());
		    	  		      });
		      return nofound;
		
		
	}

	List<Object> toList = null;

	public List<String> requered(String voAttachment) {
		  List<Object> toList= getBodyrequestToList(voAttachment);
		  Map<String,String> requiredMap= mappingError.getMapRequired();
		  List<String> required=new ArrayList<String>();
		    
		    requiredMap.forEach((llave,valor)->{
		    		if(!toList.contains(llave)) {
		    		 if(valor.indexOf(getTipoServicio())>=0) {
		    			 required.add(llave);
		    	 }
		    	
		    }  
		    });
		return required;

	}

	public List<String> noSupport(String voAttachment)
	{
		System.out.println("\nVALIDANDO 'NOT SUPPORTED VALUES'");
		
		List<Object> toList = getBodyrequestToList(voAttachment);
		
		MappingError map = new MappingError();
		map.error();
		
		List<String>		lista		= map.mapEveryIdM(map.getBuscador());
		Map<String,String>	noSupport	= map.getNoSupport();
		
		List<String> noSuport=new ArrayList<String>();
		
		noSupport.forEach((llave,valor)-> {
			if (toList.contains(llave))
			{
				String post = "";
				
				System.out.println("llave:"+llave);
				
				if ((toList.indexOf(llave)) > 0)
					post = toList.get((toList.indexOf(llave))).toString();
				
				System.out.println("valor:"+valor);
				if (valor.equals("S"))
				{
					if (StringToInteger(post))
					{
						noSuport.add(post);
						System.out.println("post:"+post);
					}
				}
				else if (valor.equals("I"))
				{
					if (StringToInteger(post))
						noSuport.add(post);
				}
			}
		});
		
		System.out.println("FIN DE LAS VALIDACIONES.");
		return noSuport;
	}

	public boolean StringToInteger(String valor) {
		try {
			Integer.parseInt(valor);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public Validador(Map<ids, String> objeto, Environment env) {
		this.objeto = objeto;
		this.env = env;
	}

	public Map<String, String> validarNotas() {
		Map<String, String> nuevo = new HashMap<>();

		String notas[] = env.getProperty("subNotas").split(",");
		List<Object> notita = Arrays.asList(env.getProperty("subNotas").split(","));

//	    console.println( objeto.keySet());
		// console.println("notita",notita);

		objeto.keySet().forEach(item -> {
			String id = item.getId();
			if (notita.contains(id))
				nuevo.put(id, (String) notita.get(notita.indexOf(id)));

		});

		return nuevo;
	}

	public Map<ids, Object> validarStatusChange() {

		JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
		ObjectNode notification = nodeFactory.objectNode();
		ObjectNode appsObj = nodeFactory.objectNode();
		ObjectNode target = nodeFactory.objectNode();

		ArrayNode apps = nodeFactory.arrayNode();
		ArrayNode platforms = nodeFactory.arrayNode();
		apps.addPOJO(objeto);
		apps.add(appsObj);

		Map<ids, Object> nuevo = new HashMap<>();

		String notas[] = env.getProperty("subStatusChange").split(",");
		List<Object> notita = Arrays.asList(env.getProperty("subStatusChange").split(","));
		ObjectNode pushContent = nodeFactory.objectNode();
		ArrayNode array = nodeFactory.arrayNode();

		objeto.keySet().forEach(item -> {
			String id = item.getId().replaceAll("\"", "");
			if (notita.contains(id)) {
				pushContent.putPOJO(id, objeto.get(item));
				array.addPOJO(pushContent);
				nuevo.put(item, objeto.get(item));

			} else {
				target.put(id, objeto.get(item));

			}
			target.putPOJO("subStatusChange", array);
		});

		nuevo.keySet().forEach(item -> {
			String id = item.getId().replaceAll("\"", "");
			// if(notita.contains(id))
			// console.println("item",nuevo.get(item));
			objeto.put(new ids("subStatusChange"), nuevo.toString());
		});
		return nuevo;
	}

	/**
	 * @param objetoAnterior
	 * @param nuevoObjeto
	 * @param valor
	 * @return
	 */

//public map<String,String> parseador(){}
	@Autowired
	public Map<String, String> mapita() {
		System.out.println("Entra");
		List<ArsClientTag> algo = dataSetController.busquedaCompaniaPaisCliente("GMGLOBAL", "BRA", "GM_CLARO_BRA");
		Map<String, String> mapita = new HashMap<>();

		algo.forEach(item -> {
			ArsTag tag = item.getArsTag();

			mapita.put(tag.getTag().toString(), String.valueOf(tag.getId_remedy()));
		});

		return mapita;

	}

	public String timeTimestampToString(String valor) {
		long unixSeconds = Integer.parseInt(valor);
		Date date = new java.util.Date(unixSeconds * 1000L);

		return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'").format(date);
	}

	public Map<String, String> cambiadorDeValor(JsonNode objeto) {
		JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
		ObjectNode pushContent = nodeFactory.objectNode();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode actualObj = objeto;
//	    console.println("que mando",objeto);

		Map<String, String> mapaAValidar = mapita();
		Map<String, String> nuevoMapa = new HashMap<>();
		if (actualObj.get("resultado") == null | actualObj.get("resultado").get(0) == null)
			return null;
		actualObj.get("resultado").get(0).get("field").iterator().forEachRemaining(item -> {
			// console.println(" requerido en validador.*****.....",mapaAValidar);
			if (mapaAValidar.containsValue(item.get("id").toString())) {
				mapaAValidar.forEach((llave, valor) -> {
					// console.println("..........",item.get("id").toString());
					if (valor.equals(item.get("id").toString())) {
						if (item.get("value").toString().indexOf("Timestamp") > 1)
							nuevoMapa.put(llave, timeTimestampToString(
									item.get("value").toString().replaceAll("\"", "").replaceAll("[^0-9]", "")));
						else
							nuevoMapa.put(llave, item.get("value").toString().replaceAll("\"", ""));
					}
				});

			}
		});
		console.println("nuevo valor del mapa", nuevoMapa);
		return nuevoMapa;
	}

	boolean exceptionHijoSoloObjeto = false;

//------------------------------------------------------------

	public ObjectNode validar3(Map<String, String> objetoAnterior) throws excepcionElemento {

		Map<String, String> map = new HashMap<String, String>();
		Map<String, String> map2 = new HashMap<String, String>();
		Map<String, String> map3 = new HashMap<String, String>();
		Map<String, String> map4 = new HashMap<String, String>();
		Map<String, String> map5 = new HashMap<String, String>();
		Map<String, String> map6 = new HashMap<String, String>();
		Map<String, String> mapfinal = new HashMap<>();

		Map<String, String> mapdeeper = new HashMap<String, String>();
		JsonObject jsonObjectFinal = new JsonObject();
		JsonObject jsonObjectinsideNoArray = new JsonObject();
		JsonObject jsonObjectinside = new JsonObject();
		JsonObject jsonObjectinsidedeeper = new JsonObject();
		JsonObject jsonObjectinside3 = new JsonObject();
		JsonObject jsonObject = new JsonObject();
		JsonObject jsonObjectonedelimiter = new JsonObject();
		JsonArray jsonArray = new JsonArray();
		JsonArray jsonArray_A = new JsonArray();

		String[] temp = null;
		String delimiter1 = "~";
		String delimiter2 = "!";

		List<String> listaaddjson = new ArrayList();
		List<String> listafind = new ArrayList();

//	 List<ArsClientTag> alltags = validador.getDataSetContr	oller().fncBusquedaIdPaisCompania((long) 6);
		List<tags> alltagsSinFiltro = mappingError.getBuscador();// validador.getDataSetController().fncBusquedaIdPaisCompania((long)
																	// 6);
		List<tags> alltags = new ArrayList();

		for (tags tags : alltagsSinFiltro) {
			// console.println("tag en validador para ver si sitene N o
			// Q",tags.getRequerido());
			// console.println("tag en validador
			// mappingError.validarNoContengaN(tag)",mappingError.validarNoContengaN(tags));
			// console.println("alltags.add(tag)",(new tags() instanceof tags) );
			if (mappingError.validarNoContengaN(tags)) {
				alltags.add(tags);
			}

		}

		for (int w = 0; w < alltags.size(); w++) {
			String nuevoquerya = alltags.get(w).getNombreTags();
			temp = nuevoquerya.split(delimiter1);

			if (nuevoquerya.contains(delimiter1)) {

				String[] newtemp = temp[1].split(delimiter2);

				jsonObjectonedelimiter.add(nuevoquerya, null);

			}
		}

		for (int w = 0; w < alltags.size(); w++) {
			String nuevoquerya = alltags.get(w).getNombreTags();

			if (nuevoquerya.contains(delimiter1) && !nuevoquerya.contains(delimiter2)) {
				temp = nuevoquerya.split(delimiter1);
				listafind.add(temp[0]);

			}

		}
		List<String> listonedelimiternorepetaed = listafind.stream().distinct().collect(Collectors.toList());

		// System.out.println("iterando"+listonedelimiternorepetaed);

		for (int w = 0; w < alltags.size(); w++) {
			String nuevoquerya = alltags.get(w).getNombreTags();

			if (nuevoquerya.contains(delimiter2) && nuevoquerya.contains(delimiter1)) {
				temp = nuevoquerya.split(delimiter1);
				listaaddjson.add(temp[1].split(delimiter2)[0]);

			}

		}
		List<String> listaaddjsonnorepetaed = listaaddjson.stream().distinct().collect(Collectors.toList());

		// System.out.println("iterando"+listaaddjsonnorepetaed);
		for (int w = 0; w < listonedelimiternorepetaed.size(); w++) {
			map.clear();
			map6.clear();
			for (String keyStr : jsonObjectonedelimiter.keySet()) {
				temp = keyStr.split(delimiter1);

				System.out.println("__" + listonedelimiternorepetaed.get(w) + " " + temp[0]);
				if (listonedelimiternorepetaed.get(w).equals(temp[0])) {
					map.put(temp[1], "null");
					map6.put(temp[1], "null");
					if (temp[1].contains(delimiter2)) {
						map6.put(temp[1].split(delimiter2)[0], "null");

					}
					System.out.println("AQUI ESTA EL !" + temp[1]);
				}
			}

			Gson gsonObj = new Gson();
			String jsonStr = gsonObj.toJson(map);
			JsonObject convertedObject = new Gson().fromJson(jsonStr, JsonObject.class);
			for (int r = 0; r < listaaddjsonnorepetaed.size(); r++) {
				mapdeeper.clear();
				for (String convertedObjectElement : convertedObject.keySet()) {

					if (convertedObjectElement.contains(delimiter2)) {
						temp = convertedObjectElement.split(delimiter2);

						if (listaaddjsonnorepetaed.get(r).equals(temp[0])) {
							jsonObjectinsidedeeper.add(temp[1], null);
							mapdeeper.put(temp[1], "null");
						}
					}

				}
				if (!mapdeeper.isEmpty()) {
					// System.out.println("listaaddjsonnorepetaed 3:
					// "+listaaddjsonnorepetaed.get(r)+"mapdeeper"+mapdeeper);
					for (Entry<String, String> entry : mapdeeper.entrySet()) {

						jsonObjectinside.add(entry.getKey(), null);

					}
					jsonObjectinside3.add(listaaddjsonnorepetaed.get(r), jsonObjectinside);

					map2.put(listaaddjsonnorepetaed.get(r), jsonObjectinside.toString());

					// System.out.println("listaaddjsonnorepetaed HDP:
					// "+listaaddjsonnorepetaed.get(r)+"jsonObjectinside "+jsonObjectinside);
					for (Entry<String, String> entry : mapdeeper.entrySet()) {

						jsonObjectinside.remove(entry.getKey());

					}

				}
			}

			jsonArray.add(convertedObject);

			map3.put(listonedelimiternorepetaed.get(w), jsonArray.toString());
			// System.out.println("jsonobject 1"+jsonObject);
			// System.out.println(listonedelimiternorepetaed.get(w)+"___aqui se
			// setea___"+jsonArray);
			jsonArray.remove(convertedObject);

		}

		for (Entry<String, String> entry : map3.entrySet()) {

			JsonParser parser = new JsonParser();
			JsonElement tradeElement = parser.parse(entry.getValue());

			JsonArray trade = tradeElement.getAsJsonArray();
			JsonObject jsonObjectLastFinal = tradeElement.getAsJsonArray().get(0).getAsJsonObject();

			map4.clear();
			for (String innervalue : jsonObjectLastFinal.keySet()) {
				if (!innervalue.contains(delimiter2)) {

					map4.put(innervalue, null);
				}
				if (innervalue.contains(delimiter2)) {
					for (Entry<String, String> entrydelimiter2 : map2.entrySet()) {

						if (innervalue.contains(entrydelimiter2.getKey())) {
							map4.put(innervalue.split(delimiter2)[0], entrydelimiter2.getValue());

						}

					}
				}

			}

			for (Entry<String, String> entrynewmap : objetoAnterior.entrySet()) {
				if (entrynewmap.getKey().contains(delimiter1)) {
					// System.out.println("entra
					// aqui11111111}"+entrynewmap.getKey().split(delimiter1)[1]);
					map4.replace(entrynewmap.getKey().split(delimiter1)[1], null, "\"" + entrynewmap.getValue() + "\"");
				}

			}
			// System.out.println("aqui esta aaaaa"+map4);
			map5.put(entry.getKey(), map4.toString());

			JsonElement tradeElement2 = parser.parse(map4.toString());
			// System.out.println("lets seee"+tradeElement2);
			jsonArray_A.add(tradeElement2);
			jsonObject.remove(entry.getKey());
			// System.out.println(" "+entry.getKey()+"jsonArray_A "+jsonArray_A);
			jsonObject.add(entry.getKey(), jsonArray_A);
			mapfinal.put(entry.getKey(), jsonArray_A.toString());
			jsonArray_A.remove(0);

		}

		for (Entry<String, String> entry : mapfinal.entrySet()) {
			JsonParser parser = new JsonParser();

			JsonElement tradeElement = parser.parse(entry.getValue());
			JsonArray trade = tradeElement.getAsJsonArray();
			jsonObjectFinal.add(entry.getKey(), trade);

		}
		for (int w = 0; w < alltags.size(); w++) {
			String nuevoquerya = alltags.get(w).getNombreTags();
			if (!nuevoquerya.contains(delimiter1) && nuevoquerya.contains(delimiter2)) {
				temp = nuevoquerya.split(delimiter2);
				jsonObjectinsideNoArray.add(temp[1], null);
				jsonObjectFinal.add(temp[0], jsonObjectinsideNoArray);

			}
		}
		for (int w = 0; w < alltags.size(); w++) {
			String nuevoquerya = alltags.get(w).getNombreTags();
			temp = nuevoquerya.split(delimiter1);
			if (!nuevoquerya.contains(delimiter1) && !nuevoquerya.contains(delimiter2)) {
				jsonObjectFinal.add(nuevoquerya, null);

			}
		}

		for (Entry<String, String> entry : objetoAnterior.entrySet()) {
			// System.out.println("you " + entry.getKey() + " , " + entry.getValue());
			if (!entry.getKey().contains(delimiter1)) {
				if (entry.getKey().equals("status")) {
					if (Integer.parseInt(entry.getValue()) == 4 || Integer.parseInt(entry.getValue()) == 5
							|| Integer.parseInt(entry.getValue()) == 6) {
						jsonObjectFinal.addProperty(entry.getKey(), "Closed");

					}
					if (Integer.parseInt(entry.getValue()) == 2 || Integer.parseInt(entry.getValue()) == 3) {
						jsonObjectFinal.addProperty(entry.getKey(), "Pending");
					}
					if (Integer.parseInt(entry.getValue()) == 0 || Integer.parseInt(entry.getValue()) == 1) {
						jsonObjectFinal.addProperty(entry.getKey(), "Open");
					}
				} else
					jsonObjectFinal.addProperty(entry.getKey(), entry.getValue());
			}

		}

//		System.out.println("FINAL      " + jsonObjectFinal);

//		console.println("buuuuuuuuuuuu objeto anterior", objetoAnterior);

		ObjectNode resultFinal = null;

		try {
			resultFinal = (ObjectNode) new ObjectMapper().readTree(jsonObjectFinal.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resultFinal;

	}

//-------------------------------------

	public ObjectNode nuevoMapita(Map<String, String> objeto) throws excepcionElemento {
		JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
		ObjectNode notification = nodeFactory.objectNode();
		// console.println("valod del oboejt", objeto);
		return validar3(objeto);
		// return validar(objeto,notification,"subStatusChange" );
	}

	public Map<String, String> validarMapita(String valor) {
		Map<String, String> nuevo = new HashMap<>();

		// String notas[] = env.getProperty(valor).split(",");
		List<Object> notita = Arrays.asList(env.getProperty(valor).split(","));
		if (notita.size() <= 0)
			return null;
		// console.println("notita",notita);
		objeto.keySet().forEach(item -> {
			String id = item.getId().replaceAll("\"", "");

			// System.out.println("padalustro" + id);
			if (notita.contains(id))
				nuevo.put(id, (String) notita.get(notita.indexOf(id)));
		});
		return nuevo;
	}

	public List<String> validarCabezera(Map<String, String> headerRequest) {
		JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
		ObjectNode notification = nodeFactory.objectNode();
		console.println("******************** validador de cabecera ***********************");
		String enterpriseId = headerRequest.get("enterpriseid");
		String correlationId = headerRequest.get("correlationid");
		String providerId = headerRequest.get("providerid");
		String country = headerRequest.get("country");
		String countryM2M = headerRequest.get("countrym2m");

		console.println("entrepriseId", enterpriseId);
		console.println("correlationId", correlationId);
		console.println("prividerId", providerId);
		console.println("country", country);
		console.println("countryM2M ", countryM2M);

		List<String> requiredHeader = new ArrayList<String>();
		if (enterpriseId == null || enterpriseId.trim().length() == 0)
			requiredHeader.add("enterpriseId");
//	      if(correlationId==null||correlationId.trim().length()==0)   
//	    	  required.add("correlationId");
		if (providerId == null || providerId.trim().length() == 0)
			requiredHeader.add("providerId");
		if (country == null || country.trim().length() == 0)
			requiredHeader.add("country");
		if (countryM2M == null || countryM2M.trim().length() == 0)
			requiredHeader.add("countryM2M");
		return requiredHeader;

	}

	@Override
	public String toString() {
		return "Validador [tipoServicio=" + tipoServicio + "]";
	}

}
