package com.telcel.gm.match;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.telcel.gm.component.console;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

public class clasificadorIds {
	private Map<ids,tags> map;
	private int sizeResult;
	private JsonObject result;
	private elementosIdValores idValores;

	JsonObject valoresId = new JsonObject();

	private List<tagsIds> elementosValores;
	
	public clasificadorIds(Object objeto) {
		Gson gson = new Gson();    
		String content = gson.toJson(objeto);
		JsonObject g = new JsonParser().parse(content).getAsJsonObject();
		JsonArray array =g.get("resultado").getAsJsonArray();
		//console.println(gson.toJson(array.get(0)));
		String algo=gson.toJson(array.get(0));
	    idValores=new elementosIdValores();
	    idValores.convertirJson();
	    
		JsonObject nuevoJson = new JsonParser().parse(algo).getAsJsonObject();
		result=nuevoJson;


		 this.elementosValores=new ArrayList<>();
    	this.map=new HashMap<>();
	    console.println(sizeResult);
	    clasificadorArgumentosOpcion();
	}
	
	

	public void clasificadorArgumentosOpcion() {  
		result.get("Field").getAsJsonArray().forEach(item -> {
			//try {
				String id=item.getAsJsonObject().get("id").getAsString();
				String idvalor=idValores.getJsonElementLLave(id);
				   
			    if(idvalor!=null) {
			    	JsonElement valor=item.getAsJsonObject().get("value");
			    	elementosValores.add(new tagsIds(new ids(idvalor), new tags((valor!=null)?valor.toString():"")));
			    		this.map.put(new ids(idvalor),
						     new tags((valor!=null)?valor.toString().replaceAll("\"", ""):""));
 
			   }
//			} catch (excepcionElemento e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		    // console.println("id : ",item.getAsJsonObject().get("id"));
		     //console.println("tag : ",item.getAsJsonObject().get("value"));
					   
					});
	    }
		
    public Map<ids,tags> getMap() {
    	return map;
    }
    public List<tagsIds> getList() {
    	return elementosValores;
    }
	@Override
	public String toString() {
	
		String representacion="Clasifidador [";
		representacion+="mapa "+this.elementosValores	;
		representacion+="\n";
		representacion+="]";
		return representacion;
	}



	
	
	/*public ClasificadorArgumentoOpcion(Object [] argumentoComoString) {
		this.argumentoComoString=argumentoComoString;
		 this.argumentoComoArgumentoOpcion=new ArrayList<>();
		 this.map=new HashMap<>();
		 clasificadorArgumentosOpcion();
	}public ArgumentoValorOpcion operador() {
		 for (Map.Entry<ArgumentoNombreOpcion, ArgumentoValorOpcion> opcion : map.entrySet()){
		    	if(opcion.getKey().esOpcionDeOperacion())
		    		return opcion.getValue();
			    
		  	}
		 return null;
	}
	public ArgumentoValorOpcion alfabeto() {
		 for (Map.Entry<ArgumentoNombreOpcion, ArgumentoValorOpcion> opcion : map.entrySet()){
		    	if(opcion.getKey().esOpcionDeAlfabeto())
		    		return opcion.getValue();
			    
		  	}
		 return null;
	}
	public ArgumentoValorOpcion texto() {
		 for (Map.Entry<ArgumentoNombreOpcion, ArgumentoValorOpcion> opcion : map.entrySet()){
		    	if(opcion.getKey().esOpcionDeTextoClaro()||opcion.getKey().esOpcionDeArchivo())
		    		return opcion.getValue();
			    
		  	}
		 return null;
	}
	public ArgumentoValorOpcion archivo() {
		 for (Map.Entry<ArgumentoNombreOpcion, ArgumentoValorOpcion> opcion : map.entrySet()){
		    	if(opcion.getKey().esOpcionDeArchivo())
		    		return opcion.getValue();
			    
		  	}
		 return null;
	}
	public ArgumentoValorOpcion desplazamiento() {
		 for (Map.Entry<ArgumentoNombreOpcion, ArgumentoValorOpcion> opcion : map.entrySet()){
		    	if(opcion.getKey().esOpcionDeDesplazamiento())
		    		return opcion.getValue();
			    
		  	}
		 return null;
	}
	public ArgumentoValorOpcion salida() {
		 for (Map.Entry<ArgumentoNombreOpcion, ArgumentoValorOpcion> opcion : map.entrySet()){
		    	if(opcion.getKey().esOpcionSalida())
		    		return opcion.getValue();
			    
		  	}
		 return null;
	}
	public Map<ArgumentoNombreOpcion,ArgumentoValorOpcion> getMap(){
		//this.clasificadorArgumentosOpcion();
		return map;
	}
	public String toString() {
		String representacion="Clasifidador [";
		representacion+="argumentoComoString"+Arrays.toString(this.argumentoComoString);
		representacion+="\n";
		representacion+="mapa "+this.map;
		representacion+="\n";
		representacion+="argumentoComoArgumentoOpcion"+this.argumentoComoArgumentoOpcion;
		representacion+="]";
		return representacion;
	}*/

}
