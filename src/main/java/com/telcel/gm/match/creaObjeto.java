package com.telcel.gm.match;






import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.telcel.gm.component.console;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;


public class creaObjeto {
	private Map<ids,String> map;
	private int sizeResult;
	private JsonObject result;
	private elementosIdValores idValores;

	JsonObject valoresId = new JsonObject();

	private List<tagsIds> elementosValores;
	
	public creaObjeto() {
	
	}
	
	public creaObjeto(Object objeto) {
		Gson gson = new Gson();    
		String content = gson.toJson(objeto);
		JsonObject g = new JsonParser().parse(content).getAsJsonObject();
	  // console.println("objeto en crearobjeto",objeto);
		JsonArray array =g.get("resultado").getAsJsonArray();
	
		//console.println(gson.toJson(array.get(0)));
		String algo=gson.toJson(array.get(0));
	    idValores=new elementosIdValores();
	    idValores.convertirJson();
	    
		JsonObject nuevoJson = new JsonParser().parse(algo).getAsJsonObject();
		result=nuevoJson;


		 this.elementosValores=new ArrayList<>();
    	this.map=new HashMap<>();
	    console.println(sizeResult);
	    
	    creaAnalizaObjeto(result);
	}
	
	
	
	public JsonObject convertir(Object objeto) {
		Gson gson = new Gson();    
		String content = gson.toJson(objeto);
		JsonObject g = new JsonParser().parse(content).getAsJsonObject();
	//   console.println("objeto en crearobjeto",objeto);
		    idValores=new elementosIdValores();
	    idValores.convertirJson();
	    
	 return g;
	}
	public JsonNode StringtoJsonNode(Object objeto) {
		 ObjectMapper mapper = new ObjectMapper();
		 JsonNode jsonNode =null;
	try {
		jsonNode=mapper.readTree(objeto.toString());
	
	} catch (IOException e) {
		jsonNode=mapper.valueToTree(objeto);//mapper.readTree(objeto.toString());
	}
			return jsonNode;

	}
	public ObjectNode StringtoObjectNode(Object objeto) {
		 ObjectMapper mapper = new ObjectMapper();
		 return (ObjectNode)StringtoJsonNode(objeto);

	}
	
	public String toString(Object objeto) {
		Gson gson = new Gson();    
		String content = gson.toJson(objeto);
		JsonObject g = new JsonParser().parse(content).getAsJsonObject();
		    idValores=new elementosIdValores();
	    idValores.convertirJson();
	    
	 return content;
	}
	
	public JsonObject StringToJson(String objeto) {
		Gson gson = new Gson();    
		String content = gson.toJson(objeto);
		JsonObject g = new JsonParser().parse(objeto).getAsJsonObject();
		    idValores=new elementosIdValores();
	    idValores.convertirJson();
	    
	 return g;
	}	
	public JsonObject convertirJson(Object objeto) {
		Gson gson = new Gson();    
		String content = gson.toJson(objeto);
		JsonObject g = new JsonParser().parse(content).getAsJsonObject();
		JsonArray array =g.get("Field").getAsJsonArray();
		String algo=gson.toJson(array.get(0));
		JsonObject nuevoJson = new JsonParser().parse(algo).getAsJsonObject();
		result=nuevoJson;

	 return nuevoJson;
	}
	

	public void creaAnalizaObjeto(JsonObject objeto) {  
		JsonObject json=convertirJson(objeto);
	      objeto.get("Field").getAsJsonArray().forEach(item -> {
				String id=item.getAsJsonObject().get("id").getAsString();
				String idvalor=idValores.getJsonElementLLave(id);
				   
			    if(idvalor!=null) {
			    	JsonElement valor=item.getAsJsonObject().get("value");
			    	valoresId.addProperty(idvalor, (valor!=null)?valor.toString():"");
			    	
			    	elementosValores.add(new tagsIds(new ids(idvalor), new tags((valor!=null)?valor.toString():"")));
			    	
			    	this.map.put(new ids(idvalor),
			    				(valor!=null)?valor.toString().replaceAll("\"", ""):"");		
			    			// new tags((valor!=null)?valor.toString():""));
                       
			   }
					   
					});
	    }
		
    public Map<ids,String> getMap() {
    	return map;
    }
    public List<tagsIds> getList() {
    	return elementosValores;
    }
    public JsonObject getJson() {
    	return valoresId;
    }
	@Override
	public String toString() {
	
		String representacion="Clasifidador [";
		representacion+="mapa "+this.valoresId	;
		representacion+="\n";
		representacion+="]";
		return representacion;
	}



	
	
	/*public ClasificadorArgumentoOpcion(Object [] argumentoComoString) {
		this.argumentoComoString=argumentoComoString;
		 this.argumentoComoArgumentoOpcion=new ArrayList<>();
		 this.map=new HashMap<>();
		 clasificadorArgumentosOpcion();
	}public ArgumentoValorOpcion operador() {
		 for (Map.Entry<ArgumentoNombreOpcion, ArgumentoValorOpcion> opcion : map.entrySet()){
		    	if(opcion.getKey().esOpcionDeOperacion())
		    		return opcion.getValue();
			    
		  	}
		 return null;
	}
	public ArgumentoValorOpcion alfabeto() {
		 for (Map.Entry<ArgumentoNombreOpcion, ArgumentoValorOpcion> opcion : map.entrySet()){
		    	if(opcion.getKey().esOpcionDeAlfabeto())
		    		return opcion.getValue();
			    
		  	}
		 return null;
	}
	public ArgumentoValorOpcion texto() {
		 for (Map.Entry<ArgumentoNombreOpcion, ArgumentoValorOpcion> opcion : map.entrySet()){
		    	if(opcion.getKey().esOpcionDeTextoClaro()||opcion.getKey().esOpcionDeArchivo())
		    		return opcion.getValue();
			    
		  	}
		 return null;
	}
	public ArgumentoValorOpcion archivo() {
		 for (Map.Entry<ArgumentoNombreOpcion, ArgumentoValorOpcion> opcion : map.entrySet()){
		    	if(opcion.getKey().esOpcionDeArchivo())
		    		return opcion.getValue();
			    
		  	}
		 return null;
	}
	public ArgumentoValorOpcion desplazamiento() {
		 for (Map.Entry<ArgumentoNombreOpcion, ArgumentoValorOpcion> opcion : map.entrySet()){
		    	if(opcion.getKey().esOpcionDeDesplazamiento())
		    		return opcion.getValue();
			    
		  	}
		 return null;
	}
	public ArgumentoValorOpcion salida() {
		 for (Map.Entry<ArgumentoNombreOpcion, ArgumentoValorOpcion> opcion : map.entrySet()){
		    	if(opcion.getKey().esOpcionSalida())
		    		return opcion.getValue();
			    
		  	}
		 return null;
	}
	public Map<ArgumentoNombreOpcion,ArgumentoValorOpcion> getMap(){
		//this.clasificadorArgumentosOpcion();
		return map;
	}
	public String toString() {
		String representacion="Clasifidador [";
		representacion+="argumentoComoString"+Arrays.toString(this.argumentoComoString);
		representacion+="\n";
		representacion+="mapa "+this.map;
		representacion+="\n";
		representacion+="argumentoComoArgumentoOpcion"+this.argumentoComoArgumentoOpcion;
		representacion+="]";
		return representacion;
	}*/

}
