package com.telcel.gm.match;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.telcel.gm.component.console;

public class ids {
	
   private	String id;
	private String idValidos;
	private JsonObject idprueba;
	
	public JsonObject getIdprueba() {
		return idprueba;
	}
	public void setIdprueba(JsonObject idprueba) {
		this.idprueba = idprueba;
	}
	public ids(String id) {
		this.id=id;
		idprueba= new JsonObject();
		this.idprueba.addProperty("id", id);
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdValidos() {
		return idValidos;
	}
	public void setIdValidos(String idValidos) {
		this.idValidos = idValidos;
	}
	 public boolean esValido() {
		 return true;
	 }
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return id.replaceAll("\"", "");
	}
	 
	
	

}
