package com.telcel.gm.match;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.telcel.gm.component.console;
import com.telcel.remedy.model.constantes.ConstantesRemedy;
import com.telcel.remedy.model.entity.ArsCategoria;

public class GestionCampoPais {
	

JsonNodeFactory nodeFactory = JsonNodeFactory.instance;

////ObjectNode pushContent = nodeFactory.objectNode();
ObjectNode notification = nodeFactory.objectNode();
ObjectNode appsObj = nodeFactory.objectNode();
ObjectNode appsObj2 = nodeFactory.objectNode();
ObjectNode target = nodeFactory.objectNode();

ObjectNode objetoDefault = nodeFactory.objectNode();

public String getToCountryM2MCountry(String countryM2M) {
		Map<String,String> mapeadorPais =new HashMap();
		mapeadorPais.put("AR", "ARG");
		mapeadorPais.put("UY", "URY");
		mapeadorPais.put("PY", "PRY");
		mapeadorPais.put("EC", "ECU");
		mapeadorPais.put("CL", "CHL");
		mapeadorPais.put("PE", "PER");
		mapeadorPais.put("CO", "COL");
		mapeadorPais.put("BO", "BOL");
		mapeadorPais.put("BR", "BRA");

    return mapeadorPais.get(countryM2M);
}

public boolean isToLocal( Map<String,String> headerRequest) {// se dirige al local???
	return headerRequest.get("providerid").compareTo(ConstantesRemedy.CREATEINCIDENTLOCAL)==0;
	
}

public String changeClient(Map<String,String> headerRequest,String client) {
	             if(!isToLocal(headerRequest))
	            	 return ConstantesRemedy.USERLOCAL;
	             
	    return client;         
	             
}
public boolean isPAU(String pau) {
	 return (pau.toUpperCase().equals("AR")|pau.toUpperCase().equals("UY")|pau.toUpperCase().equals("PY"));
}

public boolean createCloud(String pais) {
	
	return (pais.toUpperCase().equals("BR"));
}

public Map<String,String> changeHeaderToCloud(Map<String,String> headerRequest) {
	if(!isToLocal(headerRequest))
	if(!isPAU(headerRequest.get("countrym2m"))){
       headerRequest.replace("enterpriseid","GM_CLARO_BRA");
	   headerRequest.replace("country","BRA");
	   headerRequest.replace("countrym2m","BR");
	   }
  return  headerRequest;
}

public Map<String,String> forceHeaderToCloud(Map<String,String> headerRequest) {
	   headerRequest.replace("enterpriseid","GM_CLARO_BRA");
	   headerRequest.replace("country","BRA");
	   headerRequest.replace("countrym2m","BR");
  return  headerRequest;
}


public ObjectNode extracionCampo(String country,String campoAExtraer) {
	//console.println("en mapeador de pais en campo",mapeadorCountry(country).get(campoAExtraer));
	return (ObjectNode)mapeadorCountry(country).get(campoAExtraer);
}


public ObjectNode mapeadorCountry(String country) {
	 ObjectNode mapa=mapOfCountry();
		//console.println("en mapeador de pais",mapa.get(country));
		 	return (ObjectNode)mapa.get(country);
	 	
}
public ObjectNode mapOfCountry() {
	JsonNodeFactory nodeFactory = JsonNodeFactory.instance;

//	-----Map<ids,String> objeto = new HashMap<>();
	Map<String,Map> objeto = new HashMap<>();

	Map<String,Map> objetoPais = new HashMap<>();
//	//-----ObjectNode pushContent = nodeFactory.objectNode();
	ObjectNode conctactoArgentina = nodeFactory.objectNode();
	ObjectNode NodoArgentina = nodeFactory.objectNode();
	ObjectNode appsObj2 = nodeFactory.objectNode();
	ObjectNode NodoPadre = nodeFactory.objectNode();
	ObjectNode Nodo = nodeFactory.objectNode();
	ArrayNode apps = nodeFactory.arrayNode();
	ArrayNode platforms = nodeFactory.arrayNode();

	Map<String,Map> hijosObjetoPais = new HashMap<>();
	
	Map<String,String> contacto = new HashMap<>();
			conctactoArgentina.put( "Company","CLARO ARGENTINA" );
			conctactoArgentina.put( "lastName","GENERAL MOTORS ARGENTINA" );
			conctactoArgentina.put( "firstName","CUENTA GENERICA" );			
			conctactoArgentina.put( "ContactCompany","CLARO ARGENTINA" );
			NodoArgentina.put("contacto",conctactoArgentina);
			
	Map<String,String> valorCaja = new HashMap<>();
			appsObj2.put( "serviceType","0" );
			appsObj2.put( "reportedSource","5000" );
			appsObj2.put( "caseType","CREATE" );
			appsObj2.put( "process","CPD" );
	NodoArgentina.put("caja",appsObj2);
	Nodo.put("ARG",NodoArgentina);
	//----------------------------------mexico

	ObjectNode objetoContactoMexico = nodeFactory.objectNode();
	ObjectNode notificacionMexico = nodeFactory.objectNode();
	notificacionMexico.put( "Company","TELCGEL" );
	notificacionMexico.put( "firstName","GENERAL MOTORS" );
	notificacionMexico.put( "lastName","CUENTA GENERICA" );	
	notificacionMexico.put( "ContactCompany","TELCEL" );
	
	objetoContactoMexico.put("contacto", notificacionMexico);
	objetoContactoMexico.put("caja", appsObj2);
	Nodo.put("MEX",objetoContactoMexico);
	
	NodoPadre.putAll(Nodo);
	//NodoPadre.putAll(objetoContactoMexico);
	

	
	console.println("imprimiento valores del mapeador..",NodoPadre);
	
	return NodoPadre;
	
}

public List<ArsCategoria> filtrarByNumero(List<ArsCategoria> listita,String cadenaNumerico,String country) {
		
		return listita.stream().filter(item->(item.getId_cat().equals(cadenaNumerico.replaceAll("\"", ""))&
			 (item.getPaisCompania().getArsPais().getPais().equals(country))))
	 .collect(Collectors.toList());
}

public List<tags> filtradorSinNull(List<tags> listaTag) {
			return listaTag.stream().filter(item->!(item.getValorDefault()==null)
		).collect(Collectors.toList());
		
	
	//return null;
}

public void clearCampoDefault() {
	objetoDefault.removeAll();
	//console.println("verificacion de clear objetoDefault", objetoDefault);
	
}

public ObjectNode gestionadorCampoPais(List<tags> listaTag) {
//	console.println("entraa en gestionadorCampoPais", listaTag);
	if(listaTag.size()==0)
		return null;
	
///	console.println("verificacion de clear objetoDefault  gestionadorCampoPais....", objetoDefault);
	objetoDefault.removeAll();
	Map<String,String> campoDefault=new HashMap<>();
	filtradorSinNull(listaTag).forEach(item->{
	//	console.println("item.getNombreTags() gestionadorCampoPais....", item.getNombreTags());
		//console.println("item.getValorDefault()gestionadorCampoPais....", item.getValorDefault());
			objetoDefault.put(item.getNombreTags(), item.getValorDefault());
		});
	
//	console.println("lista en gestion para validar null  ",filtradorSinNull(listaTag));
	console.println(" campos default obtenidos  ",objetoDefault);
	
return objetoDefault;

}










}
