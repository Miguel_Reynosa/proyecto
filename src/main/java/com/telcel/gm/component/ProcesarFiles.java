package com.telcel.gm.component;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.LinkOption;
import java.util.ArrayList;

import org.apache.commons.io.FilenameUtils;
import org.apache.tika.Tika;
import org.apache.tika.detect.Detector;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;

public class ProcesarFiles 
{
	
	/**
	 * 
	 * @param pathToFile
	 * @return ArrayList<String> con los nombres de las carpetas padres
	 */
	public ArrayList<String> verificarNombresDirectorios(String pathToFile)
	{
		ArrayList<String> listaNombresDirectorios = new ArrayList<String>();
		
		// CHECAMOS EL TIPO DE SISTEMA
		boolean isWindows = isWindowsFormatPath(pathToFile);
		String charSeparator = "";
		
		if(isWindows)
		{
			//charSeparator = "\\\\";
			charSeparator = "/";
			// REVISAMOS EL LISTADO DE DIRECTORIOS
			System.out.println("\nPROCEDEMOS A REVISAR LOS DIRECTORIOS... --OK");
			String pathWin = pathToFile.replace(":", "");
			pathWin.replace("\\", "/");
			
			listaNombresDirectorios = obtenerListaNombresDirectorios(pathWin, charSeparator);
		}
		else
		{
			charSeparator = "/";
			// REVISAMOS EL LISTADO DE DIRECTORIOS
			System.out.println("\nPROCEDEMOS A REVISAR LOS DIRECTORIOS... --OK");
			listaNombresDirectorios = obtenerListaNombresDirectorios(pathToFile, charSeparator);
		}
				
			
		return listaNombresDirectorios;
	}
	
	
	/**
	 * Solo para ambiente local
	 * @param pathToFile
	 * @return
	 */
	public boolean isPathValido(String pathToFile)
	{
		boolean pathValido = false;
		
		Path path = Paths.get(pathToFile);
		
		pathValido = Files.exists(path, new LinkOption[] {LinkOption.NOFOLLOW_LINKS});
		
		System.out.println("Path válido? " + pathValido);
		
		return pathValido;
	}

	
	public ArrayList<String> obtenerListaNombresDirectorios(String pathToFile, String charSeparator)
	{
		ArrayList<String> listaNombresDirectorios = new ArrayList<>();
		return listaNombresDirectorios;
	}
	public String tika(File file)
	{
		try
		{
			Tika defaultTika = new Tika();
			System.out.println("esto es tika"+defaultTika.detect(file));
			InputStream is = new BufferedInputStream(new FileInputStream(file));
			AutoDetectParser parser = new AutoDetectParser();
			Detector detector = parser.getDetector();
			Metadata md = new Metadata();
			md.add(Metadata.RESOURCE_NAME_KEY, file.getName());
			MediaType mediaType = detector.detect(is, md);
		//	return mediaType.toString();
			return defaultTika.detect(file);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}
	public String checkMimeTypeOnTika(String pathToFile)
	{
		String mimeTypeOnTika = "";
		File file = new File(pathToFile);
		System.out.println("pathfile::"+pathToFile);
		mimeTypeOnTika = tika(file);
		
		System.out.println("tika " + mimeTypeOnTika);

		return mimeTypeOnTika;
	}

	public String getFileNameWithoutExt(String aFileNameWithExt)
	{
	
		String fileNameWithoutExt = FilenameUtils.removeExtension(aFileNameWithExt);
		
		System.out.println("\n\t Retornamos nombre fichero sin extension: " + fileNameWithoutExt);
		
		return fileNameWithoutExt;
	}
	
	public boolean isWindowsFormatPath(String pathToFile)
	{
		System.out.println("ENTRANDO A isWindowsFormatPath --OK");
		boolean windowsFormat = false;
		
	//	Path path = Paths.get(pathToFile);
		//String rootDirectory = "" + path.getRoot().getFileName();
		
		if(!pathToFile.startsWith("/"))
		{
			// NO SE TRATA DE UN PATH UNIX, SE TRATA DE UN PATH WINDOWS
			windowsFormat = true;
		}
		
	//	System.out.println("ES UN PATH WINDOWS " + path.toUri()+ "? " + windowsFormat);
		
		return windowsFormat;
	}
	
	public String obtenerNombreFichero(String pathToFile)
	{					
		//Path path = Paths.get(pathToFile).normalize();
		//String fileName = "" + path.getFileName();
		boolean isWindows=isWindowsFormatPath(pathToFile);
		String charSeparator = "";
		System.out.println("isWindows.."+isWindows);
		if(isWindows)
		{
			//charSeparator = "\\\\";
			charSeparator = "/";
			// REVISAMOS EL LISTADO DE DIRECTORIOS
			System.out.println("\nPROCEDEMOS A REVISAR LOS DIRECTORIOS en windows... --OK");
		//	String pathWin = pathToFile.replace(":", "");
			pathToFile=pathToFile.replace("\\", "/");
		}
		else
		{
			charSeparator = "/";
			// REVISAMOS EL LISTADO DE DIRECTORIOS
			System.out.println("\nPROCEDEMOS A REVISAR LOS DIRECTORIOS en linux... --OK");
		}
		System.out.println(" el nuevo directorio es:"+pathToFile);
		File file = new File(pathToFile);
		
		System.out.println("FileName: " + file.getName());
		//System.out.println("FileSystem: " + path.getFileSystem());
		
		
	//	Path path2 = Paths.get(pathToFile).normalize();
	//	String fileName2 = "" + path.getFileName();
		//System.out.println("fileName2: " + fileName2);
		
		return file.getName();
	}
	
	/**
	 * 
	 * @param pathToFile
	 * @return String con el tipo de fichero que se está indicando en el pathToFile
	 */
	public String obtenerTipoFichero(String pathToFile)
	{
		String tipoFichero = ""; 
		File file = new File(pathToFile);
		try 
		{
			tipoFichero = Files.probeContentType(file.toPath());
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		return tipoFichero;
	}
}
