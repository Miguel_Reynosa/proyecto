package com.telcel.gm.component;



import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.log4j.Logger;

import com.telcel.remedy.controllers.GeneralMotorControllers;

public final class console {
	static Logger LOGGER = Logger.getLogger(GeneralMotorControllers.class);


	public static String marcaDeTiempo() {
		LocalDateTime lDT = LocalDateTime.now();
		DateTimeFormatter oP = DateTimeFormatter.ofPattern("H:m:s:S");
		return lDT.format(oP);
	}
	public static void println(String mensaje) {
		LOGGER.debug("[  mensaje ] "  + " : ");
		LOGGER.debug("\n" +mensaje);
		LOGGER.debug(" \n" );
			
		System.out.println("[  mensaje ] "  + " : " + mensaje);
	}
	
	public static void println(Object mensaje) {
		LOGGER.debug("[ mensaje Objeto ] "  + " : " + mensaje);
		LOGGER.debug("\n" );
		System.out.println(marcaDeTiempo()+" [  mensaje ] "  + " : " + mensaje);
		
	}
	public static void println(String sujeto, String mensaje) {
		LOGGER.debug("["+ marcaDeTiempo() +"] " + sujeto + " : " + mensaje);
		LOGGER.debug("\n" );
		System.out.println(marcaDeTiempo()+" [  "+sujeto+ "] "  + " : " + mensaje);
		}
	public static void println(String sujeto, Object mensaje) {
		LOGGER.debug("["+ marcaDeTiempo() +"] " + sujeto + " : " + mensaje);
		LOGGER.debug("\n" );
		System.out.println("[  sujeto ] "  +sujeto+ " : " + mensaje);
		}
	public static void println(String sujeto, int mensaje) {
		LOGGER.debug("["+ marcaDeTiempo() +"] " + sujeto + " : " + mensaje);
	}
	
	public static void println(String sujeto, Object[] mensaje) {
		  System.out.println("["+ marcaDeTiempo() +"] "+ sujeto+ " : " );
			
		for (int i = 0; i < mensaje.length; i++) {
			LOGGER.info("["+ marcaDeTiempo() +"] " + " : " + mensaje[i]);
			LOGGER.info("\n" );
	
			
		}
	}
	
	// PRINTLN CON INFO
	public static void printlnInfo(String mensaje) {
		LOGGER.debug("[  mensaje ] "  + " : ");
		LOGGER.debug("\n" +mensaje);
		LOGGER.debug(" \n" );
			
		//System.out.println("[  mensaje ] "  + " : " + mensaje);
	}
	
	public static void printlnInfo(Object mensaje) {
		LOGGER.info("[ mensaje Objeto ] "  + " : " + mensaje);
		LOGGER.info("\n" );
	}
	public static void printlnInfo(String sujeto, String mensaje) {
		LOGGER.info("["+ marcaDeTiempo() +"] " + sujeto + " : " + mensaje);
		LOGGER.info("\n" );
	}
	public static void printlnInfo(String sujeto, Object mensaje) {
		LOGGER.info("["+ marcaDeTiempo() +"] " + sujeto + " : " + mensaje);
		LOGGER.info("\n" );
	}
	public static void printlnInfo(String sujeto, int mensaje) {
		LOGGER.info("["+ marcaDeTiempo() +"] " + sujeto + " : " + mensaje);
	}
	
	public static void printlnInfo(String sujeto, Object[] mensaje) {
		 // System.out.println("["+ marcaDeTiempo() +"] "+ sujeto+ " : " );
			
		for (int i = 0; i < mensaje.length; i++) {
			LOGGER.info("["+ marcaDeTiempo() +"] " + " : " + mensaje[i]);
			LOGGER.info("\n" );
	
			
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static String arrayToString(Object o) {
	String cadena="";
	
	return cadena;
		
	}
	
}