package com.telcel.gm.component;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.telcel.gm.model.ResposeGM;
import com.telcel.gm.respuesta.Respuesta;

public class ValidarProvider {
	

	HttpHeaders responseHeaders = new HttpHeaders();
	public boolean valProv(String providerId, String[] voProvId) {
		console.println("Entra arr",voProvId);
		boolean temp=false;
		for (int i = 0; i < voProvId.length; i++) {
			//console.println("Recorrido",voProvId[i]);
			if(providerId.equals(voProvId[i])) {
				//console.println("Es igual",providerId);
				//console.println("con",voProvId[i]);
				temp=false;
				break;
			}
			else {
				console.println("No encuentra",providerId);
				temp=true;
			}
        }
		
		return temp;
	}

	
	public ResponseEntity<String> closeRequest(Respuesta respuesta, String providerId) {
		respuesta.setCode("ATTA001");
		respuesta.setReason(providerId + " is  no supported ");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
				.body(respuesta.getRepuestaError().toString());
	}
	public ResponseEntity<ResposeGM> closeRequest(ResposeGM voattach, String providerId) {
		voattach.setCode("ATTA001");
		voattach.setReason(providerId + " is  no supported ");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
				.body(voattach);
	}
	public ResponseEntity<ObjectNode> closeRequestON(Respuesta respuesta, String providerId) {
		respuesta.setCode("ATTA001");
		respuesta.setReason(providerId + " is  no supported ");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
				.body(respuesta.getRepuestaError());
	}


}
