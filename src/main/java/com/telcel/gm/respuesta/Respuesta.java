
package com.telcel.gm.respuesta;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.validator.internal.util.privilegedactions.GetMethod;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.telcel.gm.component.console;
import com.telcel.gm.match.creaObjeto;
import com.telcel.gm.match.ids;

public class Respuesta {
   private 	String respuesta;
   private 	String code;
   private 	String reason;
   private 	String status;
   private 	String referenceError;
   private 	String type;
   
   private 	String message;
   private creaObjeto objeto;
   private 	String schemaLocation;
   private 	String codigoEstado;

//	private Map<String,String> map;
  private  ObjectNode  map;
  
  public Respuesta() {
	 // this.map=new HashMap<>();
	  this.objeto=new creaObjeto();
	   this.respuesta="";
	   this.code="";
	   this.reason="";
	   this.status="";
	   this.referenceError="";
	  this. type="";
	   this.message="";	 
	   this.schemaLocation="";
       JsonNodeFactory factory = JsonNodeFactory.instance;
        map = factory.objectNode();
  }
  
   public String getMessage() {
	return message;
}

public String getCodigoEstado() {
	return codigoEstado;
}

public void setCodigoEstado(String codigoEstado) {
	this.codigoEstado = codigoEstado;
}

public void setMessage(String message) {
	this.message = message;
}


   
 
   
	public String getSchemaLocation() {
	return schemaLocation;
}

public void setSchemaLocation(String schemaLocation) {
	this.schemaLocation = schemaLocation;
}

	public String getCode() {
	return code;
}

public void setCode(String code) {
	String codigoEstado=code;
	this.codigoEstado=codigoEstado;

	
	this.code=codigoEstado;
}

public String getReason() {
	return reason;
}

public void setReason(String reason) {

	this.reason = reason;
}
public void setReason(Object objeto,String reason) {
    if(objeto==null)
    	this.reason="";
 	this.reason = objeto+" "+reason;
}
public String getStatus() {
	
	
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getReferenceError() {
	return referenceError;
}

public void setReferenceError(String referenceError) {
	this.referenceError = referenceError;
}

public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}


	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public ObjectNode getMap() {
		return map;
	}
	
	public JsonElement getRespuestaJsonResultado(JsonObject objeto) {
			    return objeto.get("resultado");
	}
	public ObjectNode getRepuestaError() {
		   map.put("code",getCode());
		   map.put("reason",getReason()); 
		   map.put("message",getMessage()); 
		   map.put("status",getStatus()); 
		   map.put("referenceError",getReferenceError());
		   map.put("schemaLocation",getSchemaLocation());
		
		return map;
	}

	public void setMap(ObjectNode map) {
		this.map = map;
	}
	

}
