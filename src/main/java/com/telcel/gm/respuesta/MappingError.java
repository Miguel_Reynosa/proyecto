package com.telcel.gm.respuesta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.telcel.gm.component.console;
import com.telcel.gm.match.GestionCampoPais;
import com.telcel.gm.match.Validador;
import com.telcel.gm.match.tags;
import com.telcel.remedy.model.entity.ArsClientTag;

public class MappingError {

	List<tags> listita = new ArrayList<>();
	List<tags> listitaupd = new ArrayList<>();
	List<ArsClientTag> map;
	Validador validador;

	public void clearListaTag() {
		listita.clear();
	}

	public void bucadorTags(Validador validador, String usuario, String pais, String alias) {
		// map =validador.getDataSetController().busquedaCompaniaPaisCliente("TEMIP",
		// "MEXICO", "GM_MX");

		console.println("****************BUSCANDO TAGS******************************* ");
		console.println("pais... en buscadorTag ", pais);
		console.println("alias ", alias);
		map = validador.getDataSetController().busquedaCompaniaPaisCliente(usuario, pais, alias);
		this.validador = validador;
		// console.println("mapitata en buscador tags",map);
		console.println("tipo de servicio", validador.getTipoServicio());

		// tags(String tag,String id_remedy,String formulario,String requerido,String
		// tipo)
		clearListaTag();
		map.forEach(item -> {
			if (item.getRequerido() != null)

				if (item.getRequerido().indexOf("N") >= 0 | item.getRequerido().indexOf(validador.getTipoServicio()) >= 0)
					// if(item.getArsTag().getRequerido().indexOf(validador.getTipoServicio())>=0)
					// // ESTO DEDE ESTAR ACTIVO
					listita.add(new tags(item.getArsTag().getTag(), Long.toString(item.getArsTag().getId_remedy()),
							item.getArsTag().getFormulario(), item.getRequerido(), item.getArsTag().getTipo(),
							item.getValorDefault()));
		});
		if (usuario.equals("addAtachment")) {
			listita.add(new tags("name", "1000000351", "WL", "A", "S", "N"));
			listita.add(new tags("description", "1000000151", "WL", "A", "S", "N"));
			listita.add(new tags("size", "1000000351", "WL", "A", "I", "N"));
			listita.add(new tags("mimeType", "1000000351", "WL", "A", "S", "N"));
			listita.add(new tags("file", "1000000351", "WL", "A", "S", "N"));
			listita.add(new tags("troubleTicketId", "1000000492", "WL", "A", "S", "N"));
			listita.add(new tags("externalId", "1100000492", "WL", "A", "S", "N"));

		}
		if (validador.getTipoServicio().toUpperCase().compareTo("U") == 0) {

			listita.add(new tags("idremedy", "536890101", "WL", "N", "S", ""));
			listita.add(new tags("SupportGroup", "1000000217", "WL", "N", "S", ""));
		}

		// listita= validador.mapita();
		console.println("************************** TAGS ENCONTRADOS ***********", listita);

	}

	public List<tags> getBuscador() {
		return listita;
	}

	GestionCampoPais campoPais = new GestionCampoPais();

	public ObjectNode getCamposDefault() {
		console.println("entraa en getDefault");
		// campoPais.gestionadorCampoPais(getBuscador());
		console.println("sale de getDefault");
		return campoPais.gestionadorCampoPais(getBuscador());
	}

	public void error() {

		boolean flagprueba = false;

	}

	public static String mapEveryId(Map<String, String> m) {
		List<String> list = new ArrayList<String>(((Map<String, String>) m).keySet());
		List<String> someList = new ArrayList<String>();
		for (int i = 0; i < m.size(); i++) {
			if (list.get(i).contains("�")) {
				String notas[] = list.get(i).split("�");
				someList.add(notas[1]);
			}
			if (list.get(i).contains("~") && !list.get(i).contains("�")) {
				String notas[] = list.get(i).split("~");
				someList.add(notas[1]);

			}
		}

		return null;

	}

	public Map<String, String> mapRequired = new HashMap<>();
	public Map<String, String> noSupport = new HashMap<>();
	public Map<String, String> mapTagIdremedy = new HashMap<>();
	public Map<String, String> mapTagIdremedyopcional = new HashMap<>();

	public Map<String, String> getMapRequired() {
		return mapRequired;
	}

	public Map<String, String> getNoSupport() {
		return noSupport;
	}

	public Map<String, String> getMapTagIdremedy() {
		return mapTagIdremedy;
	}

	public Map<String, String> getMapTagIdremedyOpcional() {
		return mapTagIdremedyopcional;
	}

	public String extraerValorCunaAdmiracion(String valor) {
		// console.println("valor de tag parseado desde
		// extraerValorCu�aAdmiracion..",valor);
		if (valor == null)
			return null;
		if (!valor.contains("!") & !valor.contains("~")) {

			return valor;
		}
		if (valor.contains("!")) {
			String notas[] = valor.split("!");
			return notas[1];
		}
		if (valor.contains("~") && !valor.contains("!")) {
			String notas[] = valor.split("~");
			return notas[1];

		}

		return "";
	}

	public boolean validarNoContengaN(tags tag) {
		return (tag.getRequerido().contentEquals("Q"));

	}

	public List<String> mapEveryIdM(List<tags> listita) {
		List<String> someList = new ArrayList<String>();
		// console.println("LISTA DE LA BASE DE DATOS ",listita);

		for (int i = 0; i < listita.size(); i++) {
			if (!((String) listita.get(i).getNombreTags()).contains("!")
					& !((String) listita.get(i).getNombreTags()).contains("~")) {
				String notas[] = ((String) listita.get(i).getNombreTags()).split("!");
				someList.add(listita.get(i).getNombreTags().toUpperCase());
				if (listita.get(i).getValorDefault() == null)
					mapRequired.put(listita.get(i).getNombreTags(), listita.get(i).getRequerido());
				noSupport.put(listita.get(i).getNombreTags(), listita.get(i).getTipo());

				if (listita.get(i).getRequerido().compareTo("N") < 0)
					mapTagIdremedy.put(listita.get(i).getNombreTags(), listita.get(i).getId_remedy());
				//console.println("yyyyyyyyyyyy222222" + listita.get(i).getNombreTags());
				if (listita.get(i).getRequerido().compareTo(validador.getTipoServicio()) < 0)
					mapTagIdremedyopcional.put(listita.get(i).getNombreTags(), listita.get(i).getId_remedy());

			}
			if (((String) listita.get(i).getNombreTags()).contains("!")) {
				String notas[] = ((String) listita.get(i).getNombreTags()).split("!");

				someList.add((notas[1]).toUpperCase());
				if (listita.get(i).getValorDefault() == null)
					mapRequired.put(notas[1], listita.get(i).getRequerido());
				noSupport.put(notas[1], listita.get(i).getTipo());

				if (listita.get(i).getRequerido().compareTo("N") < 0)
					mapTagIdremedy.put(notas[1], listita.get(i).getId_remedy());
				if (listita.get(i).getRequerido().compareTo(validador.getTipoServicio()) < 0)
					mapTagIdremedyopcional.put(listita.get(i).getNombreTags(), listita.get(i).getId_remedy());

			}
			if (((String) listita.get(i).getNombreTags()).contains("~")
					&& !((String) listita.get(i).getNombreTags()).contains("!")) {
				String notas[] = ((String) listita.get(i).getNombreTags()).split("~");
				someList.add((notas[0]).toUpperCase()); // ESTO SE AGREGO PARA QUE TAMBIEN RECIBA AL PADRE
				someList.add((notas[1]).toUpperCase());
				if (listita.get(i).getValorDefault() == null)
					mapRequired.put(notas[1], listita.get(i).getRequerido());
				noSupport.put(notas[1], listita.get(i).getTipo());

				if (listita.get(i).getRequerido().compareTo("N") < 0)
					mapTagIdremedy.put(notas[1], listita.get(i).getId_remedy());
				if (listita.get(i).getRequerido().compareTo(validador.getTipoServicio()) < 0)
					mapTagIdremedyopcional.put(listita.get(i).getNombreTags(), listita.get(i).getId_remedy());

			}
		}

		console.println("lista de los datos ya parseados ", someList);

		return someList;

	}

}
