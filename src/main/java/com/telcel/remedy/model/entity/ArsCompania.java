package com.telcel.remedy.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ARS_COMPANIA")
public class ArsCompania implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="ID_COMPANIA")
	private long id_compania;
	
	@Column(name="COMPANIA")
	private String compania;
	
	@Column(name="ESTADO")
	private int estado;
	
	public long getId_compania() {
		return id_compania;
	}
	
	public void setId_compania(long id_compania) {
		this.id_compania = id_compania;
	}
	
	public String getCompania() {
		return compania;
	}
	
	public void setCompania(String compania) {
		this.compania = compania;
	}
	
	public int getEstado() {
		return estado;
	}
	
	public void setEstado(int estado) {
		this.estado = estado;
	}
	
	@Override
	public String toString()
	{
		return "ArsCompania:["
				+ "id_compania=" + id_compania
				+ ", compania=" + compania
				+ ", estado=" + estado + "]";
	}
	
}
