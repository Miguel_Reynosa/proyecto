package com.telcel.remedy.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ARS_PAIS")
public class ArsPais implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="ID_PAIS")
	private long id_pais;
	
	@Column(name="PAIS")
	private String pais;
	
	@Column(name="ESTADO")
	private int estado;
	
	@Column(name="PAISINCIDENTE")
	private String paisIncidente;
	
	
	//Getters & Setters
	public long getId_pais() {
		return id_pais;
	}
	
	public void setId_pais(long id_pais) {
		this.id_pais = id_pais;
	}
	
	public String getPais() {
		return pais;
	}
	
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	public int getEstado() {
		return estado;
	}
	
	public void setEstado(int estado) {
		this.estado = estado;
	}
	
	public String getPaisIncidente() {
		return paisIncidente;
	}
	
	public void setPaisIncidente(String paisincidente) {
		this.paisIncidente = paisincidente;
	}
	
	@Override
	public String toString()
	{
		return "ArsPais ["
				+ "id_pais="		+ id_pais
				+ ", pais="			+ pais
				+ ", estado="		+ estado
				+ ", paisIncidente="+ paisIncidente
				+ "]";
	}
	
}
