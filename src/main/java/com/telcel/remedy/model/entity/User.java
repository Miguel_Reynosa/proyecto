package com.telcel.remedy.model.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
@Table(name = "ARS_USER")
@Entity
public class User implements UserDetails, Serializable {
//public class User implements  Serializable {
//public class User {
	    @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    private Long id;
	 
	    @Column(name="USERNAME",nullable = false, unique = true)
	    private String username;
	 
	    @Column(name="PASSWORD")
	    private String password;
	
	    @Column(name="ESTADO")
	    private boolean enabled;
	    
	    
	
		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}

		public String getName() {
			return username;
		}

		public void setName(String name) {
			this.username = name;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			final List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority("User"));
	        return authorities;
		}
		
		@Override
		public boolean isAccountNonExpired() {
			// TODO Auto-generated method stub
			 return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			// TODO Auto-generated method stub
			 return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			// TODO Auto-generated method stub
			 return true;
		}

		@Override
		public boolean isEnabled() {
			// TODO Auto-generated method stub
			return true;
		}
	    
		
	 

}
