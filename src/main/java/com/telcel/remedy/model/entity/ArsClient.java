package com.telcel.remedy.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ARS_CLIENT")
public class ArsClient implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ARC_CLAVE")
	private Long arcClave;
	
	@Column(name = "ARC_ID")
	private String arcId;
	
	@Column(name = "ARC_DESCRIPTION")
	private String arcDescription;
	
	@Column(name = "ARC_STATUS")
	private String arcStatus;
	
	public Long getArcClave() {
		return arcClave;
	}
	
	public void setArcClave(Long arcClave) {
		this.arcClave = arcClave;
	}
	
	public String getArcId() {
		return arcId;
	}
	
	public void setArcId(String arcId) {
		this.arcId = arcId;
	}
	
	public String getArcDescription() {
		return arcDescription;
	}
	
	public void setArcDescription(String arcDescription) {
		this.arcDescription = arcDescription;
	}
	
	public String getArcStatus() {
		return arcStatus;
	}
	
	public void setArcStatus(String arcStatus) {
		this.arcStatus = arcStatus;
	}
	
	@Override
	public String toString()
	{
		return "ArsClient ["
				+ "arcClave=" + arcClave
				+ ", arcId=" + arcId
				+ ", arcDescription=" + arcDescription
				+ ", arcStatus=" + arcStatus
				+ "]";
	}
	
}