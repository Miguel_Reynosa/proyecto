package com.telcel.remedy.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="ARS_GRUPOS")
public class ArsGrupo  implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id	
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="ID_GRUPO")
	private long id_grupo;


	@Column(name="GRUPO_ASIGNADO")
	private String grupo_asignado;
	
	@Column(name="COMPANIA")
	private String compania;

	@Column(name="ORGANIZACION_ASIGNADO")
	private String organizacion_asignado;


	
	@Column(name="COMP_SUP")
	private String comp_sup;

	@Column(name="ID_REM")
	private String id_remedy;
	

	@Column(name="ID_SGP")
	private String id_sgp;
	
	@Column(name="NOMBRE_COMPLETO")
	private String nombre_completo;
	
	@Column(name="LOGIN_ID")
	private String login_id;

	public ArsGrupo() {
		
	}
	

	public ArsGrupo(String compania, String organizacion_asignado,String grupo_asignado, String comp_sup,String id_remedy,String id_sgp) {
	
		this.compania = compania;
		this.grupo_asignado = grupo_asignado;
		this.comp_sup = comp_sup;
		this.id_remedy=id_remedy;
		this.id_sgp=id_sgp;
		this.organizacion_asignado=organizacion_asignado;
		
	}

	/**/

	
	public void setId_grupo(long id_grupo) {
		this.id_grupo = id_grupo;
	}
	
	public long getId_grupo() {
		return id_grupo;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}
	
	public String getCompania() {
		return compania;
	}

	public String getId_remedy() {
		return id_remedy;
	}


	public void setId_remedy(String id_remedy) {
		this.id_remedy = id_remedy;
	}

	public String getGrupo_asignado() {
		return grupo_asignado;
	}

	public void setGrupo_asignado(String grupo_asignado) {
		this.grupo_asignado = grupo_asignado;
	}

	public String getComp_sup() {
		return comp_sup;
	}

	public void setComp_sup(String comp_sup) {
		this.comp_sup = comp_sup;
	}
	

	public String getId_sgp() {
		return id_sgp;
	}


	public void setId_sgp(String id_sgp) {
		this.id_sgp = id_sgp;
	}


	public String getOrganizacion_asignado() {
		return organizacion_asignado;
	}


	public void setOrganizacion_asignado(String organizacion_asignado) {
		this.organizacion_asignado = organizacion_asignado;
	}


	public List<Object> getConcatenarValoresSinSgp(){
		List<Object> concatenador=new ArrayList<>();
		      concatenador.add(getGrupo_asignado());
		      concatenador.add(getCompania());
		      concatenador.add(getOrganizacion_asignado());
		      concatenador.add(getComp_sup()	);
//		      concatenador.add(getUserLogin()	); ESTA EN PENDIENTES POR REVISAR
//		      concatenador.add(getUserAssignee()	); YA QUE MANDA ERROR AL  MOMENTO DE CREAR INCIDENTE

		
		return concatenador;
	}
	public List<Object> getConcatenarValoresConSgp(){
		List<Object> concatenador=new ArrayList<>();
		      concatenador.add(getGrupo_asignado());
		      concatenador.add(getCompania());
		      concatenador.add(getOrganizacion_asignado());
		      concatenador.add(getComp_sup()	);
		      concatenador.add(getId_sgp()	);		

		return concatenador;
	}


	public String getNombre_completo() {
		return nombre_completo;
	}


	public void setNombre_completo(String nombre_completo) {
		this.nombre_completo = nombre_completo;
	}


	public String getLogin_id() {
		return login_id;
	}


	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}


	@Override
	public String toString()
	{
		return "ArsGrupo ["
				+ "id_grupo="				+ id_grupo
				+ ", grupo_asignado="		+ grupo_asignado
				+ ", compania="				+ compania
				+ ", organizacion_asignado="+ organizacion_asignado
				+ ", comp_sup="				+ comp_sup
				+ ", id_remedy="			+ id_remedy
				+ ", id_sgp="				+ id_sgp
				+ ", nombre_completo="		+ nombre_completo
				+ ", login_id="				+ login_id
				+ "]";
	}
	
}
