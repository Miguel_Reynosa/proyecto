package com.telcel.remedy.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ARS_FIELDS")
public class ArsFields implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	private Long arfAreClave;
	private Long arfFieldId;
	private String arfFieldName;
	private String arfDescription;
	private String arfDefaultValue;
	private String arfStatus;
	
	
	public Long getArfAreClave() {
		return arfAreClave;
	}
	public void setArfAreClave(Long arfAreClave) {
		this.arfAreClave = arfAreClave;
	}
	public Long getArfFieldId() {
		return arfFieldId;
	}
	public void setArfFieldId(Long arfFieldId) {
		this.arfFieldId = arfFieldId;
	}
	public String getArfFieldName() {
		return arfFieldName;
	}
	public void setArfFieldName(String arfFieldName) {
		this.arfFieldName = arfFieldName;
	}
	public String getArfDescription() {
		return arfDescription;
	}
	public void setArfDescription(String arfDescription) {
		this.arfDescription = arfDescription;
	}
	public String getArfDefaultvalue() {
		return arfDefaultValue;
	}
	public void setArfDefaultvalue(String arfDefaultvalue) {
		this.arfDefaultValue = arfDefaultvalue;
	}
	public String getArfStatus() {
		return arfStatus;
	}
	public void setArfStatus(String arfStatus) {
		this.arfStatus = arfStatus;
	}
	
	
}
