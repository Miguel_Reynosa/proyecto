package com.telcel.remedy.model.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ARS_USERS")
public class ArsUsers implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ARU_CLAVE")
	private Long aruClave;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ARU_ARC_CLAVE")
	
	private ArsClient aruArcClave;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ARU_CSR_CLAVE")
		private CtgServidores aruCsrClave;
	
	@Column(name = "ARU_USR")
	private String aruUsr;
	@Column(name = "ARU_PWD")
	private String aruPwd;

	public Long getAruClave() {
		return aruClave;
	}

	public void setAruClave(Long aruClave) {
		this.aruClave = aruClave;
	}

	public String getAruUsr() {
		return aruUsr;
	}

	public void setAruUsr(String aruUsr) {
		this.aruUsr = aruUsr;
	}

	public String getAruPwd() {
		return aruPwd;
	}

	public void setAruPwd(String aruPwd) {
		this.aruPwd = aruPwd;
	}

	public ArsClient getAruArcClave() {
		return aruArcClave;
	}

	public void setAruArcClave(ArsClient aruArcClave) { 
		this.aruArcClave = aruArcClave;
	}

	public CtgServidores getAruCsrClave() {
		return aruCsrClave;
	}

	public void setAruCsrClave(CtgServidores aruCsrClave) {
		this.aruCsrClave = aruCsrClave;
	}

	@Override
	public String toString() {
		return "ArsUsers [aruClave=" + aruClave + ", aruArcClave=" + aruArcClave + ", aruCsrClave=" + aruCsrClave
				+ ", aruUsr=" + aruUsr + ", aruPwd=" + aruPwd + "]";
	}
	
	
	

}
