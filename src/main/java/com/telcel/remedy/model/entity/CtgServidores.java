package com.telcel.remedy.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CTG_SERVIDORES")
public class CtgServidores implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "CSR_CLAVE")
	private Long csrClave;
	@Column(name = "CSR_NOMBRE")
	private String csrNombre;
	@Column(name = "CSR_ID")
	private String csrId;
	@Column(name = "CSR_EQP")
	private String csrEqp;
	@Column(name = "CSR_PRT")
	private String csrPrt;
	@Column(name = "CSR_USR")
	private String csrUsr;
	@Column(name = "CSR_PWD")
	private String csrPwd;
	@Column(name = "CSR_ACTIVO")
	private String csrActivo;
	
	
	public Long getCsrClave() {
		return csrClave;
	}
	public void setCsrClave(Long csrClave) {
		this.csrClave = csrClave;
	}
	public String getCsrNombre() {
		return csrNombre;
	}
	public void setCsrNombre(String csrNombre) {
		this.csrNombre = csrNombre;
	}
	public String getCsrId() {
		return csrId;
	}
	public void setCsrId(String csrId) {
		this.csrId = csrId;
	}
	public String getCsrEqp() {
		return csrEqp;
	}
	public void setCsrEqp(String csrEqp) {
		this.csrEqp = csrEqp;
	}
	public String getCsrPrt() {
		return csrPrt;
	}
	public void setCsrPrt(String csrPrt) {
		this.csrPrt = csrPrt;
	}
	public String getCsrUsr() {
		return csrUsr;
	}
	public void setCsrUsr(String csrUsr) {
		this.csrUsr = csrUsr;
	}
	public String getCsrPwd() {
		return csrPwd;
	}
	public void setCsrPwd(String csrPwd) {
		this.csrPwd = csrPwd;
	}
	public String getCsrActivo() {
		return csrActivo;
	}
	public void setCsrActivo(String csrActivo) {
		this.csrActivo = csrActivo;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "CtgServidores [csrClave=" + csrClave + ", csrNombre=" + csrNombre + ", csrId=" + csrId + ", csrEqp="
				+ csrEqp + ", csrPrt=" + csrPrt + ", csrUsr=" + csrUsr + ", csrPwd=" + csrPwd + ", csrActivo="
				+ csrActivo + "]";
	}
	
	
}
