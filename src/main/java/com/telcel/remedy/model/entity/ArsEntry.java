package com.telcel.remedy.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ARS_ENTRY")
public class ArsEntry implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	private Long areClave;
	private String areFormName;
	private String areIfaceCreate;
	private String areRelationship;
	private String areCrudAction;
	private Long areKey;
	private String areDatasource;
	private String areIfaceUpdate;
	
	public Long getAreClave() {
		return areClave;
	}
	public void setAreClave(Long areClave) {
		this.areClave = areClave;
	}
	public String getAreFormName() {
		return areFormName;
	}
	public void setAreFormName(String areFormName) {
		this.areFormName = areFormName;
	}
	public String getAreIfaceCreate() {
		return areIfaceCreate;
	}
	public void setAreIfaceCreate(String areIfaceCreate) {
		this.areIfaceCreate = areIfaceCreate;
	}
	public String getAreRelationship() {
		return areRelationship;
	}
	public void setAreRelationship(String areRelationship) {
		this.areRelationship = areRelationship;
	}
	public String getAreCrudAction() {
		return areCrudAction;
	}
	public void setAreCrudAction(String areCrudAction) {
		this.areCrudAction = areCrudAction;
	}
	public Long getAreKey() {
		return areKey;
	}
	public void setAreKey(Long areKey) {
		this.areKey = areKey;
	}
	public String getAreDatasource() {
		return areDatasource;
	}
	public void setAreDatasource(String areDatasource) {
		this.areDatasource = areDatasource;
	}
	public String getAreIdaceUpdate() {
		return areIfaceUpdate;
	}
	public void setAreIdaceUpdate(String areIdaceUpdate) {
		this.areIfaceUpdate = areIdaceUpdate;
	}
	
	
}
