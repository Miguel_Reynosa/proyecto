package com.telcel.remedy.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ARS_TAG")
public class ArsTag implements Serializable
{
	private static final long serialVersionUID =1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="ID_TAG")
	private long id_tag;
	
	@Column(name="TAG")
	private String tag;
	
	@Column(name="ID_REMEDY")
	private long id_remedy;
	
	@Column(name="FORMULARIO")
	private String formulario;

	@Column(name="TIPO")
	private String tipo;
	
	
	
	//Getters & Setters
	public long getId_tag() {
		return id_tag;
	}
	
	public void setId_tag(long id_tag) {
		this.id_tag = id_tag;
	}
	
	public String getTag() {
		return tag;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	public long getId_remedy() {
		return id_remedy;
	}
	
	public void setId_remedy(long id_remedy) {
		this.id_remedy = id_remedy;
	}
	
	public String getFormulario() {
		return formulario;
	}
	
	public void setFormulario(String formulario) {
		this.formulario = formulario;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	@Override
	public String toString() {
		return "\nArsTag ["
				+ "id_tag="			+ id_tag
				+ ", tag="			+ tag
				+ ", id_remedy="	+ id_remedy
				+ ", formulario="	+ formulario
				+ ", tipo="			+ tipo
				+ "]";
	}

}
