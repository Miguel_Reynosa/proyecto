package com.telcel.remedy.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ARS_CLIENT_FIELDS")
public class ArsClientField {
	
	@Id
	private Long acfArcClave;
	private Long acfAreClave;
	private Long acfFieldId;
	private String acfFieldName;
	private String acfDescription;
	private String acfDefaultValue;
	private String acfStatus;
	public Long getAcfArcClave() {
		return acfArcClave;
	}
	public void setAcfArcClave(Long acfArcClave) {
		this.acfArcClave = acfArcClave;
	}
	public Long getAcfAreClave() {
		return acfAreClave;
	}
	public void setAcfAreClave(Long acfAreClave) {
		this.acfAreClave = acfAreClave;
	}
	public Long getAcfFieldId() {
		return acfFieldId;
	}
	public void setAcfFieldId(Long acfFieldId) {
		this.acfFieldId = acfFieldId;
	}
	public String getAcfFielName() {
		return acfFieldName;
	}
	public void setAcfFielName(String acfFielName) {
		this.acfFieldName = acfFielName;
	}
	public String getAcfDescription() {
		return acfDescription;
	}
	public void setAcfDescription(String acfDescription) {
		this.acfDescription = acfDescription;
	}
	public String getAcfDefauldValue() {
		return acfDefaultValue;
	}
	public void setAcfDefauldValue(String acfDefauldValue) {
		this.acfDefaultValue = acfDefauldValue;
	}
	public String getAcfStatus() {
		return acfStatus;
	}
	public void setAcfStatus(String acfStatus) {
		this.acfStatus = acfStatus;
	}
	
	
}
