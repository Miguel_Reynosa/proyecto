package com.telcel.remedy.model.entity;

import java.io.Serializable;
import java.util.stream.Stream;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ARS_PAISCOMPANIA")
public class ArsPaisCompania implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	
	@Column(name="ID_PAISCOMPANIA")
	private long id_paiscompania;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_PAIS", nullable=false)
	private ArsPais arsPais;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_COMPANIA", nullable=false)
	private ArsCompania arsCompania;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_CLIENT", nullable=false)
	private ArsClient arsClient;
	
	
	
	//Getters & Setters
	public long getId_paiscompania() {
		return id_paiscompania;
	}

	public void setId_paiscompania(long id_paiscompania) {
		this.id_paiscompania = id_paiscompania;
	}

	//Con JOIN
	public ArsCompania getArsCompania() {
		return arsCompania;
	}

	public void setArsCompania(ArsCompania arsCompania) {
		this.arsCompania = arsCompania;
	}

	public ArsPais getArsPais() {
		return arsPais;
	}

	public void setArsPais(ArsPais arsPais) {
		this.arsPais = arsPais;
	}

	public ArsClient getArsClient() {
		return arsClient;
	}

	public void setArsClient(ArsClient arsClient) {
		this.arsClient = arsClient;
	}

	@Override
	public String toString()
	{
		return "ArsPaisCompania ["
				+ "id_paiscompania="+ id_paiscompania
				+ ", arsPais="		+ arsPais
				+ ", arsCompania="	+ arsCompania
				+ ", arsClient="	+ arsClient
				+ "]";
	}
}
