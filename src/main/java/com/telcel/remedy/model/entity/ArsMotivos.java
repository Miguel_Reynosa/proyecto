package com.telcel.remedy.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ARS_MOTIVOS")
public class ArsMotivos implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="ID_MOTIVO")
	private long id_motivo;
	
	@Column(name="MOTIVOS")
	private String motivos;

	public ArsMotivos() {
		
	}
	
	public ArsMotivos( String motivo) {
		this.motivos = motivo;
	}

	public long getId_motivo() {
		return id_motivo;
	}

	public void setId_motivo(long id_motivo) {
		this.id_motivo = id_motivo;
	}

	public String getMotivos() {
		return motivos;
	}

	public void setMotivos(String motivos) {
		this.motivos = motivos;
	}
	

	@Override
	public String toString()
	{
		return "ArsMotivo [id_motivo=" + id_motivo + ", motivos=" + motivos + "]";
	}
	
}
