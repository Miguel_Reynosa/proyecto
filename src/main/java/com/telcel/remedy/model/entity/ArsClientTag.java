package com.telcel.remedy.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ARS_CLIENTTAG")
public class ArsClientTag implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="ID_CLIENTTAG")
	private long id_clienttag;
	
	/*
	@Column(name="ID_TAG")
	private long id_tag;
	/**/
	
	/*
	@Column(name="ID_PAISCOMPANIA")
	private long id_paiscompania;
	/**/
	
	/*JOIN ArsTag*/
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TAG",nullable=false)
	private ArsTag arsTag;
	/**/
	
	/*JOIN con ArsPaisCompania por ID_PAISCOMPANIA*/
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_PAISCOMPANIA",nullable=false)
	private ArsPaisCompania arsPaisCompania;
	
	@Column(name="VALORDEFAULT")
	private String valorDefault;
	
	@Column(name="REQUERIDO")
	private String requerido;
	
	
	/**/
	
	//Getters & Setters
	public long getId_clienttag() {
		return id_clienttag;
	}

	public void setId_clienttag(long id_clienttag) {
		this.id_clienttag = id_clienttag;
	}
	
	/*
	public long getId_tag() {
		return id_tag;
	}

	public void setId_tag(long id_tag) {
		this.id_tag = id_tag;
	}
	
	public long getId_paiscompania() {
		return id_paiscompania;
	}

	public void setId_paiscompania(long id_paiscompania) {
		this.id_paiscompania = id_paiscompania;
	}/**/
	
	/*	Con JOIN  */
	public ArsTag getArsTag() {
		return arsTag;
	}

	public void setArsTag(ArsTag arsTag) {
		this.arsTag = arsTag;
	}
	
	public ArsPaisCompania getArsPaisCompania() {
		return arsPaisCompania;
	}

	public void setArsPaisCompania(ArsPaisCompania arsPaisCompania) {
		this.arsPaisCompania = arsPaisCompania;
	}/**/
	
	

	public String getValorDefault() {
		return valorDefault;
	}

	public void setValorDefault(String valorDefault) {
		this.valorDefault = valorDefault;
	}
	

	public String getRequerido() {
		return requerido;
	}

	public void setRequerido(String requerido) {
		this.requerido = requerido;
	}

	@Override
	public String toString() {
		return "ArsClientTag [id_clienttag=" + id_clienttag
				+ ", arsTag=" + arsTag
				+ ", arsPaisCompania=" + arsPaisCompania
				+ ", valorDefault=" + valorDefault
				+ ", requerido=" + requerido
				+ "]";
	}


	
	
	
}
