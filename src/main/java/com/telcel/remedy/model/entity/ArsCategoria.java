package com.telcel.remedy.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="ARS_CATEGORIA")
public class ArsCategoria  implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="ID_CAT")
	private Long id_cat;
	
	@Column(name="NOTAS")
	private String notas;
	
	@Column(name="RESUMEN")
	private String resumen;
	
	@Column(name="ID_REM")
	private String id_remedy;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_GRUPO", nullable=false)
	private ArsGrupo grupo;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_GPOLOC", nullable=true)
	private ArsGrupo grupoLocal;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_MOTIVO", nullable=false)
	private ArsMotivos motivo;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_PC", nullable=false)
	private ArsPaisCompania paisCompania;
	
	@Column(name="ID")
	private String id;
	
	@Column(name="ESCALA")
	private String escala;
	
	
	//GETTERS & SETTERS
	public Long getId_cat() {
		return id_cat;
	}

	public void setId_cat(Long id_cat) {
		this.id_cat = id_cat;
	}
	
	public String getNotas() {
		return notas;
	}
	
	public void setNotas(String notas) {
		this.notas = notas;
	}
	
	public String getResumen() {
		return resumen;
	}
	
	public void setResumen(String resumen) {
		this.resumen = resumen;
	}
	
	public String getId_remedy() {
		return id_remedy;
	}
	
	public void setId_remedy(String id_remedy) {
		this.id_remedy = id_remedy;
	}
	
	public ArsGrupo getGrupo() {
		return grupo;
	}
	
	public void setGrupo(ArsGrupo grupo) {
		this.grupo = grupo;
	}
	
	public ArsGrupo getGrupoLocal() {
		return grupoLocal;
	}
	
	public void setGrupoLocal(ArsGrupo grupoLocal) {
		this.grupoLocal = grupoLocal;
	}
	
	public ArsMotivos getMotivo() {
		return motivo;
	}
	
	public void setMotivo(ArsMotivos motivo) {
		this.motivo = motivo;
	}
	
	public ArsPaisCompania getPaisCompania() {
		return paisCompania;
	}
	
	public void setPaisCompania(ArsPaisCompania paisCompania) {
		this.paisCompania = paisCompania;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getEscala() {
		return escala;
	}
	
	public void setEscala(String escala) {
		this.escala = escala;
	}
	

	@Override
	public String toString()
	{
		return "ArsCategoria ["
				+ "id_cat="			+ id_cat
				+ ", notas="		+ notas
				+ ", resumen="		+ resumen
				+ ", id_remedy="	+ id_remedy
				+ ", grupo="		+ grupo
				+ ", grupoLocal="	+ grupoLocal
				+ ", motivo="		+ motivo
				+ ", paisCompania="	+ paisCompania
				+ ", id="			+ id
				+ ", escala="		+ escala
				+ "]";
	}
	
}
