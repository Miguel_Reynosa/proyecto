package com.telcel.remedy.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ARS_FORMULARIO")
public class ArsFormulario implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_FORMULARIO")
	private Long id_formulario;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_PAISCOMPANIA",nullable=false)
	private ArsPaisCompania paisCompania;
	
	@Column(name="FORMULARIO")
	private String formulario;
	
	@Column(name="CRUD_ACTION")
	private String crudAction;

	
	//GETTERS & SETTERS
	public Long getId_formulario() {
		return id_formulario;
	}

	public void setId_formulario(Long id_formulario) {
		this.id_formulario = id_formulario;
	}

	public ArsPaisCompania getPaisCompania() {
		return paisCompania;
	}

	public void setPaisCompania(ArsPaisCompania paisCompania) {
		this.paisCompania = paisCompania;
	}

	public String getFormulario() {
		return formulario;
	}

	public void setFormulario(String formulario) {
		this.formulario = formulario;
	}

	public String getCrudAction() {
		return crudAction;
	}

	public void setCrudAction(String crudAction) {
		this.crudAction = crudAction;
	}
	
	
	
	@Override
	public String toString()
	{
		return "ArsFormulario [id_formulario=" + id_formulario
				+ ", paisCompania=" + paisCompania
				+ ", formulario=" + formulario
				+ ", crudAction=" + crudAction
				+ "]";
	}
}
