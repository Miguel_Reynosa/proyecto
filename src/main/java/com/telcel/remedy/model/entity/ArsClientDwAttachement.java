package com.telcel.remedy.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ARS_CLIENT_DW_ATTACHEMENT")
public class ArsClientDwAttachement implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	@Id
	@Column(name = "ACD_ARC_CLAVE")
	private Long acdArcClave;
	@Column(name = "ACD_ARE_CLAVE")
	private Long acdAreClave;
	@Column(name = "ACD_FIELD_ID")
	private Long acdFielId;
	@Column(name = "ACD_DESCRIPTION")
	private String acdDescription;
	@Column(name = "ACD_FIELD_DW")
	private String  acdFielDw;
	
	
	public Long getAcdArcClave() {
		return acdArcClave;
	}
	public void setAcdArcClave(Long acdArcClave) {
		this.acdArcClave = acdArcClave;
	}
	public Long getAcdAreClave() {
		return acdAreClave;
	}
	public void setAcdAreClave(Long acdAreClave) {
		this.acdAreClave = acdAreClave;
	}
	public Long getAcdFielId() {
		return acdFielId;
	}
	public void setAcdFielId(Long acdFielId) {
		this.acdFielId = acdFielId;
	}
	public String getAcdDescription() {
		return acdDescription;
	}
	public void setAcdDescription(String acdDescription) {
		this.acdDescription = acdDescription;
	}
	public String getAcdFielDw() {
		return acdFielDw;
	}
	public void setAcdFielDw(String acdFielDw) {
		this.acdFielDw = acdFielDw;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
