package com.telcel.remedy.model.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//import org.assert.util.Arrays;

import com.google.common.collect.Maps;

@Entity
@Table(name="ARS_URGENCY")
public class ArsUrgency implements Serializable
{
	private static final long serialVersionUID =1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="ID")
	private long id;
	
//	@ManyToOne(fetch=FetchType.EAGER)
//	@JoinColumn(name="ID_PAISCOMPANIA", nullable=false)
//	private ArsPaisCompania id_paisCompania;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_PAISCOMPANIA",nullable=false)
	private ArsPaisCompania paisCompania;
	
	@Column(name="VALOR")
	private String valor;
	
	@Column(name="NUMERO")
	private Integer numero;

	//Setters And Getters
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ArsPaisCompania getPaisCompania() {
		return paisCompania;
	}

	public void setPaisCompania(ArsPaisCompania paisCompania) {
		this.paisCompania = paisCompania;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
	public  List<Object> getValores() {
        return Arrays.asList(valor.split(";"));
	}
	public void mapaNumeroValor() {
	        Map<Integer,String> nuevoMapa=new HashMap<>();
	        
	        nuevoMapa.put((Integer)getNumero(), getValores().get(1).toString());
	}

	
	@Override
	public String toString() {
		return "\nArsUrgency ["
				+ "id=" + id
				+ ", paisCompania=" + paisCompania
				+ ", valor=" + valor
				+ ", numero=" + numero
				+ "]";
	}
	
	
}
