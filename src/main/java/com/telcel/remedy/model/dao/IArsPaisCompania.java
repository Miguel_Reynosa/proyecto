package com.telcel.remedy.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telcel.remedy.model.entity.ArsPaisCompania;
import com.telcel.remedy.model.entity.ArsPais;
import com.telcel.remedy.model.entity.ArsCompania;

public interface IArsPaisCompania extends CrudRepository<ArsPaisCompania, Long> {
	
	//ByIdCompania
	@Query("select P from ArsPaisCompania as P "
			+ "where P.arsCompania.id_compania=:psAlias")
	public List<ArsPaisCompania> findByIdCompania(@Param("psAlias")Long psAlias);
	
	//ByEstadoCompania
	@Query("select P from ArsPaisCompania as P "
			+ "where P.arsCompania.estado=:psAlias")
	public List<ArsPaisCompania> findByEstadoCompania(@Param("psAlias")Integer psAlias);
	
	//ByIdPais
	@Query("select P from ArsPaisCompania as P "
			+ "where P.arsPais.id_pais=:psAlias")
	public List<ArsPaisCompania> findByIdPais(@Param("psAlias")Long psAlias);
	
	//ByEstadoPais
	@Query("select P from ArsPaisCompania as P "
			+ "where P.arsPais.estado=:psAlias")
	public List<ArsPaisCompania> findByEstadoPais(@Param("psAlias")Integer psAlias);
	
//******************************creado/07/11
	//ByCompaniaPais
	@Query("select P from ArsPaisCompania as P "
			+ "where P.arsCompania.compania=:psAlias and P.arsPais.pais=:psAlias")
	public ArsPaisCompania findByIdCompaniaIdPais(@Param("psAlias") String psAlias);
//	public ArsPaisCompania findByIdCompania(@Param("psAlias")Long psAlias);

//***************EJEMPLO SUB-SUB-CONSULTA	
//	select * from table1 where number in (
//			  select jboss2 from table2 where jboss1 in (
//			    select number from table1 where name = 'nam1'))
//******************************************	
	
	//***********creado/06/11
//	public ArsPaisCompania findTodo();
	//***************
	
	
	
	
	/*
	 * 
	 * 	//Pais
	@Query("select id_pais from ArsPaisCompania"
			+"where P.arsPais.pais=:pais")
	public ArsPaisCompania findByPais(@Param("pais") String pais);
	 * 
	 * 
	 */

	@Query("select PC from ArsPaisCompania as PC where PC.arsCompania.compania=:compania")
	public ArsPaisCompania findByCompania(@Param("compania")String compania);
}
