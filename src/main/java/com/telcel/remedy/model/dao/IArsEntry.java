package com.telcel.remedy.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.telcel.remedy.model.entity.ArsEntry;

public interface IArsEntry extends CrudRepository<ArsEntry, Long>{

}
