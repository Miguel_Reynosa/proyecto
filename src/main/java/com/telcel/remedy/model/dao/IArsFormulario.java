package com.telcel.remedy.model.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telcel.remedy.model.entity.ArsFormulario;

public interface IArsFormulario  extends CrudRepository<ArsFormulario, Long>
{
	@Query("select F from ArsFormulario as F where F.paisCompania.arsCompania.compania=:compania and F.formulario=:formulario")
	public ArsFormulario findByCompAndForm(@Param("compania")String compania, @Param("formulario")String formulario);
}
