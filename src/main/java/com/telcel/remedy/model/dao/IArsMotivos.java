package com.telcel.remedy.model.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telcel.remedy.model.entity.ArsMotivos;

public interface IArsMotivos extends CrudRepository<ArsMotivos, Long>
{
	@Query("select M from ArsMotivos as M where M.motivos like :motivos")
	public ArsMotivos findByMotivos(@Param("motivos")String motivos);
	
}
