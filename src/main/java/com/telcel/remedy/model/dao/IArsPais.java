package com.telcel.remedy.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telcel.remedy.model.entity.ArsPais;

public interface IArsPais extends CrudRepository<ArsPais, Long>
{
	@Query("select P from ArsPais as P where P.estado=:psAlias")
	public List<ArsPais> findByEstado(@Param("psAlias") Integer psAlias);
	
	@Query("select P from ArsPais as P where P.paisIncidente=:paisIncidente")
	public ArsPais findByPaisIncidente(@Param("paisIncidente")String paisIncidente);
	
	@Query("select P from ArsPais as P where P.pais=:pais")
	public ArsPais findByPais(@Param("pais")String pais);

}
