package com.telcel.remedy.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.telcel.remedy.model.entity.CtgServidores;

public interface ICtgServidores extends CrudRepository<CtgServidores, Long>{

}
