package com.telcel.remedy.model.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telcel.remedy.model.entity.ArsUsers;

public interface IArsUsers extends CrudRepository<ArsUsers, Long>{
	
	@Query("select u from ArsUsers u join ArsClient c on u.aruClave= c.arcClave where c.arcId=:psAlias") 
	public ArsUsers findByAlias(@Param("psAlias") String psAlias);
	
}
