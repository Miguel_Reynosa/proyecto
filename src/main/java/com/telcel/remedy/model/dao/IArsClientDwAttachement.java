package com.telcel.remedy.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.telcel.remedy.model.entity.ArsClientDwAttachement;

public interface IArsClientDwAttachement extends CrudRepository<ArsClientDwAttachement, Long>{
	
}
