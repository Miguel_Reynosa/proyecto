package com.telcel.remedy.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.telcel.remedy.model.entity.ArsClientField;

public interface IArsClientField  extends CrudRepository<ArsClientField, Long>{

}
