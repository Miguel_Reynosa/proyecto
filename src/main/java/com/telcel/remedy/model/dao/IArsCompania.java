package com.telcel.remedy.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telcel.remedy.model.entity.ArsCompania;

public interface IArsCompania extends CrudRepository<ArsCompania, Long> {
	
	@Query("from ArsCompania as Com where Com.id.estado=:psAlias")
	public List<ArsCompania> findByEstado(@Param("psAlias")Integer psAlias);

	@Query("select C from ArsCompania as C where C.compania=:compania")
	public ArsCompania findByCompania(@Param("compania")String compania);
}
