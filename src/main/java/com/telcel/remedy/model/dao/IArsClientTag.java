package com.telcel.remedy.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telcel.remedy.model.entity.ArsClientTag;
import com.telcel.remedy.model.entity.ArsPaisCompania;

public interface IArsClientTag extends CrudRepository<ArsClientTag, Long>
{
	/*ByIdTag*/
	@Query("select C from ArsClientTag as C "
			+ "where C.arsTag.id_tag=:psAlias")
	public ArsClientTag findByIdTag(@Param("psAlias")Long psAlias);
	/**/
	
	/*ByIdRemedy*/
	@Query("select C from ArsClientTag as C "
			+ "where C.arsTag.id_remedy=:psAlias")
	public ArsClientTag findByIdRemedy(@Param("psAlias")Long psAlias);
	/**/
	
	/*ByIdCompania*/
	@Query("select C from ArsClientTag as C "
			+ "where C.arsPaisCompania.id_paiscompania=:psAlias")
	public List<ArsClientTag> findByIdPaisCompania(@Param("psAlias")Long psAlias);
	/**/
	
	/*ByIdCompania*/
	@Query("select C from ArsClientTag as C "
			+ "where C.arsPaisCompania.arsCompania.id_compania=:psAlias")
	public ArsClientTag findByIdCompania(@Param("psAlias")Long psAlias);
	/**/
	
	/*ByEstadoCompania*/
	@Query("select C from ArsClientTag as C "
			+ "where C.arsPaisCompania.arsCompania.estado=:psAlias")
	public List<ArsClientTag> findByEstadoCompania(@Param("psAlias")Integer psAlias);
	/**/
	
	/*ByIdPais*/
	@Query("select C from ArsClientTag as C "
			+ "where C.arsPaisCompania.arsPais.id_pais=:psAlias")
	public List<ArsClientTag> findByIdPais(@Param("psAlias")Long psAlias);
	/**/
	
	/*ByEstadoPais*/
	@Query("select C from ArsClientTag as C "
			+ "where C.arsPaisCompania.arsPais.estado=:psAlias")
	public List<ArsClientTag> findByEstadoPais(@Param("psAlias")Integer psAlias);
	/**/
	//public  List<ArsClientTag> findByArsPaisCompania(ArsPaisCompania pais);
	
	//Iterable<ArsClientTag> findByArsPaisCompania();
	
	
//	@Query("select C from ArsClientTag as C where C.arsPaisCompania.arsCompania.compania=:compania and C.arsTag.tag")
//	public List<ArsClientTag> findByCompaniaAndTag(@Param("compania")String compania, @Param("tag")String tag);
	
}
