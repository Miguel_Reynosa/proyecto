package com.telcel.remedy.model.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telcel.remedy.model.entity.ArsCategoria;
import com.telcel.remedy.model.entity.ArsClientTag;
import com.telcel.remedy.model.entity.ArsGrupo;

public interface IArsGrupos  extends CrudRepository<ArsGrupo, Long>
{
	@Query("select G from ArsGrupo as G where G.grupo_asignado=:grupo_asignado and G.compania=:compania")
	public ArsGrupo getByGrupoCompania(@Param("grupo_asignado")String grupo_asignado, @Param("compania")String compania);
}
