package com.telcel.remedy.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telcel.remedy.model.entity.ArsTag;

public interface IArsTag extends CrudRepository<ArsTag, Long>
{
	@Query("select T from ArsTag as T "
			+ "where T.id_remedy=:psAlias")
	public ArsTag findByIdRemedy(@Param("psAlias") Long psAlias);
	
	@Query("select T from ArsTag as T where T.tag=:tag")
	public ArsTag findByTag(@Param("tag") String tag);
	
	@Query("select T from ArsTag as T where T.formulario=:formulario")
	public List<ArsTag> findTagsByFormulario(@Param("formulario")String formulario);
	
}
