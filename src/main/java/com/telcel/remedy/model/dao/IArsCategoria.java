package com.telcel.remedy.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telcel.remedy.model.entity.ArsCategoria;

public interface IArsCategoria extends CrudRepository<ArsCategoria, Long>
{
	@Query("select C from ArsCategoria as C where C.id=:id")
	public List<ArsCategoria> findListById(@Param("id")String id);
	
	@Query("select C from ArsCategoria as C where C.paisCompania.arsPais.pais=:pais")
	public List<ArsCategoria> findListByPais(@Param("pais")String pais);
	
	@Query("select C from ArsCategoria as C where C.paisCompania.arsCompania.compania=:compania")
	public List<ArsCategoria> findListByCompania(@Param("compania")String compania);
	
	@Query("select C from ArsCategoria as C where C.id=:id and C.paisCompania.arsCompania.compania=:compania")
	public ArsCategoria findByIdAndCompania(@Param("id")String id, @Param("compania")String compania);
	
}
