package com.telcel.remedy.model.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telcel.remedy.model.entity.ArsClient;
import com.telcel.remedy.model.entity.ArsPais;

public interface IArsClient extends CrudRepository<ArsClient, Long>{

	ArsClient findByArcId(String arcId);
//	public List<ArsPais> findByEstado(@Param("psAlias") Integer psAlias);

	
}
