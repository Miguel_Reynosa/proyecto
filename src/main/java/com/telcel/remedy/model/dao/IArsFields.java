package com.telcel.remedy.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.telcel.remedy.model.entity.ArsFields;

public interface IArsFields extends CrudRepository<ArsFields, Long>{

}
