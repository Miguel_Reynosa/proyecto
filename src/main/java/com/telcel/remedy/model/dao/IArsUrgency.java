package com.telcel.remedy.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.telcel.remedy.model.entity.ArsUrgency;

public interface IArsUrgency extends CrudRepository<ArsUrgency, Long>
{
	@Query("select U from ArsUrgency as U where U.valor=:valor")
	public List<ArsUrgency> findByValor(@Param("valor")String valor);
	
	@Query("select U from ArsUrgency as U where U.paisCompania.arsCompania.compania=:compania and U.valor=:valor")
	public ArsUrgency findByCompaniaAndValor(@Param("compania")String compania, @Param("valor")String valor);
}