package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsUsers;

public interface IArsUsersService  {
	public List<ArsUsers> findAll();
	public ArsUsers findById(Long id);
	public ArsUsers save(ArsUsers voClient);
	public void delete(Long id);
	public ArsUsers findByAlias(String psAlias);
}
