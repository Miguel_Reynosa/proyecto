package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.telcel.remedy.model.dao.IArsClient;
import com.telcel.remedy.model.entity.ArsClient;

@Service
public class ArsClientServiceImpl implements IArsClientService{
	@Autowired
	IArsClient voArsClientDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<ArsClient> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsClient>) voArsClientDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public ArsClient findById(Long id) {
		// TODO Auto-generated method stub
		return voArsClientDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public ArsClient save(ArsClient voClient) {
		// TODO Auto-generated method stub
		return voArsClientDao.save(voClient);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voArsClientDao.deleteById(id);
	}

	/*
	 * 
	@Override
	public ArsClient findByClient(String client) {
		// TODO Auto-generated method stub
		return null;
	}

	 */
	//*************cambio/06/11
	@Override
	@Transactional
	public ArsClient findByArcId(String arcId) {
		// TODO Auto-generated method stub
		return voArsClientDao.findByArcId(arcId);
	}
	//*************

}
