package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telcel.remedy.model.dao.IArsClientField;
import com.telcel.remedy.model.entity.ArsClientField;

@Service
public class ArsClientFieldServiceImpl implements IArsClientFieldService{
	
	@Autowired
	IArsClientField voField;

	@Override
	public List<ArsClientField> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsClientField>)voField.findAll();
	}

	@Override
	public ArsClientField findById(Long id) {
		// TODO Auto-generated method stub
		return voField.findById(id).orElse(null);
	}

	@Override
	public ArsClientField save(ArsClientField voArsClientFiel) {
		// TODO Auto-generated method stub
		return voField.save(voArsClientFiel);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voField.deleteById(id);
	}
	
}
