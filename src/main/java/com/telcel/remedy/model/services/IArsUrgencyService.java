package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsUrgency;

public interface IArsUrgencyService
{
	public List<ArsUrgency>	findAll();
	public ArsUrgency		findById(Long id);
	public ArsUrgency		save (ArsUrgency urgency );
	public void delete (Long id);
	
	public List<ArsUrgency> findByValor(String valor);
	
	public ArsUrgency findByCompaniaAndValor(String compania, String valor);
}
