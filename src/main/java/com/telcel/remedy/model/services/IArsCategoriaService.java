	package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsCategoria;

public interface IArsCategoriaService
{
	public List<ArsCategoria> findAll();	
	public ArsCategoria findById(Long id);
	public ArsCategoria save(ArsCategoria voClientFiel);
	public void delete(Long id);
	
	public List<ArsCategoria> findListByIdCat(String id);
	public List<ArsCategoria> findListByPais(String pais);
	public List<ArsCategoria> findListByCompania(String compania);
	
	public ArsCategoria findByIdAndCompania(String id, String compania);
	
}
