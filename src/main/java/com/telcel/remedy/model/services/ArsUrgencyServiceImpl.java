package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.telcel.remedy.model.dao.IArsUrgency;
import com.telcel.remedy.model.entity.ArsUrgency;


@Service
public class ArsUrgencyServiceImpl implements IArsUrgencyService {

	@Autowired
	IArsUrgency voArsUrgencyDao;

	@Override
	@Transactional(readOnly = true)
	public List<ArsUrgency> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsUrgency>) voArsUrgencyDao.findAll();
	}

	@Override
	public ArsUrgency findById(Long id) {
		// TODO Auto-generated method stub
		return voArsUrgencyDao.findById(id).orElse(null);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voArsUrgencyDao.deleteById(id);
	}

	@Override
	public ArsUrgency save(ArsUrgency urgency) {
		// TODO Auto-generated method stub
		return voArsUrgencyDao.save(urgency);
	}
	
	@Override
	public List<ArsUrgency> findByValor(String valor)
	{
		return (List<ArsUrgency>)voArsUrgencyDao.findByValor(valor);
	}
	
	@Override
	public ArsUrgency findByCompaniaAndValor(String compania, String valor)
	{
		return voArsUrgencyDao.findByCompaniaAndValor(compania, valor);
	}
	
}
