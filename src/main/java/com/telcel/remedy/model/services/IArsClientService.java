package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsClient;

public interface IArsClientService {
	
	public List<ArsClient> findAll();
	public ArsClient findById(Long id);
	public ArsClient save(ArsClient voClient);
	public void delete(Long id);
	
//	public ArsClient findByClient(String client);
	//**************cambio/06/11
	public ArsClient findByArcId(String arcId);
	//**************

}
