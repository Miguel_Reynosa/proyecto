package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsClientDwAttachement;

public interface IArsClientDwAttachementService {
	public List<ArsClientDwAttachement> findAll();
	public ArsClientDwAttachement findById(Long id);
	public ArsClientDwAttachement save(ArsClientDwAttachement voClient);
	public void delete(Long id);
}
