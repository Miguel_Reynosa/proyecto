package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsPais;

public interface IArsPaisService {
	
	public List<ArsPais> findAll();
	public ArsPais findById(Long id);
	public ArsPais save (ArsPais voPais);
	public void delete (Long id);
	
	public List<ArsPais> findByEstado(Integer psAlias);
	
	public List<ArsPais> findPaisesByPais(String psAlias);
	
	public ArsPais findByPaisIncidente(String paisIncidente);
	
	public ArsPais findPais(String pais);
	
}
