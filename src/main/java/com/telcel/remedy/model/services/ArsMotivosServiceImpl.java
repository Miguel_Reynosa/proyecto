package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.telcel.remedy.model.dao.IArsMotivos;
import com.telcel.remedy.model.entity.ArsMotivos;


@Service
public class ArsMotivosServiceImpl implements IArsMotivosService
{

	@Autowired
	IArsMotivos voArsMotivosDao;
	
	
	@Override
	@Transactional(readOnly= true)
	public List<ArsMotivos> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsMotivos>)voArsMotivosDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public ArsMotivos findById(Long id) {
		// TODO Auto-generated method stub
		return voArsMotivosDao.findById(id).orElse(null);
	}

	@Override
	public ArsMotivos save(ArsMotivos voMotivos) {
		// TODO Auto-generated method stub
		return voArsMotivosDao.save(voMotivos);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voArsMotivosDao.deleteById(id);
	}

	@Override
	public ArsMotivos findByMotivos(String motivos)
	{
		return voArsMotivosDao.findByMotivos(motivos);
	}

}
