package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.telcel.gm.component.console;
import com.telcel.remedy.model.dao.IArsCompania;
import com.telcel.remedy.model.entity.ArsCompania;

@Service
public class ArsCompaniaServiceImpl implements IArsCompaniaService {
	
	@Autowired
	IArsCompania voArsCompaniaDao;

	@Override
	@Transactional(readOnly=true)
	public List<ArsCompania> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsCompania>)voArsCompaniaDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public ArsCompania findById(Long id) {
		// TODO Auto-generated method stub
		return voArsCompaniaDao.findById(id).orElse(null);
	}

	@Override
	public ArsCompania save(ArsCompania voCompania) {
		// TODO Auto-generated method stub
		return voArsCompaniaDao.save(voCompania);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voArsCompaniaDao.deleteById(id);
	}
	
	@Override
	public List<ArsCompania> findByEstado(Integer estado)
	{
		return (List<ArsCompania>)voArsCompaniaDao.findByEstado(estado);
	}

	@Override
	public ArsCompania findByCompania(String compania)
	{
		return voArsCompaniaDao.findByCompania(compania);
	}
	
	

}
