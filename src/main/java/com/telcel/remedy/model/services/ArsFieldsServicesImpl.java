package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telcel.remedy.model.dao.IArsFields;
import com.telcel.remedy.model.entity.ArsFields;

@Service
public class ArsFieldsServicesImpl implements IArsFieldsService{
	
	@Autowired
	IArsFields voArsFieldDao;
	
	@Override
	public List<ArsFields> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsFields>) voArsFieldDao.findAll();
	}

	@Override
	public ArsFields findById(Long id) {
		// TODO Auto-generated method stub
		return voArsFieldDao.findById(id).orElse(null);
	}

	@Override
	public ArsFields save(ArsFields voField) {
		// TODO Auto-generated method stub
		return voArsFieldDao.save(voField);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voArsFieldDao.deleteById(id);
	}
	
}
