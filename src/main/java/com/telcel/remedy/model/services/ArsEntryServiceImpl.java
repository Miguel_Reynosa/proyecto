package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telcel.remedy.model.dao.IArsEntry;
import com.telcel.remedy.model.entity.ArsEntry;

@Service
public class ArsEntryServiceImpl implements IArsEntryService{
	
	@Autowired
	IArsEntry voArsEntryDao;

	@Override
	public List<ArsEntry> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsEntry>)voArsEntryDao.findAll();
	}

	@Override
	public ArsEntry findById(Long id) {
		// TODO Auto-generated method stub
		return voArsEntryDao.findById(id).orElse(null);
	}

	@Override
	public ArsEntry save(ArsEntry voEntry) {
		// TODO Auto-generated method stub
		return voArsEntryDao.save(voEntry);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voArsEntryDao.deleteById(id);
	}
	
}
