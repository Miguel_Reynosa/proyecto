package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telcel.remedy.model.dao.ICtgServidores;
import com.telcel.remedy.model.entity.CtgServidores;

@Service
public class CtgServidoresServiceImpl implements ICtgServidoresService{
	@Autowired
	ICtgServidores voCtgServidoresDao;

	@Override
	public List<CtgServidores> findAll() {
		// TODO Auto-generated method stub
		return (List<CtgServidores>) voCtgServidoresDao.findAll();
	}

	@Override
	public CtgServidores findById(Long id) {
		// TODO Auto-generated method stub
		return voCtgServidoresDao.findById(id).orElse(null);
	}

	@Override
	public CtgServidores save(CtgServidores voServidor) {
		// TODO Auto-generated method stub
		return voCtgServidoresDao.save(voServidor);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voCtgServidoresDao.deleteById(id);
	}
	
	
}
