package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsMotivos;

public interface IArsMotivosService
{
	public List<ArsMotivos> findAll();
	public ArsMotivos findById(Long id);
	public ArsMotivos save (ArsMotivos voMotivos);
	public void delete (Long id);
	
	public ArsMotivos findByMotivos(String motivo);
}
