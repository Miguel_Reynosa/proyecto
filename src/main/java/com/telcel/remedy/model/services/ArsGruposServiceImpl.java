package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telcel.remedy.model.dao.IArsGrupos;
import com.telcel.remedy.model.entity.ArsGrupo;

@Service
public class ArsGruposServiceImpl implements IArsGruposService{
	
	@Autowired
	IArsGrupos voGrupos;

	@Override
	public List<ArsGrupo> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsGrupo>)voGrupos.findAll();
	}

	@Override
	public ArsGrupo findById(Long id) {
		// TODO Auto-generated method stub
		return voGrupos.findById(id).orElse(null);
	}

	@Override
	public ArsGrupo save(ArsGrupo voArsGrupos) {
		// TODO Auto-generated method stub
		return voGrupos.save(voArsGrupos);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voGrupos.deleteById(id);
	}

	@Override
	public ArsGrupo findByGrupoCompania(String grupo_asignado, String compania)
	{
		return voGrupos.getByGrupoCompania(grupo_asignado, compania);
	}


}
