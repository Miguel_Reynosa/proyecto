package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsFields;

public interface IArsFieldsService {

	public List<ArsFields> findAll();
	public ArsFields findById(Long id);
	public ArsFields save(ArsFields voClient);
	public void delete(Long id);
}
