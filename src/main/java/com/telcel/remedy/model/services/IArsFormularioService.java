package com.telcel.remedy.model.services;

import java.util.List;


import com.telcel.remedy.model.entity.ArsFormulario;

public interface IArsFormularioService {
	public List<ArsFormulario> findAll();
	public ArsFormulario findById(Long id);
	public ArsFormulario save(ArsFormulario voClient);
	public void delete(Long id);

	public List<ArsFormulario> findByPais(String pais);
	public List<ArsFormulario> findByCompania(String compania);	
	
	public ArsFormulario findByCompAndForm(String compania, String formulario);

}
