package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.telcel.gm.component.console;
import com.telcel.remedy.model.dao.IArsPaisCompania;
import com.telcel.remedy.model.entity.ArsPaisCompania;

@Service
public class ArsPaisCompaniaServiceImpl implements IArsPaisCompaniaService
{
	@Autowired
	IArsPaisCompania voArsPaisCompaniaDao;

	@Override
	@Transactional(readOnly = true)
	public List<ArsPaisCompania> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsPaisCompania>)voArsPaisCompaniaDao.findAll();
	}

	@Override
	public ArsPaisCompania findById(Long id) {
		// TODO Auto-generated method stub
		return voArsPaisCompaniaDao.findById(id).orElse(null);
	}

	@Override
	public ArsPaisCompania save(ArsPaisCompania voPaisCompania) {
		// TODO Auto-generated method stub
		return voArsPaisCompaniaDao.save(voPaisCompania);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voArsPaisCompaniaDao.deleteById(id);
	}
	
	@Override
	public List<ArsPaisCompania> findByIdCompania(Long psAlias) {
		// TODO Auto-generated method stub
		console.println("*********** "+psAlias +" *********");
		return (List<ArsPaisCompania>)voArsPaisCompaniaDao.findByIdCompania(psAlias);
	}
	
	@Override
	public List<ArsPaisCompania> findByEstadoCompania(Integer psAlias) {
		// TODO Auto-generated method stub
		console.println("*********** "+psAlias +" *********");
		return (List<ArsPaisCompania>)voArsPaisCompaniaDao.findByEstadoCompania(psAlias);
	}
	
	@Override
	public List<ArsPaisCompania> findByIdPais(Long psAlias) {
		// TODO Auto-generated method stub
		console.println("*********** "+psAlias +" *********");
		return (List<ArsPaisCompania>)voArsPaisCompaniaDao.findByIdPais(psAlias);
	}
	
	@Override
	public List<ArsPaisCompania> findByEstadoPais(Integer psAlias) {
		// TODO Auto-generated method stub
		console.println("*********** "+psAlias +" *********");
		return (List<ArsPaisCompania>)voArsPaisCompaniaDao.findByEstadoPais(psAlias);
	}

	@Override
	public ArsPaisCompania findByCompania(String compania)
	{
		return voArsPaisCompaniaDao.findByCompania(compania);
	}
	
}
