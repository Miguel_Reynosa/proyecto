package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.CtgServidores;



public interface ICtgServidoresService {
	
	public List<CtgServidores> findAll();
	public CtgServidores findById(Long id);
	public CtgServidores save(CtgServidores voClient);
	public void delete(Long id);

}
