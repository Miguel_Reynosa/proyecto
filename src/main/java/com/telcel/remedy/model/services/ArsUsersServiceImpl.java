package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telcel.gm.component.console;
import com.telcel.remedy.model.dao.IArsUsers;
import com.telcel.remedy.model.entity.ArsUsers;

@Service
public class ArsUsersServiceImpl implements IArsUsersService{
	
	@Autowired
	IArsUsers voArsUserDao;
	
	@Override
	public List<ArsUsers> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsUsers>)voArsUserDao.findAll();
	}

	@Override
	public ArsUsers findById(Long id) {
		// TODO Auto-generated method stub
		return voArsUserDao.findById(id).orElse(null);
	}

	@Override
	public ArsUsers save(ArsUsers voUser) {
		// TODO Auto-generated method stub
		return voArsUserDao.save(voUser);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voArsUserDao.deleteById(id);
	}

	@Override
	public ArsUsers findByAlias(String psAlias) {
		// TODO Auto-generated method stub
		console.println("************************************************* "+psAlias +" *****************");
		return voArsUserDao.findByAlias(psAlias);
	}
	
	

}
