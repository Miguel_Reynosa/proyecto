package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.telcel.gm.component.console;
import com.telcel.remedy.model.dao.IArsCategoria;
import com.telcel.remedy.model.entity.ArsCategoria;


@Service
public class ArsCategoriaServiceImpl implements IArsCategoriaService {

	@Autowired
	IArsCategoria voCategoria;

	@Override
//	@Transactional(readOnly = true)
	@Transactional(readOnly = true)
	public List<ArsCategoria> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsCategoria>)voCategoria.findAll();
	}

	@Override
//	@Transactional(readOnly = true)
	@Transactional(readOnly = true)
	public ArsCategoria findById(Long id) {
		// TODO Auto-generated method stub
		return voCategoria.findById(id).orElse(null);
	}

	@Override
	public ArsCategoria save(ArsCategoria voArsClientFiel) {
		// TODO Auto-generated method stub
		return voCategoria.save(voArsClientFiel);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voCategoria.deleteById(id);
	}
	
	@Override
	public List<ArsCategoria> findListByIdCat(String idCat)
	{
		return (List<ArsCategoria>)voCategoria.findListById(idCat);
	}

	@Override
	public List<ArsCategoria> findListByPais(String pais)
	{
		return (List<ArsCategoria>)voCategoria.findListByPais(pais);
	}
	
	@Override
	public List<ArsCategoria> findListByCompania(String compania)
	{
		return (List<ArsCategoria>)voCategoria.findListByCompania(compania);
	}
	
	@Override
	public ArsCategoria findByIdAndCompania(String id, String compania)
	{
		return voCategoria.findByIdAndCompania(id, compania);
	}
}
