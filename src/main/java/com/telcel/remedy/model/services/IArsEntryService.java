package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsEntry;

public interface IArsEntryService {

	public List<ArsEntry> findAll();
	public ArsEntry findById(Long id);
	public ArsEntry save(ArsEntry voClient);
	public void delete(Long id);
}
