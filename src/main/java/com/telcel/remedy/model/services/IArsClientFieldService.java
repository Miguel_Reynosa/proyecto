package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsClientField;

public interface IArsClientFieldService {
	
	public List<ArsClientField> findAll();
	public ArsClientField findById(Long id);
	public ArsClientField save(ArsClientField voClient);
	public void delete(Long id);
	

}
