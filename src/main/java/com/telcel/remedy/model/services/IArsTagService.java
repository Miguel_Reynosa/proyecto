package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsTag;

public interface IArsTagService
{
	public List<ArsTag> findAll();
	public ArsTag findById(Long id);
	public ArsTag save (ArsTag voTag);
	public void delete (Long id);
	
	public ArsTag findByIdRemedy(Long id);
	
	public ArsTag findByTag(String tag);
	
	public List<ArsTag> findTagsByFormulario(String formulario);
	
}
