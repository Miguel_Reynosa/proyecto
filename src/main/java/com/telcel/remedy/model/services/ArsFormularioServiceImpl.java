package com.telcel.remedy.model.services;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telcel.remedy.model.dao.IArsCategoria;
import com.telcel.remedy.model.dao.IArsFormulario;
import com.telcel.remedy.model.entity.ArsCategoria;
import com.telcel.remedy.model.entity.ArsFormulario;

@Service
public class ArsFormularioServiceImpl implements IArsFormularioService{

	
	@Autowired
	IArsFormulario formularios;

	@Override
	public List<ArsFormulario> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsFormulario>) formularios.findAll();
	}


	@Override
	public List<ArsFormulario> findByPais(String pais) {
		return  ((Collection<ArsFormulario>) formularios.findAll()).stream().filter(x -> x.getPaisCompania().getArsPais().getPais().equals(pais))
				.collect(Collectors.toList());
	}

	@Override
	public List<ArsFormulario> findByCompania(String compania)
	{
		return  ((Collection<ArsFormulario>) formularios.findAll()).stream().filter(x -> x.getPaisCompania().getArsCompania().getCompania().equals(compania))
				.collect(Collectors.toList());
	}

	@Override
	public ArsFormulario findById(Long id) {
		// TODO Auto-generated method stub
		return formularios.findById(id).orElse(null);
	}

	@Override
	public ArsFormulario save(ArsFormulario voClient) {
		// TODO Auto-generated method stub
		return formularios.save(voClient);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArsFormulario findByCompAndForm(String compania, String formulario)
	{
		return formularios.findByCompAndForm(compania, formulario);
	}
}
