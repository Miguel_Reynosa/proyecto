package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsCompania;

public interface IArsCompaniaService
{
	public List<ArsCompania> findAll();
	public ArsCompania findById(Long id);
	public ArsCompania save(ArsCompania voCompania);
	public void delete (Long id);
	
	public List<ArsCompania> findByEstado(Integer psAlias);
	
	public ArsCompania findByCompania(String compania);
}
