package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.telcel.gm.component.console;
import com.telcel.remedy.model.dao.IArsPais;
import com.telcel.remedy.model.entity.ArsPais;

@Service
public class ArsPaisServiceImpl implements IArsPaisService {
	
	@Autowired
	IArsPais voArsPaisDao;

	@Override
	@Transactional(readOnly = true)
	public List<ArsPais> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsPais>)voArsPaisDao.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public ArsPais findById(Long id) {
		// TODO Auto-generated method stub
		return voArsPaisDao.findById(id).orElse(null);
	}
	//************************
	/*
	@Override
	@Transactional(readOnly = true)
	public ArsPais findByPais(String pais) {
		// TODO Auto-generated method stub
		return voArsPaisDao.findByPais(pais).orElse(null);
	}
	*/
	//************************
	@Override
	public ArsPais save(ArsPais voPais) {
		// TODO Auto-generated method stub
		return voArsPaisDao.save(voPais);
	}
	
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voArsPaisDao.deleteById(id);
	}
	//------------------------------
	@Override
	public List<ArsPais> findByEstado(Integer estado)
	{
		return (List<ArsPais>)voArsPaisDao.findByEstado(estado);
	}
//----------------------
	@Override
	public List<ArsPais> findPaisesByPais(String psAlias) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public ArsPais findByPaisIncidente(String paisIncidente)
	{
		return voArsPaisDao.findByPaisIncidente(paisIncidente);
	}

	@Override
	public ArsPais findPais(String pais) {
		return voArsPaisDao.findByPais(pais);
	}
}
