package com.telcel.remedy.model.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.telcel.gm.component.console;
import com.telcel.remedy.model.dao.UserRepository;
import com.telcel.remedy.model.entity.User;


@Service
public class UserDetailsServiceImpl  implements UserDetailsService  {

	
	  @Autowired
	    private UserRepository userRepository;
	  private User user;

	    @Override
	    @Transactional(readOnly = true)
	    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	    	//console.println("algo por aqui con el user name?",username);
	         user = userRepository.findByUsername(username);
	        console.println("esto es user???",user.getUsername());
	        console.println("esto es user???",user.getPassword());
	        if (user != null) {
	            return user;
	        }

	        throw new UsernameNotFoundException(username);
	    }

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}
	    
}

