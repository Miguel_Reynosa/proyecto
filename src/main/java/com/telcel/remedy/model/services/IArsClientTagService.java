package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsClientTag;
import com.telcel.remedy.model.entity.ArsPaisCompania;

public interface IArsClientTagService
{
	public abstract List<ArsClientTag> findAll();
	public abstract  ArsClientTag findById(Long id);
	public abstract  ArsClientTag save (ArsClientTag voClientTag);
	public abstract  void delete (Long id);

	//from ArsTag
	public ArsClientTag findByIdTag(Long id);
	public ArsClientTag findByIdRemedy(Long id);
	//****************************************************************
		//from ArsPaisCompania
	public List<ArsClientTag> findByIdPaisCompania(Long id);
//	public  abstract List<ArsClientTag> findByPaisCompania();

	//from ArsCompania
	public ArsClientTag findByIdCompania(Long id);
	public List<ArsClientTag> findByEstadoCompania(Integer id);
	//from ArsPais
	public List<ArsClientTag> findByIdPais(Long id);
	public List<ArsClientTag> findByEstadoPais(Integer id);
	//******************************************************
	
	
//	public List<ArsClientTag> findByCompaniaAndTag(String compania, String tag);
}
