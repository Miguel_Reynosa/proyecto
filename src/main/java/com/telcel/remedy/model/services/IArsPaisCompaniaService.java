package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsPaisCompania;

public interface IArsPaisCompaniaService {
	
	public List<ArsPaisCompania> findAll();
	public ArsPaisCompania findById(Long id);
	public ArsPaisCompania save (ArsPaisCompania voPaisCompania);
	public void delete (Long id);

	//from ArsCompania
	public List<ArsPaisCompania> findByIdCompania(Long psAlias);
	public List<ArsPaisCompania> findByEstadoCompania(Integer psAlias);

	//from ArsPais
	public List<ArsPaisCompania> findByIdPais(Long psAlias);
	public List<ArsPaisCompania> findByEstadoPais(Integer psAlias);
	
	
	public ArsPaisCompania findByCompania(String compania);
	
}	
