package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.telcel.gm.component.console;
import com.telcel.remedy.model.dao.IArsClientTag;
import com.telcel.remedy.model.entity.ArsClientTag;

@Service("clientTag")
public class ArsClientTagServiceImpl implements IArsClientTagService {
	
	@Autowired
	IArsClientTag voArsClientTagDao;

	@Override
	@Transactional(readOnly = true)
	public List<ArsClientTag> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsClientTag>)voArsClientTagDao.findAll();
	}

	@Override
	public ArsClientTag findById(Long id) {
		// TODO Auto-generated method stub
		return voArsClientTagDao.findById(id).orElse(null);
	}

	@Override
	public ArsClientTag save(ArsClientTag voClientTag) {
		// TODO Auto-generated method stub
		return voArsClientTagDao.save(voClientTag);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voArsClientTagDao.deleteById(id);
	}
	
	@Override
	public ArsClientTag findByIdTag(Long psAlias) {
		// TODO Auto-generated method stub
		console.println("************************************************* "+psAlias +" *****************");
		return voArsClientTagDao.findByIdTag(psAlias);
	}
	
	@Override
	public ArsClientTag findByIdRemedy(Long psAlias) {
		// TODO Auto-generated method stub
		console.println("************************************************* "+psAlias +" *****************");
		return voArsClientTagDao.findByIdRemedy(psAlias);
	}
	
	@Override
	public List<ArsClientTag> findByIdPaisCompania(Long psAlias) {
		// TODO Auto-generated method stub
		console.println("************************************************* "+psAlias +" *****************");
		return voArsClientTagDao.findByIdPaisCompania(psAlias);
	}
	
	@Override
	public ArsClientTag findByIdCompania(Long psAlias) {
		// TODO Auto-generated method stub
		console.println("************************************************* "+psAlias +" *****************");
		return voArsClientTagDao.findByIdCompania(psAlias);
	}
	
	@Override
	public List<ArsClientTag> findByEstadoCompania(Integer psAlias) {
		// TODO Auto-generated method stub
		console.println("************************************************* "+psAlias +" *****************");
		return (List<ArsClientTag>)voArsClientTagDao.findByEstadoCompania(psAlias);
	}
	
	@Override
	public List<ArsClientTag> findByIdPais(Long psAlias) {
		// TODO Auto-generated method stub
		console.println("************************************************* "+psAlias +" *****************");
		return (List<ArsClientTag>)voArsClientTagDao.findByIdPais(psAlias);
	}
	
	@Override
	public List<ArsClientTag> findByEstadoPais(Integer psAlias) {
		// TODO Auto-generated method stub
		console.println("************************************************* "+psAlias +" *****************");
		return (List<ArsClientTag>)voArsClientTagDao.findByEstadoPais(psAlias);
	}

	
	
//	@Override
//	public List<ArsClientTag> findByCompaniaAndTag(String compania, String tag)
//	{
//		return voArsClientTagDao.findByCompaniaAndTag(compania, tag);
//	}
}
