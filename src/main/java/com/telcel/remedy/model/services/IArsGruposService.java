package com.telcel.remedy.model.services;

import java.util.List;

import com.telcel.remedy.model.entity.ArsGrupo;

public interface IArsGruposService {
	
	public List<ArsGrupo> findAll();
	public ArsGrupo findById(Long id);
	public ArsGrupo save (ArsGrupo voGrupo);
	public void delete (Long id);
	
	public ArsGrupo findByGrupoCompania(String grupo_asignado, String compania);
}
