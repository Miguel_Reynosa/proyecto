package com.telcel.remedy.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telcel.gm.component.console;
import com.telcel.remedy.model.dao.IArsTag;
import com.telcel.remedy.model.entity.ArsTag;

@Service
public class ArsTagServiceImpl implements IArsTagService {
	
	@Autowired
	IArsTag voArsTagDao;

	@Override
	public List<ArsTag> findAll() {
		// TODO Auto-generated method stub
		return (List<ArsTag>)voArsTagDao.findAll();
	}

	@Override
	public ArsTag findById(Long id) {
		// TODO Auto-generated method stub
		return voArsTagDao.findById(id).orElse(null);
	}

	@Override
	public ArsTag save(ArsTag voTag) {
		// TODO Auto-generated method stub
		return voArsTagDao.save(voTag);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voArsTagDao.deleteById(id);
	}
	
	@Override
	public ArsTag findByIdRemedy(Long psAlias) {
		// TODO Auto-generated method stub
		console.println("************************************************* "+psAlias +" *****************");
		return voArsTagDao.findByIdRemedy(psAlias);
	}
	
	public ArsTag findByTag(String tag)
	{
		return voArsTagDao.findByTag(tag);
	}
	
	public List<ArsTag> findTagsByFormulario(String formulario)
	{
		return (List<ArsTag>)voArsTagDao.findTagsByFormulario(formulario);
	}
	
}
