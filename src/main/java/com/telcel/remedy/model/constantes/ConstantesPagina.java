package com.telcel.remedy.model.constantes;

public interface ConstantesPagina
{
	String CAMPO_VACIO		= "�ERROR! Falt� agregar un campo.";
	String CAMPO_INEXISTENTE= "�ERROR! No existe un valor ingresado.";
	
	String REGISTRO_REPETIDO= "�ERROR! Ya existe un registro con esos datos.";
	String REGISTRO_EXITOSO	= "Se agreg� el registro correctamente.";
}
