package com.telcel.remedy.ars.model;
public class InsertVO {
	private String sistema;
	private String formulario;
	private RemedyControlEntry columnas;
	private String condiciones;
	private RemedyControlEntry resultado;
	

	public InsertVO() {
		
	}

	public String getSistema() {
		return sistema;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public String getFormulario() {
		return formulario;
	}

	public void setFormulario(String formulario) {
		this.formulario = formulario;
	}	

	public RemedyControlEntry getColumnas() {
		return columnas;
	}

	public void setColumnas(RemedyControlEntry columnas) {
		this.columnas = columnas;
	}

	public String getCondiciones() {
		return condiciones;
	}

	public void setCondiciones(String condiciones) {
		this.condiciones = condiciones;
	}

	public RemedyControlEntry getResultado() {
		return resultado;
	}

	public void setResultado(RemedyControlEntry resultado) {
		this.resultado = resultado;
	}

	
}
