package com.telcel.remedy.ars.model;

import java.util.List;

import com.telcel.gm.component.console;

public class RemedyControlResultado{
	
	private String sistema;
	private String formulario;
	private String columnas;
	private String condiciones;
	private String orden;
	private List<RemedyControlEntry> resultado;
	private String ttidwlg;
	private String troubleTicketINC;
	
	public RemedyControlResultado() {
		
	}
	public RemedyControlResultado(String troubleTicketINC,String sistema, String formulario, String columnas, String condiciones,
			String orden) {
		super();
		this.sistema = sistema;
		this.formulario = formulario;
		this.columnas = columnas;
		this.condiciones = condiciones;
		this.orden = orden;
		this.troubleTicketINC=troubleTicketINC;
	}
	
	
	public String getTtidwlg() {
		return ttidwlg;
	}
	public void setTtidwlg(String ttidwlg) {
		this.ttidwlg = ttidwlg;
	}
	public String getTroubleTicketINC() {
		return troubleTicketINC;
	}
	public void setTroubleTicketINC(String troubleTicketINC) {
		this.troubleTicketINC = troubleTicketINC;
	}
	public String getSistema() {
		return sistema;
	}
	public void setSistema(String sistema) {
		this.sistema = sistema;
	}
	public String getFormulario() {
		return formulario;
	}
	public void setFormulario(String formulario) {
		this.formulario = formulario;
	}
	public String getColumnas() {
		return columnas;
	}
	public void setColumnas(String columnas) {
		this.columnas = columnas;
	}
	public String getCondiciones() {
		return condiciones;
	}
	public void setCondiciones(String condiciones) {
		this.condiciones = condiciones;
	}
	public String getOrden() {
		return orden;
	}
	public void setOrden(String orden) {
		this.orden = orden;
	}
	public List<RemedyControlEntry> getResultado() {
		return resultado;
	}
	public void setResultado(List<RemedyControlEntry> resultado) {
		console.println("algo en setresutlatdo",resultado);
		this.resultado = resultado;
	}
//	@Override
//	public String toString() {
//		return "RemedyControlResultado [sistema=" + sistema + ", formulario=" + formulario + ", columnas=" + columnas
//				+ ", condiciones=" + condiciones + ", orden=" + orden + ", resultado=" + resultado + "]";
//	}
	
	
	
	
}
