package com.telcel.remedy.ars.model;

public class UpdateVO {
	private String sistema;
	private String formulario;
	private String id;
	private RemedyControlEntry columnas;
	private String condiciones;
	private String resultado;

	public UpdateVO() {

	}

	public UpdateVO(String sistema, String formulario, RemedyControlEntry columnas, String condiciones) {
		this.sistema = sistema;
		this.formulario = formulario;
		this.columnas = columnas;
		this.condiciones = condiciones;
	}

	public String getSistema() {
		return sistema;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public String getFormulario() {
		return formulario;
	}

	public void setFormulario(String formulario) {
		this.formulario = formulario;
	}

	public RemedyControlEntry getColumnas() {
		return columnas;
	}

	public void setColumnas(RemedyControlEntry columnas) {
		this.columnas = columnas;
	}

	public String getCondiciones() {
		return condiciones;
	}

	public void setCondiciones(String condiciones) {
		this.condiciones = condiciones;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "UpdateVO [sistema=" + sistema + ", formulario=" + formulario + ", id=" + id + ", columnas=" + columnas
				+ ", condiciones=" + condiciones + ", resultado=" + resultado + "]";
	}
	
	

}
