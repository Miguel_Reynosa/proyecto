package com.telcel.remedy.ars.model;

import java.util.List;

public class RemedyControlEntry {
	private List<FieldRC> Field;
	
	private String Error;
	
	private String Reg;
	
	private String Nuevo;

	public List<FieldRC> getField() {
		return Field;
	}

	public void setField(List<FieldRC> Field) {
		this.Field = Field;
	}

	public RemedyControlEntry(List<FieldRC> Field) {
		this.Field = Field;
	}

	public RemedyControlEntry() {
		
	}

	public String getError() {
		return Error;
	}

	public void setError(String error) {
		Error = error;
	}

	public String getReg() {
		return Reg;
	}

	public void setReg(String reg) {
		Reg = reg;
	}

	public String getNuevo() {
		return Nuevo;
	}

	public void setNuevo(String nuevo) {
		Nuevo = nuevo;
	}

	@Override
	public String toString() {
		return "RemedyControlEntry [Field=" + Field + ", Error=" + Error + ", Reg=" + Reg + ", Nuevo=" + Nuevo + "]";
	}
	
	
	
	
	
	
	
	
}
