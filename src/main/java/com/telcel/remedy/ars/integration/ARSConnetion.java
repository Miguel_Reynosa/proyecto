package com.telcel.remedy.ars.integration;


import java.util.List;

import org.springframework.stereotype.Component;

import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ARServerUser;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.StatusInfo;
import com.infomedia.utils.DinamicVO;
import com.softcoatl.arsi.core.ARServerUserFactory;
import com.softcoatl.comm.Host;
import com.telcel.gm.component.GuardarTexto;
import com.telcel.gm.component.console;
import com.telcel.remedy.pruebasCodigo;
import com.telcel.remedy.model.entity.ArsUsers;


@Component
public class ARSConnetion {
	
	private ARServerUser voArServerUser;
	
	
	public ARSConnetion() throws ARException {
		//voArServerUser = createConnection();
	}
	public void printStatusList(List<StatusInfo> statusList) {
		if (statusList == null || statusList.size()==0) {
		console.println("Status List is empty.");
		return;
		}
		console.println("Message type: ");
		switch(statusList.get(0).getMessageType())
		{
		case Constants.AR_RETURN_OK:
		     console.println("Note");
		break;
		case Constants.AR_RETURN_WARNING:
		console.println("Warning");
		break;
		case Constants.AR_RETURN_ERROR:
		console.println("Error");
		    break;
		case Constants.AR_RETURN_FATAL:
		         console.println("Fatal Error");
		break;
		default:
		console.println("Unknown (" +
		          statusList.get(0).getMessageType() + ")");
		break;
		}
		console.println("Status List:");
		for (int i=0; i < statusList.size(); i++) {
		
		console.println(statusList.get(i).getMessageText());
		
		console.println(statusList.get(i).getAppendedText());
		}
}
	  ARServerUser 	  server;
	  public void handleException(ARException e, String errMessage){
			console.println(errMessage);
			printStatusList(server.getLastStatus());
			console.println("Stack Trace:");
			e.printStackTrace();
}
	 void connect() {
         console.println("Connecting to AR Server... to ArsConnection");
         try {
        	 console.println("server en connect", server);
             server.verifyUser();
         } catch (ARException e) {
             //This exception is triggered by a bad server, password or,
             //if guest access is turned off, by an unknown username.
             handleException(e, "Cannot verify user " +
                          server.getUser() + ".");
             System.exit(1);
         }
         console.println("Connected to AR Server end ArsConnection " +
             server.getServer());
     }
    
	
	public ARServerUser createConnection(ArsUsers voUser) throws ARException {		
//		server = new ARServerUser();
		  console.println("inciando conextion");
//		  Host hos=getHost(voUser);
//          server.setServer(hos.getHST());
//          server.setUser(hos.getUSR());
//          server.setPassword(hos.getPWD());
//          server.setPort(hos.getPRT());
          console.println("server es ",server);
        //  connect();
//		  	server.setServer("100.127.3.7");
//	        server.setUser(voUser.getAruUsr());
//	        server.setPassword(voUser.getAruPwd());
//	        server.setPort(46262);
//          console.println("finalizando");
//      	GuardarTexto texto=new GuardarTexto();
//		texto.guardar("hola por aqui en texto api");
//	
		  
        //  return server;
		
		return new ARServerUserFactory().getRemedyConnection(getHost(voUser), getHostAttributes(""));
	}
	
	public DinamicVO getHostAttributes(String system) {
		DinamicVO attributes = new DinamicVO();
		attributes.setCampo("CLIENT_TYPE", "2");
		return attributes;
	}

	public Host getHost(ArsUsers voUser) {
		Host host = new Host();
		console.println("el usuario llegado es",voUser);
		//host.setPRT(Integer.parseInt("46262"));
		//console.println("puerto",voUser.getAruCsrClave().getCsrPrt());
		host.setPRT(Integer.parseInt(voUser.getAruCsrClave().getCsrPrt()));
		//host.setPRT(39450);
		//--- host.setPRT(39450);
		   if (voUser == null) {
			   console.println("hola en null");
			host.setHST("");		
			host.setUSR("");
			host.setPWD("");
			
				
		}else {
		//	GuardarTexto texto=new GuardarTexto();
			//texto.guardar("hola por aqui en texto");
			

//		    pruebasCodigo test2 = new pruebasCodigo(39450,"gm.chl","gm.2020","100.127.28.134");
		      
//			host.setHST("100.127.28.134");// ip de QA		
//			host.setUSR("gm.chl");//gm.chl	
//			host.setPWD("gm.2020");
		//	(46262,"rc_rutinas","Rc_#rm15","100.127.3.7");
//			host.setHST("100.127.4.76");		
//			host.setUSR("rc_rutinas");
//			host.setPWD("Rc_#rm15");
			
			//console.println("usuario",voUser.getAruUsr());
			//console.println("pass",voUser.getAruPwd());
			//host.setHST("100.127.28.134");		
			host.setHST(voUser.getAruCsrClave().getCsrEqp());		
			host.setUSR(voUser.getAruUsr());
			host.setPWD(voUser.getAruPwd());
			
			if(voUser.getAruCsrClave().getCsrUsr()!=null) {
				host.setUSR(voUser.getAruCsrClave().getCsrUsr());
				host.setPWD(voUser.getAruCsrClave().getCsrPwd());
					
			}
			
			
		}
		
		
		return host;
	}

	public ARServerUser getVoArServerUser() {
		return voArServerUser;
	}

	public void setVoArServerUser(ARServerUser voArServerUser) {
		this.voArServerUser = voArServerUser;
	}
	
	
	
}
