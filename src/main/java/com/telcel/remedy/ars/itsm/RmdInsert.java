package com.telcel.remedy.ars.itsm;

import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ARServerUser;
import com.bmc.arsys.api.AttachmentValue;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.Value;
import com.telcel.gm.component.console;
import com.telcel.remedy.ars.integration.ARSConnetion;
import com.telcel.remedy.ars.model.FieldRC;
import com.telcel.remedy.ars.model.InsertVO;
import com.telcel.remedy.ars.model.RemedyControlEntry;
import com.telcel.remedy.model.entity.ArsUsers;
import com.telcel.remedy.model.services.IArsUsersService;

@Service
//@Qualifier("attachmentService")
public class RmdInsert {

	@Autowired
	private ARSConnetion voARSConnetion;

	@Autowired
	@Qualifier("arsUsersServiceImpl")
	IArsUsersService voUsuarios;

	public InsertVO fncRmdInsert(InsertVO voParametros) throws ARException {
		RemedyControlEntry voEntryRC = new RemedyControlEntry();
		Entry voEntry = new Entry();
		  ARServerUser voConection=null; 
		try {
			ArsUsers voUser = voUsuarios.findByAlias(voParametros.getSistema());
			
			
			console.println("mensaje para user voUser",voUser);
			 voConection = voARSConnetion.createConnection(voUser);
			  console.println("conexion en RmdInsert-....--------------------------",voConection);
			voEntry = fncSetEntryRC(voParametros.getColumnas());
			console.println("formulario en insert",voParametros.getFormulario());
			voEntryRC.setNuevo(voConection.createEntry(voParametros.getFormulario(), voEntry));
			console.println("finalizando........................................");

		} catch (ARException e) {
			console.println("causa........................................");

			//console.println(e.getCause());
			voEntryRC.setError(e.toString());
		}
		voParametros.setResultado(voEntryRC);

		return voParametros;

	}

	public Entry fncSetEntryRC(RemedyControlEntry remedyControlEntry) {
		Entry voEntry = new Entry();
		String vsValue = "";
		String[] voValue = null;
		AttachmentValue voAttach = new AttachmentValue();
		for (FieldRC voCampo : remedyControlEntry.getField()) {
			vsValue = voCampo.getValue();
			if (vsValue.startsWith("<FILE>") && vsValue.endsWith("</FILE>")) {
				vsValue = vsValue.replace("<FILE>", "");
				vsValue = vsValue.replace("</FILE>", "");
				voValue = vsValue.split("~");
				console.println("*********************************************" +
				vsValue);
				console.println("KHE"+voValue);
				byte[] attach = Base64.getMimeDecoder().decode(voValue[1]);
				voAttach.setName(voValue[0]);
				voAttach.setValue(attach);
				voEntry.put(voCampo.getId(), new Value(voAttach));

			} else {
				voEntry.put(voCampo.getId(), new Value(voCampo.getValue()));
			}

		}

		return voEntry;
	}

}
