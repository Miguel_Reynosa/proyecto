package com.telcel.remedy.ars.itsm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ARServerUser;
import com.bmc.arsys.api.Entry;
import com.telcel.gm.component.console;
import com.telcel.remedy.ars.integration.ARSConnetion;
import com.telcel.remedy.ars.model.UpdateVO;
import com.telcel.remedy.model.entity.ArsUsers;
import com.telcel.remedy.model.services.IArsUsersService;

@Service
public class RmdUpdate {

	@Autowired
	private ARSConnetion voARSConnetion;
	
	@Autowired
	@Qualifier("rmdInsert")
	private RmdInsert voInsert;
	
	@Autowired 
	@Qualifier("arsUsersServiceImpl")
	IArsUsersService voUsuarios;
	
	public UpdateVO fncRmdUpdate(UpdateVO voParametros) {
		
		try {
			
			console.println("updata en cos",voParametros.getSistema());
			console.println("formulario en RmdUpdate en cos",voParametros.getFormulario());
			ArsUsers voUser = voUsuarios.findByAlias(voParametros.getSistema());
			Entry voEntry = voInsert.fncSetEntryRC(voParametros.getColumnas());
			console.println("voEntry en update",voEntry);
			
			ARServerUser voConn = voARSConnetion.createConnection(voUser);
			//voParametros.setFormulario("HPD:IncidentInterface_Create");
			console.println("voParametros en update",voParametros);
			voConn.setEntry(voParametros.getFormulario(), voParametros.getId(),voEntry,null, 0 );
			//voConn.setEntry(voParametros.getFormulario(), "INC000000106192",voEntry,null, 0 );
			
			
			voParametros.setResultado("OK");
		} catch (ARException e) {
			voParametros.setResultado(e.toString());
		}
		
		return voParametros;
	}
}
