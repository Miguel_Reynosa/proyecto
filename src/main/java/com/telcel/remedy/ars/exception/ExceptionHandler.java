package com.telcel.remedy.ars.exception;

public interface ExceptionHandler {
	public void handleException(Throwable t);
}
