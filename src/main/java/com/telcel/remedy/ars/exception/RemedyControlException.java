package com.telcel.remedy.ars.exception;

public class RemedyControlException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public RemedyControlException() {
	}

	public RemedyControlException(String message, Throwable cause) {
		super(message, cause);
	}

	public RemedyControlException(String message) {
		super(message);
	}

	public RemedyControlException(Throwable cause) {
		super(cause);
	}
	
}
