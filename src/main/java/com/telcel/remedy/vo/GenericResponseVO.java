package com.telcel.remedy.vo;

public class GenericResponseVO
{
	boolean agregar;
	String	mensaje;
	Object	ValueObjectVO;
	
	//GETTERS & SETTERS
	public boolean isAgregar() {
		return agregar;
	}
	public void setAgregar(boolean agregar) {
		this.agregar = agregar;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Object getValueObjectVO() {
		return ValueObjectVO;
	}
	public void setValueObjectVO(Object ValueObjectVO) {
		this.ValueObjectVO = ValueObjectVO;
	}
	
	@Override
	public String toString()
	{
		StringBuilder response = new StringBuilder();
		
		response.append("\n");
		response.append("********************************************");
		response.append("\n");
		response.append("Agregar: \t");
		response.append(agregar);
		response.append("\n");
		response.append("Mensaje: \t");
		response.append(mensaje);
		response.append("\n");
		response.append("More info: \t");
		response.append(ValueObjectVO);
		response.append("\n");
		response.append("********************************************");
		response.append("\n");
		
		return response.toString();
	}
}
