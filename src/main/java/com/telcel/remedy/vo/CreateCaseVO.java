package com.telcel.remedy.vo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.telcel.gm.model.CreateCase;
import com.telcel.gm.model.Formulario;
import com.telcel.gm.model.TroubleTicketAttachment;
import com.telcel.gm.services.AttachmentService;
import com.telcel.remedy.ars.itsm.RmdInsert;
import com.telcel.remedy.ars.itsm.RmdSelect;
import com.telcel.remedy.ars.itsm.RmdUpdate;
import com.telcel.remedy.controllers.DataSetController;
import com.telcel.remedy.model.dao.IArsClientTag;

@Service
public class CreateCaseVO {
	
	
	@Autowired
	IArsClientTag voArsClientTagDao;

	@Autowired
	@Qualifier("rmdInsert")
	RmdInsert voInsert;

	@Autowired
	@Qualifier("rmdSelect")
	RmdSelect voSelect;

	@Autowired
	@Qualifier("attachmentService")
	AttachmentService voAttachmentService;

	@Autowired
	private Environment env;

	@Autowired
	DataSetController dataController;

	@Autowired
	@Qualifier("rmdUpdate")
	RmdUpdate voUpdate;
	
	@Autowired
	//@Qualifier("rmdUpdate")
	Formulario formularioVO;
	
	
	String incidentNumber;
	String numberWorklog;
	
	CreateCase entityCase;
	
	TroubleTicketAttachment attachmentVO;
	
	String idLocal;
	

	public String getIdLocal() {
		return idLocal;
	}


	public void setIdLocal(String idLocal) {
		this.idLocal = idLocal;
	}


	public TroubleTicketAttachment getAttachmentVO() {
		return attachmentVO;
	}


	public void setAttachmentVO(TroubleTicketAttachment attachmentVO) {
		this.attachmentVO = attachmentVO;
	}


	public String getNumberWorklog() {
		return numberWorklog;
	}


	public void setNumberWorklog(String numberWorklog) {
		this.numberWorklog = numberWorklog;
	}


	public CreateCase getEntityCase() {
		return entityCase;
	}


	public void setEntityCase(CreateCase entityCase) {
		this.entityCase = entityCase;
	}


	public String getIncidentNumber() {
		return incidentNumber;
	}


	public void setIncidentNumber(String incidentNumber) {
		this.incidentNumber = incidentNumber;
	}


	String cuerpo;
	public  CreateCaseVO(String cuerpo) {
	
		this.cuerpo=cuerpo;
	}
	
	
	public Formulario getFormularioVO() {
		return formularioVO;
	}


	public void setFormularioVO(Formulario formularioVO) {
		this.formularioVO = formularioVO;
	}


	public CreateCaseVO() {
		
	}
	public String getVoCuerpo() {
		return cuerpo;
	}


	public void setVoCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}


	public IArsClientTag getVoArsClientTagDao() {
		return voArsClientTagDao;
	}

	public void setVoArsClientTagDao(IArsClientTag voArsClientTagDao) {
		this.voArsClientTagDao = voArsClientTagDao;
	}

	public RmdInsert getVoInsert() {
		return voInsert;
	}

	public void setVoInsert(RmdInsert voInsert) {
		this.voInsert = voInsert;
	}

	public RmdSelect getVoSelect() {
		return voSelect;
	}

	public void setVoSelect(RmdSelect voSelect) {
		this.voSelect = voSelect;
	}

	public AttachmentService getVoAttachmentService() {
		return voAttachmentService;
	}

	public void setVoAttachmentService(AttachmentService voAttachmentService) {
		this.voAttachmentService = voAttachmentService;
	}

	public Environment getEnv() {
		return env;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

	public DataSetController getDataController() {
		return dataController;
	}

	public void setDataController(DataSetController dataController) {
		this.dataController = dataController;
	}

	public RmdUpdate getVoUpdate() {
		return voUpdate;
	}

	public void setVoUpdate(RmdUpdate voUpdate) {
		this.voUpdate = voUpdate;
	}


	@Override
	public String toString() {
		return "CreateCaseVO [voArsClientTagDao=" + voArsClientTagDao + ", voInsert=" + voInsert + ", voSelect="
				+ voSelect + ", voAttachmentService=" + voAttachmentService + ", env=" + env + ", dataController="
				+ dataController + ", voUpdate=" + voUpdate + ", formularioVO=" + formularioVO + ", cuerpo=" + cuerpo
				+ "]";
	}
	
	

}
