package com.telcel.remedy.util;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.telcel.remedy.controllers.ArsClientController;
import com.telcel.remedy.controllers.DataSetController;

import com.telcel.remedy.model.constantes.ConstantesPagina;
import com.telcel.remedy.model.entity.ArsClient;
import com.telcel.remedy.model.entity.ArsClientTag;
import com.telcel.remedy.model.entity.ArsCompania;
import com.telcel.remedy.model.entity.ArsFormulario;
import com.telcel.remedy.model.entity.ArsGrupo;
import com.telcel.remedy.model.entity.ArsMotivos;
import com.telcel.remedy.model.entity.ArsPais;
import com.telcel.remedy.model.entity.ArsPaisCompania;
import com.telcel.remedy.model.entity.ArsTag;
import com.telcel.remedy.model.entity.ArsUrgency;

import com.telcel.remedy.vo.GenericResponseVO;

public class Validaciones
{
	public GenericResponseVO validarPais(Map<String, String> paisMap)
	{
		DataSetController dataSetController = new DataSetController();
		GenericResponseVO genericResponseVO = new GenericResponseVO();
		
		System.out.println("\nVALIDANDO PA�S...");
		
		String	pais			= paisMap.get("pais");
//		int		estado			= Integer.parseInt(paisMap.get("estado"));
		String	paisIncidente	= paisMap.get("paisIncidente");
		
		//VALIDAR CAMPOS NULOS O VAC�OS
		if(pais==null || pais.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		if(paisMap.get("estado")==null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
		}
		if(paisIncidente==null || paisIncidente.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
		//VALIDA SI YA EXISTE EL REGISTRO
		ArsPais arsPaisFound = null;
		arsPaisFound = dataSetController.findPaisByPais(pais);
		
		if(arsPaisFound!=null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_REPETIDO);
			
			return genericResponseVO;
		}
		else
		{
			//S� SE AGREGA
			genericResponseVO.setAgregar(true);
		}
		
		return genericResponseVO;
	}

	public GenericResponseVO validarCompania(Map<String, String> companiaMap)
	{
		GenericResponseVO genericResponseVO = new GenericResponseVO();
		DataSetController dataSetController	= new DataSetController();

		System.out.println("\nVALIDANDO COMP��A...");
		
		String	compania;
		int		estado;
		
		//DATOS PARA LLENAR EL CAMPO "compania"
		String servicio	= companiaMap.get("servicio").toUpperCase();
		String empresa	= companiaMap.get("empresa").toUpperCase();
		String pais		= companiaMap.get("pais").toUpperCase();
		
		//VALIDACIONES DE CAMPOS NULOS O VAC�OS
		if(empresa==null || empresa.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		if(pais==null || pais.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
		if(empresa.compareTo("CLARO")==0)
		{
			//AGREGAR LOS 3 CAMPOS
			
			//VALIDAR EL CAMPO "servicio"
			if(servicio==null || servicio.compareTo("")==0)
			{
				genericResponseVO.setAgregar(false);
				genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
				
				return genericResponseVO;
			}
			
			compania = servicio + "_" + empresa + "_" + pais;
		}
		else
		{
			//AGREGAR SOLAMENTE EMPRESA Y PA�S
			//EMPRESA PUEDE SER "CLOUD" O "LOCAL"
			compania = empresa+"_"+pais;
		}
		
		genericResponseVO.setValueObjectVO(compania);
		
		
		if(companiaMap.get("estado")==null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
		//VALIDA SI YA EXISTE EL REGISTRO
		ArsCompania arsCompaniaFound = null;
		arsCompaniaFound = dataSetController.findCompaniaByCompania(compania);
		
		if(arsCompaniaFound!=null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_REPETIDO);
		}
		else
		{
			//S� SE AGREGA
			genericResponseVO.setAgregar(true);
		}
		
		return genericResponseVO;
	}

	public GenericResponseVO validarPaisCompania(Map<String, String> paisCompaniaMap)
	{
		DataSetController dataSetController = new DataSetController();
		GenericResponseVO genericResponseVO = new GenericResponseVO();
		
		System.out.println("\nVALIDANDO PAIS-COMPANIA...");
		
		String pais		= paisCompaniaMap.get("pais").toUpperCase();
		String compania	= paisCompaniaMap.get("compania").toUpperCase();
		String cliente	= paisCompaniaMap.get("cliente").toUpperCase();
		
		//VALIDAR CAMPOS VAC�OS
		if(pais==null || pais.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(compania==null || compania.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(cliente==null || cliente.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
		//B�SQUEDA DE REGISTROS REPETIDOS
		ArsPaisCompania arsPaisCompania = null;
		arsPaisCompania = dataSetController.findPaisCompaniaByCompania(compania);
		
		if(arsPaisCompania!=null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_REPETIDO);
			
			return genericResponseVO;
		}
		
		//BUSCAR SI EXISTE EL PA�S
		ArsPais arsPais = null;
		arsPais = dataSetController.findPaisByPais(pais);
		if(arsPais==null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_INEXISTENTE);
			
			return genericResponseVO;
		}

		//BUSCAR SI EXISTE LA COMPA��A
		ArsCompania arsCompania = null;
		arsCompania = dataSetController.findCompaniaByCompania(compania);
		if(arsCompania==null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_INEXISTENTE);
			
			return genericResponseVO;
		}

		//BUSCAR SI EXISTE EL CLIENTE
		ArsClientController arsClientController = new ArsClientController();
		ArsClient arsClient = null;
		arsClient = arsClientController.findByArcId(cliente);
		if(arsClient==null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_INEXISTENTE);
			
			return genericResponseVO;
		}

		genericResponseVO.setAgregar(true);
		
		return genericResponseVO;
	}

	public GenericResponseVO validarTag(Map<String, String> tagMap)
	{
		GenericResponseVO genericResponseVO = new GenericResponseVO();
		DataSetController dataSetController = new DataSetController();
		
		genericResponseVO.setAgregar(true);
		
		String	tag			= tagMap.get("tag");
		int		id_remedy	= Integer.parseInt(tagMap.get("id_remedy"));
		String	formulario	= tagMap.get("formulario");
		String	tipo		= tagMap.get("tipo");
		
		//VALIDAR CAMPOS
		if(tag==null || tag.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(tagMap.get("id_remedy")==null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(formulario==null || formulario.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(tipo==null || tipo.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
		//BUSCAR SI EXISTE OTRO REGISTRO CON LOS MISMOS DATOS
		ArsTag arsTag = null;
		arsTag = dataSetController.findTagByTag(tag);
		if(arsTag!=null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_REPETIDO);
		}
		
		return genericResponseVO;
	}

//	public GenericResponseVO validarClientTag(Map<String, String> clientTagMap)
//	{
//		DataSetController dataSetController = new DataSetController();
//		GenericResponseVO genericResponseVO = new GenericResponseVO();
//		
//		genericResponseVO.setAgregar(true);
//		
//		String paisCompania	= clientTagMap.get("paisCompania");
//		String tag			= clientTagMap.get("tag");
//		String valorDefault	= clientTagMap.get("tag");
//		String requerido	= clientTagMap.get("requerido");
//		
//		if(paisCompania==null || paisCompania.compareTo("")==0)
//		{
//			genericResponseVO.setAgregar(false);
//			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
//			
//			return genericResponseVO;
//		}
//
//		if(tag==null || tag.compareTo("")==0)
//		{
//			genericResponseVO.setAgregar(false);
//			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
//			
//			return genericResponseVO;
//		}
//
//		/* PUEDEN SER NULOS (VAC�OS)
//		if(valorDefault==null || valorDefault.compareTo("")==0)
//		{
//			genericResponseVO.setAgregar(false);
//			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
//			
//			return genericResponseVO;
//		}
//
//		if(requerido==null || requerido.compareTo("")==0)
//		{
//			genericResponseVO.setAgregar(false);
//			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
//			
//			return genericResponseVO;
//		}
//		/**/
//		
//		//OBTENER COMPANIA PARA BUSCAR ARS_PAISCOMPANIA
//		String compania = getCompania(paisCompania);
//		ArsPaisCompania arsPaisCompania = dataSetController.findPaisCompaniaByCompania(compania);
//		if(arsPaisCompania==null)
//		{
//			genericResponseVO.setAgregar(false);
//			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_INEXISTENTE);
//			
//			return genericResponseVO;
//		}
//		else
//		{
//			genericResponseVO.setValueObjectVO(compania);
//		}
//		
//		//BUSCAR REGISTRO DE ARS_TAG
//		ArsTag arsTag = dataSetController.findTagByTag(tag);
//		if(arsTag==null)
//		{
//			genericResponseVO.setAgregar(false);
//			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_INEXISTENTE);
//			
//			return genericResponseVO;
//		}
//		
//		//B�SQUEDA DE REGISTROS REPETIDOS
//		ArsClientTag arsClientTag = dataSetController.findCTByCompaniaAndTag(compania, tag);
//		if(arsClientTag!=null)
//		{
//			genericResponseVO.setAgregar(false);
//			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_REPETIDO);
//			
//			return genericResponseVO;
//		}
//		
//		return genericResponseVO;
//	}

	public GenericResponseVO validarMotivos(Map<String, String> motivosMap)
	{
		DataSetController dataSetController = new DataSetController();
		GenericResponseVO genericResponseVO = new GenericResponseVO();
		
		//co = Categor�a Operacional
		String co1 = motivosMap.get("catOp1");
		String co2 = motivosMap.get("catOp2");
		String co3 = motivosMap.get("catOp3");
		
		//cp = Categor�a de Producto
		String cp1 = motivosMap.get("catProd1");
		String cp2 = motivosMap.get("catProd2");
		String cp3 = motivosMap.get("catProd3");
		
		//CATEGOR�AS OPERACIONALES
		if(co1==null || co1.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
		if(co2==null || co2.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
		if(co3==null || co3.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
		//CATEGOR�AS DE PRODUCTO
		if(cp1==null || cp1.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(cp2==null || cp2.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(cp3==null || cp3.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
		//LLENAR EL CAMPO DE MOTIVOS
		String motivos = co1+";"+co2+";"+co3+";"+cp1+";"+cp2+";"+cp3;
		genericResponseVO.setValueObjectVO(motivos);
		
		ArsMotivos arsMotivos = null;
		arsMotivos = dataSetController.findMotivoByMotivo(motivos);
		if(arsMotivos!=null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_REPETIDO);
		}
		
		return genericResponseVO;
	}

	public GenericResponseVO validarGrupo(Map<String, String> grupoMap)
	{
		DataSetController dataSetController = new DataSetController();
		GenericResponseVO genericResponseVO = new GenericResponseVO();
		
		genericResponseVO.setAgregar(true);
		
		String grupo_asignado		= grupoMap.get("grupo_asignado");
		String compania				= grupoMap.get("compania");
		String organizacion_asignado= grupoMap.get("organizacion_asignado");
		String comp_sup				= grupoMap.get("comp_sup");
		String id_remedy			= grupoMap.get("id_remedy");
		String id_sgp				= grupoMap.get("id_sgp");//*
		String nombre_completo		= grupoMap.get("nombre_completo");
		String login_id				= grupoMap.get("login_id");
		
		if(grupo_asignado==null || grupo_asignado.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(compania==null || compania.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(organizacion_asignado==null || organizacion_asignado.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(comp_sup==null || comp_sup.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(id_remedy==null || id_remedy.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(id_sgp==null || id_sgp.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(nombre_completo==null || nombre_completo.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(login_id==null || login_id.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
		//B�SQUEDA DE ARS_GRUPO POR GRUPO_ASIGNADO Y COMPA��A
		ArsGrupo arsGrupo = null;
		arsGrupo = dataSetController.findGrupoByGrupoCompania(grupo_asignado, compania);
		if(arsGrupo!=null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_REPETIDO);
			
			return genericResponseVO;
		}
		
		return genericResponseVO;
	}

	public GenericResponseVO validarFormulario(Map<String, String> formularioMap)
	{
		DataSetController dataSetController = new DataSetController();
		GenericResponseVO genericResponseVO = new GenericResponseVO();
		
		String paisCompania = formularioMap.get("paisCompania");
		String formulario	= formularioMap.get("formulario");
		String crudAction	= formularioMap.get("crudAction");
		
		if(paisCompania==null || paisCompania.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(formulario==null || formulario.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(crudAction==null || crudAction.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
//		formulario = formulario.replaceAll(" ", "_");
		
		//USAR EL METODO getCompania PARA EXTRAER EL CAMPO [compania]
		String compania = getCompania(paisCompania);
		
		if(compania==null || compania.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
		//OBTENER EL REGISTRO DE ARS_PAISCOMPANIA
		ArsPaisCompania arsPaisCompania = dataSetController.findPaisCompaniaByCompania(compania);
		if(arsPaisCompania==null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_INEXISTENTE);
			
			return genericResponseVO;
		}
		else
		{
			/*
			 * ASIGNAR EL CAMPO [compania] A [ValueObjectVO] dentro
			 * de [genericResponseVO] para regresarlo en la respuesta
			 * ya validado.
			 */
//			genericResponseVO.setValueObjectVO(compania);
		}
		
		//B�SQUEDA DE REGISTROS REPETIDOS
		ArsFormulario arsFormulario = null;
		arsFormulario = dataSetController.findFormularioByCompaniaAndForm(compania, formulario);
		if(arsFormulario!=null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_REPETIDO);
			
			return genericResponseVO;
		}
		
		genericResponseVO.setAgregar(true);
		
		return genericResponseVO;
	}

	public GenericResponseVO validarUrgency(Map<String, String> urgencyMap)
	{
		DataSetController dataSetController = new DataSetController();
		GenericResponseVO genericResponseVO = new GenericResponseVO();
		
		genericResponseVO.setAgregar(true);

		String	paisCompania= urgencyMap.get("paisCompania");
		String	valor		= urgencyMap.get("valor");
		int		numero		= Integer.parseInt(urgencyMap.get("numero"));
		
		if(paisCompania==null || paisCompania.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(valor==null || valor.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}

		if(urgencyMap.get("numero").compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
		//EXTRAER LA COMPA��A DE paisCompania
		String compania=getCompania(paisCompania);
		if(compania.compareTo("")==0)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
			
			return genericResponseVO;
		}
		
		//BUSQUEDA DE ARS_PAISCOMPANIA
		ArsPaisCompania arsPaisCompania = dataSetController.findPaisCompaniaByCompania(compania);
		if(arsPaisCompania==null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.CAMPO_INEXISTENTE);
			
			return genericResponseVO;
		}
		else
		{
			genericResponseVO.setValueObjectVO(compania);
		}
		
		//B�SQUEDA DE ARS_URGENCY POR VALOR Y COMPA��A
		ArsUrgency arsUrgency = new ArsUrgency();
		arsUrgency = dataSetController.findUrgencyByCompaniaAndValor(compania, valor);
		if(arsUrgency!=null)
		{
			genericResponseVO.setAgregar(false);
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_REPETIDO);
			
			return genericResponseVO;
		}
		
		return genericResponseVO;
	}
	
	
	
	
	

	//EXTRAE LA COMPA��A DE paisCompania
	private String getCompania(String paisCompania)
	{
		String compania="";
		
		Pattern pattern = Pattern.compile("'(\\w+?)'+'(\\w+?)'");
		Matcher matcher = pattern.matcher(paisCompania);
		while(matcher.find())
		{
			String key	= matcher.group(1);
			String value= matcher.group(2);
			
			if(key.compareTo("compania")==0)
				compania=value;
		}
		
		if(compania.compareTo("")==0)
			System.out.println("NO SE ENCONTR� el campo [compania] dentro de [paisCompania].");
		
		return compania;
	}

}
