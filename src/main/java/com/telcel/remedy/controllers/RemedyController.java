package com.telcel.remedy.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bmc.arsys.api.ARException;
import com.telcel.remedy.ars.itsm.RmdSelect;
import com.telcel.remedy.ars.itsm.RmdInsert;
import com.telcel.remedy.ars.itsm.RmdUpdate;
import com.telcel.remedy.ars.model.InsertVO;
import com.telcel.remedy.ars.model.RemedyControlResultado;
import com.telcel.remedy.ars.model.UpdateVO;

@RestController()
public class RemedyController {
	
	@Autowired 
	@Qualifier("rmdSelect")
	RmdSelect voRC;
	
	@Autowired
	@Qualifier("rmdInsert")
	RmdInsert voInsert;
	
	@Autowired
	@Qualifier("rmdUpdate")
	RmdUpdate voUpdate;
	
	
	@GetMapping("/rmdSelect")
	public RemedyControlResultado fncTest(@RequestBody RemedyControlResultado voParametros) throws ARException{
			//RemedyControlResultado voParametros = new RemedyControlResultado(psSistema, psFormulario, psColumnas, psCondiciones,psOrden);			
			return voRC.fncRmdSelect(voParametros);
	}
	
	@PostMapping("/rmdInsert")
	public InsertVO fncRmdInsert(@RequestBody InsertVO voParametros) throws ARException {
		return voInsert.fncRmdInsert(voParametros);
	}
	
	@PutMapping("/rmdUpdate")	
	public UpdateVO fncRmdUpdate(@RequestBody UpdateVO voPArams) throws ARException{
		return voUpdate.fncRmdUpdate(voPArams);
	}
	
}
