package com.telcel.remedy.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Entry;
import com.telcel.remedy.ars.itsm.RmdSelect;
import com.telcel.remedy.ars.itsm.RmdInsert;
import com.telcel.remedy.ars.model.RemedyControlEntry;
import com.telcel.remedy.ars.model.RemedyControlResultado;
import com.telcel.remedy.model.dao.IArsUrgency;
import com.telcel.remedy.model.dao.ICtgServidores;
import com.telcel.remedy.model.entity.ArsClient;
import com.telcel.remedy.model.entity.ArsClientDwAttachement;
import com.telcel.remedy.model.entity.ArsClientField;
import com.telcel.remedy.model.entity.ArsCompania;
import com.telcel.remedy.model.entity.ArsEntry;
import com.telcel.remedy.model.entity.ArsFields;
import com.telcel.remedy.model.entity.ArsPais;
import com.telcel.remedy.model.entity.ArsUrgency;
import com.telcel.remedy.model.entity.ArsUsers;
import com.telcel.remedy.model.entity.CtgServidores;
import com.telcel.remedy.model.services.IArsClientDwAttachementService;
import com.telcel.remedy.model.services.IArsClientFieldService;
import com.telcel.remedy.model.services.IArsClientService;
import com.telcel.remedy.model.services.IArsEntryService;
import com.telcel.remedy.model.services.IArsFieldsService;
import com.telcel.remedy.model.services.IArsUrgencyService;
import com.telcel.remedy.model.services.IArsUsersService;
import com.telcel.remedy.model.services.ICtgServidoresService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ArsClientController
{
	@Autowired
//	@Qualifier("ArsClientServiceImpl")
	IArsClientService voServices;
	
	@Autowired 
	@Qualifier("ctgServidoresServiceImpl")
	ICtgServidoresService voServidores;
	
	@Autowired 
	@Qualifier("arsUsersServiceImpl")
	IArsUsersService voUsuarios;
	
	@Autowired 
	@Qualifier("arsClientDwAttachementServiceImpl")
	IArsClientDwAttachementService voAttachement;
	
	@Autowired 
	@Qualifier("arsClientFieldServiceImpl")
	IArsClientFieldService voClientField;

	@Autowired 
	@Qualifier("arsFieldsServicesImpl")
	IArsFieldsService voFields;
	
	@Autowired 
	@Qualifier("arsEntryServiceImpl")
	IArsEntryService voEntry;
	//*************************************
	@Autowired 
	@Qualifier("arsUrgencyServiceImpl")
	IArsUrgencyService voUrgency;
	//	IArsUrgency voArsUrgencyDao;
	//*************************************
	@GetMapping("/arsClient")
	public List<ArsClient> fncBusqueda(){
		return voServices.findAll();
	}
	
	@GetMapping("/ctgServidores")
	public List<CtgServidores> fncBusquedaServidores(){
		return voServidores.findAll();
	}
	
	@GetMapping("/arsUsers")
	public List<ArsUsers> fncBusquedaUsuarios(){
		return voUsuarios.findAll();
	}
	
	@GetMapping("/arsUsers/{psAlias}")
	public ArsUsers fncBusquedaUsuariosPorAlias(@PathVariable String psAlias){
		return voUsuarios.findByAlias(psAlias);
	}
	
	@GetMapping("/arsClientFild")
	public List<ArsClientField> fncBusquedaCampos(){
		return voClientField.findAll();
	}
	
	@GetMapping("/arsClientDwAttachement")
	public List<ArsClientDwAttachement> fncBusquedarDescarga(){
		return voAttachement.findAll();
	}
	
	@GetMapping("/arsFields")
	public List<ArsFields> fncBusquedarFields(){
		return voFields.findAll();
	}
	
	@GetMapping("/arsEntry")
	public List<ArsEntry> fncBusquedarEntry(){
		return voEntry.findAll();
	}

	@RequestMapping("/arsClient/{id}")
	public ArsClient fncBusquedaID(@PathVariable Long id) {
		//voServices.delete(id);
		return voServices.findById(id);
	}
	//*************cambio/06/11
	@GetMapping("/arsClient/{arcId}")
//	@GetMapping("/arsClient")
	public ArsClient findByArcId(@PathVariable String arcId){
		return voServices.findByArcId(arcId);
	}
	//*************
	
/*
	@RequestMapping("/arsClient/{client}")
	public ArsClient findByClient(@PathVariable String client) {
		//voServices.delete(id);
		return voServices.findByClient(client);
	}
*/
	//**********************************
/*
 * 	@GetMapping("/arsClient/{cliente}")
	public ArsClient clienteByCliente(@PathVariable String cliente){
		return voServices.findByClient(cliente);
	}
 */
	//**********************************
	

}
