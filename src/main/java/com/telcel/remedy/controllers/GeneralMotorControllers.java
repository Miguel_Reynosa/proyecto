package com.telcel.remedy.controllers;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import java.util.stream.Collectors;
import org.apache.http.Header;
import org.apache.http.HttpResponse;

import com.google.common.util.concurrent.Service;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.CloseableHttpClient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.web.bind.annotation.PutMapping;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
//import org.apache.log4j.Logger;
import org.hibernate.criterion.CountProjection;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bmc.arsys.api.ARException;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonElement;
import com.telcel.gm.component.Herramienta;
import com.telcel.gm.component.ValidarProvider;
import com.telcel.gm.component.console;
import com.telcel.gm.match.GestionCampoPais;
import com.telcel.gm.match.GestionCategoria;
import com.telcel.gm.match.Validador;
import com.telcel.gm.match.creaObjeto;
import com.telcel.gm.match.excepcionElemento;
import com.telcel.gm.model.CreateCase;
import com.telcel.gm.model.Formulario;

//import com.telcel.gm.model.ErrorHandler;
//import com.telcel.gm.model.NotifyCase;
import com.telcel.gm.model.ResposeGM;
import com.telcel.gm.model.TroubleTicketAttachment;
import com.telcel.gm.respuesta.Respuesta;
import com.telcel.gm.security.Greeting;
import com.telcel.gm.services.AttachmentService;
import com.telcel.gm.services.CreateCaseService;
import com.telcel.gm.services.DeleteCase;
import com.telcel.gm.services.QueryCase;
import com.telcel.gm.services.RetriveAttachmentService;
import com.telcel.gm.services.UpdateCaseService;
import com.telcel.remedy.SendPostRequest;
import com.telcel.remedy.pruebasCodigo;
import com.telcel.remedy.ars.itsm.RmdInsert;
import com.telcel.remedy.ars.itsm.RmdSelect;
import com.telcel.remedy.ars.itsm.RmdUpdate;
import com.telcel.remedy.ars.model.FieldRC;
import com.telcel.remedy.ars.model.InsertVO;
import com.telcel.remedy.ars.model.RemedyControlEntry;
import com.telcel.remedy.ars.model.RemedyControlResultado;
import com.telcel.remedy.ars.model.UpdateVO;
import com.telcel.remedy.facade.GeneralMotorsFacade;
import com.telcel.remedy.facade.GeneralMotorsFacadeFacadeImpl;

import com.telcel.remedy.model.constantes.ConstantesRemedy;
import com.telcel.remedy.model.dao.IArsClientTag;
import com.telcel.remedy.model.entity.ArsCategoria;
import com.telcel.remedy.model.entity.ArsPaisCompania;
import com.telcel.remedy.model.services.IArsFormularioService;
import com.telcel.remedy.vo.CreateCaseVO;

import com.telcel.gm.model.CreateCase;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController()
public class GeneralMotorControllers {
	// static Logger logger = Logger.getLogger(GeneralMotorControllers.class);
	@Autowired
	CreateCaseVO createCaseVO;

	static final String xApiVersion = "1";
	static CloseableHttpClient httpClient;
	static HttpResponse httpResponse;
	static String userCredentials;
	static HttpPost postRequest;
	static String basicAuth;
	static String uri;
	List<String> listafindfinal = new ArrayList();
	ValidarProvider valP = new ValidarProvider();

		@RequestMapping(value = "/troubleticket", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<String> addAttachment(@RequestBody String voAttachment,
			@RequestHeader Map<String, String> headerRequest, HttpServletResponse httpServletResponse)
			throws ARException, IOException {

		GeneralMotorsFacade create = new GeneralMotorsFacadeFacadeImpl();
		createCaseVO.setVoCuerpo(voAttachment);
		return create.createCase(headerRequest, createCaseVO);
		// return null;
	}

	// ------------------------------------------------------
//	@PatchMapping("/troubleticket/{id}")

	@RequestMapping(value = "/troubleticket/{id}", method = RequestMethod.PUT, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 401, message = "You are not authorized access the resource"),
			@ApiResponse(code = 404, message = "The resource not found") })
	public ResponseEntity<String> updateCase(
			@RequestBody String voAttachment
			, @PathVariable String id
			, @RequestHeader Map<String, String> headerRequest
			, HttpServletResponse httpServletResponse)
				throws ARException, IOException
	{
		console.println("Log4j: empieza Update Case !!");
		console.println("GeneralMotorsControllers: updateCase");
		
		createCaseVO.setIncidentNumber(id);
		
		// Llamar al servicio (update)
		GeneralMotorsFacade create = new GeneralMotorsFacadeFacadeImpl();
		
		console.println("VoCase", createCaseVO);
		
		createCaseVO.setVoCuerpo(voAttachment);
		
		return create.updateCase(headerRequest, createCaseVO);
	}

	@GetMapping("/troubleticket/")
	public ResponseEntity<ObjectNode> queryNormal(CreateCase model, @RequestHeader Map<String, String> headerRequest,
			HttpServletResponse httpServletResponse) throws ARException, IOException {
		ResponseEntity<ObjectNode> queryCase = null;

		queryCase = QueryCase("", model, headerRequest, httpServletResponse);
		return queryCase;
	}

	@RequestMapping(value = "/notify", method = RequestMethod.PUT, produces = "application/json")
	public String notifyService(@RequestBody String bodyJson, @RequestHeader Map<String, String> headerRequest)
			throws UnsupportedEncodingException, IOException, JSONException {

		JsonObject jsonObject = (new JsonParser()).parse(bodyJson).getAsJsonObject();
		String id = jsonObject.get("eventId").toString().replaceAll("\"", "");
		String eventType = jsonObject.get("eventType").toString().replaceAll("\"", "");
		String status = jsonObject.get("statusNotify").toString().replaceAll("\"", "");

		String enterpriseId = "GM_CLARO_BRA";
		String correlationId = "0";
		String providerId = headerRequest.get("providerid");
		String country = "BR";
		String countryM2M = "BR";

		Long eventIdref = 0L;

		File file = new File("/home/remedy/filename_all_reloaded4.txt");

		Scanner scanner = new Scanner(file);

		// now read the file line by line...
		int lineNum = 0;
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			JSONObject jsonObj = new JSONObject(line);
			long valorjson = Long.parseLong((String) jsonObj.get("id"));
			if (jsonObj.getString("eventId").equals(id) && jsonObj.get("status").equals("OPEN")) {
				eventIdref = jsonObj.getLong("id");
				correlationId = jsonObj.get("CorrelationId").toString();
				countryM2M = jsonObj.get("countryM2M").toString();
				country = jsonObj.get("country").toString();
				enterpriseId = jsonObj.get("enterpriseId").toString();
			}
		}

		Long a = System.currentTimeMillis();
		Long b = 0L;

		try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream("/home/remedy/instancesToSend.txt", true), "UTF-8"))) {
			Gson gson = new Gson();
			long eventIdint = System.currentTimeMillis();
			int correlationidprueba = 0;
			String input = "{\"enterpriseId\":" + "\"" + enterpriseId + "\",\"providerId\":\"" + providerId
					+ "\",\"id\":\"" + eventIdref + "\",\"country\":\"" + country + "\",\"countryM2M\":\"" + countryM2M
					+ "\",\"helpId\":\"" + eventIdint + "\",\"eventId\":\"" + id + "\",\"eventType\":" + "\""
					+ eventType + "\"" + ",\"externalId\":" + "null" + ",\"status\":\"" + status + "\""
					+ ",\"CorrelationId\":\"" + correlationId + "\"" + "}";
			console.println("INPUT WITH NEW ID " + input);
			writer.write(input);
			writer.newLine();
		}

		return "success";
	}

	@GetMapping("/troubleticket/{id}")
	public ResponseEntity<ObjectNode> QueryCase(@PathVariable String id, CreateCase model,
			@RequestHeader Map<String, String> headerRequest, HttpServletResponse httpServletResponse)
			throws ARException, IOException {
		console.println("Log4j: empieza queyCase !!");
		GeneralMotorsFacade create = new GeneralMotorsFacadeFacadeImpl();
		console.println("VoCase", createCaseVO);
		createCaseVO.setEntityCase(model);
		createCaseVO.setIncidentNumber(id);

		System.out.println("model createCase" + model);
		return create.queryCase(headerRequest, createCaseVO);

	}

	@DeleteMapping("/troubleticket/{id}")
	public ResponseEntity<String> deleteCase(@RequestBody String voAttachment, @PathVariable String id,
			HttpServletRequest headerRequest, HttpServletResponse httpServletResponse) throws ARException, IOException {
		console.println("Log4j : empieza deleteCase !!");

		// DeleteCase delete=new
		// DeleteCase(voUpdate,voAttachmentService,voSelect,voInsert,env); SE COMENTO
		// ESTO TEMPORALMENTE
		DeleteCase delete = new DeleteCase();// SE PUSO ESTO TEMPORALMENTE

		return delete.delete(voAttachment, id, headerRequest, httpServletResponse);
	}

	@RequestMapping(value = "/troubleticketAttachment/{troubleticketId}/troubleticket/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ObjectNode> retrieveAttachment(@PathVariable String id, @PathVariable String troubleticketId,
			@RequestHeader Map<String, String> headerRequest, HttpServletResponse httpServletResponse)
			throws ARException {
		console.println("Log4j : empieza retrieveTroubleTicket !!");
		HttpHeaders responseHeaders = new HttpHeaders();
		GestionCampoPais campoPais = new GestionCampoPais();
		headerRequest = campoPais.changeHeaderToCloud(headerRequest);

		Respuesta respuesta = new Respuesta();

		GeneralMotorsFacade create = new GeneralMotorsFacadeFacadeImpl();
		console.println("VoCase", createCaseVO);
		createCaseVO.setNumberWorklog(troubleticketId);
		createCaseVO.setIncidentNumber(id);

		return create.retrieveAttachment(headerRequest, createCaseVO);
	}

	@RequestMapping(value = "/troubleticket/{id}/troubleticketAttachment/{troubleticketId}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<String> deleteAttachment(@PathVariable String id, @PathVariable String troubleticketId,
			HttpServletRequest headerRequest, HttpServletResponse httpServletResponse) throws ARException {
		console.println("Log4j : empieza deleteTroubleTicket !!");

		RetriveAttachmentService retrive = new RetriveAttachmentService();
		// SE COMENTO EL DE ABAJO PARA AVANCE DE LOS DEMAS METODOS.LL
		//	return voSelect.fncRmdDeleteAttatchment(retrive.fncAttachmentRretrieve(id, troubleticketId,"TEMIP"),troubleticketId,headerRequest,httpServletResponse);
		return null;

	}

	@RequestMapping(value = "/troubleticketAttachment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ResposeGM> addAttachment(@RequestBody @Valid TroubleTicketAttachment voAttachment,
			@RequestHeader Map<String, String> headers, BindingResult result) throws ARException, IOException {
		GestionCampoPais campoPais = new GestionCampoPais();
		headers = campoPais.changeHeaderToCloud(headers);

		Respuesta respuesta = new Respuesta();

		GeneralMotorsFacade create = new GeneralMotorsFacadeFacadeImpl();
		console.println("VoCase", createCaseVO);
		createCaseVO.setIncidentNumber(voAttachment.getId());
		createCaseVO.setAttachmentVO(voAttachment);
		ResponseEntity<ResposeGM> attachmentResponse = create.addAttachment(headers, createCaseVO);

		console.println("attachmentResponse", attachmentResponse);
		console.println("attachmentResponse.getBody()", attachmentResponse.getBody());

		if (attachmentResponse.getBody() != null)
			try {
				String bodyJson = "{\"eventType\":" + "\"AddTroubletTicketAttachment\"" + ",\"statusNotify\":\""
						+ "ATTACHED" + "\"" + ",\"eventId\":\"" + voAttachment.getTroubleTicketId() + "\"" + "}";
				console.println("bodyjson  sds" + bodyJson);
				notifyService(bodyJson, headers);
			} catch (IOException | JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return attachmentResponse;

	}

	@RequestMapping(value = "/getIncidentNumber", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> QueryIncNumber(@RequestHeader Map<String, String> headers) throws ARException {
		console.println("\nEntra a QueryIncNumber");

		// CONSEGUIR EL ID DE R CLOUD (¿¿¿poner condiciones???)
		console.println("Log4j: empieza queyCase !!");
		String id = headers.get("id");
		String idLocal = headers.get("idlocal");

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

		GeneralMotorsFacade create = new GeneralMotorsFacadeFacadeImpl();
		console.println("VoCase", createCaseVO);
		// createCaseVO.setEntityCase(model);

		createCaseVO.setIncidentNumber(id);
		createCaseVO.setIdLocal(idLocal);
		ResponseEntity<String> attachmentResponse = create.QueryIncNumber(headers, createCaseVO);

		return attachmentResponse;
	}
	/*
	 * *******************************************************************************
	 * import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono

@Component
class CorsFilter : WebFilter {
    override fun filter(ctx: ServerWebExchange?, chain: WebFilterChain?): Mono<Void> {
        if (ctx != null) {
            ctx.response.headers.add("Access-Control-Allow-Origin", "*")
            ctx.response.headers.add("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS")
            ctx.response.headers.add("Access-Control-Allow-Headers", "DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range")
            if (ctx.request.method == HttpMethod.OPTIONS) {
                ctx.response.headers.add("Access-Control-Max-Age", "1728000")
                ctx.response.statusCode = HttpStatus.NO_CONTENT
                return Mono.empty()
            } else {
                ctx.response.headers.add("Access-Control-Expose-Headers", "DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range")
                return chain?.filter(ctx) ?: Mono.empty()
            }
        } else {
            return chain?.filter(ctx) ?: Mono.empty()
        }
    }
}
	 * *******************************************************************************
	 */
	/*****
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * public void Reader() throws IOException, JSONException {
	 * console.println("Debug reader inside 0 "); int counter = 0; // File file =
	 * new File("C:/Users/ARCabrera/lol.txt"); File file = new
	 * File("/home/remedy/filename_all_reloaded4.txt"); List<String> listafind = new
	 * ArrayList();
	 * 
	 * try { Scanner scanner = new Scanner(file); int lineNum = 0; while
	 * (scanner.hasNextLine()) { String line = scanner.nextLine();
	 * listafind.add(line); } listToSend(listafind); } catch(FileNotFoundException
	 * e) { }
	 * 
	 * }
	 * 
	 * 
	 * 
	 * private void listToSend (List<String> listafind) throws JSONException,
	 * IOException {
	 * 
	 * long eventIdint = System.currentTimeMillis(); long eventIdintminute =
	 * (System.currentTimeMillis())-60000;
	 * 
	 * for(int c = 0; c<listafind.size();c++) { JSONObject jsonObj = new
	 * JSONObject(listafind.get(c)); Long x =
	 * Long.parseLong(jsonObj.get("id").toString()); Long y =
	 * Long.parseLong(jsonObj.get("helpId").toString()); console.println("SHOULD BE
	 * JUST ONE "+jsonObj.toString()); if(x< eventIdint &&x> eventIdintminute||y<
	 * eventIdint &&y> eventIdintminute) { listafindfinal.add(jsonObj.toString());
	 * console.println("HERE IS THE JSON NEW"+jsonObj.toString());
	 * 
	 * 
	 * if(!jsonObj.getString("id").equals("0")) {
	 * pruebas(jsonObj.getString("eventType"), jsonObj.getString("eventId"),
	 * jsonObj.getString("id"), jsonObj.getString("status"),
	 * jsonObj.getString("CorrelationId"), jsonObj.getString("enterpriseId"),
	 * jsonObj.getString("providerId"), jsonObj.getString("country"),
	 * jsonObj.getString("countryM2M"));
	 * 
	 * 
	 * } else { console.println("EVENT ID IS NOT IN DATABASE"); }
	 * 
	 * } }
	 * 
	 * }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * Map<Boolean, Boolean> cache = new ConcurrentHashMap<>();
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * public void pruebas(String eventType ,String eventId,String id, String
	 * status, String correlationId, String enterpriseId, String providerId, String
	 * country, String countryM2M) throws IOException { String json = ""; String
	 * timeStamp = new SimpleDateFormat("yyyyMMdd
	 * HH:mm:ss").format(Calendar.getInstance().getTime());
	 * 
	 * String input =
	 * "{\"eventType\":"+"\""+eventType+"\",\"eventTime\":\""+timeStamp+"\",\"CorrelationId\":\""+correlationId+"\",\"eventId\":\""+eventId+"\",\"id\":\""+id+"\",\"externalId\":"+"null"+",\"status\":\""+status+"\""+"}";
	 * String serviceURL =""; try (BufferedWriter writer = new BufferedWriter(new
	 * OutputStreamWriter( new FileOutputStream("/home/remedy/filename.txt", true),
	 * "UTF-8"))) { writer.write(input); writer.newLine();
	 * if(eventType.equals("AddTroubletTicketAttachment")) { serviceURL =
	 * "http://100.127.44.3:8080/customerinteraction/remedy/troubleticketattachmentevent";
	 * }else { serviceURL =
	 * "http://100.127.44.3:8080/customerinteraction/remedy/troubleticketevent"; }
	 * console.println("service url"+serviceURL);
	 * 
	 * httpClient = HttpClientBuilder.create().build(); String userpass =
	 * "soaTester" + ":" + "claro#123"; String userCredentials = "Basic " +
	 * javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes("UTF-8"));
	 * 
	 * //Gera Header para envio postRequest = new HttpPost(serviceURL);
	 * postRequest.addHeader("Authorization",userCredentials);
	 * postRequest.addHeader("X-Api-Version", xApiVersion);
	 * postRequest.addHeader("enterpriseId", enterpriseId);
	 * postRequest.addHeader("correlationId", correlationId);
	 * postRequest.addHeader("providerId", providerId);
	 * postRequest.addHeader("country", country);
	 * postRequest.addHeader("countryM2M", countryM2M);
	 * 
	 * postRequest.addHeader("CorrelationId", correlationId);
	 * 
	 * 
	 * 
	 * 
	 * console.println("Authorization "+userCredentials);
	 * console.println("X-Api-Version " + xApiVersion);
	 * console.println("enterpriseId "+ enterpriseId);
	 * console.println("CorrelationId "+ correlationId);
	 * console.println("correlationId "+ correlationId); console.println("providerId
	 * "+ providerId); console.println("country "+ country);
	 * console.println("countryM2M "+ countryM2M);
	 * 
	 * 
	 * StringEntity inputEntity = new StringEntity(input, "UTF-8");
	 * inputEntity.setContentType("application/json");
	 * 
	 * postRequest.setEntity(inputEntity); //manda
	 * 
	 * httpResponse = httpClient.execute(postRequest); String ResponseTratado ="";
	 * ResponseTratado += httpResponse.getStatusLine()+"\n"; Header[] headers =
	 * httpResponse.getAllHeaders();
	 * 
	 * 
	 * for (Header header : headers) ResponseTratado += header.getName() +" "+
	 * header.getValue()+"\n"; console.println("ResponseTratado"+ResponseTratado);
	 * String jsonDoResponse = new BufferedReader(new
	 * InputStreamReader(httpResponse.getEntity().getContent())).readLine();
	 * console.println("json response"+jsonDoResponse);
	 * 
	 * 
	 * } }
	 * 
	 ********/

}
	