package com.telcel.remedy.controllers;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.SessionStatus;
import com.bmc.thirdparty.org.springframework.validation.BindingResult;
import com.google.gson.JsonObject;
import com.telcel.gm.component.console;
import com.telcel.remedy.model.constantes.ConstantesPagina;
import com.telcel.remedy.model.entity.ArsCategoria;
import com.telcel.remedy.model.entity.ArsClient;
import com.telcel.remedy.model.entity.ArsClientTag;
import com.telcel.remedy.model.entity.ArsGrupo;
import com.telcel.remedy.model.entity.ArsMotivos;
import com.telcel.remedy.model.entity.ArsCompania;
import com.telcel.remedy.model.entity.ArsFormulario;
import com.telcel.remedy.model.entity.ArsPais;
import com.telcel.remedy.model.entity.ArsPaisCompania;
import com.telcel.remedy.model.entity.ArsTag;
import com.telcel.remedy.model.entity.ArsUrgency;
import com.telcel.remedy.model.services.IArsCategoriaService;
import com.telcel.remedy.model.services.IArsGruposService;
import com.telcel.remedy.model.services.IArsClientTagService;
import com.telcel.remedy.model.services.IArsCompaniaService;
import com.telcel.remedy.model.services.IArsFormularioService;
import com.telcel.remedy.model.services.IArsMotivosService;
import com.telcel.remedy.model.services.IArsPaisCompaniaService;
import com.telcel.remedy.model.services.IArsPaisService;
import com.telcel.remedy.model.services.IArsTagService;
import com.telcel.remedy.model.services.IArsUrgencyService;
import com.telcel.remedy.util.Validaciones;
import com.telcel.remedy.vo.GenericResponseVO;

@RestController
@RequestMapping("/api")
//@CrossOrigin(origins = {"http://localhost:4200"})
@CrossOrigin
public class DataSetController
{
	@Autowired
	private IArsCompaniaService companiaDao;
	
	@Autowired
	private IArsPaisService paisDao;
	
	@Autowired
	private IArsPaisCompaniaService paisCompaniaDao;
	
	@Autowired
	private IArsCategoriaService categoriaDao;
	
	@Autowired
	private IArsTagService tagDao;
	
	@Autowired
	private IArsGruposService gruposDao;
	
	@Autowired
	private IArsUrgencyService urgencyDao;
	
	@Autowired
	IArsClientTagService clientTagDao;
	
	@Autowired
	ArsClientController clientService;
	
	@Autowired
	private IArsMotivosService motivosDao;
	
	@Autowired
	private IArsFormularioService formularioDao;

	//--------------------------------------------------------------------------------------------------------------------------
	//************************************************C O M P A N I A***********************************************************	

	@GetMapping("/arsCompania")
	public List<ArsCompania> findAllCompanias()
	{
		System.out.println("Getting all companias");
		return companiaDao.findAll();
	}
		
	@GetMapping("/arsCompania/id/{id}")
	public ArsCompania findCompaniaById(@PathVariable Long id)
	{
		return companiaDao.findById(id);
	}

	@GetMapping("/arsCompania/compania/{compania}")
	public ArsCompania findCompaniaByCompania(@PathVariable String compania)
	{
		compania = compania.toUpperCase();
		
		System.out.println("compania:"+compania);
		
		ArsCompania arsCompaniaFound = null;
		
		arsCompaniaFound = companiaDao.findByCompania(compania);
		
		if(arsCompaniaFound==null)
		{
			System.out.println("No se encontr� ning�n registro con esos datos.");
		}
		else
		{
			System.out.println("Compa��a encontrada:"+arsCompaniaFound);
		}
		
		return arsCompaniaFound;
	}
	
	@GetMapping("/arsCompania/companias/{filtro}")
	public List<ArsCompania> fncBusquedaByCompania(@PathVariable String filtro)
	{
		return companiaDao.findAll().stream().filter(x -> x.getCompania().equals(filtro.toUpperCase()))
				.collect(Collectors.toList());
	}
	
	@GetMapping("/arsCompania/estado/{estado}")
	public List<ArsCompania> findCompaniaByEstado(@PathVariable Integer estado)
	{
		System.out.println("\nEstado recibido en arsCompania:"+estado);
		
		List<ArsCompania> arsCompaniaList = null;
		arsCompaniaList = companiaDao.findByEstado(estado);
		
		if(arsCompaniaList==null || arsCompaniaList.isEmpty())
		{
			System.out.println("No se encontraron registros con los datos ingresados.");
			return null;
		}
		else
		{
			System.out.println("REGISTROS ENCONTRADOS:"+arsCompaniaList);
		}
		
		return companiaDao.findByEstado(estado);
	}
	
	@PostMapping("/arsCompania")
	public GenericResponseVO addCompania(@RequestBody Map<String, String> companiaMap)
	{
		Validaciones validaciones = new Validaciones();
		GenericResponseVO genericResponseVO = new GenericResponseVO();
		
		genericResponseVO = validaciones.validarCompania(companiaMap);
		
		if(genericResponseVO.isAgregar()==true)
		{
			ArsCompania arsCompania = new ArsCompania();
			
			//EXTRAER DATOS
			String	compania= genericResponseVO.getValueObjectVO().toString();
			int		estado	= Integer.parseInt(companiaMap.get("estado"));
			
			if(compania.compareTo("")==0)
			{
				genericResponseVO.setMensaje(ConstantesPagina.CAMPO_VACIO);
				return genericResponseVO;
			}
			
			//SETEAR DATOS
			arsCompania.setCompania(compania);
			arsCompania.setEstado(estado);
			
			//MOSTRARLOS
			System.out.println(arsCompania.toString());
			
			//GUARDAR REGISTRO
			companiaDao.save(arsCompania);
			
			//SETEAR RESPUESTA AL USUARIO
			genericResponseVO.setValueObjectVO(arsCompania);
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_EXITOSO);
		}
		
		return genericResponseVO;
	}
	
	//----------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------P A I S------------------------------------------------------------------	
	@GetMapping("/arsPais")
	public List<ArsPais> findAllPais()
	{
		System.out.println("Getting all paises");
		return paisDao.findAll();
	}
		
	@GetMapping("/arsPais/id/{id}")
	public ArsPais findPaisById(@PathVariable Long id)
	{
		return paisDao.findById(id);
	}

	@GetMapping("/arsPais/pais/{pais}")
	public ArsPais findPaisByPais(@PathVariable String pais)
	{
		pais = pais.toUpperCase();
		
		System.out.println("Pa�s ingresado en ArsPais:"+pais);
		
		ArsPais arsPaisFound = paisDao.findPais(pais);
		
		if(arsPaisFound==null)
		{
			System.out.println("No se encontr� ning�n registro con el pa�s ["+pais+"]");
		}
		else
		{
			System.out.println("REGISTRO ENCONTRADO:"+arsPaisFound);
		}
		
		return arsPaisFound;
	}
	
	@GetMapping("/arsPais/paisIncidente/{paisIncidente}")
	public ArsPais findPaisByPaisIncidente(@PathVariable String paisIncidente)
	{
		paisIncidente = paisIncidente.toUpperCase();
		
		System.out.println("\nPa�s Incidente ingresado en arsPais:"+paisIncidente);
		
		ArsPais arsPaisFound = paisDao.findByPaisIncidente(paisIncidente);
		
		if(arsPaisFound==null)
		{
			System.out.println("No se encontr� ning�n registro con el paisIncidente:"+paisIncidente);
		}
		else
		{
			System.out.println("REGISTRO ENCONTRADO:"+arsPaisFound);
		}
		
		return arsPaisFound;
	}
	
	@GetMapping("/arsPais/estado/{estado}")
	public List<ArsPais> paisEstado(@PathVariable Integer estado)
	{
		System.out.println("Estado recibido en arsPais:"+estado);
		
		List<ArsPais> arsPaisList = null;
		
		arsPaisList = paisDao.findByEstado(estado);
		
		if(arsPaisList==null || arsPaisList.isEmpty())
		{
			System.out.println("No se encontraron registros con el estado: "+estado);
			return null;
		}
		else
		{
			System.out.println("REGISTROS ENCONTRADOS:"+arsPaisList);
		}
		
		return arsPaisList;
	}

	@GetMapping("/arsPais/paises/{filtro}")
	public List<ArsPais> paisByAlias(@PathVariable String filtro)
	{
		return paisDao.findAll().stream().filter(x -> x.getPais().equals(filtro.toUpperCase()))
				.collect(Collectors.toList());
	}
	
	@PostMapping("/arsPais")
	public GenericResponseVO addPais(@RequestBody Map<String, String> paisMap)
	{
		GenericResponseVO	genericResponseVO	= new GenericResponseVO();
		Validaciones		validaciones		= new Validaciones();
		
		//VALIDAR DATOS RECIBIDOS
		genericResponseVO = validaciones.validarPais(paisMap);
		
		System.out.println("Agregar: "+genericResponseVO.isAgregar());
		if(genericResponseVO.isAgregar()==true)
		{
			ArsPais arsPais = new ArsPais();
			
			//EXTRAER CAMPOS
			String	pais			= paisMap.get("pais");
			int		estado			= Integer.parseInt(paisMap.get("estado"));
			String	paisIncidente	= paisMap.get("paisIncidente");
			
			//MOSTRAR CAMPOS
			System.out.println("DATOS RECIBIDOS:"
					+ "\npais:"			+ pais
					+ "\nestado:"		+ estado
					+ "\npaisIncidente:"+ paisIncidente
			);
			
			//SETEAR CAMPOS
			arsPais.setPais(pais);
			arsPais.setEstado(estado);
			arsPais.setPaisIncidente(paisIncidente);
			
			//MOSTRAR CAMPOS SETEADOS
			System.out.println(arsPais.toString());
			//ASIGNARLOS A UN VO EN LA RESPUESTA (PARA QUE EL USUARIO VEA QU� SE AGREG�)
			genericResponseVO.setValueObjectVO(arsPais.toString());
			
			//AGREGAR REGISTRO
			paisDao.save(arsPais);
			
			//SETEAR MENSAJE DE REGISTRO EXITOSO
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_EXITOSO);
			genericResponseVO.setValueObjectVO(arsPais);
		}
		
		return genericResponseVO;
	}
	
	//----------------------------------------------------------------------------------------------------------------------
	//------------------------------------P A I S  C O M P A N I A----------------------------------------------------------
	@GetMapping("/arsPaisCompania")
	public List<ArsPaisCompania> findAllPaisCompania()
	{
		System.out.println("Getting all pais-companias");
		return paisCompaniaDao.findAll();
	}

	@GetMapping("/arsPaisCompania/id/{id}")
	public ArsPaisCompania findPaisCompaniaById(@PathVariable Long id)
	{
		return paisCompaniaDao.findById(id);
	}
	
	@GetMapping("/arsPaisCompania/pais/{pais}")
	public List<ArsPaisCompania> findPaisCompaniaByPais(@PathVariable String pais)
	{
		System.out.println("DATOS INGRESADOS:"
				+ "\n+ pais:"+pais.toUpperCase());
		
		List<ArsPaisCompania> paisCompaniasFound = null;
		
		paisCompaniasFound = paisCompaniaDao.findAll().stream().filter(
				x -> x.getArsPais().getPais().equals(pais.toUpperCase())
			).collect(Collectors.toList()
		);
		
		if(paisCompaniasFound==null)
		{
			System.out.println("No se encontr� ning�n registro con los datos ingresados.");
		}
		else
		{
			System.out.println("REGISTROS ENCONTRADOS:\n"+paisCompaniasFound);
		}
		
		return paisCompaniasFound;
	}
	
//	@GetMapping("/arsPaisCompania/pais/{pais}/compania/{compania}")
//	public ArsPaisCompania findPaisCompaniaByPaisCompania(@PathVariable String pais, @PathVariable String compania)
//	{
//		pais	= pais.toUpperCase();
//		compania= compania.toUpperCase();
//		
//		System.out.println("DATOS INGRESADOS:"
//				+ "\n+ pais:"+pais
//				+ "\n+ compania:"+compania
//		);
//		
//		ArsPaisCompania paisCompaniaFound = paisCompaniaDao.findByCompania(pais, compania);
//		
//		if(paisCompaniaFound==null)
//		{
//			System.out.println("No se encontr� ning�n registro con los datos ingresados.");
//		}
//		else
//		{
//			System.out.println("Registro encontrado:"+paisCompaniaFound);
//		}
//		
//		return paisCompaniaDao.findByCompania(pais, compania);
//	}

	//UTIL PARA BUSCAR UN �NICO REGISTRO
	@GetMapping("/arsPaisCompania/compania/{compania}")
	public ArsPaisCompania findPaisCompaniaByCompania(@PathVariable String compania)
	{
		compania = compania.toUpperCase();
		
		System.out.println("DATOS INGRESADOS:"
				+ "\n+ compania:"+compania);
		
		ArsPaisCompania paisCompaniaFound = null;
		paisCompaniaFound = paisCompaniaDao.findByCompania(compania);
		
		if(paisCompaniaFound == null)
		{
			System.out.println("No se encontr� ning�n registro con los datos ingresados.");
		}
		else
		{
			System.out.println("REGISTRO ENCONTRADO:\n"+paisCompaniaFound.toString());
		}
		
		return paisCompaniaFound;
	}
	
	@PostMapping("/arsPaisCompania")
//	public void addPaisCompania(@RequestBody Map<String,String> paisCompaniaMap)
//	public String addPaisCompania(@RequestBody Map<String,String> paisCompaniaMap)
	public GenericResponseVO addPaisCompania(@RequestBody Map<String,String> paisCompaniaMap)
	{
		ArsPaisCompania		arsPaisCompania		= new ArsPaisCompania();
		GenericResponseVO	genericResponseVO	= new GenericResponseVO();
		Validaciones		validaciones		= new Validaciones();
		
		genericResponseVO = validaciones.validarPaisCompania(paisCompaniaMap);
		
		if(genericResponseVO.isAgregar())
		{
			//SE EXTRAEN LOS CAMPOS POR SEPARADO
			String pais		= paisCompaniaMap.get("pais");
			String compania	= paisCompaniaMap.get("compania");
			String cliente	= paisCompaniaMap.get("cliente");

			//SE HACE LA B�SQUEDA DE SUS RESPECTIVAS ENTIDADES
			ArsPais		arsPais		= findPaisByPais(pais);
			ArsCompania	arsCompania = findCompaniaByCompania(compania);
			ArsClient	arsClient	= clientService.findByArcId(cliente);
			
			//SE AGREGAN A CADA CAMPO CORRESPONDIENTE
			arsPaisCompania.setArsPais(arsPais);
			arsPaisCompania.setArsCompania(arsCompania);
			arsPaisCompania.setArsClient(arsClient);
			
			paisCompaniaDao.save(arsPaisCompania);
			
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_EXITOSO);
			genericResponseVO.setValueObjectVO(arsPaisCompania);
		}
		
		return genericResponseVO;
		
		/*
		String mensaje;
		ArsPaisCompania arsPaisCompania = new ArsPaisCompania();
		
		// SE EXTRAEN LOS DATOS RECIBIDOS
		String pais		= paisCompaniaMap.get("pais").toUpperCase();
		String compania	= paisCompaniaMap.get("compania").toUpperCase();
		String cliente	= paisCompaniaMap.get("cliente").toUpperCase();
		
		//IMPRIMIR LOS DATOS QUE SE EXTRAJERON
		System.out.println("\nDATOS RECIBIDOS:"
				+ "\npais:"		+ pais
				+ "\ncompania:"	+ compania
		);
		
		// SE CREA UNA VARIABLE PARA BUSCAR SI YA EXISTE EL REGISTRO EN ARS_PAISCOMPANIA
		ArsPaisCompania paisCompaniaFound = null;
		
//		if(pais!=null && pais.compareTo("")!=0 && compania!=null && compania.compareTo("")!=0)
		if(compania!=null && compania.compareTo("")!=0)
		{
			paisCompaniaFound = findPaisCompaniaByCompania(compania);
		}
		else
		{
			System.out.println("No se ingres� la compa��a");
			return null;
		}
		
		System.out.println("Pais-Compania encontrado:"+paisCompaniaFound);
		
		//CONDICI�N PARA VALIDAR SI YA EXISTE EL REGISTRO CON LOS DATOS RECIBIDOS O NO
		if(paisCompaniaFound!=null)
		{
			mensaje = "Ya exise un registro con esos datos";
//			System.out.println("Ya exise un registro con el pa�s ["+pais+"] y la compa��a ["+compania+"]");
			return mensaje;
		}
		else
		{
			// SE AGREGA REGISTRO DE ARS_PAISCOMPANIA
			System.out.println("Agregando registro...");
			
			// BUSCAR DATOS PARA ARS_PAISCOMPANIA
			ArsPais		paisConseguido		= findPaisByPais(pais);
			ArsCompania	companiaConseguida	= findCompaniaByCompania(compania);
			ArsClient	clienteConseguido	= clientService.findByArcId(cliente);
			
			//IMPRIMIR DATOS CONSEGUIDOS
			System.out.println("DATOS CONSEGUIDOS"
					+ "\nArsPais:"		+ paisConseguido
					+ "\nArsCompania:"	+ companiaConseguida
					+ "\nArsClient:"	+ clienteConseguido
			);
			
			//SE AGREGAN LOS DATOS A ARS_PAISCOMPANIA
			arsPaisCompania.setArsPais(paisConseguido);
			arsPaisCompania.setArsCompania(companiaConseguida);
			arsPaisCompania.setArsClient(clienteConseguido);
			
			//SE GUARDA EL REGISTRO
			paisCompaniaDao.save(arsPaisCompania);
			
			mensaje = "Registro agregado exitosamente.";
			System.out.println(mensaje);
			return mensaje;
		}
		/**/
	}
	
	/*ByIdCompania
	@GetMapping("/arsPaisCompania/id/{id}")
	public List<ArsPaisCompania> fncPaisByCompaniaById(@PathVariable Long id) {
		return paisCompaniaDao.findByIdCompania(id);
	}/**/
	/*ByEstadoCompania*/
	@GetMapping("/arsPaisCompania/estadoComp/{psAlias}")
	public List<ArsPaisCompania> paisByCompaniaByAlias(@PathVariable Integer psAlias) {
		return paisCompaniaDao.findByEstadoCompania(psAlias);
	}
	
		//from ArsPais
	/*ByIdPais*/
	@GetMapping("/arsPaisCompania/idPais/{id}")
	public List<ArsPaisCompania> paisByCompaniaById(@PathVariable Long id) {
		return paisCompaniaDao.findByIdPais(id);
	}/**/
	/*ByEstadoPais*/
	@GetMapping("/arsPaisCompania/estadoPais/{psAlias}")
	public List<ArsPaisCompania> paisCompaniaByEstadoByAlias(@PathVariable Integer psAlias) {
		return paisCompaniaDao.findByEstadoPais(psAlias);
	}/**/
	
	/*ByCompania*/
	@GetMapping("/arsPaisCompania/companias/compania/{filtro}")
	public List<ArsPaisCompania> fncBusquedaFiltroCompania(@PathVariable String filtro){
		return paisCompaniaDao.findAll().stream().filter(x -> x.getArsCompania().getCompania().equals(filtro))
				.collect(Collectors.toList());
	}/**/

	/*ByCompania and client*/ 
	@GetMapping("/arsPaisCompania/companias/compania/{compania}/paises/pais/{pais}")
	public List<ArsPaisCompania> fncBusquedaFiltroCompaniaAndCountry(@PathVariable String compania, @PathVariable String pais){
		return paisCompaniaDao.findAll().stream().filter(x -> (x.getArsCompania().getCompania().equals(compania))& (x.getArsPais().getPais().equals(pais)) )
				.collect(Collectors.toList());

	
	}/**/

	
	/*ByPais
	@GetMapping("/arsPaisCompania/pais/{filtro}")
	public List<ArsPaisCompania> fncBusquedaFiltroPais(@PathVariable String filtro){
		return paisCompaniaDao.findAll().stream().filter(x -> x.getArsPais().getPais().equals(filtro.toUpperCase()))
				.collect(Collectors.toList());
	}/**/

		//from ArsClient
	/*ByClient*/
	@GetMapping("/arsPaisCompania/cliente/{filtro}")
	public List<ArsPaisCompania> fncBusquedaFiltroCliente(@PathVariable String filtro){
		return paisCompaniaDao.findAll().stream().filter(x -> x.getArsClient().getArcId().equals(filtro.toUpperCase()))
				.collect(Collectors.toList());
	}

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------- C A T E G O R I A ---------------------------------------------------------			
	@GetMapping("/arsCategoria")
	public List<ArsCategoria> getCategoria()
	{
		System.out.println("Getting all categorias");
		return categoriaDao.findAll();
	}
	
	@GetMapping("/arsCategoria/id/{id}")
	public ArsCategoria findCategoriaById(@PathVariable Long id)
	{
		return categoriaDao.findById(id);
	}
	
	@GetMapping("arsCategoria/idCategoria/{idCat}")
	public List<ArsCategoria> findCategoriaByIdCat(@PathVariable String idCat)
	{
		idCat = idCat.replaceAll(",", ";");
		
		System.out.println("'\nDATOS RECIBIDOS EN ARS_CATEGORIA:"
				+ "\n> id:"+idCat);
		
		List<ArsCategoria> arsCategoriaList = null;
		
		arsCategoriaList = categoriaDao.findListByIdCat(idCat);
		
		if(arsCategoriaList==null || arsCategoriaList.isEmpty())
		{
			System.out.println("No se encontraron registros con los datos ingresados.");
		}
		else
		{
			
			System.out.println("REGISTROS ENCONTRADOS:"
					+ "\n"+arsCategoriaList);
		}
		
		return arsCategoriaList;
	}
	
	@GetMapping("arsCategoria/pais/{pais}")
	public List<ArsCategoria> findCategoriaByPais(@PathVariable String pais)
	{
		pais = pais.toUpperCase();
		
		System.out.println("DATOS RECIBIDOS EN ARS_CATEGORIA:"
				+ "\n> id:"+pais);
		
		List<ArsCategoria> arsCategoriaList = null;
		
		arsCategoriaList = categoriaDao.findListByPais(pais);
		
		if(arsCategoriaList==null || arsCategoriaList.isEmpty())
		{
			System.out.println("No se encontraron registros con los datos ingresados.");
		}
		else
		{
			System.out.println("REGISTROS ENCONTRADOS:"+arsCategoriaList);
		}
		
		return arsCategoriaList;
	}

	@GetMapping("arsCategoria/compania/{compania}")
	public List<ArsCategoria> findCategoriaByCompania(@PathVariable String compania)
	{
		System.out.println("DATOS RECIBIDOS EN ARS_CATEGORIA:"
				+ "\n> compania:"+compania);
		
		List<ArsCategoria> arsCategoriaList = null;
		
		arsCategoriaList = categoriaDao.findListByCompania(compania);
		
		if(arsCategoriaList==null || arsCategoriaList.isEmpty())
		{
			System.out.println("No se encontraron registros con los datos ingresados.");
		}
		else
		{
			System.out.println("REGISTROS ENCONTRADOS:"+arsCategoriaList);
		}
		
		return arsCategoriaList;
	}
	
	@GetMapping("/arsCategoria/id/{id}/compania/{compania}")
	public ArsCategoria findCategoriaByIdAndCompania(@PathVariable String id, @PathVariable String compania)
	{
		id		= id.replaceAll(",", ";");
		compania= compania.toUpperCase();
		
		System.out.println("\nDATOS RECIBIDOS EN CATEGORIA:"
				+ "\n+ id:"+id
				+ "\n+ compania:"+compania);
		
		ArsCategoria arsCategoriaFound = null;
		arsCategoriaFound = categoriaDao.findByIdAndCompania(id, compania);
		
		if(arsCategoriaFound==null)
		{
			System.out.println("No se encontr� ning�n registro con los datos ingresados.");
		}
		else
		{
			System.out.println("REGISTRO ENCONTRADO:\n"+arsCategoriaFound);
		}
		
//		return categoriaDao.findByIdAndCompania(id, compania);
		return arsCategoriaFound;
	}

	@PostMapping("/arsCategoria")
	public void saveCategoria(@RequestBody Map<String, String> categoriaMap)
	{
		ArsCategoria arsCategoria = new ArsCategoria();
		
		//TODOS LOS DATOS RECIBIDOS
		System.out.println(categoriaMap);
		
		// CONSEGUIR CAMPOS EN VARIABLES SENCILLAS
		String notas		= "";
		String resumen		= "";
		String id_remedy	= "1000000063;1000000064;1000000065;200000003;200000004;200000005;1000000151;1000000000"; //SIEMPRE ES EL MISMO
        //PARA CONSEGUIR GRUPO
        String grupo        = categoriaMap.get("grupo");
    	//PARA CONSEGUIR GRUPO LOCAL
        String grupoLocal   = categoriaMap.get("grupoLocal");
    	//PARA CONSEGUIR MOTIVO
        String motivo       = categoriaMap.get("motivo");
        //PARA CONSEGUIR PAISCOMPANIA
        String paisCompania	= categoriaMap.get("paisCompania");
        String id           = categoriaMap.get("id");
        String escala       = categoriaMap.get("escala");
        
        //PARA UTILIZARLO EN PATRONES Y CONSEGUIR CAMPOS Y VALORES
        Matcher matcher;
        String key;
        String value;
        
        /* EJEMPLO DE LO QUE SE DEBE RECIBIR
	        String motivo		= "FALLA,GENERAL MOTORS,ERROR RECEIVING CALLING NUMBERS,SERVICIO,NEGOCIO,GENERAL MOTORS",
	        String grupo        = "'grupo_asignado':'ARG-FJ-SCL GM UY' / 'compania':'AMERICA MOVIL'",
	        String grupoLocal   = "'grupo_asignado':'ARG-FJ-SCL GM UY' / 'compania':'AMERICA MOVIL'",
	        String paisCompania = "'pais':'PRUEBA' / 'compania':'PRUEBA_CLARO_PRUEBA'",
        /**/
        
        System.out.println("\nDATOS RECIBIDOS EN ARS_CATEGORIA:"
        		+ "\nnotas:			[" + notas			+"]"
        		+ "\nresumen:		[" + resumen		+"]"
        		+ "\nid_remedy:		[" + id_remedy		+"]"
                + "\ngrupo:			[" + grupo			+"]"
                + "\ngrupoLocal:	[" + grupoLocal		+"]"
                + "\nmotivo:		[" + motivo			+"]"
                + "\npaisCompania:	[" + paisCompania	+"]"
                + "\nid:			[" + id				+"]"
                + "\nescala:		[" + escala			+"]"
        );
        
        //CONSEGUIR LAS NOTAS DESDE EL VALOR DE MOTIVOS
        motivo = motivo.replaceAll(" ", "_");
        Pattern pattern = Pattern.compile("(\\w+?),(\\w+?),(\\w+?),(\\w+?),(\\w+?),(\\w.*)");
        matcher = pattern.matcher(motivo);
        while(matcher.find())
        {
        	notas = matcher.group(3);
        	
        	notas = notas.replaceAll("_", " ");
        	System.out.println("notas:"+notas);
        }
        motivo = motivo.replaceAll("_", " ");
	    
        //PARA LLENAR EL CAMPO DE RESUMEN DESDE EL VALOR DE COMPANIA EN LA TABLA DE ARS_PAISCOMPANIA
        String servicio="";
        
        paisCompania = paisCompania.replaceAll("_", "-");
        pattern = Pattern.compile("'compania':'(\\w+?)-(\\w+?)-(\\w+?)'");
        matcher = pattern.matcher(paisCompania);
        while(matcher.find())
        {
        	servicio = matcher.group(1);
        }
        paisCompania = paisCompania.replaceAll("-", "_");
        
        //VALIDAR QUE HAYA VALORES TANTO EN EL CAMPO DE "servicio" COMO EN EL DE "notas" PARA PODER LLENAR "resumen"
        if(servicio.compareTo("")!=0 || notas.compareTo("")!=0)
        	resumen = "CPD AMX_"+servicio+" "+notas;
        
        System.out.println("resumen:"+resumen);
        
        
        //CAMBIO DE PATR�N PARA CONSEGUIR LOS DEM�S CAMPOS
        pattern = Pattern.compile("'(\\w+?)':'(\\w+?)'");
        
        
        //OBTENER LOS VALORES DE "grupo" PARA BUSCAR EL REGISTRO
        grupo = grupo.replaceAll("-", "_");
        grupo = grupo.replaceAll(" ", "x");
        
        //PARA VERIFICAR QUE CUMPLEN CON LOS CAMBIOS
        System.out.println("\ngrupo:"+grupo);
        
        //CAMPOS NECESARIOS PARA ENCONTRAR UN REGISTRO DE "ARS_GRUPOS"
        String grupo_asignado   = "";
        String gCompania        = "";
        
        matcher = pattern.matcher(grupo);
        System.out.println("key:value");
        while(matcher.find())
        {
            key		= matcher.group(1);
            value	= matcher.group(2);
            
            value = value.replaceAll("_", "-");
            value = value.replaceAll("x", " ");
            
            System.out.println(key+":"+value);
            
            switch(key)
            {
                case "grupo_asignado":
                    grupo_asignado = value;
                case "compania":
                    gCompania = value;
            }
        }
        
        
        //GRUPO LOCAL
        grupoLocal = grupoLocal.replaceAll("-", "_");
        grupoLocal = grupoLocal.replaceAll(" ", "x");
        
        //PARA VERIFICAR QUE SE TENGAN BIEN LOS DATOS
        System.out.println("\ngrupoLocal:"+grupoLocal);
        
        //CAMPOS NECESARIOS PARA CONSEGUIR EL GRUPO LOCAL
        String grupo_asignadoLocal   = "";
        String gCompaniaLocal        = "";
        
        matcher = pattern.matcher(grupoLocal);
        System.out.println("key:value");
        while(matcher.find())
        {
            key = matcher.group(1);
            value = matcher.group(2);
            
            value = value.replaceAll("_", "-");
            value = value.replaceAll("x", " ");
            
            System.out.println(key+":"+value);
            
            switch(key)
            {
                case "grupo_asignado":
                    grupo_asignadoLocal = value;
                case "compania":
                    gCompaniaLocal = value;
            }
        }
        
        
        //PAIS-COMPANIA
//        paisCompania = paisCompania.replaceAll("_", "-");
//        paisCompania = paisCompania.replaceAll("-", "_");
        
        //PARA COMPROBAR QUE EST�N BIEN LOS VALORES
        System.out.println("\npaisCompania:"+paisCompania);
        
        //CAMPOS NECESARIOS PARA BUSCAR UN REGISTRO EN ARS_PAISCOMPANIA
//        String pais     = "";
        String compania = "";
        
        matcher = pattern.matcher(paisCompania);
        System.out.println("key:value");
        while(matcher.find())
        {
            key		= matcher.group(1);
            value	= matcher.group(2);
            
            System.out.println(key+":"+value);
            
            switch(key)
            {
//                case "pais":
//                    pais = value;
                case "compania":
                    compania = value;
            }
        }
        
        // CONSEGUIR CAMPOS EN VARIABLES QUE SE CONECTAN CON OTRAS TABLAS
        ArsGrupo		grupoConseguido			= findGrupoByGrupoCompania(grupo_asignado, gCompania);
        ArsGrupo		grupoLocalConseguido	= findGrupoByGrupoCompania(grupo_asignadoLocal, gCompaniaLocal);
        ArsMotivos      motivoConseguido    	= findMotivoByMotivo(motivo);
        ArsPaisCompania	paisCompaniaConseguido	= findPaisCompaniaByCompania(compania);
        
        
        ArsCategoria categoriaFound=null;
        categoriaFound = findCategoriaByIdAndCompania(id, compania);
//        categoriaEncontrada = findCategoriaByPaisId(pais, compania, id);
        System.out.println("Categor�a encontrada:"+categoriaFound);
        
        String mensaje;
        if(categoriaFound!=null)
        {
        	mensaje = "Ya existe un registro con esos valores.";
        	System.out.println(mensaje);
        }
        else
        {
        	//S� SE AGREGA EL REGISTRO
        	
	        // SETEAR VALORES
	        arsCategoria.setNotas(notas);
	        arsCategoria.setResumen(resumen);
	        arsCategoria.setId_remedy(id_remedy);
	        arsCategoria.setGrupo(grupoConseguido);
	        arsCategoria.setGrupoLocal(grupoLocalConseguido);
	        arsCategoria.setMotivo(motivoConseguido);
	        arsCategoria.setPaisCompania(paisCompaniaConseguido);
	        arsCategoria.setId(id);
	        arsCategoria.setEscala(escala);
	        
	        System.out.println(arsCategoria.toString());
	        
	        categoriaDao.save(arsCategoria);
	        mensaje = "Registro agregado exitosamente";
	        System.out.println(mensaje);
        }
	}
	
	//---------------------------------------------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------- T A G -----------------------------------------------------------------
	@GetMapping("/arsTag")
	public List<ArsTag> findAllTags()
	{
		System.out.println("Getting all tags");
		return tagDao.findAll();
	}
	
	@GetMapping("/arsTag/id/{id}")
	public ArsTag findTagById(@PathVariable Long id)
	{
		return tagDao.findById(id);
	}
	
	@GetMapping("/arsTag/tag/{tag}")
	public ArsTag findTagByTag(@PathVariable String tag)
	{
		System.out.println("\nDATOS RECIBIDOS EN ARS_TAG"
				+ "\n+ tag:"+tag
		);
		
		ArsTag arsTagFound = null;
		arsTagFound = tagDao.findByTag(tag);
		
		if(arsTagFound==null)
		{
			System.out.println("No hay ning�n registro con los datos ingresados.");
		}
		else
		{
			System.out.println("REGISTRO ENCONTRADO:\n"+arsTagFound);
		}
		
		return arsTagFound;
	}
	
	@GetMapping("/arsTag/formulario/{formulario}")
	public List<ArsTag> findTagsByFormulario(@PathVariable String formulario)
	{
		formulario = formulario.toUpperCase();
		
		System.out.println("\nDATOS INGRESADOS:\n+ formulario:"+formulario);
		
		List<ArsTag> arsTagsFound = null;
		arsTagsFound = tagDao.findTagsByFormulario(formulario);
		
		if(arsTagsFound==null)
		{
			System.out.println("No se encontraron registros con los datos ingresados");
		}
		else
		{
			System.out.println("REGISTROS ENCONTRADOS:\n"+arsTagsFound.toString());
		}
		
		return arsTagsFound;
	}

	@GetMapping("/arsTag/tipo/{tipo}")
	public List<ArsTag> findTagsByTipo(@PathVariable String tipo)
	{
		System.out.println("DATOS RECIBIDOS:\n+ tipo:"+tipo.toUpperCase());
		
		List<ArsTag> arsTags = null;
		arsTags = tagDao.findAll().stream().filter(
							x -> x.getTipo().equals(tipo.toUpperCase())
					).collect(Collectors.toList());
		
		if(arsTags==null)
		{
			System.out.println("No se encontraron registros con los datos ingresados.");
		}
		else
		{
			System.out.println("REGISTROS ENCONTRADOS:"+arsTags);
		}
		
		return arsTags;
	}
	
	@PostMapping("/arsTag")
	public void addTag(@RequestBody ArsTag arsTag)
//	public GenericResponseVO addTag(@RequestBody Map<String, String> tagMap)
	{
		tagDao.save(arsTag);
		
		//
//		ArsTag arsTag = new ArsTag();
//		Validaciones validaciones = new Validaciones();
//		GenericResponseVO genericResponseVO = new GenericResponseVO();
//		
//		genericResponseVO = validaciones.validarTag(tagMap);
//		
//		if(genericResponseVO.isAgregar())
//		{
//			String	tag			= tagMap.get("tag");
//			int		id_remedy	= Integer.parseInt(tagMap.get("id_remedy"));
//			String	formulario	= tagMap.get("formulario");
//			String	tipo		= tagMap.get("tipo");
//			
//			arsTag.setTag(tag);
//			arsTag.setId_remedy(id_remedy);
//			arsTag.setFormulario(formulario);
//			arsTag.setTipo(tipo);
//			
//			tagDao.save(arsTag);
//			
//			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_EXITOSO);
//			genericResponseVO.setValueObjectVO(arsTag);
//		}
//		
//		return genericResponseVO;
	}
	
	@GetMapping("/arsTag/tags/{filtro}")
	public List<ArsTag> tagByAlias(@PathVariable String filtro)
	{
		return tagDao.findAll().stream().filter(x -> x.getTag().equals(filtro))
				.collect(Collectors.toList());
	}

	/*ByIdRemedy*/
	@GetMapping("/arsTag/idRem/{id}")
	public ArsTag tagByIdRemedy(@PathVariable Long id) {
		return tagDao.findByIdRemedy(id);
	}/**/
	
	//FindByFormulario
	@GetMapping("/arsTag/form/{filtro}")
	public List<ArsTag> fncBusquedaForm(@PathVariable String filtro) {
		return tagDao.findAll().stream().filter(x -> x.getFormulario().equals(filtro))
				.collect(Collectors.toList());
	}
	
	//---------------------------------------------------------------------------------------------------------------------------------------
	//--------------------------------------------------- C L I E N T E  T A G -----------------------------------------------------------------				
	@GetMapping("/arsClientTag")
	public List<ArsClientTag> BusquedaClienTag()
	{
		System.out.println("Getting all client-tags");
		return clientTagDao.findAll();
	}
	
	@GetMapping("/arsClientTag/id/{id}")
	public ArsClientTag fncClientTagById(@PathVariable Long id) {
		return clientTagDao.findById(id);
	}
	
		//from ArsTag
	/*ByIdTag*/
	@GetMapping("/arsClientTag/idTag/{id}")
	public ArsClientTag fncBusquedaIdTag(@PathVariable Long id) {
		return clientTagDao.findByIdTag(id);
	}/**/
	
	/*ByTag*/
	@GetMapping("/arsClientTag/tag/{filtro}")
	public List<ArsClientTag> fncBusquedaTag(@PathVariable String filtro) {
		return clientTagDao.findAll().stream().filter(x -> x.getArsTag().getTag().equals(filtro))
				.collect(Collectors.toList());
	}/**/
	
	/*ByIdRemedy*/
	@GetMapping("/arsClientTag/idRem/{id}")
	public ArsClientTag fncBusquedaIdRemedy(@PathVariable Long id) {
		return clientTagDao.findByIdRemedy(id);
	}/**/
	
	/*ByFormulario*/
	@GetMapping("/arsClientTag/form/{filtro}")
	public List<ArsClientTag> fncBusquedaFormulario(@PathVariable String filtro) {
		return clientTagDao.findAll().stream().filter(x -> x.getArsTag().getFormulario().equals(filtro))
				.collect(Collectors.toList());
	}/**/
	
	/*ByFormulario*/
//	@GetMapping("/arsClientTag/req/{filtro}")
//	public List<ArsClientTag> fncBusquedaRequerido(@PathVariable String filtro) {
//		return clientTagDao.findAll().stream().filter(x -> x.getArsTag().getRequerido().equals(filtro.toUpperCase()))
//				.collect(Collectors.toList());
//	}/**/
	
		//from ArsCompania
	/*ByIdPaisCompania*/
	@GetMapping("/arsClientTag/idPaisCompania/{id}")
	public List<ArsClientTag>fncBusquedaIdPaisCompania(@PathVariable Long id) {
		return clientTagDao.findByIdPaisCompania(id);
	}/**/
	//from ArsCompania
		/*ByIdCompania*/
		@GetMapping("/arsClientTag/idCompania/{id}")
		public ArsClientTag fncBusquedaIdCompania(@PathVariable Long id) {
			return clientTagDao.findByIdCompania(id);
		}/**/
		
		/*ByCompania*/
		@GetMapping("/arsClientTag/compania/{filtro}")
		public List<ArsClientTag> fncBusquedaCompania(@PathVariable String filtro) {
			return clientTagDao.findAll().stream().filter(x -> x.getArsPaisCompania().getArsCompania().getCompania().equals(filtro.toUpperCase()))
					.collect(Collectors.toList());
		}/**/
		
		/*ByEstadoCompania*/
		@GetMapping("/arsClientTag/estadoCompania/{id}")
		public List<ArsClientTag> fncBusquedaEstadoCompania(@PathVariable Integer id) {
			return clientTagDao.findByEstadoCompania(id);
		}/**/
		
	//from ArsPais
		/*ByIdPais*/
		@GetMapping("/arsClientTag/idPais/{id}")
		public List<ArsClientTag> fncBusquedaIdPais(@PathVariable Long id) {
			return clientTagDao.findByIdPais(id);
		}/**/
		
		/*ByPais*/
		@GetMapping("/arsClientTag/pais/{filtro}")
		public List<ArsClientTag> fncBusquedaPais(@PathVariable String filtro) {
			return clientTagDao.findAll().stream().filter(x -> x.getArsPaisCompania().getArsPais().getPais().equals(filtro.toUpperCase()))
					.collect(Collectors.toList());
		}/**/
		
		/*ByEstadoPais*/
		@GetMapping("/arsClientTag/estadoPais/{id}")
		public List<ArsClientTag> fncBusquedaEstadoPais(@PathVariable Integer id) {
			return clientTagDao.findByEstadoPais(id);
		}
	/**/
	
		//from ArsClient
	/*ByClient*/
	@GetMapping("/arsClientTag/cliente/{filtro}")
	public List<ArsClientTag> fncBusquedaCliente(@PathVariable String filtro) {
		return clientTagDao.findAll().stream().filter(x -> x.getArsPaisCompania().getArsClient().getArcId().equals(filtro.toUpperCase()))
				.collect(Collectors.toList());
	}/**/
	
	@GetMapping("/arsClientTag/compania/{compania}/tag/{tag}")
	public List<ArsClientTag> findCTByCompaniaAndTag(@PathVariable String compania, @PathVariable String tag)
	{
		return null;
//		return clientTagDao.findByCompaniaAndTag(compania, tag);
	}
	
	@PostMapping("/arsClientTag")
	public void saveClientTag(@RequestBody ArsClientTag arsClientTag)
//	public GenericResponseVO saveClientTag(@RequestBody Map<String, String> clientTagMap)
	{
		clientTagDao.save(arsClientTag);
//		Validaciones		validaciones		= new Validaciones();
//		GenericResponseVO	genericResponseVO	= new GenericResponseVO();
//		
//		genericResponseVO = validaciones.validarClientTag(clientTagMap);
//		
//		if(genericResponseVO.isAgregar())
//		{
////			String paisCompania = clientTagMap.get("paisCompania");
//			String tag			= clientTagMap.get("tag");
//			String valorDefault	= clientTagMap.get("valorDefault");
//			String requerido	= clientTagMap.get("requerido");
//			
//			String compania = genericResponseVO.getValueObjectVO().toString();
//			
//			ArsPaisCompania arsPaisCompania = findPaisCompaniaByCompania(compania);
//			ArsTag			arsTag			= findTagByTag(tag);
//			
//			ArsClientTag arsClientTag = new ArsClientTag();
//			
//			arsClientTag.setArsPaisCompania(arsPaisCompania);
//			arsClientTag.setArsTag(arsTag);
//			arsClientTag.setValorDefault(valorDefault);
//			arsClientTag.setRequerido(requerido);
//			
//			clientTagDao.save(arsClientTag);
//			
//			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_EXITOSO);
//			genericResponseVO.setValueObjectVO(arsClientTag);
//		}
//		
//		return genericResponseVO;
	}
	
	/*ByCompania*/
	@GetMapping("/arsClientTag/clientes/{filtro}/pais/{pais}/companias/{compania}")
	public List<ArsClientTag> busquedaCompaniaPaisCliente(@PathVariable String filtro,@PathVariable String pais,@PathVariable String compania) {
		  console.println("algo por aquix?",filtro);
		  console.println("algo por aquir?",pais);
		  console.println("algo por aquitt?",compania);
		 // console.println("algo por aqui?",clientTagDao.findAll());
		 return clientTagDao.findAll().stream().filter(x ->
		                                             (x.getArsPaisCompania()
		                                             .getArsClient().
		                                             getArcId().equals(filtro.toUpperCase()))&
		                                                     (x.getArsPaisCompania().getArsPais().getPais().
		                                                    		 equals(pais.toUpperCase()))
		                                                    		 &
				                                                     (x.getArsPaisCompania().getArsCompania().getCompania().
				                                                    		 equals(compania.toUpperCase()))
		                                             )
								.collect(Collectors.toList());
	}/**/
	
	//-----------------------------------------------------------------------------------------------------------------------------------
	//-------------------------------------------------------- U R G E N C Y ------------------------------------------------------------	
	@GetMapping("/arsUrgency")
	public List<ArsUrgency> fncBusquedaUrgency()
	{
		System.out.println("Getting all urgencies");
		return urgencyDao.findAll();
	}

	@GetMapping("/arsUrgency/pais/{pais}")
	public List<ArsUrgency> findUrgencyByPais(@PathVariable String pais)
	{
		if(pais==null)
		{
			System.out.println("No se ingres� el pa�s.");
			return null;
		}
		
		List<ArsUrgency> arsUrgenciesFound = null;
		arsUrgenciesFound = urgencyDao.findAll().stream().filter(
								x -> x.getPaisCompania().getArsPais().getPais().equals(pais.toUpperCase())
							).collect(Collectors.toList());
		
		if(arsUrgenciesFound==null || arsUrgenciesFound.isEmpty())
		{
			System.out.println("No se encontraron registros con el pa�s: "+pais);
		}
		else
		{
			System.out.println("REGISTROS ENCONTRADOS:"+arsUrgenciesFound);
		}
		
		return arsUrgenciesFound;
	}

	@GetMapping("/arsUrgency/compania/{compania}")
	public List<ArsUrgency> findUrgencyByCompania(@PathVariable String compania)
	{
		if(compania==null)
		{
			System.out.println("No se ingres� la compa��a.");
			return null;
		}
		
		List<ArsUrgency> arsUrgenciesFound = null;
		arsUrgenciesFound = urgencyDao.findAll().stream().filter(
								x -> x.getPaisCompania().getArsCompania().getCompania().equals(compania.toUpperCase())
							).collect(Collectors.toList());
		
		if(arsUrgenciesFound==null || arsUrgenciesFound.isEmpty())
		{
			System.out.println("No se encontraron registros con la compa��a: "+compania);
		}
		else
		{
			System.out.println("\nREGISTROS ENCONTRADOS:"+arsUrgenciesFound);
		}
		
		return arsUrgenciesFound;
	}
	
	@GetMapping("/arsUrgency/valor/{valor}")
	public List<ArsUrgency> findUrgencyByValor(@PathVariable String valor)
	{
		valor = valor.replaceAll(",", ";");
		valor = valor.toUpperCase();
		
		System.out.println("\nDATOS RECIBIDOS EN ARS_URGENCY:"
				+ "\n> valor:"+valor
		);
		
		List<ArsUrgency> arsUrgencyFound = null;
		arsUrgencyFound = urgencyDao.findByValor(valor);
		
		if(arsUrgencyFound==null)
		{
			System.out.println("No se encontraron registros con los datos ingresados.");
		}
		else
		{
			System.out.println("\nREGISTROS ENCONTRADOS:"+arsUrgencyFound);
		}
		
		return arsUrgencyFound;
	}
	
	@GetMapping("/arsUrgency/compania/{compania}/valor/{valor}")
	public ArsUrgency findUrgencyByCompaniaAndValor(@PathVariable String compania, @PathVariable String valor)
	{
		compania = compania.toUpperCase();
		valor = valor.replaceAll(",", ";");
		
		System.out.println("\nDATOS RECIBIDOS:"
				+ "\n+ compania: "+compania
				+ "\n+ valor:"+valor
		);
		
		return urgencyDao.findByCompaniaAndValor(compania, valor);
	}
	
	@PostMapping("/arsUrgency")
//	public void addUrgency(@RequestBody Map<String, String> urgencyMap)
	public GenericResponseVO addUrgency(@RequestBody Map<String, String> urgencyMap)
	{
		ArsUrgency			arsUrgency			= new ArsUrgency();
		GenericResponseVO	genericResponseVO	= new GenericResponseVO();
		Validaciones		validaciones		= new Validaciones();
		
		genericResponseVO = validaciones.validarUrgency(urgencyMap);
		if(genericResponseVO.isAgregar())
		{
			String	paisCompania= urgencyMap.get("paisCompania");
			String	valor		= urgencyMap.get("valor");
			int		numero		= Integer.parseInt(urgencyMap.get("numero"));
			
			//FUNCIONAR�?
//			ArsPaisCompania arsPaisCompania = (ArsPaisCompania) genericResponseVO.getValueObjectVO();
			
			String compania = genericResponseVO.getValueObjectVO().toString();
			ArsPaisCompania arsPaisCompania = findPaisCompaniaByCompania(compania);
			
			arsUrgency.setPaisCompania(arsPaisCompania);
			arsUrgency.setValor(valor);
			arsUrgency.setNumero(numero);
			
			urgencyDao.save(arsUrgency);
			
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_EXITOSO);
			genericResponseVO.setValueObjectVO(arsUrgency);
		}
		
		return genericResponseVO;
		
		
		
		
		
		/*
		String	paisCompania= urgencyMap.get("paisCompania");
		String	valor		= urgencyMap.get("valor");
		int		numero		= Integer.parseInt(urgencyMap.get("numero"));
		
//		String pais		= "";
		String compania	= "";
		
		Pattern pattern = Pattern.compile("'(\\w+?)':'(\\w+?)'");
		Matcher matcher = pattern.matcher(paisCompania);
		while(matcher.find())
		{
			String key	= matcher.group(1);
			String value= matcher.group(2);
			
			switch(key)
			{
//				case "pais":
//					pais=value;
//					break;
				case "compania":
					compania=value;
					break;
			}
		}
		
		ArsUrgency urgencyFound = null;
		urgencyFound = findUrgencyByCompaniaAndValor(compania, valor);
		
		System.out.println("Urgency encontrado:"+urgencyFound);
		if(urgencyFound!=null)
		{
			System.out.println("Ya existe un registro con esos datos.");
		}
		else
		{
			ArsPaisCompania arsPaisCompania = findPaisCompaniaByCompania(compania);
			
			ArsUrgency arsUrgency = new ArsUrgency();
			
			arsUrgency.setPaisCompania(arsPaisCompania);
			arsUrgency.setValor(valor);
			arsUrgency.setNumero(numero);
			
			urgencyDao.save(arsUrgency);
		}
		/**/
	}
	
	//-------------------------------------------------------------------------------------------------------------------------
	//-------------------------------------------- M O T I V O S --------------------------------------------------------------
	@GetMapping("/arsMotivos")
	public List<ArsMotivos> motivosAll()
	{
		System.out.println("Getting all motivos");
		return motivosDao.findAll();
	}
	
	@GetMapping("/arsMotivos/id/{id}")
	public ArsMotivos motivosById(@PathVariable Long id)
	{
		return motivosDao.findById(id);
	}
	
	@GetMapping("/arsMotivos/motivos/{motivos}")
	public ArsMotivos findMotivoByMotivo(@PathVariable String motivos)
	{
		System.out.println("motivos recibido: "+motivos);
		
		motivos = motivos.replaceAll(",", ";");
		motivos = motivos.toUpperCase();
		
		System.out.println("motivos cambiado: "+motivos);
		
		ArsMotivos arsMotivosFound = motivosDao.findByMotivos(motivos);
		
		if(arsMotivosFound==null)
		{
			System.out.println("No se encontr� ning�n registro con esas categor�as.");
			return null;
		}
		else
		{
			System.out.println("REGISTROS ENCONTRADOS:"+arsMotivosFound);
		}
		
		return arsMotivosFound;
	}
	
	@PostMapping("/arsMotivos")
//	public void addMotivos(@PathVariable Map<String, String> motivosMap)
	public GenericResponseVO addMotivos(@PathVariable Map<String, String> motivosMap)
	{
		ArsMotivos			arsMotivos			= new ArsMotivos();
		Validaciones		validaciones		= new Validaciones();
		GenericResponseVO	genericResponseVO	= new GenericResponseVO();
		
		genericResponseVO = validaciones.validarMotivos(motivosMap);
		
		if(genericResponseVO.isAgregar())
		{
			String motivos = genericResponseVO.getValueObjectVO().toString();
			
			arsMotivos.setMotivos(motivos);
			
			motivosDao.save(arsMotivos);
			
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_EXITOSO);
			genericResponseVO.setValueObjectVO(arsMotivos);
		}
		
		return genericResponseVO;
		
		
		
		
		
		/*
		//co = Categor�a Operacional
		String co1 = motivosMap.get("catOp1");
		String co2 = motivosMap.get("catOp2");
		String co3 = motivosMap.get("catOp3");

		//cp = Categor�a de Producto
		String cp1 = motivosMap.get("catProd1");
		String cp2 = motivosMap.get("catProd2");
		String cp3 = motivosMap.get("catProd3");
		
		String motivos = co1+";"+co2+";"+co3+";"+cp1+";"+cp2+";"+cp3;
		System.out.println("motivos:"+motivos);
		
		ArsMotivos motivosEncontrados=null;
		motivosEncontrados = findMotivoByMotivo(motivos);
		System.out.println(motivosEncontrados);
		
		if(motivosEncontrados!=null)
		{
			System.out.println("Ya existe ese registro");
		}
		else
		{
			arsMotivos.setMotivos(motivos);
			
			motivosDao.save(arsMotivos);
			
			System.out.println("Se agreg� el registro exitosamente.");
		}
		/**/
	}
	
	
	//------------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------- G R U P O S ------------------------------------------------------
	@GetMapping("/arsGrupos")
	public List<ArsGrupo> getGrupos()
	{
		System.out.println("Getting all grupos");
		return gruposDao.findAll();
	}
	
	@GetMapping("/arsGrupos/id/{id}")
	public ArsGrupo findById(@PathVariable Long id)
	{
		return gruposDao.findById(id);
	}
	
	@GetMapping("/arsGrupos/grupo/{grupo_asignado}/compania/{compania}")
	public ArsGrupo findGrupoByGrupoCompania(@PathVariable String grupo_asignado, @PathVariable String compania)
	{
		return gruposDao.findByGrupoCompania(grupo_asignado, compania);
	}
	
	@PostMapping("/arsGrupos")
//	public void addGrupo(@RequestBody Map<String, String> grupoMap)
	public GenericResponseVO addGrupo(@RequestBody Map<String, String> grupoMap)
	{
		ArsGrupo arsGrupo = new ArsGrupo();
		Validaciones validaciones = new Validaciones();
		GenericResponseVO genericResponseVO = new GenericResponseVO();
		
		genericResponseVO = validaciones.validarGrupo(grupoMap);
		
		if(genericResponseVO.isAgregar())
		{
			String grupo_asignado		= grupoMap.get("grupo_asignado");
			String compania				= grupoMap.get("compania");
			String organizacion_asignado= grupoMap.get("organizacion_asignado");
			String comp_sup				= grupoMap.get("comp_sup");
			String id_remedy			= grupoMap.get("id_remedy");
			String id_sgp				= grupoMap.get("id_sgp");//*
			String nombre_completo		= grupoMap.get("nombre_completo");
			String login_id				= grupoMap.get("login_id");
			
			arsGrupo.setGrupo_asignado(grupo_asignado);
			arsGrupo.setCompania(compania);
			arsGrupo.setOrganizacion_asignado(organizacion_asignado);
			arsGrupo.setComp_sup(comp_sup);
			arsGrupo.setId_remedy(id_remedy);
			arsGrupo.setId_sgp(id_sgp);
			arsGrupo.setNombre_completo(nombre_completo);
			arsGrupo.setLogin_id(login_id);
			
			gruposDao.save(arsGrupo);
			
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_EXITOSO);
			genericResponseVO.setValueObjectVO(arsGrupo);
			
		}
		
		return genericResponseVO;
		
		
		
		/*
		System.out.println(grupoMap.toString());
		
		String grupo_asignado		= grupoMap.get("grupo_asignado");
		String compania				= grupoMap.get("compania");
		String organizacion_asignado= grupoMap.get("organizacion_asignado");
		String comp_sup				= grupoMap.get("comp_sup");
		String id_remedy			= grupoMap.get("id_remedy");
		String id_sgp				= grupoMap.get("id_sgp");//*
		String nombre_completo		= grupoMap.get("nombre_completo");
		String login_id				= grupoMap.get("login_id");
		
		ArsGrupo grupoEncontrado = null;
		grupoEncontrado = findGrupoByGrupoCompania(grupo_asignado, compania);
		System.out.println(grupoEncontrado);
		
		if(grupoEncontrado!=null)
		{
			System.out.println("Ya existe ese registro.");
		}
		else
		{
			arsGrupo.setGrupo_asignado(grupo_asignado);
			arsGrupo.setCompania(compania);
			arsGrupo.setOrganizacion_asignado(organizacion_asignado);
			arsGrupo.setComp_sup(comp_sup);
			arsGrupo.setId_remedy(id_remedy);
			arsGrupo.setId_sgp(id_sgp);
			arsGrupo.setNombre_completo(nombre_completo);
			arsGrupo.setLogin_id(login_id);
			
			System.out.println(arsGrupo.toString());
			
			gruposDao.save(arsGrupo);
			System.out.println("Registro agregado correctamente.");
		}
		/**/
	}
	
//----------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------- F O R M U L A R I O -----------------------------------------------------
	@GetMapping("/arsFormulario")
	public List<ArsFormulario> formularioAll()
	{
		System.out.println("Getting all formularios");
		return formularioDao.findAll();
	}
	
	@GetMapping("/arsFormulario/id/{id}")
	public ArsFormulario getFormularioById(@PathVariable Long id)
	{
		return formularioDao.findById(id);
	}
	
	@GetMapping("/arsFromulario/compania/{compania}/formulario/{formulario}")
	public ArsFormulario findFormularioByCompaniaAndForm(@PathVariable String compania, @PathVariable String formulario)
	{
		return formularioDao.findByCompAndForm(compania, formulario);
	}
	
	@PostMapping("/arsFormulario")
	public GenericResponseVO addFormulario(@RequestBody Map<String, String> formularioMap)
	{
		Validaciones		validaciones		= new Validaciones();
		GenericResponseVO	genericResponseVO	= new GenericResponseVO();
		
		genericResponseVO = validaciones.validarFormulario(formularioMap);
		
		if(genericResponseVO.isAgregar())
		{
			ArsFormulario arsFormulario = new ArsFormulario();
			
			//EXTRAER LOS VALORES DE [formularioMap]
//			String paisCompania = formularioMap.get("paisCompania");//SE OBTIENE DESPUES
			String formulario	= formularioMap.get("formulario");
			String crudAction	= formularioMap.get("crudAction");
			
			String compania = genericResponseVO.getValueObjectVO().toString();
			
			ArsPaisCompania arsPaisCompania = findPaisCompaniaByCompania(compania);
			
			arsFormulario.setPaisCompania(arsPaisCompania);
			arsFormulario.setFormulario(formulario);
			arsFormulario.setCrudAction(crudAction);
			
			formularioDao.save(arsFormulario);
			
			genericResponseVO.setMensaje(ConstantesPagina.REGISTRO_EXITOSO);
			genericResponseVO.setValueObjectVO(arsFormulario);
			
		}
		
		return genericResponseVO;
		
		
		/*
		formulario = formulario.replaceAll(" ", "_");
		
		String pais		= "";
		String compania	= "";
		
		Pattern pattern = Pattern.compile("'(\\w+?)':'(\\w+?)");
		Matcher matcher = pattern.matcher(paisCompania);
		while(matcher.find())
		{
			String key		= matcher.group(1);
			String value	= matcher.group(2);
			
			switch(key)
			{
				case "pais":
					pais = value;
					break;
				case "compania":
					compania=value;
					break;
			}
			
		}
		
		ArsFormulario formularioFound=null;
		formularioFound = findFormularioByCompaniaAndForm(compania, formulario);
		
		System.out.println("Formulario encontrado:"+formularioFound);
		if(formularioFound!=null)
		{
			System.out.println("Ya existe un registro con esos datos"
					+ "\n"+formularioFound);
		}
		else
		{
			ArsPaisCompania arsPaisCompania = findPaisCompaniaByCompania(compania);
			
			ArsFormulario arsFormulario = new ArsFormulario();
			
			arsFormulario.setPaisCompania(arsPaisCompania);
			arsFormulario.setFormulario(formulario);
			arsFormulario.setCrudAction(crudAction);
			
			formularioDao.save(arsFormulario);
			System.out.println("�Se ingres� el registro correctamente!");
		}
		/**/
	}
}

