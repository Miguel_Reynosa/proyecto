package com.telcel.remedy.facade;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.springframework.core.env.Environment;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.bmc.arsys.api.ARException;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.telcel.gm.component.Herramienta;
import com.telcel.gm.component.ValidarProvider;
import com.telcel.gm.component.console;
import com.telcel.gm.match.GestionCampoPais;
import com.telcel.gm.match.Validador;
import com.telcel.gm.match.creaObjeto;
import com.telcel.gm.match.excepcionElemento;
import com.telcel.gm.model.CreateCase;
import com.telcel.gm.model.Formulario;
import com.telcel.gm.model.ResposeGM;
import com.telcel.gm.model.TroubleTicketAttachment;
import com.telcel.gm.respuesta.Respuesta;
import com.telcel.gm.services.AttachmentService;
import com.telcel.gm.services.CreateCaseService;
import com.telcel.gm.services.QueryCase;
import com.telcel.gm.services.RetriveAttachmentService;
import com.telcel.gm.services.UpdateCaseService;
import com.telcel.remedy.ars.itsm.RmdInsert;
import com.telcel.remedy.ars.itsm.RmdSelect;
import com.telcel.remedy.ars.itsm.RmdUpdate;
import com.telcel.remedy.ars.model.InsertVO;
import com.telcel.remedy.controllers.DataSetController;
import com.telcel.remedy.model.constantes.ConstantesRemedy;
import com.telcel.remedy.model.entity.ArsPaisCompania;
import com.telcel.remedy.model.services.IArsFormularioService;
import com.telcel.remedy.vo.CreateCaseVO;

public class GeneralMotorsFacadeFacadeImpl implements GeneralMotorsFacade {

	
	
	@Override
	public ResponseEntity<String> createCase( Map<String,String> headerRequest,CreateCaseVO createCaseVO) {
		
		RmdInsert  		  voInsert		    = createCaseVO.getVoInsert();
		RmdSelect  		  voSelect 		    = createCaseVO.getVoSelect();
		String            cuerpo		    = createCaseVO.getVoCuerpo();
		DataSetController dataController    = createCaseVO.getDataController();
		Formulario 		  formulario       = createCaseVO.getFormularioVO();
		AttachmentService attachmentService = createCaseVO.getVoAttachmentService();

		CreateCaseService create=new CreateCaseService(voInsert, voSelect,cuerpo,dataController,formulario, attachmentService);
		 CreateCase        createCase 	    =new CreateCase(headerRequest);

		 
			
			 console.println("header andes enviar ",headerRequest);
			 ResponseEntity<String> valorCase=null;
				creaObjeto creaObjetoCreate = new creaObjeto();
				ObjectNode mapa=creaObjetoCreate.StringtoObjectNode(cuerpo);
			   ObjectNode mapaCloud=mapa;
				
				GestionCampoPais gestioncampo=new GestionCampoPais();
				console.println("country....",headerRequest.get("countrym2m"));
				console.println("country buscado....", gestioncampo.getToCountryM2MCountry(headerRequest.get("countrym2m")));
			    Respuesta respuesta = new Respuesta();
				HttpHeaders responseHeaders = new HttpHeaders();
				  String incidenNumberLocal="";
			     try {// VER QUE ESTA MAL EN LAS VALIDACIONES
					
				  create.getGroupLocalToCloud(cuerpo,gestioncampo.getToCountryM2MCountry(headerRequest.get("countrym2m")));
					
				  
				  //Callable< ResponseEntity<String> > task = () -> 
				  {
					  ResponseEntity<String> resultado;
					// valorCase=create.createCase(mapa.toString(), headerRequest, httpServletResponse);
					 valorCase=create.createCase(mapa.toString(), headerRequest);
					 console.println(" MEXICO/Local completo*****************"+valorCase);	 
					  // if(resultado.getHeaders().get("correlationId")!=null)// COMENTADO POR LO MIENTRAS PARA VER LOS TRAZO DE ERRORES
					  mapaCloud.put("idremedy","");
					  if(valorCase!=null)
						 if(valorCase.getHeaders().get("correlationId")!=null) {// COMENTADO POR LO MIENTRAS PARA VER LOS TRAZO DE ERRORES
						  incidenNumberLocal=valorCase.getHeaders().get("correlationId").get(0);
						  mapaCloud.put("idremedy",incidenNumberLocal);
					  }
					  
				  };	
				  if(headerRequest.get("estado")!=null)// PARA REASIGNACION DE CASOS
					  if(headerRequest.get("estado").equals("reasignacion"))
						  return valorCase;
				 // ExecutorService executorService = Executors.newFixedThreadPool(1);
				ExecutorService executorService= Executors.newSingleThreadExecutor(); 
				  String paisLocal= headerRequest.get("countrym2m");//obtener el contry antes de cambiarlo
				  headerRequest.replace("enterpriseid", "GM_CLARO_BRA");
				  headerRequest.replace("countrym2m", "BR");
				  console.println(" cabezera despues de l cambio**************"+headerRequest);	 
				 	boolean LOCAL=false;
	     		  if(create.isPAU(paisLocal)) {
	     			 if(valorCase.getHeaders().get("correlationId")!=null)
						  createCase.setNewIncident(valorCase.getHeaders().get("correlationId").get(0)); //SETEA EL NUEVO INCIDENTE EN MEXICO PARA USO DE NOTIFICACION A BRASIL SOLO DEBE SER PARA ESTE CASO 
						//  console.println("createCase  para la ligar al local PAU ",createCase);					
						  create.throwsNotify(createCase);					  
						  LOCAL=true;
	     		  }
	     		 console.println(" PRIMER IF es es local",LOCAL);
				LOCAL=create.isPAU(paisLocal);
				 console.println("  es local",LOCAL);
				  Callable<ResponseEntity<String>> tarea2=null;
				  if(!LOCAL) {
					  console.println("creando incidente para la ligar al local");
					  gestioncampo.clearCampoDefault();
					  create.getCategoria();
					//  create.getGroupLocalToCloud(voAttachment,gestioncampo.getToCountryM2MCountry(headerRequest.get("countrym2m")));
							
					  //tarea2 = () -> 
					  {
						  ResponseEntity<String> resultado;
				
						 // String incidenNumberLocal="INC000000301121";
//						  mapaCloud.put("nameClient",incidenNumberLocal);
						valorCase=create.createCase(mapaCloud.toString(), headerRequest);
					
						//  valorCase=create.createCase(mapaCloud.toString(), headerRequest, httpServletResponse);
						  console.println("INCIDENTE EN MEXICO*****************"+valorCase);	 
							
						  console.println(" NUMERO DE INCIDENTE EN MEXICO*****************"+valorCase.getHeaders().get("correlationId").get(0));	 
						createCase.setNewIncident(valorCase.getHeaders().get("correlationId").get(0)); //SETEA EL NUEVO INCIDENTE EN MEXICO PARA USO DE NOTIFICACION A BRASIL SOLO DEBE SER PARA ESTE CASO 
						 create.throwsNotify(createCase);
						//  return resultado;
						};

		 
		 
		 
			}
				  //comentado solo para las pruebas de colombia-------
		} catch (excepcionElemento | ARException e) {
					// TODO Auto-generated catch block
						respuesta.setCode("CTT001");
						respuesta.setReason("Downstream System Error: " + e.getMessage());
						respuesta.setMessage(e.getMessage());
						e.printStackTrace();
						return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
								.body(respuesta.getRepuestaError().toString());
		//			
		//			
				}
			   //  console.println("**************************** YO SOY EL VALOR DE VALORCASE*****************************   "+ valorCase);
				// console.println("YO SOY EL VALOR DE LOCAL*****************"+valorCase.getHeaders().get("correlationId").get(0));	 
									 
									 
				return  valorCase;//create.createCase(voAttachment, headerRequest,httpServletResponse);
			
			}		

	
	
	
	
	@Override
	public ResponseEntity<String> updateCase(Map<String, String> headerRequest, CreateCaseVO createCaseVO)
	{
		RmdInsert  		  voInsert		    = createCaseVO.getVoInsert();
		RmdSelect  		  voSelect 		    = createCaseVO.getVoSelect();
		String            cuerpo		    = createCaseVO.getVoCuerpo();
		DataSetController dataController    = createCaseVO.getDataController();
		Formulario 		  formulario		= createCaseVO.getFormularioVO();
		AttachmentService attachmentService = createCaseVO.getVoAttachmentService();
		RmdUpdate 		  rmdUpdate			= createCaseVO.getVoUpdate();
		Environment 	  env				= createCaseVO.getEnv();
		String			  idIncident		= createCaseVO.getIncidentNumber();
		
		Respuesta respuesta = new Respuesta();
		HttpHeaders responseHeaders = new HttpHeaders();
		
		responseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		console.println("Log4j: empieza Update Case !!");
		console.println("GeneralMotorsControllers: updateCase");
		
		Validador validar=new Validador(env,dataController);
		
		UpdateCaseService update = new UpdateCaseService(
				rmdUpdate
				, voSelect
				, voInsert
				, attachmentService
				, env
				, validar
				, formulario
		);
		
		//& update=new UpdateCaseService();
		console.println("cuerpo........update..--"+cuerpo);
		
		console.println("\nPais.........." //Para saber que pais es.
				+ "\nenterpriseId: "	+ headerRequest.get("enterpriseid")
				+ "\ncountry: "			+ headerRequest.get("country")
				+ "\ncountryM2M: "		+ headerRequest.get("countrym2m")
		);
		
		String idLocal = headerRequest.get("idlocal");
		
		// idLocal indica que viene o trae datos del remedy Local
		console.println("idLocal para el pais................"+idLocal);
		
		// cuadno tiene idLocal este servicio es consumidos por wsdl
		if(idLocal==null)
		{
			console.println("ENTRA A (CLOUD): (Brasil)");
			
			//setear los headers
			headerRequest.replace("enterpriseid", "GM_CLARO_BRA");
			headerRequest.replace("country", "BRA");
			headerRequest.replace("countrym2m", "BR");
		}
		else if(idLocal.compareTo("true")==0)
		{// si idLocal esta en true indica que el flujo tiene que ir al remedy local
			console.println("ENTRA A LOCAL");
			
			//setear los headers
			headerRequest.replace("enterpriseid",	headerRequest.get("enterpriseid"));
			headerRequest.replace("country",		headerRequest.get("country"));
			headerRequest.replace("countrym2m",		headerRequest.get("countrym2m"));
		}
		else
		{
			console.println("El idLocal ["+idLocal+"] no esta identificado");
		}
		
		console.println("headerRequest.........."+headerRequest);
		
		//Llamar al servicio (update)
		ResponseEntity<String> updateSalida=null;
		try
		{
			updateSalida = update.updateCase( //Este es el servicio ya con los headers
					cuerpo
					, idIncident
					, headerRequest
			);
		}
		catch (ARException e)
		{
			return updateSalida;
		}
		
		return updateSalida;
	}




	@Override
	public ResponseEntity<ObjectNode> queryCase(Map<String, String> headerRequest, CreateCaseVO createCaseVO) {
		
		RmdSelect  		  voSelect 		    = createCaseVO.getVoSelect();
		String            cuerpo		    = createCaseVO.getVoCuerpo();
		DataSetController dataController    = createCaseVO.getDataController();
		Formulario 		  formulario       = createCaseVO.getFormularioVO();
		AttachmentService attachmentService = createCaseVO.getVoAttachmentService();
		Environment 	  env = createCaseVO.getEnv();
		CreateCase 		  entityCase		= createCaseVO.getEntityCase();
		String			 idIncident	  	=     createCaseVO.getIncidentNumber();
		
		 Validador validar=new Validador(env,dataController);
		 QueryCase update=new QueryCase(env,voSelect,attachmentService,validar,formulario);
		 ResponseEntity<ObjectNode> query=null;
    try {
		 query= update.queryCpd(idIncident, entityCase,headerRequest);
	} catch (ARException e) {
		return query;
	}
    return query;
		
		//return null;
	}





	@Override
	public ResponseEntity<ObjectNode> retrieveAttachment(Map<String, String> headerRequest, CreateCaseVO createCaseVO) {
		// TODO Auto-generated method stub
		
		RmdSelect  		  voSelect 		    = createCaseVO.getVoSelect();
		String            cuerpo		    = createCaseVO.getVoCuerpo();
		DataSetController dataController    = createCaseVO.getDataController();
		Formulario 		  formulario       = createCaseVO.getFormularioVO();
		AttachmentService attachmentService = createCaseVO.getVoAttachmentService();
		Environment 	  env = createCaseVO.getEnv();
		CreateCase 		  entityCase		= createCaseVO.getEntityCase();
		String			 idIncident	  	=     createCaseVO.getIncidentNumber();
		String			 numWorklog  	=     createCaseVO.getNumberWorklog();
			
		
		
		
		ValidarProvider valP = new ValidarProvider();

		
		
		
		
		
		console.println("Log4j : empieza retrieveTroubleTicket !!");
		HttpHeaders responseHeaders = new HttpHeaders();
		GestionCampoPais campoPais=new GestionCampoPais();
		headerRequest=campoPais.changeHeaderToCloud(headerRequest);
		   
		Respuesta respuesta=new Respuesta();
		  String enterpriseId=headerRequest.get("enterpriseid");
		   String correlationId=headerRequest.get("correlationid");
		   String providerId=headerRequest.get("providerid");
		   String country=headerRequest.get("country");
		   String countryM2M=headerRequest.get("countrym2m");			   
			 Validador validado = new Validador(env,dataController);
			 console.println("dentro de fuera",idIncident.length());

		if ( idIncident.length()>= 16) {
		respuesta.setCode("RTTA004");
		respuesta.setReason("Incident number "+ idIncident +" is/are not found ");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
		.body(respuesta.getRepuestaError());
		}
		
		
		
		String  valorNumero=idIncident.substring(3,idIncident.length());
		String valorCadenita=idIncident.substring(0, 3);
		String otroID=valorCadenita.toUpperCase()+valorNumero;
	//	console.println("dentro de fuera",troubleticketId.length());
		    List<String> validarHeader = validado.validarCabezera(headerRequest);
			if(validarHeader.size()>0) {
				respuesta.setCode("RTTA002");
				respuesta.setReason(validarHeader+" is required ");
				console.println("esta cosa entro aqui????");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
						.body(respuesta.getRepuestaError());
			}

			if (idIncident.length()<15) {   
				respuesta.setCode("RTTA001");
				respuesta.setReason("id is  no supported ");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
				.body(respuesta.getRepuestaError());
				}
				
				if(correlationId.length()!=16) {
				respuesta.setCode("RTTA001");
				respuesta.setReason(correlationId + " is  no supported ");
				// respuesta.setMessage(viInsert.getResultado().getError());
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
				.body(respuesta.getRepuestaError());
				}
				
		if(countryM2M.length()!=2) {
			respuesta.setCode("RTTA001");
			respuesta.setReason(countryM2M + " is  no supported ");
			// respuesta.setMessage(viInsert.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
			.body(respuesta.getRepuestaError());
			}
			
			if(enterpriseId.length()!=12) {
			respuesta.setCode("RTTA001");
			respuesta.setReason(enterpriseId + " is  no supported ");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
			.body(respuesta.getRepuestaError());
			}
			
			String[] voProvId = env.getProperty("ProviderID").split(",");
			if(valP.valProv(providerId, voProvId)) {
				return valP.closeRequestON(respuesta, providerId);
			}
			
			if(country.length()!=3) {
			respuesta.setCode("RTTA001");
			respuesta.setReason(country + " is  no supported ");
			// respuesta.setMessage(viInsert.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
			.body(respuesta.getRepuestaError());
			}

		 String  valorNumerico=idIncident.substring(3,idIncident.length());
			 String valorCadena=idIncident.substring(0, 3);
			 String nuevoId=valorCadena.toUpperCase()+valorNumerico;
			 console.println("valor de los 3 primeros ",nuevoId.matches("INC"+"\\d{12}"));
			
		
		if (!nuevoId.matches("INC"+"\\d{12}") ) {	 
			respuesta.setCode("RTT001");
			respuesta.setReason("id is no supported ");
			// respuesta.setMessage(viInsert.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(respuesta.getRepuestaError());
		}
		
		//VALIDACION PARA ID troubleticket
		if (numWorklog.length()!=15) {	 
			respuesta.setCode("RTT001");
			respuesta.setReason("idtroubleticket is  no supported ");
			// respuesta.setMessage(viInsert.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(respuesta.getRepuestaError());
		}
		if (!numWorklog.contains("WLG")) {   
			respuesta.setCode("RTTA001");
			respuesta.setReason("idtroubleticket is  no supported ");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
			.body(respuesta.getRepuestaError());
			}
			
			
			if (!nuevoId.contains("INC")) {   
			respuesta.setCode("RTTA001");
			respuesta.setReason("id is  no supported ");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
			.body(respuesta.getRepuestaError());
			}
		 String  valorNum=numWorklog.substring(3,numWorklog.length());
		 String valorCad=numWorklog.substring(0, 3);
		 String nuevotroubleticket=valorCad.toUpperCase()+valorNum;
		// console.println("valor num", Herramienta.StringToInteger(valorNum));
		 //console.println("valor de los 3 nuevotroubleticket ",nuevotroubleticket.matches("WLG"+"\\d{12}"));
		
		// if (!nuevotroubleticket.matches("WLG"+"\\d{12}")) {	 
		
		if (!nuevotroubleticket.matches("WLG"+"\\d{12}")| !Herramienta.StringToInteger(valorNum)) {	 
		respuesta.setCode("RTTA001");
		respuesta.setReason("idtroubletickets is no supported ");
		// respuesta.setMessage(viInsert.getResultado().getError());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
				.body(respuesta.getRepuestaError());
		}
		
		  console.println("Entra en trouble ticket");
		  RetriveAttachmentService retrive=new RetriveAttachmentService();
		  
		  creaObjeto creaObjetoCreate = new creaObjeto();
		String clientPais="";
		   
			ObjectNode resultado=null;
			GestionCampoPais compoPais=new  GestionCampoPais();
			   
			try {

				List<ArsPaisCompania> clients=	validado.getDataSetController().fncBusquedaFiltroCompaniaAndCountry(enterpriseId, compoPais.getToCountryM2MCountry(countryM2M));
				  clientPais=clients.get(0).getArsClient().getArcId();
				resultado = voSelect.fncRmdSelectAttatchment(retrive.fncAttachmentRretrieve(idIncident, numWorklog,clientPais),numWorklog,headerRequest);
			} catch (excepcionElemento e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				respuesta.setCode("RTT003");
				respuesta.setReason(e.getMessage());
				//respuesta.setReason("idtroubletickets  not found ");
				// respuesta.setMessage(viInsert.getResultado().getError());
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
						.body(respuesta.getRepuestaError());
				
			}
					
		
	return ResponseEntity.status(HttpStatus.ACCEPTED).headers(responseHeaders)
					.body(resultado);

		
		
		
		
		
		
		
		
		
		
		
		
		
		
	//	return null;
	}





	@Override
	public ResponseEntity<ResposeGM> addAttachment(Map<String, String> headerRequest, CreateCaseVO createCaseVO) {
		// TODO Auto-generated method stub
		RmdInsert  		  voInsert		    = createCaseVO.getVoInsert();
		
		RmdSelect  		  voSelect 		    = createCaseVO.getVoSelect();
		String            cuerpo		    = createCaseVO.getVoCuerpo();
		DataSetController dataController    = createCaseVO.getDataController();
		Formulario 		  formulario       = createCaseVO.getFormularioVO();
		AttachmentService attachmentService = createCaseVO.getVoAttachmentService();
		Environment 	  env = createCaseVO.getEnv();
		CreateCase 		  entityCase		= createCaseVO.getEntityCase();
		String			 idIncident	  	=     createCaseVO.getIncidentNumber();
		String			 numWorklog  	=     createCaseVO.getNumberWorklog();
		TroubleTicketAttachment			 attachmentVo  	=     createCaseVO.getAttachmentVO();
		

		
		
		//String vsUser = headers.get("Authorization").stream().findFirst().orElse("");
	GestionCampoPais campoPais=new GestionCampoPais();
	headerRequest=campoPais.changeHeaderToCloud(headerRequest);
	
	
	
		Respuesta respuesta=new Respuesta();
		String enterpriseId=headerRequest.get("enterpriseid");
		String correlationId=headerRequest.get("correlationid");
		String providerId=headerRequest.get("providerid");
		String country=headerRequest.get("country");
		String countryM2M=headerRequest.get("countrym2m");
		
		//String vsUser = "TEMIP";
		
		
		console.println("enterpriseId "+enterpriseId);
		console.println("voAttachment "+attachmentVo.toCadena());
		console.println("getDescription"+attachmentVo.getDescription());
		  if(attachmentVo.getDescription()==null) {
			  attachmentVo.setDescription(attachmentVo.getName());
		   
		  }
		  
		ResposeGM voattach = new ResposeGM();
		creaObjeto creaObjeto=new creaObjeto();


		HttpHeaders responseHeaders = new HttpHeaders();
		Validador validado = new Validador(env,dataController);
		validado.getMappingError().error();
		GestionCampoPais gestioncampo=new GestionCampoPais();
		List<ArsPaisCompania> clients=	validado.getDataSetController().fncBusquedaFiltroCompaniaAndCountry(enterpriseId, gestioncampo.getToCountryM2MCountry(countryM2M));
		String vsUser = clients.get(0).getArsClient().getArcId();
		
		vsUser=campoPais.changeClient(headerRequest,vsUser);
		
		validado.getMappingError().bucadorTags(validado,"addAtachment","MEXICO","GM_MX");
		validado.setTipoServicio("A");
		Integer tama�o= attachmentVo.getSize();
		String objetito=creaObjeto.toString(attachmentVo);
		List<String> validarFound = validado.notfound(objetito);
		console.println("validarFound",validarFound);
		List<String> validarHeader = validado.validarCabezera(headerRequest);
    if(validarHeader.size()>0) {
      voattach.setCode("ATTA002");
      voattach.setReason(validarHeader +" is required ");
          return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
          .body(voattach);
      
    }
    
    if (attachmentVo.getTroubleTicketId().length() < 15) {
      voattach.setCode("ATTA001");
      voattach.setReason(attachmentVo.getTroubleTicketId() + " is/are not supported ");
          return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
          .body(voattach);
		}
		if (tama�o > 5242880 || tama�o==0) {
      voattach.setCode("ATTA003");
      voattach.setReason("Downstream System Error: "+ tama�o + " is/are not supported ");
    
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
          .body(voattach);
    }
    
    if ( attachmentVo.getTroubleTicketId().length()> 15) {
      voattach.setCode("ATTA004");
      voattach.setReason("Incident number "+ attachmentVo.getTroubleTicketId() +" is/are not found ");
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
          .body(voattach);
    }    
    
    if(correlationId.length()<3) {
      voattach.setCode("ATTA001");
      voattach.setReason(correlationId + " is  no supported ");
      // respuesta.setMessage(viInsert.getResultado().getError());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
          .body(voattach);
    }
		if(countryM2M.length()!=2) {
      voattach.setCode("ATTA001");
      voattach.setReason(countryM2M + " is  no supported ");
      // respuesta.setMessage(viInsert.getResultado().getError());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
          .body(voattach);
		}
		if(enterpriseId.length()!=12) {
			voattach.setCode("ATTA001");
			voattach.setReason(enterpriseId + " is  no supported ");
			// respuesta.setMessage(viInsert.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(voattach);
		}
	

		if(country.length()!=3) {
			voattach.setCode("ATTA001");
			voattach.setReason(country + " is  no supported ");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(voattach);
		}
		
			if (!attachmentVo.getTroubleTicketId().matches("INC"+"\\d{12}") ) {	 
			voattach.setCode("ATT001");
			voattach.setReason("id is no supported ");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(responseHeaders)
					.body(voattach);
		}
		if (validarFound.size() >= 1) {
			respuesta.setCode("CTT004");
			respuesta.setReason(validarFound + " is/are not found ");
			voattach.setReason(" is/are not found ");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(voattach);
		}
		List<String> requiredmap = validado.requered(objetito);
		if (requiredmap.size() >= 1) {
			respuesta.setCode("CTT002");
			respuesta.setReason(requiredmap + " is/are  required ");
			voattach.setReason(" is/are not found ");
			
			// respuesta.setMessage(viInsert.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(voattach);
		}
		
		String idLocal = headerRequest.get("idLocal"); //Sin esto no puedo saber si viene de local o no.
		String tipo="";
		
		console.println("idLocal: "+idLocal); // Para saber si entra a idLocal = true o a else
		if(idLocal==null){
			tipo = ConstantesRemedy.tipoCloudToLocal;
		} else if(idLocal.compareTo("true")==0) {
			tipo = ConstantesRemedy.tipoLocalToCloud;
		} else {
			console.println("Error en el idLocal: "+idLocal);
		}
		
		console.println("tipo: "+tipo);
		
		ResponseEntity<ResposeGM> voResponse = null;
		console.println("vsUser: "+vsUser);
		
		InsertVO voResult=null;
		try {
			voResult = voInsert.fncRmdInsert(
					attachmentService.fncAttachmentInsert(
							attachmentVo, vsUser, tipo)
			);
		} catch (ARException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(voResult!=null)
		if (voResult.getResultado().getError()!=null) {
			voattach.setCode("400");
			//console.println("ff", voResult);
			voattach.setReason(voResult.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(voattach);
		 }
	    
		
		
		
		if (voResult.getResultado().getError() == null) {
			voResponse = ResponseEntity.status(HttpStatus.ACCEPTED)
					.header("correlationId", voResult.getResultado().getNuevo()).body(null);	
							
					String bodyJson = "{\"eventType\":"+"\"AddTroubletTicketAttachment\""+",\"statusNotify\":\""+"ATTACHED"+"\""+",\"eventId\":\""+attachmentVo.getTroubleTicketId()+"\""+"}";
																					
										
					console.println("bodyjson  sds"+bodyJson); 
//			try {
//						notifyService(bodyJson, headerRequest);
//					} catch (IOException | JSONException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
						
			//return voResponse;
		} else {
			voattach.setCode("400");
			//console.println("ff", voResult);
			voattach.setReason(voResult.getResultado().getError());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(voattach);
		}
		

		return voResponse;
	}





	@Override
	public ResponseEntity<String> QueryIncNumber(Map<String, String> headerRequest, CreateCaseVO createCaseVO) {
		// TODO Auto-generated method stub
		RmdInsert  		  voInsert		    = createCaseVO.getVoInsert();
		RmdSelect  		  voSelect 		    = createCaseVO.getVoSelect();
		String            cuerpo		    = createCaseVO.getVoCuerpo();
		DataSetController dataController    = createCaseVO.getDataController();
		Formulario 		  formulario       = createCaseVO.getFormularioVO();
		AttachmentService attachmentService = createCaseVO.getVoAttachmentService();
		Environment 	  env = createCaseVO.getEnv();
		CreateCase 		  entityCase		= createCaseVO.getEntityCase();
		String			 id	  	=     createCaseVO.getIncidentNumber();
		String			 numWorklog  	=     createCaseVO.getNumberWorklog();
		String			 idLocal  	=     createCaseVO.getIdLocal();
		TroubleTicketAttachment			 attachmentVo  	=     createCaseVO.getAttachmentVO();
		
		 Validador validar=new Validador(env,dataController);
		 // QueryCase getIncNumber = new QueryCase(env,voSelect,attachmentService,validar,iFormulario);
		  QueryCase getIncNumber = new QueryCase(env,voSelect,attachmentService,validar,formulario);
			
		  // Valores recibidos del WSDL
		//  String id=idIncident;
		  
		  // Valores creados para addAttachment
		  String incLocal="";
		  String Query[]=null;
		try {
			Query = getIncNumber.GetIncNumbers(id, headerRequest);
		} catch (ARException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		}
		  
		  String incCloud=Query[0];
		  String GrupoSoporte=Query[1];
		  
		//  console.println("Regresa a GeneralMotorControllers"
//		    + "\nincRecibido: "+id
//		    + "\nidLocal: "+idLocal
//		    + "\nincLocal: "+incLocal
//		    + "\nincCloud: "+incCloud
//		    + "\nGrupoSoporte: "+GrupoSoporte
		//  );
		//  
		  HttpHeaders responseHeaders = new HttpHeaders();
		  responseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		  
		  String responseInc = "{"
		      + "\"responseIncident\":\""+incCloud+"\""
		      + ", \"GrupoSoporte\":\""+GrupoSoporte+"\""
		  +"}";
		  
		  console.println("\nLo que regresa.\nresponseInc: " + responseInc);
		  
		  return ResponseEntity.status(HttpStatus.ACCEPTED).headers(responseHeaders)
		          .body(responseInc);

		
		
		
		//return null;
	}
}
