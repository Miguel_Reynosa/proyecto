package com.telcel.remedy.facade;

import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bmc.thirdparty.org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.telcel.gm.model.ResposeGM;
import com.telcel.remedy.vo.CreateCaseVO;

@Service
@Component
public interface GeneralMotorsFacade
{
	public ResponseEntity<String> createCase( Map<String,String> headerRequest,CreateCaseVO createCaseVO);
	public ResponseEntity<String> updateCase( Map<String,String> headerRequest,CreateCaseVO createCaseVO);
	public ResponseEntity<ObjectNode> queryCase( Map<String,String> headerRequest,CreateCaseVO createCaseVO);
	public ResponseEntity<ObjectNode> retrieveAttachment( Map<String,String> headerRequest,CreateCaseVO createCaseVO);
	public ResponseEntity<ResposeGM> addAttachment( Map<String,String> headerRequest,CreateCaseVO createCaseVO);
	
	public ResponseEntity<String> QueryIncNumber( Map<String,String> headerRequest,CreateCaseVO createCaseVO);
}
