package com.telcel.remedy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.bmc.thirdparty.org.springframework.beans.factory.annotation.Configurable;

@SpringBootApplication
@EnableAutoConfiguration
@Configuration
@EnableScheduling
//@Configurable
@ComponentScan({"com.telcel"})	
public class RemedyApplication {

	public static void main(String[] args) {
		SpringApplication.run(RemedyApplication.class, args);
	}
}
